//
//  Constants.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

import Foundation

struct constantsUrl {
    static let BASE_URL = "https://api.parsl.io/" // production server
//    static let BASE_URL = "https://dev.parsl.io/" // dev server

        static let SAVE_PARSL_DETAILS_URL =   BASE_URL + "save/parsl/details"
        static let UPLOAD_PARSL_VIDEO_URL =  BASE_URL + "save/parsl/video/msg"
        static let CHECK_COPOUN_DETAIL_URL =  BASE_URL + "check/coupon/details"
        static let Upload_Images_URL = BASE_URL + "upload/user/model"
        static let SAVE_PARSL_OTP_URL = BASE_URL + "save/parslotp"
        static let RESET_PASSWORD_URL  = BASE_URL + "reset/password"
        static let FORGOT_PASSWORD_URL = BASE_URL + "forget/password"
        static let GET_EMPHERALKEY  = BASE_URL + "get/ephemeral/key"
        static let LOG_IN_URL  = BASE_URL + "login"
        static let Wallet_Public_Key = BASE_URL + "get/public/key"
        static let Get_User_wallet = BASE_URL + "get/user/wallet"
        static let CHECK_VERSION_URL = BASE_URL + "check/version"
        static let START_CHAT_URL = BASE_URL + "start/chat"
        static let GET_NOTIFICATION_URL = BASE_URL + "get/user/notifications"
        static let MY_PARSAL_URL = BASE_URL + "get/myparcel"
        static let EDIT_AVATAR_URL = BASE_URL + "edit/avatars"
        static let CREATE_AVATAR_URL  = BASE_URL + "get/create/avatars"
        static let GET_VIRTUALCARD_DETAIL_URL  = BASE_URL + "get/virtual/card/details"
        static let SAVE_GUEST_DEVICE_DETAIL_URL =  BASE_URL + "save/guest/device/details"
        static let GET_CATEGORY_LIST_URL = BASE_URL + "/get/categories/list"
        static let GET_VENDOR_MODELS_URL  = BASE_URL + "get/vendor/models"
        static let GET_CATEGORY_VENDORS_URL  = BASE_URL + "get/category/vendors"
        static let GET_PARSL_DETAILS  = BASE_URL + "get/parsl/details"
        static let GET_GIFT_MODELS =  BASE_URL + "get/gift/models"
        static let START_CHAT = BASE_URL + "start/chat"
        static let GET_DETAILS_AVATAR_URL  = BASE_URL + "get/details/avatars"
        static let DELETE_AVATAR_URL  = BASE_URL + "delete/avatars"
        static let PUSH_USER_MESSAGE_URL = BASE_URL + "push/user/msg"
        static let GET_URSER_MESSAGE_URL  = BASE_URL + "get/user/msgs"
        static let SAVE_CHAT_URL = BASE_URL + "save/chat"
        static let Delete_Account = BASE_URL + "delete/account"
}
struct constantsWebsiteUrl {
    static let PARSL_WALLET_URL =  "itms-apps://itunes.apple.com/app/id1160481993"
    static let REFUND_EMAIL_URL  = "refund@parsl.io"
    static let SUPPORT_URL = "support@parsl.io"
    static let PRIVACY_POLICY_URL = "https://bstlegal.s3.amazonaws.com/Parsl/parsl_privacy_policy.pdf"
    static let TERMS_CONDITION_URL = "https://bstlegal.s3.amazonaws.com/Parsl/Terms_Conditions_PARSL.pdf"
    static let PARSL_WEBSITE = "https://linktr.ee/parsl"
    static let CHECK_UPDATE_URL = constantsUrl.BASE_URL
}

struct constantsStrings {
    static let appName = "PARSL"
    static let sureToDeleteAccount = "Are you sure you want to delete account?"
    static let requestForwarded = "Request forwarded. Check your registered email"
    static let cartScreenTextViewPlaceholder = "Hey, there is a PARSL for you"
    static let chooseOptions = "Choose option"
    static let errorTitle = "Error"
    static let errorMessage = "Unable to present Apple Pay authorization."
    static let appleMerchantIdentifier = "merchant.parsl.app"
    static let appleTransactionErrorMessage = "Unable to make Apple Pay transaction"
    static let unknownDate = "Unknown date"
    static let parsForYou = ""
    static let unsuccessfulTransactionMessage = "Transaction unsuccessful. Please try again"
    static let copounErrorTitle = "Coupon Invalid"
    static let copounErrorMessage = "Try Valid Coupon"
    static let copounMissingFieldTitle = "Missing Field"
    static let copounMissingFieldMessage = "Please Enter Copoun Code"
    static let enterName = "Sender name was empty"
    static let enterEmail = "Sender email was empty"
    static let enterValidEmail = "Please enter valid email address"
    static let parslForYou = "Hey, there is a PARSL for you"
    static let shareParslTitle = "Share PARSL"
    static let shareParslMessage = "You have already shared. Do you want to share again?"
    static let shareYes = "Yes"
    static let shareNo = "No"
    static let shareAbletext = "Download app \(constantsWebsiteUrl.PARSL_WEBSITE) or use code"
    static let enterValidPassword = "Please enter valid password"
    static let enterConfirmPassword = "Please enter confirm password"
    static let samePasswordNewConfirm = "new password and confirm password must be same"
    static let enterOldPassword = "Please enter old password"
    static let enterNewPassword = "Please enter new password"
    static let PARSLGiftCardDescription = "PARSL Gift Card"
    static let enterItemsInCart = "Please add items to cart"
    static let loginSuccess = "logged in successfully"
    static let noInternet = "No internet connection. Please try again later"
    static let acceptParslTC = "Accept PARSL terms and conditions"
    static let acceptParslPP = "Accept PARSL privacy policy"
    static let emptyPassword = "Password was empty"
    static let emptyUserName = "Username was empty"
    static let invalidUserEmail = "Invalid Username/Email"
    static let cancelLabel = "Cancel"
    static let emailPlaceHolder = "Email"
    static let passwordPlaceholder = "Password"
    static let enterOTP = "Please enter Parsl URL or OTP"
    static let signOutSignIn = "Sign in"
    static let signOut = "Sign out"
    static let folderNamelabel = "Object Name"
    static let enterFolderName = "Enter object Name"
    static let saveTitle = "Continue"
    static let imagesUPloading = "Images are being uploaded. Do not quit the app"
    static let liveSupportTitle = "Live Support"
    static let enterEmailtoTalkAgent = "Please enter your email to talk to our agent."
    static let userEmail = "User Email"
    static let startTitle = "Start"
    static let cacheCleared = "Cache cleared"
    static let appNotExist = "App does not exist"
    static let okLabel = "OK"
    static let invalidType = "Invalid type"
    static let newVersionAvailable = "New version of app is available. Kindly update it from App Store."
    static let updateLabel = "Update"
    static let unableChat = "Unable to start chat"
    static let notificationScreentitile = "Notifications"
    static let myParslScreenTitle = "My PARSLs"
    static let unavaileblNotifications = "No Notification available"
    static let unavailableParsls = "No PARSL available."
    static let downloadingLabel = "Downloading"
    static let unabletoDownloadGift = "Unable to download gift box animation, please try again later."
    static let successfullyEdited = "Successfully edited"
    static let submitSuccessfully = "submitted successfully"
    static let submitButtonTitle = "Submit"
    static let descriptionTextLabel  = "Description (optional)"
    static let parslItunesIdentifier = "1558433040"
    static let unableToFetch = "Unable to fetch data. Please try again later"
    static let unableToSaveCard = "Unable to save card in photos library"
    static let cardSaved = "Card has been saved to your photos. Don't share this information with anyone."
    static let alreadyRedeemed = "PARSL already redeemed by"
    static let typeMessageOrRecordMessage = "Type Message or Record Message"
    static let NoPreview = "No preview available"
    static let skipLabel = "Skip"
    static let viewAvatar = "Tap on screen to view Avatar"
    static let viewModel = "Tap on screen to view model"
    static let viewParsl = "Tap on screen to view PARSL"
    static let sureToClear = "Are you sure to clear the scene?"
    static let clearLabel = "Clear"
    static let itemsAlreadyAdded = "Item has already been added."
    static let MyaccountLabel = "My Account"
    static let walletLabel = "Wallet"
    static let sendlabel = "Send"
    static let redeemLabel = "Redeem"
    static let moveyourCamera = "Move your camera towards a flat surface and slowly hover around the surface and tap on surface to view object"
    static let modelNotAvailable = "Model Not Available. Please select other items"
    static let unableToDownloadModel = "Unable to download model, please try again later."
    static let tapAnywhere = "Tap on screen to view PARSL"
    static let tapToSceeLiveAvatar = "Tap on screen to view your Avatar"
    static let inValidOtpUrl = "Invalid OTP/URL input. Please try again"
    static let unableToFetchParsl = "Unable to fetch PARSL. Please try again later"
    static let agentsAreBusy = "All agents are busy at the moment.Try again later"
    static let receivedParsl = "You received a PARSL from your friend"
    static let shadowsDisable = "Shadows disable"
    static let noShadow = "This model has no shadow"
    static let shadowEnable = "Shadows enable"
    static let horizontalDisable = "horizontal disable"
    static let horizontalEnable = "horizontal enable"
    static let myAvatarScreenTitle = "My Avatars"
    static let createAvatarScreenTitle = "Create Avatar"
    static let avatarsNotavailable = "No Avatar available. Request for a new Avatar"
    static let avatarUrlGenerated = "Avatar URL Generated"
    static let deleted = "Successfully deleted"
    static let enterReceiverName = "Please enter receiver name"
    static let enterReceiverNumber = "Please enter receiver contact number"
    static let enterValidNumber = "Please enter valid number"
    static let liveSupportScreenName = "Live Support"
    static let endChatLabel = "End Chat"
    static let sureToDeleteChat = "All your chat will be deleted.Are you sure you want to continue?"
    static let parslBeingredeemedTxt = "PARSL is being redeemed"
    static let nameOfObject = "Name of object was empty"
    static let walletScreenTitile = "Wallet"
    static let parslWallet = "PARSL Wallet"
    static let copiedToClipboard = "Copied to clipboard"
    static let isSaveImagesFolder = "Do you want to delete images folder?"
    
    static let GENERIC_ALERT_TEXT_400 = "error 400"
    static let GENERIC_ALERT_TEXT_401 = "error 401"
}

struct constantDateFormate {
    static let dateFormate = "dd/MM/yyyy"
}

struct notificationNav {
    static var goToMyParslScreen = false
    static var goToChatScreen = false
    static var goToNotificationScreen = false
}
struct Helpers{
    static var copounCode : String?
    static var senderDetails : senderDetails?
    static var name : String?
    static var greetingTextViewMessage = ""
}

struct senderDetails{
    var name : String?
    var email : String?
}

struct CaptureFolders{
    static var showCaptureFolders = false
    static var captureUrl = ""
    static var isCreateNewSession = true
    static var deleteImagesFolder = false
}

public enum UIUserInterfaceIdiom : Int {

    case unspecified

    @available(iOS 3.2, *)
    case phone // iPhone and iPod touch style UI
    @available(iOS 3.2, *)
    case pad // iPad style UI
    @available(iOS 9.0, *)
    case tv // Apple TV style UI
    @available(iOS 9.0, *)
    case carPlay // CarPlay style UI
}

