//
//  ParslUtils.swift
//  Parsl
//
//  Created by broadstone on 20/04/2021.
//

import Foundation
import UIKit
import FirebaseCore
import FirebaseAnalytics


class ParslUtils: NSObject {
    static let sharedInstance = ParslUtils()
    static var sender = senderDetails()
    static var help = Helpers()
    
    var DEVICE_TOKEN: String? = ""
    var parslOtp: String? = ""
    var error400 = false
    var error401 = false
    var error403 = false
    var isSuccess = false
    
    // firebase events method
    func eventsCallForFRAnalytics(envetName: String, detailsOfEvent: [String : Any]){
            Analytics.logEvent(envetName, parameters: detailsOfEvent  )
        }
    
    func saveUserCardName(_ fullname:String)  {
        
        UserDefaults.standard.set(fullname, forKey: "fullname")
        UserDefaults.standard.synchronize()
        
    }
    
    
    func saveCardLastDigitsAndType(_ lastDigits: String, _ cardType: String)  {
        
        UserDefaults.standard.set(lastDigits, forKey: "lastDigits")
        UserDefaults.standard.set(cardType, forKey: "cardType")
        UserDefaults.standard.synchronize()
        
    }
    func saveUserEmail(_ email: String){
        UserDefaults.standard.set(email, forKey: "Email")
        UserDefaults.standard.synchronize()
    }
    func saveUserSocialEmail(_ socialEmail: String){
        UserDefaults.standard.set(socialEmail, forKey: "SocialEmail")
        UserDefaults.standard.synchronize()
    }
    func saveUserOldEmail(_ email: String){
        UserDefaults.standard.set(email, forKey: "OldEmail")
        UserDefaults.standard.synchronize()
    }
    func saveUserNumber(_ number: String){
        UserDefaults.standard.set(number, forKey: "Number")
        UserDefaults.standard.synchronize()
    }
    func saveIfUserLogin(_ loginStatus: Bool){
        UserDefaults.standard.set(loginStatus, forKey: "LoginStatus")
        UserDefaults.standard.synchronize()
    }
    func saveUserNameSpace(_ nameSpace: String){
        UserDefaults.standard.set(nameSpace, forKey: "UserNameSpace")
        UserDefaults.standard.synchronize()
    }
    func saveIfLoginScreenShown(_ loginStatus: Bool){
        UserDefaults.standard.set(loginStatus, forKey: "LoginScreenShown")
        UserDefaults.standard.synchronize()
    }
    func saveSizeOfImages(_ size: String){
        UserDefaults.standard.set(size, forKey: "ImageSize")
        UserDefaults.standard.synchronize()
    }
    func saveDirectoryPath(_ path: String){
        UserDefaults.standard.set(path, forKey: "DirectoryPath")
        UserDefaults.standard.synchronize()
    }
    func saveIfImagesUploadingDone(_ imagesStatus: Bool){
        UserDefaults.standard.set(imagesStatus, forKey: "ImagesUploadingStatus")
        UserDefaults.standard.synchronize()
    }
    func saveDepthCameraAvailable(_ camera: String) {
        UserDefaults.standard.set(camera, forKey: "DepthCameraAvailable")
        UserDefaults.standard.synchronize()
    }
    func saveLastCount (_ count: String) {
        UserDefaults.standard.set(count, forKey: "LastCount")
        UserDefaults.standard.synchronize()
    }
    func saveFolderName (_ folderName: String) {
        UserDefaults.standard.set(folderName, forKey: "FolderName")
        UserDefaults.standard.synchronize()
    }
    func saveDeviceId (_ deivceId: String) {
        UserDefaults.standard.set(deivceId, forKey: "DeviceID")
        UserDefaults.standard.synchronize()
    }
    func saveAutoThemeEnable(_ themeMode: Bool) {
        UserDefaults.standard.set(themeMode, forKey: "ThemeMode")
        UserDefaults.standard.synchronize()
    }
    func getAutoThemeEnable() -> Bool {
        let mode = (UserDefaults.standard.value(forKey: "ThemeMode") as? Bool) ?? false
        return mode
    }
    func getDeviceId() -> String {
        let id = (UserDefaults.standard.value(forKey: "DeviceID") as? String) ?? ""
        return id
    }
    func getFolderName () -> String {
        let folderName = (UserDefaults.standard.value(forKey: "FolderName") as? String) ?? ""
        return folderName
    }
    func getLastCount () -> String {
        let count = (UserDefaults.standard.value(forKey: "LastCount") as? String) ?? ""
        return count
    }
    func getIfDepthCameraAvailable () -> String {
        let camera = (UserDefaults.standard.value(forKey: "DepthCameraAvailable") as? String) ?? ""
        return camera
    }
    
    func getifImagesUPloaded() -> Bool {
        let status = (UserDefaults.standard.value(forKey: "ImagesUploadingStatus") as? Bool) ?? false
        return status
    }
    func getSizeOfImages() -> String {
        let size = (UserDefaults.standard.value(forKey: "ImageSize") as? String) ?? ""
        return size
    }
    func getDirectoryPath() -> String {
        let path = (UserDefaults.standard.value(forKey: "DirectoryPath") as? String) ?? ""
        return path
    }
    func getIfLoginScreenShown() -> Bool{
        let loginScreenStatus = (UserDefaults.standard.value(forKey: "LoginScreenShown") as? Bool) ?? false
        return loginScreenStatus
    }
    func getUserNameSpace() -> String {
        let nameSpace = (UserDefaults.standard.value(forKey: "UserNameSpace") as? String) ?? ""
        return nameSpace
    }
    func getIfuserLogin() -> Bool{
        let status = (UserDefaults.standard.value(forKey: "LoginStatus") as? Bool) ?? false
        return status
    }
    func getUserNumber() -> String {
        let userNumber = (UserDefaults.standard.value(forKey: "Number") as? String) ?? ""
        return userNumber
    }
    func getuserEmail() -> String {
        let userEmail = (UserDefaults.standard.value(forKey: "Email") as? String) ?? ""
        return userEmail
    }
    func getuserSocialEmail() -> String {
        let userSocialEmail = (UserDefaults.standard.value(forKey: "SocialEmail") as? String) ?? ""
        return userSocialEmail
    }
    func getuserOldEmail() -> String {
        let oldEmail = (UserDefaults.standard.value(forKey: "OldEmail") as? String) ?? ""
        return oldEmail
    }
    func getUserCardName() -> String {
        let cardName = UserDefaults.standard.value(forKey: "fullname") as! String
        return cardName
    }

    func getUserCardType() -> String{
        let cardType = (UserDefaults.standard.value(forKey: "cardType") as! String) ?? ""
        return cardType
    }
    func getLastDigitsOfCard() -> String{
        let lastDigits = UserDefaults.standard.value(forKey: "lastDigits") as! String
        return lastDigits
    }
    func getDateInUSLocale(timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func openUrl(url: String){
        if let Url = URL(string: url) {
            UIApplication.shared.open(Url)
        }
    }
    
    func doubleToRoundedOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = ","
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
    
    func doubleToPercentageOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = .none
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
 
    func parseDouble(strValue: String) -> Double {
        return Double(strValue.replacingOccurrences(of: ",", with: "")
                        .replacingOccurrences(of: "$", with: "")
                        .replacingOccurrences(of: " ", with: "")
                        .replacingOccurrences(of: "%", with: ""))!
    }
    
    func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
    
        let serviceUrl =  serviceName
        print("url == \(serviceUrl)")
        let url = NSURL(string: serviceUrl)
    
        //create the session object
        let session = URLSession.shared
    
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
    //        request.timeoutInterval = 20
    
        do {
    
            let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
            let dataString = String(data: data, encoding: .utf8)!
            print("data string is",dataString)
    
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
    
        } catch let error {
            print(error.localizedDescription)
        }
    
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    
        //create dataTask using the session object to send data to the server
    
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
    
            print(" response is \(response)")
    
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
    
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
    
    
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
    
            guard error == nil else {
                print("this is error")
    //            DispatchQueue.main.async {
    ////                    CozyLoadingActivity.hide()
    ////                    self.showFailureAlert(ConstantStrings.internet_off)
    //            }
                return
            }
            guard let data = data else {
                return
            }
    
            do {
                //create json object from data
    
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    success(json)
    
                    //handle json...
    
                }
    
    
            } catch let error {
    
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
    
        })
    
        task.resume()
    
    }
}
