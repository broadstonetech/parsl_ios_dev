

//
//  ImagesUploadMethod.swift
//  Parsl
//
//  Created by M Zaryab on 12/02/2022.
//

import Foundation
import NotificationCenter
import UserNotifications
import Alamofire
import SwiftUI

import UIKit

class ImageUPloadMethods {
    
    func uploadModelImages() {
        ParslUtils.sharedInstance.saveIfImagesUploadingDone(true)
        let url = constantsUrl.Upload_Images_URL
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(NSURL(fileURLWithPath: ParslUtils.sharedInstance.getDirectoryPath()) as URL, withName: "zipfile")
            multipart.append("\(ParslUtils.sharedInstance.getSizeOfImages())".data(using: .utf8)!, withName :"size")
            multipart.append(ParslUtils.sharedInstance.getUserNameSpace().data(using: .utf8)!, withName :"namespace")
            multipart.append("parsl_ios".data(using: .utf8)!, withName :"app_id")
            
        }, to: url, method: .post, headers: nil) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response { answer in
                    print(answer.response!)
                    if answer.response?.statusCode == 200 {
                        ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
                        self.notifyOnCompletion()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UploadSuccess"), object: nil)
                        //                    self.deleteZipFile()
                    }
                    else {
                        ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UploadFailed"), object: nil)
                        //                    self.deleteZipFile()
                    }
                }
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                    ParslUtils.sharedInstance.saveIfImagesUploadingDone(true)
                    print(progress)
                }
            case .failure(let encodingError):
                ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
                print("multipart upload encodingError: \(encodingError)")
            }
        }
        
    }
    
    func notifyOnCompletion() {
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = "PARSL"
        content.body = "images uploaded successfully"
        content.sound = .default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "notify", content: content, trigger: trigger)
        center.add(request) { (error) in
            if error != nil {
                print("error")
            }
        }
    }
    // zip the images folder.
    func deleteZipFile() {
        do {
            let fileManager = FileManager.default
            // Check if file exists
            var path = ParslUtils.sharedInstance.getDirectoryPath()
            path = path.replacingOccurrences(of: " ", with: "%20")
            if fileManager.fileExists(atPath: path) {
                // Delete file
                try fileManager.removeItem(atPath: path)
            } else {
                print("File does not exist")
            }
            
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    func deleteImagesFolder() {
        if CaptureFolders.deleteImagesFolder == true {
        let docFolder = CaptureFolderState.capturesFolder()
        let folderListing =
        try? FileManager.default
            .contentsOfDirectory(at: docFolder!,
                                 includingPropertiesForKeys: [.creationDateKey],
                                 options: [ .skipsHiddenFiles ])
        
        debugPrint(folderListing as Any)
        for folder in folderListing! as [URL]{
            do {
                try FileManager.default.removeItem(at: folder)
            } catch {
                debugPrint(error.localizedDescription)
            }
            break
        }
        } else {
            
        }
    }
}

