//
//  UiViewExtension.swift
//  Jedi
//
//  Created by Mac Book Pro 13 on 8/01/2019.
//
import Foundation
import UIKit

extension UIView {
    func addShadowToView(cornerRadius:CGFloat = 7, color: UIColor = UIColor.lightGray) {
//        backgroundColor = UIColor.white
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5
        layer.cornerRadius = cornerRadius
    }
    func addShadow(scale: Bool = true) {
        self.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
    func roundCorners(corners:CACornerMask, radius: CGFloat) {
       self.layer.cornerRadius = radius
       self.layer.maskedCorners = corners
    }
    func roundCornerss(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    func circularView(bordorColor: UIColor = UIColor.clear, borderWidth: CGFloat = 0.5) {
        layer.cornerRadius = frame.size.width/2
        clipsToBounds = true
        layer.masksToBounds = true
        layer.borderColor = bordorColor.cgColor
        layer.borderWidth = borderWidth
    }
    func cornerRadiuss(radius: CGFloat, borderWidth: CGFloat, color: CGColor){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
//            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
            
        }
        set {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
}
// for taking screen shot of specific uiview or subview.
extension UIView {
    
    class func image(view: UIView, subview: UIView? = nil) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
    
        view.drawHierarchy(in: view.frame, afterScreenUpdates: true)
        
        var image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!

        UIGraphicsEndImageContext()
        
        if(subview != nil){
            var rect = (subview?.frame)!
            rect.size.height *= image.scale  //MOST IMPORTANT
            rect.size.width *= image.scale    //TOOK ME DAYS TO FIGURE THIS OUT
            let imageRef = image.cgImage!.cropping(to: rect)
            image = UIImage(cgImage: imageRef!, scale: image.scale, orientation: image.imageOrientation)
        }
        
        return image
    }
}
