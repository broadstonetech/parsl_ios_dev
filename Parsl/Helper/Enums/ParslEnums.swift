//
//  ParslEnums.swift
//  Parsl
//
//  Created by M Zaryab on 03/03/2022.
//

import Foundation


// MARK: - Enums
enum DownloadStatus {
    case Downloading
    case Downloaded
    case NotDownloadable
}

enum floatingButtontitles {
    case Wallet
    case Redeem
    case Send
}
