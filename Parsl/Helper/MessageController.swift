//
//  MessageController.swift
//  Parsl
//
//  Created by Billal on 06/03/2021.
//

import Foundation
import UIKit

class MessageController: BaseViewController{
    @IBOutlet weak var messageView: UITextView!
    var message: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
