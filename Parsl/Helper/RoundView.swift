//
//  RoundedView.swift
//  Parsl
//
//  Created by Billal on 24/02/2021.
//

import UIKit

class RoundView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowRadius = 4
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale

    }
    override func layoutSubviews() {
    super.layoutSubviews()
        
        
    }
}
