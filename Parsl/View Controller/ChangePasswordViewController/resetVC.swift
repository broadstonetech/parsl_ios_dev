//
//  resetVC.swift
//  test
//
//  Created by Hussnain Ali on 31/12/2021.
// This class enable the user to change the password.

import UIKit
import Foundation
import CommonCrypto
import CryptoKit
import CommonCrypto
import Toast_Swift

class resetVC: BaseViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var oldPTF: UITextField!
    @IBOutlet weak var newPTF: UITextField!
    @IBOutlet weak var confrimPTF: UITextField!
    @IBOutlet weak var doneBtn: UIButton!
    
    // MARK: - Properties
    let skipButton = UIButton(type: .system)
    let subImageView = UIImageView()
    var style = ToastStyle()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.designUI()
        self.setUpSkipbtn()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    }
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    // MARK: - IBActions
    @IBAction func doneBtn(_ sender: Any) {
        if oldPTF.text!.isEmpty || oldPTF.text == "" || oldPTF.text == " " {
            self.view.makeToast(constantsStrings.enterOldPassword, duration: 1.0, position: .center, style: style)
            return
        }
        if newPTF.text!.isEmpty || newPTF.text == "" || newPTF.text == " " {            self.view.makeToast(constantsStrings.enterNewPassword, duration: 1.0, position: .center, style: style)
            return
        }
        if confrimPTF.text!.isEmpty || confrimPTF.text == "" || confrimPTF.text == " " {
            self.view.makeToast(constantsStrings.enterConfirmPassword, duration: 1.0, position: .center, style: style)
            return
        }
        if newPTF.text! != confrimPTF.text! {
            self.view.makeToast(constantsStrings.samePasswordNewConfirm, duration: 1.0, position: .center, style: style)
            return
        }

        if !isPasswordEmail(newPTF.text!) {
            self.view.makeToast(constantsStrings.enterValidPassword, duration: 1.0, position: .center, style: style)
            return
        }
        
        
        changePassword()
    }
    
    // MARK: - Methods
   
    
    func designUI(){
        self.stackView.addShadowToView()
        self.stackView.layer.cornerRadius = 10
        self.stackView.clipsToBounds = true
        self.doneBtn.layer.cornerRadius = self.doneBtn.frame.height/2
        self.doneBtn.layer.shadowColor = UIColor.lightGray.cgColor
        self.doneBtn.layer.shadowRadius = 4
        self.doneBtn.layer.shadowOpacity = 2
        self.doneBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func setUpSkipbtn() {
    skipButton.frame = CGRect(x: view.frame.size.width/1.3, y: 20, width: 80, height: 25)
        skipButton.setTitle(constantsStrings.cancelLabel, for: .normal)
    skipButton.backgroundColor = .clear
    skipButton.layer.cornerRadius = 5
    skipButton.clipsToBounds = true
    skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
    skipButton.setTitleColor(UIColor.link, for: .normal)
    skipButton.addTarget(self, action: #selector(skipBtnActions(_:)), for: .touchUpInside)
    view.addSubview(skipButton)
    }
    
    @objc func skipBtnActions(_ sender:UIButton!) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func changePassword() {
        ProgressHUD.show()
//        let  newPass = newPTF.text!.sha256()
//        let  confirmPass = confrimPTF.text!.sha256()
        var data = [String: Any]()
        let params = ["app_id": "parsl_ios", "new_password": newPTF.text!, "old_password": oldPTF.text!, "namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        print(params)
        
        let url = constantsUrl.RESET_PASSWORD_URL
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status {
                    DispatchQueue.main.async{
                    ProgressHUD.dismiss()
                        self.view.makeToast(message, duration: 1.0, position: .center, style: self.style)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                } else {
                    self.showAlert(withMessage: message)
                    ProgressHUD.dismiss()
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
}
