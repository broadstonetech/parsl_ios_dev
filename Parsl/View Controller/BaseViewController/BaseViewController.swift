//
//  BaseViewController.swift
//  Parsl
//
//  Created by Billal on 15/03/2021.
//

import Foundation
import UIKit
import Stripe
import SystemConfiguration
import CryptoKit
import CommonCrypto

class BaseViewController: UIViewController {
        
    var app_ID : String? = UIDevice.current.identifierForVendor?.uuidString
    var deviceToken : Data? = nil
    var error400 = false
    var error401 = false
    var error403 = false
    var isSuccess = false
    
    func showAlertWithAction(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: constantsStrings.okLabel, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }))
    
        self.present(alert, animated: true, completion: nil)
    }
    
    // convert string into has256
   
    func showAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
        })
        //        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        //        })
        alert.addAction(ok)
        //        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    

    
    func showAlert(withMessage message:String) {
        let alert = UIAlertController(title: constantsStrings.appName, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
        })
         
        alert.addAction(ok)
        //        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func getAppVersionString() -> String {
          if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
              if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                 return "version: \(version)(\(build))"
              } else {
                  return "version: \(version)"
              }
          }
          return ""
          
          
      }
    // get tax/commission on an amount
    func getTaxPrice (totalAmount: Double) -> Double {
        var amount = 0.0
        amount = totalAmount * 0
        return amount
    }
    
    func isPasswordEmail(_ testStr:String) -> Bool {
        let passwordRegEx = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        
        return passwordTest.evaluate(with: testStr)
    }
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
    }
    
    func moveToNextVC(storyBoardName: String, identifierName: String) {
        let storyBoard = UIStoryboard(name: storyBoardName, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifierName) as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func postRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ response:Data)->(), failure:@escaping ( _ error:Error)->()) {
        let parameters = sendData
        print("param: ",parameters)
        let value = serviceName.replacingOccurrences(of: " ", with: "")
        guard let url = URL(string: value)
            else{
                return
        }
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Content-Type" : "application/json"
        ]
        let session = URLSession(configuration: config)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        session.dataTask(with: request, completionHandler:{(data, response, error)
            in
            DispatchQueue.main.async {
            }
            if let response = response
            {
                print("Response: ",response)
                
                if let httpStatus = response as? HTTPURLResponse {
                    // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    if httpStatus.statusCode == 200 {
                        self.isSuccess = true
                    }else{
                        self.isSuccess = false
                    }
                    
                    if httpStatus.statusCode == 400 {
                        self.error400 = true
                    }
                    else if(httpStatus.statusCode == 401)
                    {
                        self.error401 = true
                    }
                    else if(httpStatus.statusCode == 403)
                    {
                        self.error403 = true
                    }
                    
                }
            }
            if let error = error
            {
                DispatchQueue.main.async {
                    print(error)
                    failure(error)
                }
            }
            if let data = data
            {
                success(data)
            }
        }).resume()
    }
    
    //MARK: - PostRequest for chat module
    func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
 
        let serviceUrl =  serviceName
        print("url == \(serviceUrl)")
        let url = NSURL(string: serviceUrl)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
            let dataString = String(data: data, encoding: .utf8)!
            print("data string is",dataString)
            
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                DispatchQueue.main.async {
//                    CozyLoadingActivity.hide()
//                    self.showFailureAlert(ConstantStrings.internet_off)
                }
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    success(json)
                    
                    //handle json...
         
                }
                
              
            } catch let error {
              
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    
}
extension UIViewController {
    func hideKeyboardTappedAround(){
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}

// convert string into has256
extension Data {
    public func sha256() -> String{
        return hexStringFromData(input: digest(input: self as NSData))
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
    var bytes: [UInt8] {

        return [UInt8](self)

    }
}

public extension String {
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return stringData.sha256()
        }
        return ""
    }
    func stringAt(_ i: Int) -> String {
        return String(Array(self)[i])
    }
    func charAt(_ i: Int) -> Character {
        return Array(self)[i]
    }
}
 
