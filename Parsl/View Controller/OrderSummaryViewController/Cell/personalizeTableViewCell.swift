//
//  personalizeTableViewCell.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit

class personalizeTableViewCell: UITableViewCell,UITextFieldDelegate{
    @IBOutlet weak var NameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!

    @IBOutlet weak var logInView: UIView!
    @IBOutlet weak var logInBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NameTF.delegate = self
        emailTF.delegate = self
        NameTF.cornerRadiuss(radius: NameTF.frame.size.height/2, borderWidth: 1, color: UIColor(named: "DarkImageColor")!.cgColor)
        emailTF.cornerRadiuss(radius: 22.5, borderWidth: 1, color: UIColor(named: "DarkImageColor")!.cgColor)
    }

    
     func textFieldDidChangeSelection(_ textField: UITextField) {
         ParslUtils.sender.name = NameTF.text
         ParslUtils.sender.email = emailTF.text 
      }
}
