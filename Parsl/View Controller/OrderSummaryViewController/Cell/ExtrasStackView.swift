//
//  ExtrasStackView.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit

class ExtrasStackView: UITableViewCell,UITextViewDelegate {
   
  
    @IBOutlet weak var messageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var greetingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upperBgView: UIView!
    @IBOutlet weak var lowerBgView: UIView!
    @IBOutlet weak var personalizeYourGreetingsTextView: UITextView!
    @IBOutlet weak var personalizeYourGreetingsBtn: UIButton!
    @IBOutlet weak var personaizeYourMessageBtn: UIButton!
    @IBOutlet weak var VideoImgeView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        upperBgView.layer.cornerRadius = 6
        upperBgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        upperBgView.layer.masksToBounds = true
        
        lowerBgView.layer.cornerRadius = 6
        lowerBgView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        lowerBgView.layer.masksToBounds = true
        personalizeYourGreetingsTextView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        personalizeYourGreetingsTextView.textColor = UIColor(named: "BlackWhiteColor")!
        Helpers.greetingTextViewMessage = personalizeYourGreetingsTextView.text
 }
}
