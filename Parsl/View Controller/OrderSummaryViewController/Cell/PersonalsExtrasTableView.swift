//
//  PersonalsExtrasTableView.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit

class PersonalsExtrasTableView: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var personlizationBtn: UIButton!
    @IBOutlet weak var extrasBtn: UIButton!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
        personlizationBtn.layer.cornerRadius = 20
        extrasBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        extrasBtn.layer.cornerRadius = 20
        personlizationBtn.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
