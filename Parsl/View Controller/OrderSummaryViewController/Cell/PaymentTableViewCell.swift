//
//  PaymentTableViewCell.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        bgView.layer.cornerRadius = 6
        super.setSelected(selected, animated: animated)
        payBtn.layer.cornerRadius = 20

        // Configure the view for the selected state
    }

}
