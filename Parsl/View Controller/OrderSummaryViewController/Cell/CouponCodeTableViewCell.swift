//
//  CouponCodeTableViewCell.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit

class CouponCodeTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var buttomView: UIView!
    @IBOutlet weak var transactionCostLable: UILabel!
    @IBOutlet weak var totalChargesLable: UILabel!
    @IBOutlet weak var applyCoupon: RoundButton!
    @IBOutlet weak var couponTf: UITextField!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
        bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        bgView.layer.masksToBounds = true
        couponTf.layer.cornerRadius = couponTf.frame.size.height/2
        couponTf.clipsToBounds = true
        couponTf.layer.borderColor = UIColor(named: "DarkImageColor")?.cgColor
        couponTf.layer.borderWidth = 1
        buttomView.layer.cornerRadius = 6
       buttomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMaxYCorner]
        buttomView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {

        Helpers.copounCode = couponTf.text 
      }
}
