//
//  CartDetailTableViewCell.swift
//  Parsl
//
//  Created by Billal on 20/03/2021.
//

import Foundation
import UIKit
class CartDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
    }
}
