//
//  GiftBoxTableViewCollectionCell.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import Foundation
import UIKit
import AVKit

class GiftBoxTableViewCollectionCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
  
    
    @IBOutlet weak var headingLable: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var ExtrasCollectionView: UICollectionView!
    
//    var giftWrapSelectedIndex = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
        
        DispatchQueue.main
            .async {
                if OrderSummarylistVC.giftWrapSelectedIndex == -1 {
                    giftModels.data![0].isModelSelected = true
                }
                else {
                    giftModels.data![OrderSummarylistVC.giftWrapSelectedIndex].isModelSelected = true
                }
                self.ExtrasCollectionView.reloadData()
            }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func commonInit(){
        ExtrasCollectionView.delegate = self
        ExtrasCollectionView.dataSource = self
        ExtrasCollectionView.reloadData()
    }
}
extension GiftBoxTableViewCollectionCell {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if ExtrasCollectionView.tag == 100 {
        return giftModels.data!.count
        }else if ExtrasCollectionView.tag == 200{
            return giftModels.avatars!.count
        }else{
            return  giftModels.animations.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if  ExtrasCollectionView.tag == 100 {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
            let item = giftModels.data![indexPath.row]
        cell.imagePath = item.modelFiles!.thumbnail!
        let url = URL(string: cell.imagePath)
        cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        cell.title.text = item.basicInfo!.name!
         print("boxes are ",item.basicInfo?.name,item.productID)
            cell.mainView.layer.cornerRadius = 7
            cell.mainView.clipsToBounds = true
            cell.thumbnail.layer.cornerRadius = 7
            cell.thumbnail.clipsToBounds = true
        
        if item.isModelSelected == true{
            cell.SelectedImage.isHidden = false
        }
        else{
            cell.SelectedImage.isHidden = true
        }
        
        return cell
        }
        else if ExtrasCollectionView.tag == 200 {
            let avatarCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
            let avatarItem = giftModels.avatars![indexPath.row]
            avatarCell.imagePath = avatarItem.chracter_thumbnail!
            let url = URL(string: avatarCell.imagePath)
            avatarCell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            avatarCell.title.isHidden = true
            avatarCell.mainView.layer.cornerRadius = 7
            avatarCell.mainView.clipsToBounds = true
            avatarCell.thumbnail.layer.cornerRadius = 7
            avatarCell.thumbnail.clipsToBounds = true
            avatarCell.thumbnail.backgroundColor = UIColor.gray
            if avatarItem.isAvatarSelected == true{
                avatarCell.SelectedImage.isHidden = false
            }
            else{
                avatarCell.SelectedImage.isHidden = true
            }
            return avatarCell
        }
        else  {
            let animationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
            let animationitem = giftModels.animations[indexPath.row]
            animationCell.imagePath = animationitem.animation_thumbnail!
            let url = URL(string: animationCell.imagePath)
            animationCell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            animationCell.title.isHidden = true
            animationCell.mainView.layer.cornerRadius = 7
            animationCell.mainView.clipsToBounds = true
            animationCell.thumbnail.layer.cornerRadius = 7
            animationCell.thumbnail.backgroundColor = UIColor.gray
            animationCell.thumbnail.clipsToBounds = true
            if animationitem.isAnimationSelected == true{
                animationCell.SelectedImage.isHidden = false
            }
            else{
                animationCell.SelectedImage.isHidden = true
            }
            return animationCell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         if ExtrasCollectionView.tag == 100 {
  
        for i in 0..<giftModels.data!.count{
            giftModels.data![i].isModelSelected = false
        }
            OrderSummarylistVC.giftWrapSelectedIndex = indexPath.row
        giftModels.data![indexPath.row].isModelSelected = true
            self.ExtrasCollectionView.reloadData()
         }
        else if ExtrasCollectionView.tag == 200{
            for k in 0..<giftModels.avatars!.count{
                giftModels.avatars![k].isAvatarSelected = false
            }
            OrderSummarylistVC.avatarSelectedIndex = indexPath.row
            giftModels.avatars![indexPath.row].isAvatarSelected = true
                self.ExtrasCollectionView.reloadData()

        }
        else {
            for j in 0..<giftModels.animations.count{
                giftModels.animations[j].isAnimationSelected = false
            }
            debugPrint(indexPath.row,"")
            OrderSummarylistVC.animationSelectedIndex = indexPath.row
            giftModels.animations[indexPath.row].isAnimationSelected = true
                self.ExtrasCollectionView.reloadData()

        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if  ExtrasCollectionView.tag == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count + 4{
            let width = ExtrasCollectionView.frame.size.width/3
            let height = ExtrasCollectionView.frame.size.height/1.3
            return CGSize(width: width, height: height)
        }
        else if  ExtrasCollectionView.tag == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count + 5{
            let width = ExtrasCollectionView.frame.size.width/3
            let height = ExtrasCollectionView.frame.size.height/1.5
            return CGSize(width: width, height: height)
        }
        
        else {
            let width = ExtrasCollectionView.frame.size.width/3
            let height = ExtrasCollectionView.frame.size.height/1.5
            return CGSize(width: width, height: height)
        }
        
        
        return collectionView.frame.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
