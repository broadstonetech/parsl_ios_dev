//
//  OrderSummarylistVC.swift
//  orderSummary
//
//  Created by Hussnain Ali on 11/05/2022.
//

import UIKit
import AVKit
import MobileCoreServices
import PassKit
import Alamofire
import Lottie
import UnityFramework
import Toast_Swift
import Stripe

struct ModelsQuantityStruct {
    var modelIndex: Int!
    var modelQuantity: Int!
    var modelPrice: Double!
    var totalPrice: Double!
    var isDiscounted:Bool!
}

class OrderSummarylistVC: BaseViewController,  UINavigationControllerDelegate, CartCellClickDelegate, UIImagePickerControllerDelegate{
     
    var cellCount = 4
    var selectionOfExtras = true
    var greetings = false
    var message = false
    // MARK: - Properties
    var isValidCoupon = false
    var arrColor = [(isSelected:Bool,color:UIColor)]()
    var couponResponse:                   CouponResponse!
    var taxPrice: Double            = 0.0
    var priceWithoutTax: Double     = 0.0
    var totalPrice: Double          = 0.0
    var totalPriceWithoutTax: Double          = 0.0
    var senderName: String          = ""
    var senderEmail: String         = ""
    var senderAddress: String       = ""
    var senderNumber: String        = ""
    var receiverName: String        = ""
    var receiverEmail: String       = ""
    var receiverAddress: String     = ""
    var receiverNumber: String      = ""
    var deviceTokenString:String    = ""
    var pricesList                  = [Double]()
    var selectedItemsDataList       = [SubCategoryData]()
    //these arrays are used to zip into one dictionary
    var selectedItemsArray          = [String]()
    var quantity                    = [Int]()
    var videoURL: String            = ""
    var selectedModel:String        = ""
    var isVideo:Bool = false
    var selectedItem                = 0
    var selectIndex                 = 0
    var selectIndexSec              = 0
    var saveParsl                   : SaveParsl!
    var isVideoRecorded: Bool = false
    var extrasArray                     = [GiftModelMessage]()
    var animationView: AnimationView?
    var downloadedAnimationsFilePaths = [String]()
    var isAnyDiscount:Bool = false
    var avatarCharacter = ""
    var avatarAnimation = ""
    var applePayTapped = true
    static var modelsInCart = [ModelDataStruct]()
    static var modelsPrices = [ModelsQuantityStruct]()
    static var giftWrapSelectedIndex = 0
    static  var animationSelectedIndex = -1
    static var avatarSelectedIndex = -1
    var cartDelegate: CartEmptyDelegate?
    var unityView: UIView?
    var finalCharges = ""
    var appleDataKey = ""
    var pricesListCount:Int?
    
    //Total Charges
    var totalCharges : String?
    var transactionalCost : String?
    
    @IBOutlet weak var summaryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //find Item Price
        calculateInitalModelPrices()
        summaryTableView.delegate = self
        summaryTableView.dataSource = self
        summaryTableView.reloadData()
        createNotifications()
        ParslUtils.sender.name = ""
        summaryTableView.rowHeight = UITableView.automaticDimension
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        setTheme()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        OrderSummarylistVC.modelsPrices.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func onPaymentTokenGenerated(_ notification: NSNotification) {
        ProgressHUD.dismiss()
        if let token = notification.userInfo?["token"] as? String {
            print("....\(token)")
            if let lastDigit = notification.userInfo?["lastFour"] as? String {
                if let cardType = notification.userInfo?["cardType"] as? String {
                    self.saveParslDetail(token: token,lastFour: lastDigit,cardType: cardType)
                }
                
            }
        }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else {return}
            guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
            let keyboardFrame = keyboardSize.cgRectValue
            if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= keyboardFrame.height-60
            }
        }
        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    
    @objc func showPaymenMethods (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.paymentmethods()
        }
    }
    
    @objc func hideLoginDescription (_ notification: NSNotification){
        DispatchQueue.main.async {
            self.summaryTableView.reloadData()
        }
        
        
    }
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    func createNotifications () {
        //payment token
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
        
        // keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(OrderSummarylistVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(OrderSummarylistVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
         
        NotificationCenter.default.addObserver(self, selector: #selector(showPaymenMethods(_:)), name: Notification.Name(rawValue: "ShowPaymentMehods"), object: nil)
//
        NotificationCenter.default.addObserver(self, selector: #selector(hideLoginDescription(_:)), name: Notification.Name(rawValue: "cartLoginSuccess"), object: nil)
        
    }
}

//MARK: - EXTENSION TABLEVIEW
extension OrderSummarylistVC :UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count + cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectionOfExtras == true {
            if indexPath.row == 0
            {
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell", for: indexPath) as! HeadingLabelsCell
                return cell
            }
            
            
            if indexPath.row < 1 + OrderSummarylistVC.modelsInCart.count {
                let item = OrderSummarylistVC.modelsInCart[indexPath.row - 1]
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as? CartTableViewCell
                cell?.delegate = self
                cell?.plusButton.tag = indexPath.row - 1
                cell?.minusButton.tag = indexPath.row - 1
                cell?.titleLabel.text = item.basicInfo!.name!
                cell?.itemsCountLabel.text = String(OrderSummarylistVC.modelsPrices[indexPath.row - 1].modelQuantity)
                cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)
                cell?.subtotalLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item.priceInfo.price * Double(OrderSummarylistVC.modelsPrices[indexPath.row - 1].modelQuantity)))
                let url = URL(string: OrderSummarylistVC.modelsInCart[indexPath.row - 1].modelFile.thumbnail)
                cell?.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                
                
                
                if OrderSummarylistVC.modelsPrices[indexPath.row-1].isDiscounted {
                    //discounted
                    cell!.discountedPrice.isHidden = false
                    cell!.discountedPrice.text = "$\(String(format: "%.2f",OrderSummarylistVC.modelsPrices[indexPath.row-1].totalPrice))"
                    cell!.discountedCutView.isHidden = false
                }else{
                    cell!.discountedPrice.isHidden = true
                    cell!.discountedCutView.isHidden = true
                    
                }
                return cell!
            } else if  indexPath.row < 1 + OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartDetailTableViewCell") as? CartDetailTableViewCell
                let index = indexPath.row - OrderSummarylistVC.modelsInCart.count - 1
                cell?.titleLabel.text = "\(OrderSummarylistVC.modelsInCart[index].basicInfo.name as String)"
                cell?.quantityLabel.text = "x \(String(OrderSummarylistVC.modelsPrices[index].modelQuantity))"
                cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: OrderSummarylistVC.modelsPrices[index].totalPrice)
                
                if OrderSummarylistVC.modelsPrices[index].isDiscounted{
                    cell?.priceLabel.text = "$\(String(format: "%.2f",OrderSummarylistVC.modelsPrices[index].totalPrice))"
                }
                
                return cell!
            } else  if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 1 {
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "CouponCodeTableViewCell", for: indexPath) as! CouponCodeTableViewCell
                cell.couponTf.tag = indexPath.row
                cell.applyCoupon.tag = indexPath.row
                cell.applyCoupon.addTarget(self, action: #selector(addCopounCode(sender:)), for: .touchUpInside)
                cell.totalChargesLable.text = totalCharges
                cell.transactionCostLable.text = transactionalCost
                return cell
            }else
            if indexPath.row ==  OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 2 {
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "PersonalsExtrasTableView", for: indexPath) as! PersonalsExtrasTableView
                cell.personlizationBtn.backgroundColor = UIColor(named: "dark-custom-blue")
                cell.extrasBtn.backgroundColor = UIColor(named: "light-custom-blue")
                cell.personlizationBtn.tag = indexPath.row
                cell.personlizationBtn.addTarget(self, action: #selector(personlizationBtn(sender:)), for: .touchUpInside)
                cell.extrasBtn.tag = indexPath.row
                cell.extrasBtn.addTarget(self, action: #selector(extrasBtn(sender:)), for: .touchUpInside)

                return cell
            }
            else if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count + 3{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "personalizeTableViewCell", for: indexPath) as! personalizeTableViewCell
                if ParslUtils.sharedInstance.getIfuserLogin() == true {
                    cell.emailTF.text  = ParslUtils.sharedInstance.getuserEmail()
                    cell.logInView.isHidden = true
//                    cell.heightOfBgView.constant = 136
                    cell.emailTF.isUserInteractionEnabled = false
                }else{
                    cell.logInView.isHidden = false
//                    cell.heightOfBgView.constant = 166
                }
                cell.NameTF.tag = indexPath.row
                cell.emailTF.tag = indexPath.row
                cell.logInBtn.tag = indexPath.row
                cell.logInBtn.addTarget(self, action: #selector(tapLoginPressed(sender:)), for: .touchUpInside)
                return cell
                
            }else{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
                cell.payBtn.tag = indexPath.row
                cell.payBtn.addTarget(self, action: #selector(paymentMethod(sender:)), for: .touchUpInside)
                return cell
            }
        }else{
            if indexPath.row == 0
            {
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell", for: indexPath) as! HeadingLabelsCell
                return cell
            }
            
            
            if indexPath.row < 1 + OrderSummarylistVC.modelsInCart.count {
                let item = OrderSummarylistVC.modelsInCart[indexPath.row - 1]
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as? CartTableViewCell
                cell?.delegate = self
                cell?.plusButton.tag = indexPath.row - 1
                cell?.minusButton.tag = indexPath.row - 1
                cell?.titleLabel.text = item.basicInfo!.name!
                cell?.itemsCountLabel.text = String(OrderSummarylistVC.modelsPrices[indexPath.row - 1].modelQuantity)
                cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)
                cell?.subtotalLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item.priceInfo.price * Double(OrderSummarylistVC.modelsPrices[indexPath.row - 1].modelQuantity)))
                let url = URL(string: OrderSummarylistVC.modelsInCart[indexPath.row - 1].modelFile.thumbnail)
                cell?.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                
                
                
                if OrderSummarylistVC.modelsPrices[indexPath.row-1].isDiscounted {
                    //discounted
                    cell!.discountedPrice.isHidden = false
                    cell!.discountedPrice.text = "$\(String(format: "%.2f",OrderSummarylistVC.modelsPrices[indexPath.row-1].totalPrice))"
                    cell!.discountedCutView.isHidden = false
                }else{
                    cell!.discountedPrice.isHidden = true
                    cell!.discountedCutView.isHidden = true
                    
                }
                return cell!
            } else if  indexPath.row < 1 + OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartDetailTableViewCell") as? CartDetailTableViewCell
                let index = indexPath.row - OrderSummarylistVC.modelsInCart.count - 1
                cell?.titleLabel.text = "\(OrderSummarylistVC.modelsInCart[index].basicInfo.name ?? "")"
                cell?.quantityLabel.text = "x \(String(OrderSummarylistVC.modelsPrices[index].modelQuantity))"
                cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: OrderSummarylistVC.modelsPrices[index].totalPrice)
                
                if OrderSummarylistVC.modelsPrices[index].isDiscounted{
                    cell?.priceLabel.text = "$\(String(format: "%.2f",OrderSummarylistVC.modelsPrices[index].totalPrice))"
                }
                
                return cell!
            } else  if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 1 {
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "CouponCodeTableViewCell", for: indexPath) as! CouponCodeTableViewCell
                cell.couponTf.tag = indexPath.row
                cell.applyCoupon.tag = indexPath.row
                cell.applyCoupon.addTarget(self, action: #selector(addCopounCode(sender:)), for: .touchUpInside)
                cell.totalChargesLable.text = totalCharges
                cell.transactionCostLable.text = transactionalCost
                return cell
            }else
            if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 2{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "PersonalsExtrasTableView", for: indexPath) as! PersonalsExtrasTableView
                cell.extrasBtn.backgroundColor = UIColor(named: "dark-custom-blue")
                cell.personlizationBtn.backgroundColor = UIColor(named: "light-custom-blue")
                cell.personlizationBtn.tag = indexPath.row
                cell.personlizationBtn.addTarget(self, action: #selector(personlizationBtn(sender:)), for: .touchUpInside)
                cell.extrasBtn.tag = indexPath.row
                cell.extrasBtn.addTarget(self, action: #selector(extrasBtn(sender:)), for: .touchUpInside)

                return cell
            }else
            if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 3{
                
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "ExtrasStackView", for: indexPath) as! ExtrasStackView
                cell.personalizeYourGreetingsBtn.tag = indexPath.row
                cell.personalizeYourGreetingsBtn.addTarget(self, action: #selector(personalizeYourGreetingsBtn(sender:)), for: .touchUpInside)
                cell.personaizeYourMessageBtn.tag = indexPath.row
                cell.personaizeYourMessageBtn.addTarget(self, action: #selector(personaizeYourMessageBtn(sender:)), for: .touchUpInside)
                if greetings == true{
                    cell.greetingViewHeight.constant = 105
                    cell.personalizeYourGreetingsTextView.isHidden = false
                }else{
                    cell.greetingViewHeight.constant = 35
                    cell.personalizeYourGreetingsTextView.isHidden = true
                }
                if message == true{
                    cell.messageViewHeight.constant = 105
                    cell.VideoImgeView.isHidden = false
                }else{
                    cell.messageViewHeight.constant = 35
                    cell.VideoImgeView.isHidden = true
                }
                cell.personalizeYourGreetingsTextView.tag = indexPath.row
                return cell
                
            }else
            if indexPath.row ==  OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count + 4{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "GiftBoxTableViewCollectionCell", for: indexPath) as! GiftBoxTableViewCollectionCell
                cell.headingLable.text = "Gift Box"
                cell.ExtrasCollectionView.tag = 100
                cell.commonInit()
                return cell
                
            }else
            if indexPath.row == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 5{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "GiftBoxTableViewCollectionCell", for: indexPath) as! GiftBoxTableViewCollectionCell
                cell.headingLable.text = "Avartar"
                cell.ExtrasCollectionView.tag = 200
                cell.commonInit()
                return cell
            }else
            if indexPath.row ==   OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 6{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "GiftBoxTableViewCollectionCell", for: indexPath) as! GiftBoxTableViewCollectionCell
                cell.ExtrasCollectionView.tag = 300
                cell.headingLable.text = "Animation"
                cell.commonInit()
                return cell
                
            }
            else{
                let cell = summaryTableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
                cell.payBtn.tag = indexPath.row
                cell.payBtn.addTarget(self, action: #selector(paymentMethod(sender:)), for: .touchUpInside)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    //Protocol Functions
    func deleteButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.summaryTableView.indexPath(for: cell) else {
            return
        }
        OrderSummarylistVC.modelsInCart.remove(at: indexPath.row - 1)
        OrderSummarylistVC.modelsPrices.remove(at: indexPath.row - 1)
        DispatchQueue.main.async {
            if OrderSummarylistVC.modelsInCart.count == 0{
                
                let alert = UIAlertController(title: "Cart is Empty", message: "Add Items to Cart", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                    self.cartDelegate?.cartHasBeenCleared()
                    if let navigator = self.navigationController {
                        navigator.popViewController(animated: true)
                    }
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            self.summaryTableView.reloadData()
        }
        recalculateFinalPrice()
        
    }
    func plusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.summaryTableView.indexPath(for: cell) else {
            return
        }
        
        let modelPrice = OrderSummarylistVC.modelsPrices[indexPath.row - 1]
        if modelPrice.modelQuantity < 100 {
            let modelQuantity = modelPrice.modelQuantity + 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted:modelPrice.isDiscounted)
            
            OrderSummarylistVC.modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
            if isAnyDiscount{
                self.getCopounDetails(coupunCode: Helpers.copounCode ?? "")
            }
            
            DispatchQueue.main.async {
                self.summaryTableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Total quantity should be less than 100")
        }
        
    }
    func minusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.summaryTableView.indexPath(for: cell) else {
            return
        }
        
        let modelPrice = OrderSummarylistVC.modelsPrices[indexPath.row - 1]
        
        if modelPrice.modelQuantity > 1 {
            let modelQuantity = modelPrice.modelQuantity - 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted: modelPrice.isDiscounted)
            
            OrderSummarylistVC.modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
            if isAnyDiscount{
                self.getCopounDetails(coupunCode: Helpers.copounCode  ?? "")
            }
            
            DispatchQueue.main.async {
                self.summaryTableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Quantity cannot be less than 1")
        }
        
    }
    
    
}
//MARK: - METHOD OF CELLS
extension OrderSummarylistVC{
 //payment method
    @objc func paymentMethod(sender: UIButton){

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.paymentmethods()
        }
    }
    
   func paymentmethods() {
       if ParslUtils.sender.name == "" {
           self.view.makeToast("Name was empty")
           return
       }
       else if ParslUtils.sender.email == "" {
           self.view.makeToast("Email was empty")
           return
       }
       else if !isValidEmail(ParslUtils.sender.email ?? "") {
           self.view.makeToast("Enter right email")
           return
       }
       else {
       self.popupAlert(title: constantsStrings.chooseOptions, message: nil, actionTitles: ["Apple Pay","Pay with Stripe","Cancel"], actionStyle: [.default,.default,.cancel],
           actions: [
           {
           action in
               self.applePayTapped = true
               let paymentItem = PKPaymentSummaryItem.init(label: constantsStrings.appName, amount: NSDecimalNumber(value: Double(self.finalCharges)!))
                       let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
                       if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                           let request = PKPaymentRequest()
                           request.currencyCode = "USD" // 1
                           request.countryCode = "US" // 2
                           request.merchantIdentifier = constantsStrings.appleMerchantIdentifier// 3
                           request.merchantCapabilities = PKMerchantCapability.capability3DS // 4
                           request.supportedNetworks = paymentNetworks // 5
                           request.paymentSummaryItems = [paymentItem] // 6
                           guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
                               self.displayDefaultAlert(title: constantsStrings.errorTitle, message: constantsStrings.errorMessage)

                               return
                           }
                           paymentVC.delegate = self
                           self.present(paymentVC, animated: true, completion: nil)
                       } else {
                           self.displayDefaultAlert(title: constantsStrings.errorTitle, message: constantsStrings.appleTransactionErrorMessage)
                       }
           },
           {
           action in
               self.applePayTapped = false
               let stripeVC = StipePaymentIntegration()
               self.navigationController?.present(stripeVC, animated: true, completion: nil)
           }
           ,nil], vc: self)
       }
    }
    //MARK:- ADD
    @objc func addCopounCode(sender: UIButton){
        let buttonTag = sender.tag
        if buttonTag == OrderSummarylistVC.modelsInCart.count + OrderSummarylistVC.modelsPrices.count  + 1{
            DispatchQueue.main.async { [self] in
                isAnyDiscount = false
                if Helpers.copounCode != ""{
                    self.getCopounDetails(coupunCode: Helpers.copounCode ?? "")
                }
                else
                {
                    showAlert(withTitle: constantsStrings.copounMissingFieldTitle, withMessage: constantsStrings.copounMissingFieldMessage)
                }
            }
        }
    }

    @objc func personlizationBtn(sender: UIButton){
        self.cellCount = 4
        self.selectionOfExtras = true
        DispatchQueue.main.async {
            self.summaryTableView.reloadData()
        }
        
    }
    
    @objc func extrasBtn(sender: UIButton){
     
        self.cellCount = 7
        self.selectionOfExtras = false
        DispatchQueue.main.async {
            self.summaryTableView.reloadData()
        }
    }
    //PERSONALIZE YOUR GREETINGS AND MESSAGE
    @objc func personaizeYourMessageBtn(sender: UIButton){
        self.recordVideoBtnPressed()
    }
    @objc func personalizeYourGreetingsBtn(sender: UIButton){
        greetings = true
        summaryTableView.reloadData()
    }
    
    
   // LogIn Method
    
    @objc func tapLoginPressed(sender: UIButton) {
        let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.fromSendOrRedeem = "cart"
        self.present(vc, animated: true, completion: nil)
    }
    
    //Open Camera for Taking Vedio
     func recordVideoBtnPressed(){
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .camera)
     }
}
//MARK: - VIDEO CAPTURE OUPTPUT
extension OrderSummarylistVC: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput,
                        didFinishRecordingTo outputFileURL: URL,
                        from connections: [AVCaptureConnection],
                        error: Error?) {
            guard let data = try? Data(contentsOf: outputFileURL) else {
                return
            }

            print("File size before compression: \(data.count)")
            print("File size before compression: \(Double(data.count / 1048576)) mb")

            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
            compressVideo(inputURL: outputFileURL as URL,
                          outputURL: compressedURL) { exportSession in
                guard let session = exportSession else {
                    return
                }

                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = try? Data(contentsOf: compressedURL) else {
                        return
                    }
                    
                    print("File size after compression: \(compressedData.count)")

                    print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
                case .failed:
                    break
                case .cancelled:
                   
                    break
                @unknown default:
                    break
                }
            }
        }


        func compressVideo(inputURL: URL,
                           outputURL: URL,
                           handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
            let urlAsset = AVURLAsset(url: inputURL, options: nil)
            guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                           presetName: AVAssetExportPresetMediumQuality) else {
                handler(nil)

                return
            }

            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4
            exportSession.exportAsynchronously {
                handler(exportSession)
            }
        }
    
    
}

//video Message
extension OrderSummarylistVC  {
    func imagePickerController(
      _ picker: UIImagePickerController,
      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
      dismiss(animated: true, completion: nil)

      guard
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
        mediaType == (kUTTypeMovie as String),
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
        UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
        else { return }
 
        
        guard let data = try? Data(contentsOf: url) else {
            return
        }

        print("File size before compression: \(Double(data.count / 1048576)) mb")
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
        compressVideo(inputURL: url as URL,
                      outputURL: compressedURL) { exportSession in
            guard let session = exportSession else {
                return
            }
//            let url = NSURL(string: compressedURL)
            let urll = compressedURL
            let parhUrl = urll.path
            if parhUrl == ""{
                print("url is empty......")
            }
            else{
                print("url path is \(parhUrl)")
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                    return
                }
                self.message = true
                self.videoURL = compressedURL.path
                DispatchQueue.main.async {
                    self.summaryTableView.reloadData()
                }
                
                print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                self.message = false
                break
            @unknown default:
                debugPrint("video canceled ....")
                break
            }
        }
        
    }
    
}






//MARK: - METHOD FOR CALCULATION
extension OrderSummarylistVC {
    // get the details of the copoun.
    func getCopounDetails(coupunCode: String){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        let data = ["coupon_code":coupunCode,
                    "selected_products":selectedItemsArray,
                    "date_time":dateString] as [String : Any]
        let parameters = ["app_id": "parsl_ios",
                          "data": data] as [String : Any]
        
        print(parameters)
        let url = constantsUrl.CHECK_COPOUN_DETAIL_URL
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                    
                    let decoder = JSONDecoder()
                    self.couponResponse = try? decoder.decode(CouponResponse.self, from: data)
                    dump(self.couponResponse)
                    let relief = couponResponse.data!.copounRelief
                    var discountedPrice = 0.0
                    var nonDiscountedPrice = 0.0
                    if couponResponse.data?.copounStatus == true{
                        
                        self.isAnyDiscount = true //coupon is valid
                        
                        
                        let item = couponResponse.data!.approvedProducts!
                        if item.count > 0 {
                            
                            //discounted price
                            
                            for product in item{
                                let index = OrderSummarylistVC.modelsInCart.firstIndex(where: {$0.productId == product})
                                var totalPrice = OrderSummarylistVC.modelsPrices[index!].totalPrice
                                
                                if couponResponse.data?.copounReliefType == "price_relief"{
                                    totalPrice = totalPrice! -  (Double(relief!))
                                    
                                }else{
                                    
                                    totalPrice = totalPrice! - (( (Double(relief!)) / 100) * totalPrice!)
                                }
                                
                                OrderSummarylistVC.modelsPrices[index!].totalPrice = totalPrice
                                OrderSummarylistVC.modelsPrices[index!].isDiscounted = true
                                discountedPrice = totalPrice! + discountedPrice
                                // self.calculateDiscountedPrice(indexPath: index!, discountedPrice:totalPrice!)
                                
                            }
                        }
                        
                        //denied products
                        let deniedItems = couponResponse.data!.deniedProducts!
                        if deniedItems.count > 0{
                            
                            for product in deniedItems{
                                let index = OrderSummarylistVC.modelsInCart.firstIndex(where: {$0.productId == product})
                                let totalPrice = OrderSummarylistVC.modelsPrices[index!].totalPrice
                                OrderSummarylistVC.modelsPrices[index!].isDiscounted = false
                                nonDiscountedPrice = totalPrice! + nonDiscountedPrice
                                
                            }
                        }
                        priceWithoutTax = discountedPrice + nonDiscountedPrice
                        // priceWithoutTax = pricesList.reduce(0,+)
                        taxPrice = ( 15.0 / 100) * priceWithoutTax
                        totalPrice = priceWithoutTax + taxPrice
                        DispatchQueue.main.async {
                            self.transactionalCost = "$\(String(format: "%.2f",taxPrice))"
                            self.totalCharges = "$\(String(format: "%.2f",totalPrice))"
                        }
                        
                    }else{
                        showAlert(withTitle: constantsStrings.copounErrorTitle, withMessage: constantsStrings.copounErrorMessage)
                    }
                    DispatchQueue.main.async {
                        self.summaryTableView.reloadData()
                    }
                    
                }
                
                else{
                    showAlert(withTitle: constantsStrings.copounErrorTitle, withMessage: constantsStrings.copounErrorMessage)
                }
                
            }
            catch let  error {
                print(error)
            }
        })
        task.resume()
    }
    
    
    // calculate price after adding or deleting items.
    func recalculateFinalPrice(){
        totalPrice = 0
        for i in 0..<OrderSummarylistVC.modelsPrices.count {
            totalPrice += OrderSummarylistVC.modelsPrices[i].totalPrice
        }
        totalPriceWithoutTax = totalPrice
        taxPrice = getTaxPrice(totalAmount: totalPrice)
        totalPrice = totalPrice + taxPrice
        totalCharges = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCost = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
    }
    
    // calculate prices before deleting or subtracting.
    private func calculateInitalModelPrices() {
        for i in 0..<OrderSummarylistVC.modelsInCart.count {
            let modelIndex = i
            let modelQuantity = 1
            let modelPrice = OrderSummarylistVC.modelsInCart[i].priceInfo.price ?? 0.0
            let finalPrice = modelPrice * Double(modelQuantity)
            
            let modelPriceStruct = ModelsQuantityStruct(modelIndex: modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice, totalPrice: finalPrice,isDiscounted: false)
            
            OrderSummarylistVC.modelsPrices.append(modelPriceStruct)
            
            totalPrice += finalPrice
        }
        totalPriceWithoutTax = totalPrice
        taxPrice = getTaxPrice(totalAmount: totalPrice)
        totalPrice = totalPrice + taxPrice
        totalCharges = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        self.finalCharges = ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCost = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
    }
    
    
}


// MARK: - Apple Pay Payment Delegates
extension OrderSummarylistVC: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        dismiss(animated: true, completion: nil)
        STPAPIClient.shared().createToken(with: payment, completion: { (token, error) in
            guard let token = token else {
                print(error)
            return
            }
            print(token.tokenId)
            self.saveParslDetail(token: token.tokenId, lastFour: "", cardType: "")
        })
}
    
    func saveParslDetail(token: String, lastFour: String, cardType: String) {
        //Calculate tax price
        let email  = ParslUtils.sender.email
        let name = ParslUtils.sender.name
        ProgressHUD.show()
        taxPrice = (totalPrice * 15) / 100
        priceWithoutTax = totalPrice - taxPrice
        
        //End Calculation of tax price
        var isVideoRecorded = 0
        
        if videoURL != "" {
            isVideoRecorded = 1
        }
        var itemDict = [AnyObject]()
        for i in 0..<OrderSummarylistVC.modelsInCart.count {
            let item = ["product_id": OrderSummarylistVC.modelsInCart[i].productId as AnyObject,
                        "quantity": OrderSummarylistVC.modelsPrices[i].modelQuantity as AnyObject]
            
            itemDict.append(item as AnyObject)
        }
        print(itemDict)
        if ParslUtils.sharedInstance.getIfuserLogin() == false {
            if email == ParslUtils.sharedInstance.getuserEmail(){
            ParslUtils.sharedInstance.saveUserEmail(email!)
        
        } else {
            ParslUtils.sharedInstance.saveUserOldEmail(ParslUtils.sharedInstance.getuserEmail())
            ParslUtils.sharedInstance.saveUserEmail(email!)
               }
              }
        let timestamp = Int64(Date().currentTimeMillis())
        print(timestamp)
        var senderData = [String: Any]()
        if self.applePayTapped {
            senderData       = ["card_number":"6321",
                                "card_type":"Apple Pay",
                                "sender_device_token": ParslUtils.sharedInstance.DEVICE_TOKEN,
                                "sender_device_id": deviceID,
                                "email": ParslUtils.sender.email ?? "",
                                "sender_name": ParslUtils.sender.name ?? ""
            ] as [String: Any]
        } else {
            senderData       = ["card_number":lastFour,
                                "card_type":cardType,
                                "sender_device_token": ParslUtils.sharedInstance.DEVICE_TOKEN,
                                "sender_device_id": deviceID,
                                "email": ParslUtils.sender.email ?? "",
                                "sender_name": ParslUtils.sender.name ?? ""
            ] as [String: Any]
        }
        
        let priceRelated    = ["price": totalPriceWithoutTax,
                               "currency": "usd",
                               "total_price": totalPrice, "isProcessingFeeAdded": "0"] as [String : Any]
        
        
        if Helpers.greetingTextViewMessage == constantsStrings.cartScreenTextViewPlaceholder || Helpers.greetingTextViewMessage == "" {
            Helpers.greetingTextViewMessage = constantsStrings.cartScreenTextViewPlaceholder
        }else{
            Helpers.greetingTextViewMessage  = Helpers.greetingTextViewMessage
        }
        var characterkey = ""
        var animationKey = ""
        if OrderSummarylistVC.avatarSelectedIndex != -1{
            characterkey = "avatar_\(giftModels.avatars![OrderSummarylistVC.avatarSelectedIndex].chracter_key!)"
            animationKey = giftModels.animations[0].animation_key!
        }
        if OrderSummarylistVC.animationSelectedIndex != -1{
            animationKey = giftModels.animations[OrderSummarylistVC.animationSelectedIndex].animation_key!
        }
        
  
        let data :Dictionary<String,Any>           = ["selected_products":itemDict,
                                                      "gift_model":giftModels.data![OrderSummarylistVC.giftWrapSelectedIndex].productID,
                                                      "sender_data":senderData,
                                                      "text_msg":Helpers.greetingTextViewMessage ?? "",
                                                      "parsl_timestamp":timestamp,
                                                      "parsl_stripe_token":token,
                                                      "have_video": isVideoRecorded,
                                                      "avatar_chracter": characterkey as String ,
                                                      "avatar_animation": animationKey as String,
                                                      "price_related": priceRelated] as [String : Any]
        let parameters      = ["app_id": "parsl_ios", "old_mail": ParslUtils.sharedInstance.getuserOldEmail(), "data": data] as [String : Any]
        print(parameters)
        let url = constantsUrl.SAVE_PARSL_DETAILS_URL
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        do {
            
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            //applinks:parsl.io/604e127f66c4bc23831f1fcc
            do {
                
                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                    saveParsl = try JSONDecoder().decode(SaveParsl.self, from: data)
                    print(saveParsl!)
                    if self.videoURL != ""{
                        DispatchQueue.main.async {
                            self.uploadParslVideo()
                        }
                    }
                    DispatchQueue.main.async {
                        print(parameters)
                        print(saveParsl!)
                        guard let message = saveParsl.message else {
                            return
                        }
                        if message.contains("Price cannot be zero"){
                            showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unsuccessfulTransactionMessage)
                        } else {
                            self.cartDelegate?.cartHasBeenCleared()
                            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ReLoadUnity", message: "")
                            isOrderFlowCompleted = true
                            let vc = UIStoryboard(name: "OrderSummaryStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                            vc.amount = "$" + String(saveParsl.data!.totalPrice!)
//                            vc.amount = self.finalCharges
                            vc.parslOTP = saveParsl.data!.parslOtp!
                            vc.parslURL = saveParsl.data!.parslURL!
                            vc.transaction = saveParsl.data!.transectionID!
                            vc.cardNumber = saveParsl.data!.cardNumber!
//                            vc.transactionCost = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
                            vc.transactionCost = transactionalCost
                            vc.modelsPrices = OrderSummarylistVC.modelsPrices
                            vc.modelsInCart = OrderSummarylistVC.modelsInCart
                            if Helpers.greetingTextViewMessage == constantsStrings.cartScreenTextViewPlaceholder || Helpers.greetingTextViewMessage == "" {
                                vc.msgText = constantsStrings.cartScreenTextViewPlaceholder + name! + "."
                            }else{
                                vc.msgText = Helpers.greetingTextViewMessage
                            }
                            for i in 0..<giftModels.data!.count{
                                giftModels.data![i].isModelSelected = false
                            }
                            for j in 0..<giftModels.animations.count{
                                giftModels.animations[j].isAnimationSelected = false
                            }
                            for k in 0..<giftModels.avatars!.count{
                                giftModels.avatars![k].isAvatarSelected = false
                            }
                            OrderSummarylistVC.modelsPrices.removeAll()
                            OrderSummarylistVC.modelsInCart.removeAll()
                            OrderSummarylistVC.giftWrapSelectedIndex = 0
                            OrderSummarylistVC.avatarSelectedIndex = -1
                            OrderSummarylistVC.animationSelectedIndex = -1
                            ProgressHUD.dismiss()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    
                }
            } catch let error {
                ProgressHUD.dismiss()
                print(error)
                showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unsuccessfulTransactionMessage)
            }
        })
        task.resume()
    }
    
    
    //Upload Vedio
    
    func uploadParslVideo() {
        let url = constantsUrl.UPLOAD_PARSL_VIDEO_URL
        print(saveParsl.data!.parslID!)
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(NSURL(fileURLWithPath: self.videoURL) as URL, withName: "video_msg")

            multipart.append("\(self.saveParsl.data!.parslID!)".data(using: .utf8)!, withName :"parsl_id")
        }, to: url, method: .post, headers: nil) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response { answer in
                    print(answer.response!)
                    print("statusCode: \(String(describing: answer.response?.statusCode))")
                }
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                    print(progress)
                }
            case .failure(let encodingError):
                print("multipart upload encodingError: \(encodingError)")
            }
        }
    }
}

//MARK: -TableView Protocols
protocol CartCellClickDelegate: class {
    func plusButtonTapped(cell: CartTableViewCell, sender : Any)
    func minusButtonTapped(cell: CartTableViewCell, sender : Any)
    func deleteButtonTapped(cell: CartTableViewCell, sender : Any)
    
}
