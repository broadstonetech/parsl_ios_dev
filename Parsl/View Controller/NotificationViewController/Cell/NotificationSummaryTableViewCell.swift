//
//  NotificationSummaryTableViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 21/12/2021.
//
// This class is used to design the cell of table view of the notification summary

import UIKit

class NotificationSummaryTableViewCell: UITableViewCell {

   
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainview.layer.cornerRadius = 5
        mainview.clipsToBounds = true
    }
}
