//
//  NotificationsSummryViewController.swift
//  Parsl
//
//  Created by M Zaryab on 17/12/2021.
// This class shows the essentials notifications.

import UIKit

class NotificationsSummryViewController: BaseViewController {

    
    //MARK: - Properties
    var notificationArray: NotificationHistory!
    var notificationData = [NotificationsData]()
    
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    
    // MARK: - IBOutlets
    @IBOutlet weak var notificationsTableView: UITableView!
    
    @IBOutlet weak var centerLabel: UILabel!
    //MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        title = constantsStrings.notificationScreentitile
        self.getNotificationsSummry()
        navigationBarUI()
        notificationsTableView.delegate = self
        notificationsTableView.dataSource = self
        notificationsTableView.reloadData()
        notificationsTableView.rowHeight = UITableView.automaticDimension
        centerLabel.isHidden = true
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//        notificationNav.goToNotificationScreen = false
//        notificationsTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    }
    
    //MARK: - Methods
    
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    func navigationBarUI() {
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func getNotificationsSummry() {
        ProgressHUD.show()
        let url = constantsUrl.GET_NOTIFICATION_URL
        let data = ["device_id": deviceId, "email": ParslUtils.sharedInstance.getuserEmail(), "phone_no": ParslUtils.sharedInstance.getUserNumber()]
        let params = ["data": data] as [String: AnyObject]
        print(params)
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                let status = json["status"] as! Bool
                if status == true {
                self.fetchData(response: json["data"] as? [AnyObject] ?? [AnyObject]())
                DispatchQueue.main.async{
                    self.notificationData.reverse()
                self.notificationsTableView.reloadData()
                ProgressHUD.dismiss()
                }
                }
                else {
                    DispatchQueue.main.async { 
                    ProgressHUD.dismiss()
                        self.centerLabel.isHidden = false
                        self.notificationsTableView.isHidden = true 
                    }
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            ProgressHUD.dismiss()
            print(error)
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func fetchData(response: [AnyObject]) {
        for i in 0..<response.count {
            let dataObj = response[i] as! [String: AnyObject]
            let data = fetchModelDataStruct(dataObj: dataObj)
            self.notificationData.append(data)
        }
    }

    func fetchModelDataStruct(dataObj: [String: AnyObject]) -> NotificationsData {
        let notificationRecieverName = (dataObj["notification_reciever_name"] as? String) ?? ""
        let not_type = dataObj["not_type"] as! String
        let not_message = dataObj["not_message"] as! String
        let not_title = dataObj["not_title"] as! String
        let sent_time = dataObj["sent_time"] as! Double
        return NotificationsData(not_message: not_message, not_title: not_title, not_type: not_type, notificationRecieverName: notificationRecieverName, sent_time: sent_time)
    }
    
    // MARK: - IBActions
    
}
