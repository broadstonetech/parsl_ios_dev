//
//  TransactionDetailCell.swift
//  transaction Details
//
//  Created by Hussnain Ali on 10/05/2022.
//

import UIKit

class TransactionDetailCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var transactionCost: UILabel!
    @IBOutlet weak var ammountLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
