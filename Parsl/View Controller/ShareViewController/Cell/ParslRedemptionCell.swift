//
//  ParslRedemptionCell.swift
//  transaction Details
//
//  Created by Hussnain Ali on 10/05/2022.
//

import UIKit

class ParslRedemptionCell: UITableViewCell {

    @IBOutlet weak var otpCodeLable: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
