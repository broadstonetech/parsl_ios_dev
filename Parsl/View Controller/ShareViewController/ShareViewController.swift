//
//  ShareViewController.swift
//  Parsl
//
//  Created by Billal on 08/03/2021.
// this class let the user to view the details of the bought PRASLs and enable user to share the PRASLs

import Foundation
import UIKit

class ShareViewController: PaymentsBaseClass{
    
    // MARK: - Properties
    var parslURL,parslOTP,transaction,amount,cardNumber,transactionCost, msgText:String!
    var modelsInCart = [ModelDataStruct]()
    var modelsPrices = [ModelsQuantityStruct]()
    
    var isShareBtnClicked = false
    // MARK: - IBoutlets
    @IBOutlet weak var buttonBgView: UIView!
    @IBOutlet weak var transactionDetailTableView: UITableView!

    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBgView.layer.cornerRadius = 6
        amount = String(amount.dropFirst())
        amount =  ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: Double(amount)!)
        transactionDetailTableView.delegate = self
        transactionDetailTableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    }
    
    // MARK: - IBoutlets
    @IBAction func shareButton(_ sender: Any) {
        if isShareBtnClicked {
            let alert = UIAlertController(title: constantsStrings.shareParslTitle, message: constantsStrings.shareParslMessage, preferredStyle: .alert)
            let share = UIAlertAction(title: constantsStrings.shareYes, style: .default, handler: { action in
                self.openShareActivity()
            })
            let cancel = UIAlertAction(title: constantsStrings.shareNo, style: .cancel, handler: { action in
                self.dismiss(animated: true, completion: nil)
                if let navigator = self.navigationController {
                    navigator.popViewController(animated: false)
                    navigator.popViewController(animated: true)
                }
            })
            alert.addAction(share)
            alert.addAction(cancel)
            //        alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else {
            openShareActivity()
        }
        isShareBtnClicked = true
        
    }
    
    
    
    @IBAction func mainMenu(_ sender: Any) {
        print(parslOTP!)
        if let navigator = self.navigationController {
            navigator.popViewController(animated: false)
            navigator.popViewController(animated: true)
        }
    }
    // MARK: - Mehthods
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    private func openShareActivity() {
        //let parslWebsite = "https://www.parsl.io"
        let parslWebsite = constantsWebsiteUrl.PARSL_WEBSITE
        //let shareableText = msgText + "\n" + "Visit link \(parslWebsite) and use PARSL redeem code \(parslOTP!) to unwrap your PARSL."
        let shareableText = "\(constantsStrings.shareAbletext) \(parslOTP!) to unwrap your PARSL."
        let activityVC = UIActivityViewController(activityItems: [shareableText], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.maxY, width: 0, height: 0)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
}
