//
//  ShareViewControllerExtension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit


// MARK: - Extensions

// MARK: - TableVeiw
extension ShareViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  4 + modelsInCart.count
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell")!
//            return cell
//        }
//        else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductItemsCell") as! ProductItemsCell
//            let index = indexPath.row - 1
//            cell.name.text = modelsInCart[index].basicInfo.name
//            let url = URL(string: modelsInCart[index].modelFile.thumbnail)
//            cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
//            cell.qty.text = String(modelsPrices[index].modelQuantity)
//            cell.subTotal.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
//            return cell
//        }
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
        let cell = transactionDetailTableView.dequeueReusableCell(withIdentifier: "mailedCopyCell", for: indexPath)as! mailedCopyCell
        return cell
        }else if indexPath.row == 1{
            let cell = transactionDetailTableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell", for: indexPath)
            return cell
        }else if indexPath.row == modelsInCart.count + 2{
            let cell = transactionDetailTableView.dequeueReusableCell(withIdentifier: "TransactionDetailCell", for: indexPath) as! TransactionDetailCell
            cell.transactionID.text = "\(transaction!)"
            cell.transactionCost.text = transactionCost
            cell.ammountLable.text = "$\(amount!)"
            return cell
        }else if indexPath.row == modelsInCart.count + 3{
            let cell = transactionDetailTableView.dequeueReusableCell(withIdentifier: "ParslRedemptionCell", for: indexPath) as! ParslRedemptionCell

            cell.otpCodeLable.text = parslOTP
            return cell
        }
        else{
            let cell = transactionDetailTableView.dequeueReusableCell(withIdentifier: "ProductItemsCell", for: indexPath) as! ProductItemsCell
            let index = indexPath.row - 2
                       cell.name.text = modelsInCart[index].basicInfo.name
                       let url = URL(string: modelsInCart[index].modelFile.thumbnail)
                       cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                       cell.qty.text = String(modelsPrices[index].modelQuantity)
                       cell.subTotal.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
                       return cell
        }
        
    }
}
