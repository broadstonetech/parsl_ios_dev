//
//  LoginViewController.swift
//  Parsl
//
//  Created by M Zaryab on 22/12/2021.
// This class enables the user to singin/ signup at same time using email passwowrd or using sing in with apple (apple login)

import UIKit
import AuthenticationServices
import Toast_Swift

class LoginViewController: BaseViewController {

    // MARK: - Iboutlets
    
    @IBOutlet weak var ppView: UIView!
    @IBOutlet weak var ppCheckBtn: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var TCBtn: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var checkUncheckBtn: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailview: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var passwordView: UIView!
    
    // MARK: - Properties
    let skipButton = UIButton(type: .system)
    var fromSendOrRedeem = ""
    var style = ToastStyle()
    var isCheckBtnPressed = true
    var isPPCheckBtnPressed = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSkipbtn()
        setUpSignInAppleButton()
        setUPUi()
        createTapAbleButton() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        generateNotifications()

    }
    // detect dark mode changes
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.userInterfaceStyle == .dark {
            setUpSignInAppleButton()
        }
        else {
            setUpSignInAppleButton()
        }
    }
    // MARK: - Methods
    
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    @objc func tapTCPressed(_ sender: UITapGestureRecognizer? = nil) {
         let url = URL(string: constantsWebsiteUrl.TERMS_CONDITION_URL)!
            UIApplication.shared.open(url)
        
    }
    
    @objc func ppViewPressed(_ sender: UITapGestureRecognizer? = nil) {
         let url = URL(string: constantsWebsiteUrl.PRIVACY_POLICY_URL)!
            UIApplication.shared.open(url)
    }
    
    @objc func checkUncheckPressed(_ sender: UITapGestureRecognizer? = nil) {
        if isCheckBtnPressed == true {
            self.checkUncheckBtn.image = UIImage(named: "unCheckBox")
            isCheckBtnPressed = false
        }
        else {
            self.checkUncheckBtn.image = UIImage(named: "checkBox")
            isCheckBtnPressed = true
        }
    }
    
    @objc func ppCheckBtnPressed(_ sender: UITapGestureRecognizer? = nil) {
        if isPPCheckBtnPressed == true {
            self.ppCheckBtn.image = UIImage(named: "unCheckBox")
            isPPCheckBtnPressed = false
        }
        else {
            self.ppCheckBtn.image = UIImage(named: "checkBox")
            isPPCheckBtnPressed = true
        }
    }
    
    
    @objc func handleAppleIdRequest() {
        
        if isCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslTC, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isPPCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslPP, duration: 1.0, position: .bottom, style: style)
            return
        }
        
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = self
    authorizationController.performRequests()
    }
    
    func setUPUi() {
        loginBtn.layer.cornerRadius = 18
        emailField.clipsToBounds = true
        passwordField.clipsToBounds = true
        emailview.clipsToBounds = true
        passwordView.clipsToBounds = true
        mainStackView.layer.cornerRadius = 7
        mainStackView.clipsToBounds = true
        loginBtn.clipsToBounds = true
        emailField.placeholder = constantsStrings.emailPlaceHolder
        passwordField.placeholder = constantsStrings.passwordPlaceholder
        emailField.attributedPlaceholder = NSAttributedString(
            string: constantsStrings.emailPlaceHolder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "toastBackColor")]
        )
        passwordField.attributedPlaceholder = NSAttributedString(
            string: constantsStrings.passwordPlaceholder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "toastBackColor")]
        )
    }
    
    func createTapAbleButton () {
        let tapTC = UITapGestureRecognizer(target: self, action: #selector(self.tapTCPressed(_:)))
        TCBtn.addGestureRecognizer(tapTC)
        
        let ppTap = UITapGestureRecognizer(target: self, action: #selector(self.ppViewPressed(_:)))
        ppView.addGestureRecognizer(ppTap)
        
        let checkUncheck = UITapGestureRecognizer(target: self, action: #selector(self.checkUncheckPressed(_:)))
        checkUncheckBtn.addGestureRecognizer(checkUncheck)
        let ppChecktap = UITapGestureRecognizer(target: self, action: #selector(self.ppCheckBtnPressed(_:)))
        ppCheckBtn.addGestureRecognizer(ppChecktap)
    }
    
    func generateNotifications () {
        if fromSendOrRedeem == "send"{
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPaymentMehods"), object: nil)
        }
        else if fromSendOrRedeem == "redeem" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRedeemView"), object: nil)
        }
        else if  fromSendOrRedeem == "avatar" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyAvatarLoginCompleted"), object: nil)
            }
        }
        else if  fromSendOrRedeem == "joinNow" {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinNowLoginCompleted"), object: nil)
            }
            }
        }
        
        else if  fromSendOrRedeem == "RedeemLater" {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedeemLaterLogin"), object: nil)
            }
            }
        }
        
        else if  fromSendOrRedeem == "MyParsls" {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyParslLogin"), object: nil)
            }
            }
        }
        else if  fromSendOrRedeem == "createModel" {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CreateModelLogin"), object: nil)
            }
            }
        }
    }
    // Setup UI of apple sign in button
    func setUpSignInAppleButton () {
        
        if #available(iOS 13.0, *) {
            
            let authorizationButton: ASAuthorizationAppleIDButton
            if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
                if traitCollection.userInterfaceStyle == .dark {
                    authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: .white)
                } else {
                    authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: .black)
                }
            } else {
                authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: .black)
            }
            authorizationButton.isUserInteractionEnabled = true
            authorizationButton.addTarget(self, action:
                                            #selector(handleAppleIdRequest),
                                          for: .touchUpInside)
            self.bottomView.addSubview(authorizationButton)
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false
            authorizationButton.topAnchor.constraint(equalTo: self.bottomView.topAnchor).isActive = true
            authorizationButton.trailingAnchor.constraint(equalTo: self.bottomView.trailingAnchor, constant: -30).isActive = true
            authorizationButton.leadingAnchor.constraint(equalTo: self.bottomView.leadingAnchor, constant: 30).isActive = true
            authorizationButton.topAnchor.constraint(equalTo: self.bottomView.topAnchor).isActive = true
            authorizationButton.bottomAnchor.constraint(equalTo: self.bottomView.bottomAnchor).isActive = true
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    // setup UI of skip button
    func setUpSkipbtn() {
    skipButton.frame = CGRect(x: view.frame.size.width/1.3, y: 20, width: 80, height: 25)
        if self.fromSendOrRedeem == "avatar" {
//            skipButton.isUserInteractionEnabled = false
            skipButton.setTitle(constantsStrings.cancelLabel, for: .normal)
//            skipButton.backgroundColor = UIColor.white
        }
        else {
            skipButton.setTitle(constantsStrings.cancelLabel, for: .normal)
        }
    skipButton.backgroundColor = .clear
    skipButton.layer.cornerRadius = 5
    skipButton.clipsToBounds = true
    skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
    skipButton.setTitleColor(UIColor.link, for: .normal)
    skipButton.addTarget(self, action: #selector(skipBtnActions(_:)), for: .touchUpInside)
    view.addSubview(skipButton)
    }
    
    @objc func skipBtnActions(_ sender:UIButton!) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "forgotVC") as! forgotVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loginBtnPressend(_ sender: Any) {
        if emailField.text!.isEmpty || emailField.text == "" || emailField.text == " " {
            self.view.makeToast(constantsStrings.emptyUserName, duration: 1.0, position: .bottom, style: style)
            return
        }
        if passwordField.text!.isEmpty || passwordField.text == "" || passwordField.text == " " {
            self.view.makeToast(constantsStrings.emptyPassword, duration: 1.0, position: .bottom, style: style)
            return
        }
        if !isValidEmail(emailField.text!) {
            self.view.makeToast(constantsStrings.invalidUserEmail, duration: 1.0, position: .bottom, style: style)
            return
        }
        if !isPasswordEmail(passwordField.text!) {
            self.view.makeToast(constantsStrings.enterValidPassword, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslTC, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isPPCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslPP, duration: 1.0, position: .bottom, style: style)
            return
        }
        // call method of login if above conditions match.
        loginUser(socialLogin: "",email: emailField.text!)
    }
    
    func loginUser(socialLogin: String, email: String) {
       
        ProgressHUD.show()
        var data = [String: Any]()
        var params = [String: AnyObject]()
        if socialLogin != "" {
            // if user is login using apple login
            data = ["email": email, "password": "", "old_mail": ParslUtils.sharedInstance.getuserOldEmail(),"token": socialLogin]
            params = ["app_id": "parsl_ios", "sociallogin": "apple", "data": data] as [String: AnyObject]
        }
        else {
            // if user is login using email password fields
            var password = passwordField.text!
            data = ["email": email ,"password": password, "old_mail": ParslUtils.sharedInstance.getuserOldEmail(), "token": ""]
            params = ["app_id": "parsl_ios", "sociallogin": "", "data": data] as [String: AnyObject]
        }
        print(params)
        
        let url = constantsUrl.LOG_IN_URL
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                print(json)
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                
                if status == true{
                    DispatchQueue.main.async{
                    ProgressHUD.dismiss()
                        let data = json["data"]!
                        let userNameSpace = data["namespace"] as! String
                        let email = data["email"] as! String
                        self.view.makeToast(constantsStrings.loginSuccess)
                        ParslUtils.sharedInstance.saveIfUserLogin(true)
                        ParslUtils.sharedInstance.saveUserNameSpace(userNameSpace)
                            ParslUtils.sharedInstance.saveUserEmail(email)
                         
                        self.dismiss(animated: true, completion: nil)
                        if self.fromSendOrRedeem == "cart" {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cartLoginSuccess"), object: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.view.makeToast(message)
                    }
                    
                }
                
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
}


