


import UIKit
import Photos
import Firebase
import MessageKit
import FirebaseFirestore
import InputBarAccessoryView

final class ChatViewController: MessagesViewController {
  
  private var isSendingPhoto = false {
    didSet {
      DispatchQueue.main.async {
        self.messageInputBar.leftStackViewItems.forEach { item in
        }
      }
    }
  }
  

  private var messages: [chatMessage] = []
  private var messageListener: ListenerRegistration?
  
  private let channel: Channel
    var data  = [String:Any]()
    var chatData  = [String:Any]()
    weak var timer: Timer?
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    

  
  deinit {
    timer?.invalidate()
    messageListener?.remove()
     
  }

    init(channel: Channel) {
    self.channel = channel
    super.init(nibName: nil, bundle: nil)
        title = constantsStrings.liveSupportScreenName
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .darkContent
    }
  override func viewDidLoad() {
    super.viewDidLoad()
      
      if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
                  layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
                  layout.textMessageSizeCalculator.incomingAvatarSize = .zero
              }
      
      
      
//      self.hideKeyboardWhenTappedAround()
      
     
      let logoutBarButtonItem = UIBarButtonItem(title: constantsStrings.endChatLabel, style: .done, target: self, action: #selector(endChat))
    self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
    getUserChat()
    timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(getUserChat), userInfo: nil, repeats: true)
    guard let id = channel.id else {
      navigationController?.popViewController(animated: true)

      return
    }


    
      navigationItem.largeTitleDisplayMode = .always
      
    maintainPositionOnKeyboardFrameChanged = true
    messageInputBar.inputTextView.tintColor = .primary
    messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
    
    messageInputBar.delegate = self
    messagesCollectionView.messagesDataSource = self
    messagesCollectionView.messagesLayoutDelegate = self
    messagesCollectionView.messagesDisplayDelegate = self
    
    let cameraItem = InputBarButtonItem(type: .system) // 1
    cameraItem.tintColor = .primary
    cameraItem.image = UIImage(named: "portfolio")
    cameraItem.addTarget(
      self,
      action: #selector(cameraButtonPressed), // 2
      for: .primaryActionTriggered
    )
    cameraItem.setSize(CGSize(width: 60, height: 30), animated: false)
    
    messageInputBar.leftStackView.alignment = .center
    messageInputBar.setLeftStackViewWidthConstant(to: 10, animated: false)
    messageInputBar.cornerRadius = 5
    messageInputBar.borderWidth = 2
    messageInputBar.borderColor = .gray
   // messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
  }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
      self.navigationItem.setHidesBackButton(false, animated: true)
//        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        
        
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
 
    }
    
    //MARK:- Methods
    
    private func pushUserMsgs(meessage: chatMessage){

//        let ref = self.channelReference.document()
//        let senderid = ref.documentID
        var params: [String: AnyObject]

            params = ["sender_id": deviceId! as AnyObject, "sender_name": "Parsl User" as AnyObject, "firebase_id": channel.id as AnyObject, "sender_msg": meessage.content as AnyObject, "send_by": "client" as AnyObject ]
        
        let param: [String: AnyObject] = ["data": params as AnyObject]
        print(param)
        postChatRequest(serviceName: constantsUrl.PUSH_USER_MESSAGE_URL, sendData: param as [String : AnyObject], success: { (response) in
            print("response is \(response)")
            DispatchQueue.main.async {
                let jsonData = (response as NSDictionary) as! [String : Any]
                print(jsonData)
                 
            }
        }, failure: { (data) in
            print(data)

        })

    }
    
    
    func parseData(data: [String:Any])
    {
        var senderID:String = ""
        var content :String = ""
        var senderName : String = ""
        var created :Date!
        let clientArray = data["client"] as! [[String:Any]]
        let agentArray = data["agent"] as! [[String:Any]]
        
        for item in clientArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = chatMessage(content: content,sentDate: created,senderId: senderID,senderName: senderName)
//       //     insertNewMessage(message)
            
            messages.append(message)
        }
    
        for item in agentArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = chatMessage(content: content,sentDate: created,senderId: senderID,senderName: senderName)
          // insertNewMessage(message)
            messages.append(message)
        }
        
       
        messages.sort()
        let shouldScrollToBottom = messagesCollectionView.isAtBottom
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
          DispatchQueue.main.async {
            self.messagesCollectionView.scrollToBottom(animated: true)
          }
        }
       

    }
  
  // MARK: - Actions
  
    @objc func getUserChat()
    {
        let dict : [String: AnyObject] = ["firebase_id":channel.id as AnyObject]

        //let data :Dictionary<String,Any> = ["data": dict as AnyObject]
        
        let data :[String: AnyObject] = ["data": dict as AnyObject]
        
        print(data)
        print(self.data)
        print(self.chatData)
        
        postChatRequest(serviceName: constantsUrl.GET_URSER_MESSAGE_URL, sendData: data as [String : AnyObject], success: { (response) in
//
            DispatchQueue.main.async {
                self.messages.removeAll()
                let status = response["status"] as! Bool
                if status{
                    self.data = response["data"] as! [String : Any]
                    self.chatData = self.data["chat"] as! [String : Any]
                    print(self.chatData)
                    self.parseData(data: self.chatData)
                }
                else{
                    print("something is error")
                }
            }

        }, failure: { (data) in
            print(data)

        })

    }
    
  @objc private func cameraButtonPressed() {
    let picker = UIImagePickerController()
//    picker.delegate = self
    
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      picker.sourceType = .camera
    } else {
      picker.sourceType = .photoLibrary
    }
    
    present(picker, animated: true, completion: nil)
  }
  
  // MARK: - Helpers
  
    @objc private func endChat() {
        let ac = UIAlertController(title: nil, message: constantsStrings.sureToDeleteChat, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: constantsStrings.cancelLabel, style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: constantsStrings.endChatLabel, style: .destructive, handler: { _ in
             self.deleteChat()
        }))
        present(ac, animated: true, completion: nil)
    }
    private func deleteChat() {
        timer?.invalidate()
//        //first server will save the chat then we will delete from firebase
        let data :Dictionary<String,Any> = ["firebase_id": channel.id! as String]
        print(data)
        postChatRequest(serviceName: constantsUrl.SAVE_CHAT_URL, sendData: data as [String: AnyObject], success: {(response) in
            print(response)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")

            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    // self.showAlert("Success")
                    let dictionary = response as [String:Any]
                  //  SPManager.sharedInstance.saveFirebaseid("")
                    self.navigationController?.popViewController(animated: true)
                     
                }

            }

        }, failure: { (data) in
            print(data)

        })
    }

    func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
    
        let serviceUrl =  serviceName
        print("url == \(serviceUrl)")
        let url = NSURL(string: serviceUrl)
    
        //create the session object
        let session = URLSession.shared
    
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
    //        request.timeoutInterval = 20
    
        do {
    
            let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
            let dataString = String(data: data, encoding: .utf8)!
            print("data string is",dataString)
    
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
    
        } catch let error {
            print(error.localizedDescription)
        }
    
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    
        //create dataTask using the session object to send data to the server
    
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
    
            print(" response is \(response)")
    
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
    
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
    
    
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
    
            guard error == nil else {
                print("this is error")
                return
            }
            guard let data = data else {
                return
            }
    
            do {
                //create json object from data
    
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    success(json)
    
                    //handle json...
    
                }
    
    
            } catch let error {
    
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
    
        })
    
        task.resume()
    
    }
    
  }

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
  
  func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? .primary : .incomingMessage
  }
  
  func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
    return false
  }
  
  func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
  }
  
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
  
  func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return .zero
  }
  
  func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
  }
  
  func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    
    return 0
  }
  
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
    func currentSender() -> SenderType {

            let userid = deviceId!
            let currentSender = senderr(senderId: userid, displayName: "Parsl User")
            return currentSender
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
  
  func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
    return messages.count
  }
  
  func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
    return messages[indexPath.section]
  }
  
 
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {

            let name = message.sender.displayName

            if isFromCurrentSender(message: message){

            let paragraphStyle = NSMutableParagraphStyle()

            paragraphStyle.alignment = .right

            return NSAttributedString(string: name, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])

            }

            else{

                let supportName = "Support"

                let paragraphStyle = NSMutableParagraphStyle()

                paragraphStyle.alignment = .left

                return NSAttributedString(string: supportName, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])


     

            }

        }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {

            return 30

        }
  
}

// MARK: - MessageInputBarDelegate
extension ChatViewController: InputBarAccessoryViewDelegate {

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {

        let text = text.trimmingCharacters(in: .newlines)

        print(text)

        let message = chatMessage(content: text)

        pushUserMsgs(meessage: message)

//        insertNewMessage(message)

        inputBar.inputTextView.text = ""

    }

}



// MARK: - UIImagePickerControllerDelegate

//extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//
//  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//    picker.dismiss(animated: true, completion: nil)
//
//    if let asset = info[.phAsset] as? PHAsset { // 1
//      let size = CGSize(width: 500, height: 500)
//      PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: nil) { result, info in
//        guard let image = result else {
//          return
//        }
//
//        self.sendPhoto(image)
//      }
//    } else if let image = info[.originalImage] as? UIImage { // 2
//      sendPhoto(image)
//    }
//  }
//
//  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//    picker.dismiss(animated: true, completion: nil)
//  }
//
//}

//extension ChatViewController {
//    func hideKeyboardWhenTappedAround() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//
//    @objc override func dismissKeyboard() {
//        view.endEditing(true)
//    }
//}
