//
//  RedeemTableViewCell.swift
//  Parsl
//
//  Created by Mufaza on 02/08/2021.
//

import UIKit
 

class RedeemTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnail:       UIImageView!
    @IBOutlet weak var title:           UILabel!
    @IBOutlet weak var downloadedImage:   UIButton!
    
    @IBOutlet weak var redeemgiftAmountLabel: UILabel!
    @IBOutlet weak var addToWalletBtn: UIButton!
    @IBOutlet weak var messageTextView:UITextView!
    var cellIndex: Int?
    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}



class RedeemTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var title:           UILabel!

    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
    }
     
}


class RedeemVideoTableViewCell: UITableViewCell {
  
    @IBOutlet weak var videoButton:   UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
  
}


