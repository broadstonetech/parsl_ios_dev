//
//  ViewController.swift
//  Parsl
//
//  Created by Billal on 19/02/2021.
// This class enable the user to select one more multiple PARSLs for sending purpose. In this class user can also redeem PARSls by using PARSL otp. User can project different models and experience real time.
import Foundation
import UIKit
import SDWebImage
import AVFoundation
import AVKit
import MediaPlayer
import AudioToolbox
import ARKit
import SSZipArchive
import Stripe
import Alamofire
import Toast_Swift
import JJFloatingActionButton
import StoreKit
import FirebaseAuth
import FirebaseFirestore
import UnityFramework
import Lottie
import ZIPFoundation
import SwiftUI

var selectedCategoryDataList = [SubCategoryData]()
var giftModels: GiftModel!
var isOrderFlowCompleted = false

class MainViewController: BaseViewController, UITextFieldDelegate,AVPlayerViewControllerDelegate,SendDataDelegate,NativeCallsProtocol {
    
    // MARK: - Properties
    var modelPathURL:URL? 
    var parentNode: String = ""
    var redeemgiftAmount = ""
    var prices:Double?
    var productIDs:String?
    var completeList : SubCategoryData?
    var isMessageShowing = false
    var isMenuShowing = false
    var downloadCount = 0
    var heightCount = 0
    var videoRepeatCount = 0
    var messageRepeatCount = 0
    var messageAR:String        = ""
    var quantity                = [Int]()
    var mainCategoryList:         MainCategory!
    var subCategoryList:          SubCategoryModel!
    var pricesList              = [Double]()
    var subCategoryArray        = [SubCategoryData]()
    var selectedItemsDataList   = [SubCategoryData]()
    var getParsl:                 GetParsl!
    var getParslList            = [ParslModels]()
    var liveAvatarData =          [LiveAvatarData]()
    var getModelData:             ParslGiftsModel!
    var pulseArray =           [CAShapeLayer]()
    var cartItemsArray          = [String]()
    var selectedIndex           = 0
    var cartCount :Int          = 0
    var searching               = false
    var isRedeem                = false
    var videoUrl:String         = ""
    var isFashionSelected = false
    var isfromMain = true
    var isInternetAvailable = true
    // var used for model projections
    var playVideo: SCNPlane?
    var videoNode: SCNNode?
    var modelUrl = ""
    var format:                 String?
    var downloadedModelURL:   URL?
    var isMultiNode:          Int?
    var textueresDownloadedCount = 0
    var destPathforTextures: String?
    var isAlltextureDownloaded: Bool = false
    var pricesListofCart = [Double]()
    var URLOTP:String = ""
    var isFirstTime:Bool = true
    var nameofSender = " "
    var isRedeemHalfViewShow = false
    var isAvatarDownloaded = false
    var isAvatarProjected = false
    var isShadowOff = true
    var isHorizontal = true
    var isActivateMethodCalled = false
    var giftShowed = false
    var isMyAvatarSelected = false
    
    var modelID:String?
    var modelIDWithoutHas = ""
    var selectedModelForDownload  = [String]()
    //add data to cart
    let configuration = ARWorldTrackingConfiguration()
    var dataList = [SubCategoryData]()
    var fileURL,modelTitle: String?
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    let actionButton = JJFloatingActionButton()
    
    var isVideoCompleted = false
    var isModelDownloaded = false
    var isModelProjected = false
    var isUnitysetup = false
    
    var vendersList: GetVendorsList!
    var vendors = [VendorsList]()
    
    var vendorSelectedIndex = 0
    var selectedModelIndex = 0
    var modelDataSturctArray = [ModelDataStruct]()
    var parslRedeemModel: RedeemModelStruct!
    
    var modelsInCart = [ModelDataStruct]()
    var modelsInView = [String : ModelDataStruct]()
    var downloadedModelsForRedeem = [ModelsDownloadedForRedeemStruct]()
    
    var searchResult = [ModelDataStruct]()
    var searchForLiveAvatar = [LiveAvatarData]()
    var isSearching = false
    var selectedModelIndexForRedeem = -1
    var selectedFilter = ""
    var data  = [String:Any]()
    var downloadedData = [DownloadedDataForCatAndVendors]()
    var downloadedVendors = [DownloadedDataForVendors]()
    var modelsDownloadedForSend = [ModelsDownloadedForSendStruct]()
    var isModelAvailableInSapce = false
    var isGiftModelDownloaded = false
    var isArVideoPlayed = false
    var isRedeemMessage = false
    var isVideoMessage = false
    var redeemMessage = ""
    var isSendPressed = false
    var isSearchBarShow = false
    var isfrom = ""
    var forAvatar = ""
    var animatingCardView = false
    var tappedItem =  ModelDataStruct()
    var virtualCardModel: VirtualCard!
    var isSubcategoriesAPICalled = DownloadStatus.Downloading
    var userEmail :String? = ""
    var channelListener: ListenerRegistration?
    var documentID : String? = nil
    private let db = Firestore.firestore()
    var playerLayer: AVPlayerLayer?
    var unityView: UIView?
    var player: AVPlayer?
    var documentQueryObject: QueryDocumentSnapshot?
    private var messageReference: CollectionReference?
    let skipButton = UIButton(type: .system)
    let reloadButton = UIButton(type: .system)
    lazy var COLORED_CART_IMAGE = UIImage(named: "logo_without_text")
    lazy var EMPTY_CART_IMAGE = UIImage(named: "cart")
    
     //MARK: - IBOutlets
      
    
    @IBOutlet weak var bottomViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var rotateCameraOutlet: UIButton!
    @IBOutlet weak var ssBtnOutlet: UIButton!
    @IBOutlet weak var modelStatusLabel: UILabel!
    @IBOutlet weak var subcategoriesHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var floatButtonView: UIView!
    @IBOutlet weak var redeemViewWalletBtn: UIButton!
    @IBOutlet weak var shadowBtn: UIButton!
    @IBOutlet weak var doneMainPView: UIButton!
    @IBOutlet weak var reloadBtn: UIButton!
    @IBOutlet weak var showMessageButton        : UIButton!
    @IBOutlet weak var leftButtonsView          : UIView!
    @IBOutlet weak var rightButtonsView         : UIView!
    @IBOutlet weak var senderTitle: UILabel!
    @IBOutlet weak var redeemVideoView: UIView!
    @IBOutlet weak var mainPresentedView: UIView!
//    @IBOutlet weak var addToButton: RoundButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var presentedModelImage: UIImageView!
    @IBOutlet weak var presentedTextView: UITextView!
    
    //  For Basic View
    @IBOutlet weak var mainBottomView: UIView!
    @IBOutlet weak var MainCategoryStack:     UIStackView!
    @IBOutlet weak var cartMainView:         UIView!
    @IBOutlet weak var cartImageView: UIImageView!
    @IBOutlet weak var cartCountLabel:       UILabel!
    @IBOutlet weak var RedeemHalfView: UIView!
    
    //collection viewss
    @IBOutlet weak var mainCategoryCollectionView:      UICollectionView!
    @IBOutlet weak var subCategoryCollectionView:       UICollectionView!
    @IBOutlet weak var vendorsCollectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainCategoryView:        UIView!
    @IBOutlet weak var searchBar:               UISearchBar!
    @IBOutlet weak var subCategoryView:         UIView!
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var redeemAnimatingView: UIView!
    @IBOutlet weak var videoView: VideoView!
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var modelsBgView: UIView!
    
    @IBOutlet weak var vendorDetailDialog: VendorsDetailsDialog!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var addToWalletBtn: UIButton!
    
    @IBOutlet weak var showRedeemHalfViewBtn: UIButton!
    @IBOutlet weak var redeemViewDone: UIButton!
//    @IBOutlet weak var HVBtn: UIButton!
    @IBOutlet weak var BackgroundView: AnimationView!
    @IBOutlet weak var firstCardTextView: UITextView!
    @IBOutlet weak var secondCardTextView: UITextView!
    @IBOutlet weak var firstCardView: UIView!
    @IBOutlet weak var secondCardView: UIView!
    @IBOutlet weak var RedeemAnimatingBackgroundView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var animatingHeight: NSLayoutConstraint!
    @IBOutlet weak var animatingWidth: NSLayoutConstraint!
     
    //MARK: - View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
        modelsBgView.backgroundColor = UIColor(named: "MainControllerDarkColor")!
        if ParslUtils.sharedInstance.parslOtp != "" {
            let otp = ParslUtils.sharedInstance.parslOtp ?? ""
            print(otp)
            DispatchQueue.main.async {
                self.reloadUnity()
              ProgressHUD.show()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.isModelProjected = false
                self.sendData("", "", otp)
                self.redeemAnimatingView.isHidden = true
                self.modelStatusLabel.isHidden = true
                ParslUtils.sharedInstance.parslOtp = ""
            }
        }
        
        handleNotificationNavigation()
         
        if isfrom == "MyAvatar" {
            navigationController?.isNavigationBarHidden = false
            self.navigationItem.setHidesBackButton(false, animated: true)
            self.navigationController?.navigationBar.barTintColor = UIColor.green
        }
        else {
        navigationController?.isNavigationBarHidden = true
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationController?.navigationBar.barTintColor = UIColor.green
        hideViews()
        self.videoView.isHidden = true
            self.modelStatusLabel.isHidden = true
        if isVideoCompleted == false {
            skipButton.frame = CGRect(x: view.frame.size.width/1.2, y: 60, width: 50, height: 25)
            skipButton.backgroundColor = UIColor(named: "ColorForSkipBtn")
            skipButton.layer.cornerRadius = 5
            skipButton.clipsToBounds = true
            skipButton.setTitle(constantsStrings.skipLabel, for: .normal)
            skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
            
            skipButton.setTitleColor(UIColor.white, for: .normal)
            skipButton.addTarget(self, action: #selector(SkipVideo(_:)), for: .touchUpInside)
            if isfrom != "MyAvatars"{
            view.addSubview(skipButton)
            }
        }
        if self.modelsInCart.count != 0 {
            cartImageView.image = COLORED_CART_IMAGE
        }
        else {
            cartCountLabel.text = " "
            cartImageView.image = EMPTY_CART_IMAGE
        }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isVideoCompleted {
           // sceneView.session.pause()
        }
        NotificationCenter.default.removeObserver(self)
        if !isRedeem{
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardListeners()
        FrameworkLibAPI.registerAPIforNativeCalls(self)
        if isfrom == "MyAvatars" {
            self.setUpUnityView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                let id = self.forAvatar + "," + "true"
                UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Activate", message: id)
            }
        } else {
            if ParslUtils.sharedInstance.getDeviceId() != deviceId {
                getGuestDeviceDetail()
            } else {
                debugPrint(deviceId)
            }
            self.redeemViewWalletBtn.layer.cornerRadius = 10
            self.redeemViewWalletBtn.clipsToBounds = true
            self.cartMainView.layer.cornerRadius = 5
            self.cartMainView.clipsToBounds = true
            self.redeemAnimatingView.circularView()
            self.redeemAnimatingView.clipsToBounds = true
            searchBar.delegate = self
            searchBar.isHidden = true
            self.doneMainPView.clipsToBounds = true
            self.doneMainPView.setTitleColor(UIColor.white, for: .normal)
            self.shadowBtn.setTitle(" ", for: .normal)
            self.rotateCameraOutlet.setTitle("", for: .normal)
            self.shadowBtn.setImage(UIImage(named: "shadow off"), for: .normal)
            self.shadowBtn.setTitleColor(UIColor.white, for: .normal)
            getAppVersionApi()
            self.senderTitle.text =  "Greetings from \(nameofSender)"
            
            if traitCollection.userInterfaceStyle == .dark {
                debugPrint("dark mode enabled")
            } else {
                debugPrint("light mode enabled")
            }
        
            self.reloadBtn.setTitle("", for: .normal)
            self.ssBtnOutlet.setTitle("", for: .normal)
            self.tableView.tableFooterView = UIView()
             
            self.navigationItem.title = constantsStrings.appName
            self.navigationItem.hidesBackButton = true
            self.navigationController?.navigationBar.barTintColor = UIColor.systemGray6
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
             
            mainCategoryCollectionView.isUserInteractionEnabled = true
            mainCategoryCollectionView.allowsMultipleSelection = true
            if #available(iOS 14.0, *) {
                mainCategoryCollectionView.allowsMultipleSelectionDuringEditing = true
            } else {
                // Fallback on earlier versions
            }
        
        mainCategoryCollectionView.delegate     = self
        mainCategoryCollectionView.dataSource   = self
        
        vendorsCollectionView.delegate = self
        vendorsCollectionView.dataSource = self
        vendorsCollectionView.contentInsetAdjustmentBehavior = .never
        
        subCategoryCollectionView.dataSource = self
        subCategoryCollectionView.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        tableView.rowHeight = UITableView.automaticDimension
         
        hideKeyboardTappedAround()
        
        DispatchQueue.main.async {
            self.getGiftModels()
        }
    
        NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.didEnterBackgroundNotification, object: nil)
//
//          // foreground event
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.willEnterForegroundNotification, object: nil)
//
//         // add these 2 notifications to prevent freeze on long Home button press and back
          NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.willResignActiveNotification, object: nil)
//
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.didBecomeActiveNotification, object: nil)
              
        prepareVideoViewAndLoadData()
         
        let blurViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(vendorDialogCloseBtnPressed))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(blurViewTapGesture)
             
        vendorDetailDialog.cancelBtn.addTarget(self, action: #selector(vendorDialogCloseBtnPressed), for: .touchUpInside)
        
        vendorDetailDialog.visitStore.addTarget(self, action: #selector(visitStoreBtnPressed), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showRedeemView(_:)), name: Notification.Name(rawValue: "ShowRedeemView"), object: nil)
        }
    }
     // MARK: - Methods
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
            
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    func scaleInOutAnimation() {
        
        UIView.animate(withDuration: 0.6,
        animations: {
            self.redeemAnimatingView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.6) {
                self.redeemAnimatingView.transform = CGAffineTransform.identity
            }
        })
          
    }
    
    func createPulse() {
            
            for _ in 0...2 {
                let circularPath = UIBezierPath(arcCenter: .zero, radius: ((self.redeemAnimatingView.superview?.frame.size.width )! )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
                let pulsatingLayer = CAShapeLayer()
                pulsatingLayer.path = circularPath.cgPath
                pulsatingLayer.lineWidth = 2.5
                pulsatingLayer.fillColor = UIColor.clear.cgColor
                pulsatingLayer.lineCap = CAShapeLayerLineCap.round
                pulsatingLayer.position = CGPoint(x: redeemAnimatingView.frame.size.width / 2.0, y: redeemAnimatingView.frame.size.width / 2.0)
                redeemAnimatingView.layer.addSublayer(pulsatingLayer)
                pulseArray.append(pulsatingLayer)
            }
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.animatePulsatingLayerAt(index: 0)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                    self.animatePulsatingLayerAt(index: 1)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self.animatePulsatingLayerAt(index: 2)
                    })
                })
            })
            
        }
    
    
    func animatePulsatingLayerAt(index:Int) {
           
           //Giving color to the layer
           pulseArray[index].strokeColor = UIColor.white.cgColor
           
           //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
           // 0.0 = minimum
           //1.0 = maximum
           let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
           scaleAnimation.fromValue = 0.0
           scaleAnimation.toValue = 0.9
           
           //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
           // 0.0 = minimum
           //1.0 = maximum
           let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
           opacityAnimation.fromValue = 0.9
           opacityAnimation.toValue = 0.0
          
         // Grouping both animations and giving animation duration, animation repat count
           let groupAnimation = CAAnimationGroup()
           groupAnimation.animations = [scaleAnimation, opacityAnimation]
           groupAnimation.duration = 2.3
           groupAnimation.repeatCount = .greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
           //adding groupanimation to the layer
           pulseArray[index].add(groupAnimation, forKey: "groupanimation")
           
       }
    
    
    func setUIOfReloadButton() {
        reloadButton.frame = CGRect(x: view.frame.size.width/1.15, y: 60, width: 40, height: 40)
        reloadButton.backgroundColor = UIColor(named: "ColorForSkipBtn")
        reloadButton.layer.cornerRadius = 5
        reloadButton.clipsToBounds = true
        reloadButton.setTitle("", for: .normal)
        reloadButton.setImage(UIImage(named: "reloadIcon"), for: .normal)
        reloadButton.tintColor = UIColor.white
        reloadButton.setTitleColor(UIColor.white, for: .normal)
        reloadButton.addTarget(self, action: #selector(clearUnityScreen(_:)), for: .touchUpInside)
        view.addSubview(reloadButton)
    }
    
    func handleNotificationNavigation () {
        if notificationNav.goToMyParslScreen == true {
            let storyboard = UIStoryboard(name: "AccountRelatedStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyParslsViewController") as! MyParslsViewController
            navigationController?.pushViewController(vc, animated: true)
        } else if notificationNav.goToChatScreen == true {
            notificationNav.goToChatScreen = false
            let id = UserDefaults.standard.value(forKey: "channelFirebaseID") as! String? ?? ""
            if id != "" {
                let channel = Channel(id:id)
                let vc = ChatViewController(channel: channel)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                debugPrint("key doesnot exist in userinfo")
            }
        }else if notificationNav.goToNotificationScreen == true{
            notificationNav.goToNotificationScreen = false
            let storyboard = UIStoryboard(name: "AccountRelatedStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsSummryViewController") as! NotificationsSummryViewController
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func SkipVideo(_ sender:UIButton!) {
           DispatchQueue.main.async {
               self.playerLayer?.player = nil
               self.playerLayer?.removeFromSuperlayer()
               self.skipButton.removeFromSuperview()
           }
        if !isUnitysetup{
            isUnitysetup = true
            self.setUpUnityView()
            isVideoCompleted = true
        }
       }
    
    @objc func clearUnityScreen(_ sender:UIButton!) {
        self.reloadUnity() 
        self.searchBar.isHidden = true
        self.showredeemScreen()
        self.isSendPressed = false
        self.giftShowed = false
        self.modelStatusLabel.isHidden = true
        self.reloadButton.removeFromSuperview()
    }

    
    func hideViews() {
        messageView.isHidden            = (messageView.isHidden == true)
        MainCategoryStack.isHidden      = (MainCategoryStack.isHidden == true)
        cartMainView.isHidden           = (cartMainView.isHidden == true)
        showMessageButton.isHidden      = true
        mainPresentedView.isHidden      = true
        leftButtonsView.isHidden        = true
        rightButtonsView.isHidden       = true
        subCategoryView.isHidden        = (subCategoryView.isHidden == true)
        modelsBgView.isHidden = true
        sortButton.isHidden = true
    }
    
    private func prepareVideoViewAndLoadData() {
        isVideoCompleted = false
        videoView.isHidden = false
        
        guard let videoPath = Bundle.main.path(forResource: "parsl_video", ofType:"mp4") else {
           debugPrint("error return")
            return
        }
        let url = NSURL(fileURLWithPath: videoPath) as URL
        playVideo(url: url.absoluteString)

    }
    
    func playVideo(url: String){
        if let videoURL = URL(string: url) {
            player = AVPlayer(url: videoURL)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer!.frame = self.view.bounds
            playerLayer!.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(playerLayer!)
            
            player!.play()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
        }
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        DispatchQueue.main.async {
            self.playerLayer?.player = nil
            self.playerLayer?.removeFromSuperlayer()
            self.skipButton.removeFromSuperview()
//            self.playerLayer?.isHidden = true
        }
        isVideoCompleted = true
        videoView.isHidden = true
        if !isUnitysetup{
            isUnitysetup = true
            self.setUpUnityView()

        }
    }
    @objc fileprivate func setPlayerLayerToNil() {
        player?.pause()

    }
//     // foreground event
    @objc fileprivate func reinitializePlayerLayer() {
        player?.play()
    }
    func mayBelater() {
        if ParslUtils.sharedInstance.getIfuserLogin() {
                    self.cartMainView.isHidden = false
                    self.MainCategoryStack.isHidden = false
                    self.subCategoryView.isHidden  = false
                    self.subCategoryCollectionView.isHidden = false
                    self.modelsBgView.isHidden = false
                    self.vendorsCollectionView.isHidden = false
                    self.sortButton.isHidden = false
                    self.isSendPressed = false
        } else {
            self.isSendPressed = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String) {
        print(URLOTP)
        self.isSendPressed = false
        DispatchQueue.main.async {
            self.cartMainView.isHidden = true
            self.searchBar.isHidden = true
            self.subcategoriesHeight.constant = 90
            self.actionButton.removeFromSuperview()
            self.view.addSubview(self.actionButton)
            self.actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            self.actionButton.bottomAnchor.constraint(equalTo: self.MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.URLOTP = URLOTP
            
            self.redeemGift(otp: URLOTP, name: receiverName, phoneNumber: receiverNumber)
            self.selectedModelForDownload.removeAll()
    }
}
    
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String) {
       
        self.getVirtualGiftCard(otp: self.URLOTP, receiverName: receiverName, phoneNumber: receiverNumber)
    }
    
    func showActionButton(){
        self.actionButton.isHidden = false
    }
    
    @objc func showRedeemView (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showredeemScreen()
        }
    }
    
    @objc func vendorDialogCloseBtnPressed(){
        blurView.isHidden = true
        vendorDetailDialog.isHidden = true
    }
    
    @objc func visitStoreBtnPressed(){
        
        ParslUtils.sharedInstance.openUrl(url: vendersList.data.vendorsList[vendorSelectedIndex].otherInfo.website)
    }
    
    func addKeyboardListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    //MARK: -Button Actions
    
    @IBAction func rotateCamera(_ sender: Any) {
        UnityEmbeddedSwift.sendUnityMessage("FaceMaskGlasses", methodName: "UpdateboolToggle", message: "")
        UnityEmbeddedSwift.sendUnityMessage("FaceMaskGlasses", methodName: "RemoveeboolToggle", message: "")
        
        UnityEmbeddedSwift.sendUnityMessage("CameraSwapper", methodName: "OnSwapCameraButtonPress", message: "")
    }
    
    
    @objc func imageWasSaved(_ image: UIImage, error: Error?, context: UnsafeMutableRawPointer) {
         if let error = error {
             print(error.localizedDescription)
             return
         }
     }
    
    func captureView(view: UIView) -> UIImage {
        var screenRect: CGRect = view.bounds
        UIGraphicsBeginImageContext(screenRect.size)
        UIGraphicsBeginImageContextWithOptions(screenRect.size, true, 0)
        var ctx: CGContext = UIGraphicsGetCurrentContext()!
        ctx.fill(screenRect)
        view.layer.render(in: ctx)
        var newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    @IBAction func takeScreenShot(_ sender: Any) {
        DispatchQueue.main.async {
            let image = UIView.image(view: self.unityView!, subview: self.unityView!)
            
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
            self.view.makeToast("Screenshot saved to photos")
        }
    }
    @IBAction func reloadUnityScreen(_ sender: Any) {
        let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.sureToClear, preferredStyle: .alert)
        let clearScreen = UIAlertAction(title: constantsStrings.clearLabel, style: .default, handler: { action in
            self.reloadUnity()
            
                })

        let cancel = UIAlertAction(title: constantsStrings.cancelLabel, style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(clearScreen)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    @IBAction func sortButtonTapped(_ sender: Any) {
        openBottomActionSheetForDataSorting()
    }
    
    @IBAction func onOffShadow(_ sender: Any) {
        turnModelShadowsOffOn()
    }
    @IBAction func playVideoTapped(_ sender: Any) {
        let url = URL(string: parslRedeemModel.data.receiverData.videoMsg)!
        playServerVideo(url: url)
    }
    
    
    @IBAction func hideMainPresentedView(_ sender: Any) {
//        mainPresentedView.isHidden = true
        if isItemAddedInCart(tappedItem){
            showAlertWithAction(withTitle: constantsStrings.appName, withMessage: constantsStrings.itemsAlreadyAdded)
        }else{
            modelsInCart.append(tappedItem)
            cartImageView.image = COLORED_CART_IMAGE
            cartCountLabel.text = String(modelsInCart.count)
            mainPresentedView.isHidden = true
        }
    }
    
    @IBAction func hideRedeemHalfView(_ sender: Any) {
        self.RedeemHalfView.isHidden = true
        self.redeemAnimatingView.isHidden = false
        self.createPulse()
        self.scaleInOutAnimation()
    }
     
    private func isItemAddedInCart(_ selectedItem: ModelDataStruct) -> Bool {
        for item in modelsInCart {
            if item.productId == selectedItem.productId {
                return true
            }
        }
        return false
    }
    
    @IBAction func showMessageTapped(_ sender: Any) {
        messageRepeatCount += 1
    }
    
    @IBAction func cartTapped(_ sender: Any) {
        if modelsInCart.isEmpty {
            showAlert(withTitle:constantsStrings.appName, withMessage: constantsStrings.enterItemsInCart)
        }
        else{
            
            let storyboard = UIStoryboard(name: "OrderSummaryStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderSummarylistVC") as! OrderSummarylistVC
            isOrderFlowCompleted = true
            vc.quantity = quantity
            vc.selectedItemsArray = cartItemsArray
            vc.selectedItemsDataList = selectedItemsDataList
            vc.pricesList = pricesList
            OrderSummarylistVC.modelsInCart = modelsInCart
            vc.cartDelegate = self
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func showSearch(_ sender: Any) {
        if searchBar.isHidden == true{
            searchBar.isHidden = false
        }
        else{
            searchBar.isHidden = true
            searchResult.removeAll()
            isSearching = false
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        }
    }
    
    @IBAction func mainSearchBtnPressed(_ sender: Any) {
        
    }
    
    //MARK:- Floating Buttons
    func setUpFloatingActionbutton() {
        actionButton.buttonDiameter = 50
        actionButton.buttonImageColor = UIColor(named: "BlackWhiteColor")!
        actionButton.buttonColor = UIColor(named: "CartViewBackgroundColor")!
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 140)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .bottom
            item.titleSpacing = .zero
            item.buttonColor = UIColor(named: "CartViewBackgroundColor")!
            item.buttonImageColor = UIColor(named: "BlackWhiteColor")!
            item.isSelected = true
            item.highlightedButtonColor = UIColor(named: "toastBackColor")

            
           // item.layer.si
            
        }
        
        actionButton.addItem(title:constantsStrings.MyaccountLabel,image: UIImage(named: "support")?.withRenderingMode(.alwaysTemplate)) { item in
            ParslUtils.sharedInstance.eventsCallForFRAnalytics(envetName: "Selected Model for send", detailsOfEvent: ["Type": "Send Parsl", "Message": "Models Id zzz , Model Name table ", "DeviceId": ParslUtils.sharedInstance.DEVICE_TOKEN as Any])
            let vc = UIStoryboard(name: "AccountRelatedStoryBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        actionButton.addItem(title: constantsStrings.walletLabel, image: UIImage(named: "card")?.withRenderingMode(.alwaysTemplate)) { item in
            self.searchBar.isHidden = true
            self.actionButton.isHighlighted = true
            self.homeFunctionality()
            self.isSendPressed = false
 
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
                let storyboard = UIStoryboard(name: "WalletStoryBoard", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                    self.present(vc, animated: true, completion: nil)
            }
        }
        actionButton.addItem(title: constantsStrings.redeemLabel, image: UIImage(named: "redeem")?.withRenderingMode(.alwaysTemplate)) { item in
            
            self.searchBar.isHidden = true
            self.showredeemScreen()
            self.isSendPressed = false
            self.giftShowed = false
            self.modelStatusLabel.isHidden = true
            self.reloadButton.removeFromSuperview()
            
        }
        
        actionButton.addItem(title: constantsStrings.sendlabel ,image: UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)) { item in
            if self.isSearchBarShow == true {
                self.searchBar.isHidden = false
            }
            if !self.isSendPressed {
                self.animatingCardView = false
                self.isSendPressed = true
                self.giftShowed = false
                self.modelStatusLabel.isHidden = true
                self.redeemAnimatingView.isHidden = true
                self.reloadUnity()
                self.hideViews()
                self.showMessageButton.isHidden = true
                self.cartMainView.isHidden = false
                self.MainCategoryStack.isHidden = false
                self.subCategoryView.isHidden  = false
                self.subCategoryCollectionView.isHidden = false
                if self.isInternetAvailable{
                self.modelsBgView.isHidden = false
                }
                self.vendorsCollectionView.isHidden = false
                self.sortButton.isHidden = false
                self.isRedeem = false
                self.reloadButton.removeFromSuperview()
            }
            else{
                debugPrint("unable to press send")
                }
            }
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -98).isActive = true
    }
    
    
    //MARK: -Functions for Projected Object
    
    func showredeemScreen(){
        self.searchBar.isHidden = true
        self.animatingCardView = true
        self.homeFunctionality()
        self.reloadUnity()
        self.actionButton.isHidden = true
        self.isModelProjected = false
        let vc  = UIStoryboard(name: "ARProjectionsStoryBoard", bundle: Bundle.main).instantiateViewController(identifier: "ReceiverModalViewController") as? ReceiverModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    func homeFunctionality(){
        
        self.subCategoryView.isHidden = true
        self.messageView.isHidden = true
        self.MainCategoryStack.isHidden = true
        self.isModelAvailableInSapce = false
        self.cartMainView.isHidden = true
        self.redeemAnimatingView.isHidden = true
        self.isRedeem = false
        self.hideViews()
        
    }
    func addTapGestureToScreenView(){
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.didTap(withGestureRecognizer:)))
        if isRedeemHalfViewShow == true{
        self.unityView!.addGestureRecognizer(tapGestureRecognizer)
            debugPrint("added")
        }
        else{
            self.unityView!.removeGestureRecognizer(tapGestureRecognizer)
            debugPrint("removed......")
        }
        
        
    }
     
    @IBAction func messageDoneBtnPressed(_ sender: UIButton) {
        if messageView.isHidden == false {
            messageView.isHidden = true
        }
    }
    
    @objc func didTap(withGestureRecognizer recognizer: UIGestureRecognizer) {
        if isRedeem{
            self.RedeemHalfView.isHidden = true
            self.redeemAnimatingView.isHidden = false
            self.createPulse()
            self.scaleInOutAnimation()
        }
    }
    
    @IBAction func redeemViewWalletBtnTapped(_ sender: Any) {
        
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            let vc  = UIStoryboard(name: "RedeemCardStoryBoard", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
            vc?.delegate = self
            self.navigationController?.present(vc!, animated: true, completion: nil)
        }
        else {
            let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func addToWalletBtnTapped(_ sender: UIButton) {
        
        let vc  = UIStoryboard(name: "RedeemCardStoryBoard", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
      
    }
    
    @objc func videoDidEnd(note: NSNotification) {
        videoNode?.removeFromParentNode()
        if isRedeem {
            isArVideoPlayed = true
            if isGiftModelDownloaded {
                self.view.makeToast(constantsStrings.tapAnywhere, duration: 3.0, position: .center, completion: nil)
            }else {
                ProgressHUD.show()
            }
        }else {
            DispatchQueue.main.async {
                if self.videoRepeatCount == 0 {
                    self.subCategoryView.isHidden = false
                }
            }
        }
        
    }


    @IBAction func showRedeemCustomModal(_ sender: Any) {
        redeemAnimatingView.isHidden = true
        self.senderTitle.text =  "Greetings from \(nameofSender)"
       
        UIView.transition(with: RedeemHalfView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.RedeemHalfView.isHidden = false
                            self.view.bringSubviewToFront(self.RedeemHalfView!)
                            self.tableView.reloadData()
                            
                      })
    }
    
    // MARK: - Functions Performing API Calls
    func addToWallet(name:String,phone:String,url:String){
        if Reachability.isConnectedToNetwork(){
            let data = ["parsl":url,"reciever_name":name,"reciever_phone_no":phone]
            let parameters = ["data":data]
            
            let url = URL(string: constantsUrl.GET_VIRTUALCARD_DETAIL_URL)!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                    }
                }
                catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func getGuestDeviceDetail() {
        if Reachability.isConnectedToNetwork(){
            let data = ["device_token": ParslUtils.sharedInstance.DEVICE_TOKEN, "device_id": deviceID] as [String : Any]
            let parameters = ["app_id":"parsl_ios",
                              "data":data] as [String : Any]
            
            let APIUrl = constantsUrl.SAVE_GUEST_DEVICE_DETAIL_URL
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: APIUrl)!)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                        ParslUtils.sharedInstance.saveDeviceId(self.deviceId!)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func getCategory() {
        if Reachability.isConnectedToNetwork() {
            let parameters = ["app_id":"parsel_ios"] as [String: AnyObject] 
            let url = constantsUrl.GET_CATEGORY_LIST_URL
//            ProgressHUD.show()
            
            postRequest(serviceName: url, sendData: parameters, success: { (response) in
                do {
                    self.mainCategoryList = try JSONDecoder().decode(MainCategory.self, from: response)
                    if !(self.mainCategoryList.data!.isEmpty) {
                        ProgressHUD.dismiss()
                        let firstCategoryName = self.mainCategoryList.data![0].categoryKey!
                        self.getCategoryVendors(categoryName: firstCategoryName)
                    }
                }
                        
                catch{
                    ProgressHUD.dismiss()
                }
                
            }, failure: { (data) in
                print(data)
                ProgressHUD.dismiss()
            })
             
        } else {
            self.isInternetAvailable = false
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func playServerVideo(url: URL) {
        let player = AVPlayer(url: url)
        
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) {
            vc.player?.play()
        }
    }

    func getSubCategories(key: String, vendor: String) {
        var vendors = [String]()
        vendors.append(vendor)
        let data: Dictionary<String, AnyObject> = ["category": key as AnyObject, "vendors": vendors as AnyObject]
        let parameters: Dictionary<String, AnyObject> = ["app_id": "parsl_ios" as AnyObject, "namespace": ParslUtils.sharedInstance.getUserNameSpace() as AnyObject,"data": data as AnyObject]
        print(parameters)
        let url = constantsUrl.GET_VENDOR_MODELS_URL
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                if !(self.vendersList.data.vendorsList.isEmpty) {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                    
                print(".....\(json)")
                fetchModelsApiData(data: json["data"] as? [AnyObject] ?? [AnyObject]())
                DispatchQueue.main.async {
//
                    if isfromMain {
                        self.vendorSelectedIndex = 0
                    }
                    self.isSubcategoriesAPICalled = .Downloaded
                    self.mainCategoryCollectionView.reloadData()
                    if vendersList.data.vendorsList.count == 1 {
                        self.vendorsCollectionView.reloadSections(IndexSet.init(integer: 0))
                    }
                    else{
                        self.vendorsCollectionView.reloadData()
                    }
                    self.subCategoryCollectionView.reloadData()
                    ProgressHUD.dismiss()
                    
                    
                }
                } else {
                    let welcome = try? JSONDecoder().decode(Welcome.self, from: response)
                    self.liveAvatarData = welcome?.data ?? []
                    debugPrint(liveAvatarData)
                    DispatchQueue.main.async {
                        self.isMyAvatarSelected = true
                        self.mainCategoryCollectionView.reloadData()
                        self.vendorsCollectionView.reloadData()
                        self.subCategoryCollectionView.reloadData()
                        ProgressHUD.dismiss()
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    self.isSubcategoriesAPICalled = .NotDownloadable
                }
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self.isSubcategoriesAPICalled = .NotDownloadable
            }
        })
    }
    
    func getVendors(categoryName: String) {
        let url = constantsUrl.GET_CATEGORY_VENDORS_URL
        ProgressHUD.show()
        let data = ["category": categoryName]
        let params = ["app_id":"parsl_ios", "data": data] as [String : AnyObject]
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            
            do {
                self.vendersList = try JSONDecoder().decode(GetVendorsList.self, from: response)
                self.vendors = self.vendersList.data.vendorsList
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    DispatchQueue.main.async{
                    self.downloadedVendors.append(DownloadedDataForVendors(categoryIndex: self.selectedIndex, vendorsList: self.vendersList))
                        self.vendorsCollectionView.reloadData()
                        self.mainCategoryCollectionView.reloadData()
                        ProgressHUD.dismiss()
                    }
                }
            }
                    
            catch{
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            print(data)
            ProgressHUD.dismiss()
        })
    }
    
    func getCategoryVendors(categoryName: String) {
        let url = constantsUrl.GET_CATEGORY_VENDORS_URL
        
        let data = ["category": categoryName]
        let params = ["app_id":"parsl_ios", "data": data] as [String : AnyObject]
        print("Vendors api parameter: \(params)")
        ProgressHUD.show()
        postRequest(serviceName: url, sendData: params, success: { (response) in
            
            do {
                self.vendersList = try JSONDecoder().decode(GetVendorsList.self, from: response)
                self.vendors.removeAll()
                for j in 0...self.vendersList.data.vendorsList.count {
                    print("index is \(self.vendersList.data.vendorsList.count)")
                    if self.vendersList.data.vendorsList.count != j{
                    self.vendors.append(VendorsList(vendorID: "", vendorName: self.vendersList.data.vendorsList[j].vendorName, otherInfo: OtherInfo(slogan: "s", otherInfoDescription: "<#String#>dsd", website: "sdsd", logoURL: "sdsd")))
                    } else {
                        break
                    }
                }
               
                debugPrint("Vendor api response data \(self.vendersList.data)")
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    DispatchQueue.main.async{
                    self.downloadedVendors.append(DownloadedDataForVendors(categoryIndex: self.selectedIndex, vendorsList: self.vendersList))
                        debugPrint("vendor name is......\(self.vendersList.data.vendorsList[0].vendorName)")
                    let vendorId = self.vendersList.data.vendorsList[0].vendorID
                        self.getSubCategories(key: categoryName, vendor: vendorId!)
                    }
                } else {
                    DispatchQueue.main.async {
                            self.getSubCategories(key: categoryName, vendor: " ")
                        ProgressHUD.dismiss()
                    }
                }
            } catch {
                DispatchQueue.main.async {
                ProgressHUD.dismiss()
                }
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
            ProgressHUD.dismiss()
            }
        })
    }
    
    func redeemGift(otp: String, name: String, phoneNumber: String) {
       // ProgressHUD.show("Please wait")
        self.view.isUserInteractionEnabled = true
          
        let data = ["parsl":otp.uppercased() as AnyObject,
                    "device_token":ParslUtils.sharedInstance.DEVICE_TOKEN as AnyObject,
                    "device_id":deviceID as AnyObject]
        let parameters = ["app_id":"parsl_ios" as AnyObject,"namespace": ParslUtils.sharedInstance.getUserNameSpace() as AnyObject,
                          "data":data as AnyObject]
        
        
        print(parameters)
        let url = constantsUrl.GET_PARSL_DETAILS
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: AnyObject]
                parslRedeemModel = fetchRedeemModelStruct(response: jsonResponse)
                let data = jsonResponse["data"] as! [String: AnyObject]
                let parslAmount = data["parsl_amount"] as? Double ?? 0.0
                if parslRedeemModel != nil {
                    if self.parslRedeemModel.message == "Success" {
                        DispatchQueue.main.async {
                           ProgressHUD.dismiss()
                        }
                        nameofSender = parslRedeemModel.data.senderData.senderName
                        debugPrint("name of sender is \(nameofSender)")
                        self.format = parslRedeemModel.data.giftModel.format
                        DispatchQueue.main.async {
                            self.redeemgiftAmount = "$\(ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: parslAmount))"
                        }
                 
                        if parslRedeemModel.data.receiverData.videoMsg != ""{
                            DispatchQueue.main.async { [self] in
                                isVideoMessage = true
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.isArVideoPlayed = true
                                isVideoMessage = false
                            }
                        }
                        
                        redeemMessage = parslRedeemModel.data.receiverData.textMsg
                        print(redeemMessage)
                        DispatchQueue.main.async {
                            self.isRedeem = true
                            self.MainCategoryStack.isHidden = true
                            self.rightButtonsView.isHidden       = true
                            self.leftButtonsView.isHidden        = true
                            self.subCategoryView.isHidden  = true
                            self.subCategoryCollectionView.isHidden = true
                            self.modelStatusLabel.isHidden = false
                            self.giftShowed = false
                            self.modelStatusLabel.text = constantsStrings.parslBeingredeemedTxt
                            self.setUIOfReloadButton()
                            self.animatingCardView = true
                            var style = ToastStyle()
                            style.backgroundColor = UIColor(named: "toastBackColor")!
                            style.messageColor = .black
                            self.view.makeToast(self.redeemMessage, duration: 10.0, position: .center, style: style)
                            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != ""{
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }else{
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }
                            tableView.reloadData()
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: self.parslRedeemModel.message)
                            ProgressHUD.dismiss()
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                    self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.inValidOtpUrl)
                     
                        ProgressHUD.dismiss()
                    }
                }
                
            } catch let error {
                print(error)
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                }
            }
            
            
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self.showAlert(withMessage: constantsStrings.unableToFetch)
            }
        })
    }
    
    func getGiftModels() {
        if Reachability.isConnectedToNetwork() {
            let parameters = ["app_id": "parsl_ios"] as [String : Any]
            let url = constantsUrl.GET_GIFT_MODELS
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        giftModels = try JSONDecoder().decode(GiftModel.self, from: data)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        } else {
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func saveToServer() {
        var device_info : Dictionary <String, String> = [:]
        var receiver_info : Dictionary <String, String> = [:]

         device_info = [
          "device_name":UIDevice.current.name as String,
          "device_os":UIDevice.current.systemName as String,
            "device_version":UIDevice.current.systemVersion as String,
            "device_type":UIDevice.current.model,
            "device_manufacturar":"Apple"
        ]

        let data :Dictionary<String,Any> = ["creator" :userEmail! as String,"creator_name":userEmail! as String, "phone":"","creator_email":userEmail! as String,"creator_namespace":userEmail! as String,"creater_device_info":device_info as Any,"firebase_id":self.documentID! as String,"app_id":"parsl","creator_device_token":ParslUtils.sharedInstance.DEVICE_TOKEN! as String]
        
        
        print(data)
        
        postChatRequest(serviceName:constantsUrl.START_CHAT, sendData: data as [String : AnyObject], success: { (data) in
            print("data is",data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
        
        let status = data["status"] as! Bool
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    if status{
                        print("successfuly saved to server")
                    }else{
                        self.showAlert(withMessage: constantsStrings.agentsAreBusy)
                    }
                   // self.showAlert("Success")
                    
                }
      
            }
  
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    //MARK: -Redeem Animation Methods
    
    func loadRedeemAnimationBackGroundView() {
        RedeemAnimatingBackgroundView.isHidden = false
        actionButton.removeFromSuperview()

        BackgroundView.contentMode = .scaleAspectFill
        BackgroundView.loopMode = .loop
        BackgroundView.play()
        self.view.sendSubviewToBack(self.unityView!)
        self.view.bringSubviewToFront(RedeemAnimatingBackgroundView)
        BackgroundView.bringSubviewToFront(firstCardView)
        BackgroundView.bringSubviewToFront(secondCardView)
        BackgroundView.bringSubviewToFront(buttonView)
        
        animatingCardView = true
        
        self.roundButtonView()
        
        
        self.firstCardView.alpha = 0
        self.buttonView.alpha = 0
        self.firstCardView.isHidden = false
        
        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.firstCardView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.firstCardView.alpha = 1
                self.firstCardTextView.text = constantsStrings.receivedParsl
                self.firstCardView.transform = self.firstCardView.transform.translatedBy(x: self.view.frame.width/2-self.firstCardView.frame.width/2, y: 200)
               
               
            }, completion: nil)
        }
        
        
        UIView.animate(withDuration: 1.3, delay: 0.7, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
           
            self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            
            UIView.animate(withDuration: 1, delay: 0.9, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                self.buttonView.alpha = 1
                self.buttonView.transform = self.firstCardView.transform.translatedBy(x: -32, y: 64)

            }, completion: nil)
            
        }
        
        isFirstTime = true
        
        
    }
    
    
    
    @IBAction func nxtBtnTapped(_ sender: Any) {
        
        if isFirstTime{
            UIView.animate(withDuration: 0){
                self.firstCardView.alpha = 0
                self.buttonView.alpha = 0
                self.firstCardView.isHidden = true
                
            }
             
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                self.secondCardView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.secondCardView.alpha = 1
                    self.secondCardView.isHidden = false
                    // self.cardViewHeight.constant = 350
                    self.secondCardTextView.text = self.redeemMessage
                    self.secondCardView.transform = self.secondCardView.transform.translatedBy(x: self.view.frame.width/2-self.secondCardView.frame.width/2, y: 200)
                    
                    
                }, completion: nil)
                
                
            }
            
            UIView.animate(withDuration: 1.3, delay: 0.9, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
                
                self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                
                UIView.animate(withDuration: 1, delay: 0.7, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                    self.buttonView.alpha = 1
                    self.buttonView.transform = self.secondCardView.transform.translatedBy(x: -32, y: 264)
                    
                }, completion: nil)
            }
            self.isFirstTime = false
            
        } else{
            
            RedeemAnimatingBackgroundView.isHidden = true
            self.secondCardView.isHidden = true
            view.addSubview(actionButton)
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.view.sendSubviewToBack(RedeemAnimatingBackgroundView)
            self.redeemAnimatingView.isHidden = false
            animatingCardView = false
            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != "" {
                self.loadAvatar()
            }else{
                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
            }
            self.view.makeToast(constantsStrings.viewParsl)
        }
    }

        
func roundButtonView() {
    
    buttonView.layer.cornerRadius = buttonView.frame.size.width/2
    buttonView.clipsToBounds = true

    buttonView.layer.borderColor = UIColor.white.cgColor
    buttonView.layer.borderWidth = 5.0
    
    
    firstCardView.clipsToBounds = true
    firstCardView.layer.cornerRadius = 10.0
    
    secondCardView.clipsToBounds = true
    secondCardView.layer.cornerRadius = 10.0
    
    
}
    //MARK: -Unity Helper methods
     
    func destroyUnity(_ message: String!) {
        //destroying a model
        if !isRedeem{
            let parsed = message.replacingOccurrences(of: "(Clone)", with: "")
            modelsInView.removeValue(forKey: parsed)
            
            for (index, element) in modelsInCart.enumerated() {
              
                if element.productId == message{
                    modelsInCart.remove(at: index)
                    cartCountLabel.text = String(modelsInCart.count)
                    cartImageView.image = COLORED_CART_IMAGE
                    if cartCountLabel.text == "0" {
                        self.cartCountLabel.text = " "
                        cartImageView.image = EMPTY_CART_IMAGE
                    }
                }
            }
        }
        self.view.sendSubviewToBack(self.unityView!)

    }
    func sendMSg(ID:String, defaultShadows: Bool, face: String) {
        var id = ID + "," + String(defaultShadows)
        print(id)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Activate", message: id)
        self.isActivateMethodCalled = true
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOff", message: "")
    }
    
    func projectionForJLB(ID:String, defaultShadows: Bool, whoAmI: String) {
        ProgressHUD.show()
         
//                let urlString : String = "\(ID)modelJLB"
//                var localPath: NSURL?
//
//        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
//
//        Alamofire.download(
//            ID,
//            method: .get,
//            encoding: JSONEncoding.default,
//            headers: nil,
//            to: destination).downloadProgress(closure: { (progress) in
//                //progress closure
//                print("Progress: \(progress.fractionCompleted)")
//            }).response(completionHandler: { (DefaultDownloadResponse) in
//                //here you able to access the DefaultDownloadResponse
//                //result closure
//                debugPrint(DefaultDownloadResponse)
//                debugPrint(DefaultDownloadResponse.destinationURL as Any)
//                let sourceUrl = DefaultDownloadResponse.destinationURL!
//                let fileManager = FileManager()
//                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
//                let pathWithComponent = path.appendingPathComponent("test\("directory")")
//                do {
//                    try FileManager.default.createDirectory(atPath: pathWithComponent, withIntermediateDirectories: true, attributes: nil)
//                } catch {
//                    print("error")
//                    return
//                }
//                debugPrint(pathWithComponent)
//
//                do {
//                    try fileManager.unzipItem(at: sourceUrl, to: URL(fileURLWithPath: pathWithComponent))
//                } catch {
//                    print("Extraction of ZIP archive failed with error:\(error)")
//                }
//            })
//
        var id = ID + ",\(whoAmI)"
        print(id)
        UnityEmbeddedSwift.sendUnityMessage("ModelLoader", methodName: "DownloadFile", message: id)
        self.isActivateMethodCalled = true
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOff", message: "")
    }
    
    func sendRedeemMsg(ID:String) {
        var id = ID + "," + "true"
        
        print("model id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Redeem", message: id)
    }
    
    func closeUnity() {
        
        print("closing...... ")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "CloseUnity", message: "")
    }
    
    func turnModelShadowsOffOn() {
        var style = ToastStyle()
        if !isShadowOff{
            
            self.view.makeToast(constantsStrings.shadowsDisable, duration: 3.0, position: .center, style: style)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOff", message: "")
            self.isShadowOff = true
            self.shadowBtn.setImage(UIImage(named: "shadow off"), for: .normal)
        }
        else{
            debugPrint("turning model shadows on...... ")
            if tappedItem.defaultShadow == false {
                self.view.makeToast(constantsStrings.noShadow, duration: 3.0, position: .bottom, style: style)
            }
            else {
                self.view.makeToast(constantsStrings.shadowEnable, duration: 3.0, position: .center, style: style)
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOn", message: "")
            self.isShadowOff = false
            self.shadowBtn.setImage(UIImage(named: "shadow on"), for: .normal)
            }
        }
    }
    
    func turnVerticalHorizontal() {
        var style = ToastStyle()
        if isHorizontal{
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "VertMovement", message: "")
            self.isHorizontal = false
            
//            self.HVBtn.setImage(UIImage(named: "vertical"), for: .normal)
            self.view.makeToast(constantsStrings.horizontalDisable, duration: 3.0, position: .center, style: style)

        }
        else{
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "HoriMovement", message: "")
            self.isHorizontal = true
            self.view.makeToast(constantsStrings.horizontalEnable, duration: 3.0, position: .center, style: style)
        }
    }
    
  
     
    
    func saveFirebaseid(_ firebaseId: String){
            UserDefaults.standard.set(firebaseId, forKey: "firebaseId")
            UserDefaults.standard.synchronize()
    }
    
    func openConversationWindow() {
        //check if email exists
          let sentDate = Date()
          var userName:String
          var email:String
          var phone:String?
          var namespace:String

              phone = ""
              userName = userEmail!
              email = userEmail!
              namespace = deviceId!
          
          var device_info : Dictionary <String, String> = [:]
  //        var receiver_info : Dictionary <String, String> = [:]

           device_info = [
              "device_manufacturar": "Apple" as String,
            "device_name":UIDevice.current.name as String,
            "device_os":UIDevice.current.systemName as String,
              "device_type": "iPhone" as String,
            "device_version":UIDevice.current.systemVersion as String
          ]

          var data: Dictionary<String, Any>

              data =
                  ["creator" :deviceId! as String,
                  "creator_name": userEmail! as String,
                  "phone":phone as Any,
                  "creator_email":email as String,
                  "creator_namespace":namespace as String,
                  "creater_device_info":device_info as Any,
                  "app_id":"parsl",
                  "creator_device_token":deviceToken as Any,
                  "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
          

          print(data)

        postChatRequest(serviceName:constantsUrl.START_CHAT_URL, sendData: data as [String : AnyObject], success: { (response) in
              print(response)
              let defaults = UserDefaults.standard
              let statusCode = defaults.string(forKey: "statusCode")

              if ((statusCode?.range(of: "200")) != nil){
                  DispatchQueue.main.async {
                      let status = response["status"] as! Bool
                      if status{
                          let data = response["data"] as! NSDictionary
                          let firebaseId = data["firebase_id"] as! String
                          debugPrint("firebase id is ......\(firebaseId)")
                          self.saveFirebaseid(firebaseId)
                          
                          let channel = Channel(id: firebaseId)
                          let vc = ChatViewController(channel: channel)
//
                          self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else{
                          print(response["message"] as! String)
                      }
                      
                  }

              }
              
              DispatchQueue.main.async {
                  
              }

          }, failure: { (data) in
              print(data)
              
              DispatchQueue.main.async {
                  self.view.makeToast(constantsStrings.unableChat)
                  
              }
          })

        }
         
   
    func showAlertWithAction(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
       
            self.mainPresentedView.isHidden = true 
        })
        alert.addAction(ok)

        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
   
    
}

// MARK: - Protocols..
protocol SendDataDelegate: class {
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String)
    func showActionButton()
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String)
    func mayBelater()
}

protocol CartEmptyDelegate {
    func cartHasBeenCleared()
}
 
 

