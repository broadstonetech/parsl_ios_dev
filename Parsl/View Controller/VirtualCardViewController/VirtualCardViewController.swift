//
//  VirtualCardViewController.swift
//  Parsl
//
//  Created by broadstone on 02/05/2021.
// This class show the user a virtual card when user redeem a parsl and want to add wallet

import UIKit
import PassKit
import Stripe
import Toast_Swift

class VirtualCardViewController: BaseViewController, PKAddPaymentPassViewControllerDelegate, STPIssuingCardEphemeralKeyProvider {

    // MARK: - IBoutlets
    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var cvvNo: UILabel!
    @IBOutlet weak var nameOnCardLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cvcLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var numberOnCardLabel: UILabel!
    
    // MARK: - Properties
    var virtualCardModel: VirtualCard!
    var isCardRedeemed = false
    var message = ""
    var pushProvisioningContext: STPPushProvisioningContext? = nil
//    var dataRequest: DataRequest?
    var cardType = ""
    var lastFour = ""
    var expDate = 0
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameOnCardLabel.text = virtualCardModel.data?.cardholder?.name
        cvvNo.text = virtualCardModel.data?.cvc
        expDate =  virtualCardModel.data?.expMonth ?? 0
        expiryDate.text = "\(String(expDate)) / \(virtualCardModel.data?.expYear ?? 0)   "
        ParslUtils.sharedInstance.saveUserCardName(nameOnCardLabel.text!)
        self.mainView.layer.cornerRadius = 10
        self.mainView.clipsToBounds = true
        self.setCardNumber()
//        cardType = ParslUtils.sharedInstance.getUserCardType()
//        cancel.setTitleColor(UIColor.gray, for: .normal)
        amountLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (virtualCardModel.data?.spendingControls?.spendingLimits?[0].amount)! / 100.0)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    } 
    
    // MARK: - Methods 
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    func goBackToPreviousScreen(){
        self.dismiss(animated: true, completion: nil)
    }
    func beginPushProvisioning() {
        let config = STPPushProvisioningContext.requestConfiguration(
            withName: ParslUtils.sharedInstance.getUserCardName(), // the cardholder's name
            description: constantsStrings.PARSLGiftCardDescription, // optional; a description of your card
            last4: lastFour, // optional; the last 4 digits of the card
            brand: .visa// optional; the brand of the card
        )
        let controller = STPFakeAddPaymentPassViewController(requestConfiguration: config, delegate: self)
//        let controller = PKAddPaymentPassViewController(requestConfiguration: config, delegate: self)
        self.present(controller!, animated: true, completion: nil)
      }
    
    
    func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, generateRequestWithCertificateChain certificates: [Data], nonce: Data, nonceSignature: Data, completionHandler handler: @escaping (PKAddPaymentPassRequest) -> Void) {

        self.pushProvisioningContext = STPPushProvisioningContext(keyProvider: self)
            // STPPushProvisioningContext implements this delegate method for you, by retrieving encrypted card details from the Stripe API.
            self.pushProvisioningContext?.addPaymentPassViewController(controller, generateRequestWithCertificateChain: certificates, nonce: nonce, nonceSignature: nonceSignature, completionHandler: handler);

    }
    
    func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, didFinishAdding pass: PKPaymentPass?, error: Error?) {

        self.dismiss(animated: true, completion: nil)
        self.goBackToPreviousScreen()
    }
    
    func createIssuingCardKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {

        let innerParams : [String : Any] = ["parsl" : "2BE5AB"]
        let params  = ["data" : innerParams as AnyObject]
       // let url = "https://parsl.io/get/ephemeral/key"
        let url = constantsUrl.GET_EMPHERALKEY

        postRequest(serviceName: url, sendData: params, success: { [self] (response) in
            print(response)
            do {
                let dec = try JSONDecoder().decode(dataextractor.self, from: response)
                let datakey = dec.data
                var associatedObj : [[AnyHashable : Any]] = []
                for obj in datakey.associated_objects{
                    let innerParam : [AnyHashable : Any] = ["id" : obj.id , "type" : obj.type]
                    associatedObj.append(innerParam)
                }
                let dataCompletion : [AnyHashable : Any] = ["id" : datakey.id , "object" : datakey.object , "created" : datakey.created ,"expires" : datakey.expires , "livemode" : datakey.livemode , "secret" : datakey.secret , "associated_objects" : associatedObj]

                completion(dataCompletion,nil)


            } catch let error {
                print(error)
            }

        }, failure: { (data) in
            print(data)

            DispatchQueue.main.async {
                self.showAlert(withMessage: constantsStrings.unableToFetch)
            }
        })
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
            if let error = error {
                self.view.makeToast(constantsStrings.unableToSaveCard)
                print(error.localizedDescription)
                
            } else {
                showAlertWithAction(title: constantsStrings.appName, message: constantsStrings.cardSaved)
                
            }
        }
    
    
    func setCardNumber() {
        let cardNumber = virtualCardModel.data?.number
       // cardNumber?.prefix(upTo: 4)
      let firstSet =   cardNumber!.prefix(4)
      var stripped = cardNumber!.dropFirst(4)
        
        let secondSet =   stripped.prefix(4)
        stripped = stripped.dropFirst(4)
        
        let thirdSet =   stripped.prefix(4)
        stripped = stripped.dropFirst(4)
        
        let fourthSet =   stripped.prefix(4)
        lastFour = String(fourthSet)
        self.numberOnCardLabel.text = firstSet+"   "+secondSet+"   "+thirdSet+"   "+fourthSet
    }
    
   // MARK: - IBActions
//    @IBAction func cancelBtn(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
//    }
    
    @IBAction func addToWalletTapped(_ sender: UIButton){
        
//        if isCardRedeemed == true {
//            showAlert(withMessage: "\(constantsStrings.alreadyRedeemed) \(message)")
//        }
//        else {
//        beginPushProvisioning()
//    }
        self.view.makeToast("Virtual card added to PARSL wallet")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
  
    
}
