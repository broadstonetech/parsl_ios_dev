//
//  WalletVC.swift
//  AES
//
//  Created by broadstone on 08/03/2022.
//

import UIKit
import CryptoSwift

struct DecryptedData {
    let name: String
    let cardNumber: String
    let cardExpiry: String
    let cvc: String
    let redeemCode: String
    let issueDate: String
}

class WalletVC: BaseViewController {
    // MARK: - Properties
    var walletModel: UserWalletModel!
    var privateKey = ""
    
    // MARK: - IBOutlets
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var totalBalanceLabel: UILabel!
    @IBOutlet weak var cardDetailsSV: UIStackView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardExpiryLabel: UILabel!
    @IBOutlet weak var redemptionCodeLabel: UILabel!
    @IBOutlet weak var issueDateLabel: UILabel!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var cardCvvLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var cardNoCopyBtnOutlet: UIButton!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        title = constantsStrings.walletScreenTitile
        collectionView.delegate = self
        collectionView.dataSource = self
        setNavigationBarUi()
        parentView.isHidden = true
        centerLabel.isHidden = true
        userNameLabel.text = constantsStrings.parslWallet
        cardNoCopyBtnOutlet.setTitle("", for: .normal)
        callPublicKeyApi()
    }
    override func viewWillAppear(_ animated: Bool) { 
        setTheme()
    }
    
    @IBAction func copyCardNumber(_ sender: Any) {
        UIPasteboard.general.string = (cardNumberLabel.text as? String) ?? ""
        self.view.makeToast(constantsStrings.copiedToClipboard)
    }
    // MARK: - Methods
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    func initVC () {
        
    }
    func setNavigationBarUi() { // put this in baseviwecontroller
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}
// MARK: - Extensions
extension WalletVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if walletModel == nil {
            return 0
        }
        return walletModel.data.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalletCardCell", for: indexPath) as! WalletCardCell
        var finalBalance = walletModel.data.cards[indexPath.row].currency + getPlainData(from: walletModel.data.cards[indexPath.row].encBalance)
        finalBalance = String(finalBalance.dropFirst())
//        print("final balance is \(finalBalance)")
        finalBalance = ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: Double(finalBalance)!)
        cell.balance.text = "$\(finalBalance)"
        if walletModel.data.cards[indexPath.row].isSelected == true {
            cell.cardImg.backgroundColor = UIColor(named: "CardBgColor")
        } else {
            cell.cardImg.backgroundColor = UIColor(named: "CardDarkMode")
        }
        return cell
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<walletModel.data.cards.count{
            walletModel.data.cards[i].isSelected = false
        }
        walletModel.data.cards[indexPath.row].isSelected = true
        let getDecryptedData = decryptData(at: indexPath)
        cardNameLabel.text = getDecryptedData.name
        cardNumberLabel.text = getDecryptedData.cardNumber
        cardCvvLabel.text = getDecryptedData.cvc
        cardExpiryLabel.text = getDecryptedData.cardExpiry
        redemptionCodeLabel.text = getDecryptedData.redeemCode
        issueDateLabel.text = ParslUtils.sharedInstance.getDateInUSLocale(timestamp: Int64(getDecryptedData.issueDate) ?? 0000)
        senderNameLabel.text = walletModel.data.cards[indexPath.row].senderName
        cardDetailsSV.isHidden = false
        self.collectionView.reloadSections(IndexSet(integer: 0))
    }
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 15) // 15 because of paddings
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    
    func getTotalBalance() -> Double {
        var total = 0.0
        for item in walletModel.data.cards {
            let balance = getPlainData(from: item.encBalance)
            let parseBalance = Double(balance) ?? 0.0
            
            total += parseBalance
        }
        
        return total
    }
}

//MARK: - API Calling extension
extension WalletVC {
    func callPublicKeyApi() {
        let url = constantsUrl.Wallet_Public_Key
        let params = ["namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        DispatchQueue.main.async {
            ProgressHUD.show()
        }
        
        postRequest(serviceName: url, sendData: params, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : AnyObject]
                
                let success = (json["status"] as? Bool) ?? false
                
                if success {
                    let data = json["data"] as! [String: AnyObject]
                    let publicKey = data["public_key"] as! String
                    
                    
                    DispatchQueue.main.async {
                        self.callWalletDetailsAPI(publicKey: publicKey)
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.centerLabel.isHidden = false
                        debugPrint("Unable to get wallet details - line133")
                    }
                    
                }
            } catch {
                debugPrint("Error while serialization: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                }
            }
        }, failure: { (error) in
            debugPrint(error)
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
            }
            if ParslUtils.sharedInstance.error400{
                DispatchQueue.main.async {
                    self.showAlert(withMessage: constantsStrings.GENERIC_ALERT_TEXT_400)
                }
            }
            if(ParslUtils.sharedInstance.error401)
            {
                DispatchQueue.main.async {
                    self.showAlert(withMessage: constantsStrings.GENERIC_ALERT_TEXT_401)
                }
            }else{
                
            }
        })
    }
    
    func callWalletDetailsAPI(publicKey: String) {
        let url = constantsUrl.Get_User_wallet
        
        let data = ["public_key": publicKey] as [String: AnyObject]
        let params = ["namespace": ParslUtils.sharedInstance.getUserNameSpace(),
                      "data": data] as [String: AnyObject]
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
            }
            do{
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String : AnyObject]
                debugPrint(json)
                let model = self.fetchWalletDetails(json)
                DispatchQueue.main.async {
                    self.walletModel = model
                    self.privateKey = self.createSecretKeyFrom(publicKey: publicKey, timestamp: String(model.data.timestamp))
                    self.totalBalanceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: self.getTotalBalance())
                    if self.walletModel.data.cards.count == 0 {
                        self.centerLabel.isHidden = false
                        self.parentView.isHidden = true
                    } else {
                    self.collectionView.reloadData()
                    self.parentView.isHidden = false
                    self.centerLabel.isHidden = true
                    }
                }
                
                
            } catch {
                debugPrint("Error while serialization: \(error.localizedDescription)")
                
            }
           
        }, failure: { (error) in
            debugPrint(error)
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
            }
        })
    }
    
    func fetchWalletDetails(_ json: [String: AnyObject]) -> UserWalletModel{
        let success = (json["status"] as? Bool) ?? false
        let message = json["message"] as! String
        if success {
            let message = json["message"] as! String
            let data = json["data"] as! [String: AnyObject]
            
            let timestamp = data["timestamp"] as! Int64
            let checkSum = data["check_sum"] as! String
            let cards = data["cards"] as! [AnyObject]
            
            var cardsData = [UserWalletCards]()
            
            for i in 0..<cards.count {
                let item = cards[i] as! [String: AnyObject]
                
                let encName = item["name_on_card"] as! String
                let encCardNum = item["enc_card_num"] as! String
                let encCVC = item["enc_cvc"] as! String
                let encExpiry = item["enc_expiry"] as! String
                let cardNetwork = item["card_network"] as! String
                let encBalance = item["balance"] as! String
                let currency = item["currency"] as! String
                let redeemCode = item["redeem_code"] as! String
                let issueDate = item["issue_date"] as! String
                let senderName = item["sender_name"] as! String

                
                cardsData.append(UserWalletCards.init(nameOnCard: encName, encCardNumber: encCardNum, encCVC: encCVC, encExpiry: encExpiry, cardNetwork: cardNetwork, encBalance: encBalance, currency: currency, redeemCode: redeemCode, issueDate: issueDate, senderName: senderName))
            }
            
            let walletData = UserWalletData.init(cards: cardsData, timestamp: timestamp, checkSum: checkSum)
            
            return UserWalletModel.init(success: success, message: message, data: walletData)
            
            
        }else {
            return UserWalletModel.init(success: success, message: message, data: UserWalletData.init(cards: [UserWalletCards](), timestamp: 0, checkSum: ""))
        }
    }
}
//MARK: - Decryption Algorithm
extension WalletVC {
    func createSecretKeyFrom(publicKey: String, timestamp: String) -> String {
        let constant = "553A67059CDBF40B45A"
        
        var parsedPublicKey = publicKey
        parsedPublicKey.removeAll(where: {
            $0 == "-"
        })
        
        let resultantString = parsedPublicKey + timestamp + constant
        var privateKey = ""
        
        for i in 0..<resultantString.count {
            if i%2 == 0 {
                //Do nothing
            }else {
                privateKey += resultantString.stringAt(i)
            }
        }
        
        return privateKey
    }
    
    func getPlainData(from encStr: String) -> String {
        do {
            let aes = try AES.init(key: privateKey.bytes, blockMode: ECB.init(), padding: .pkcs7)
            let str = Data(base64Encoded: encStr)
            let decryptedBytes = try aes.decrypt(str!.bytes)
            var decText = String(bytes: decryptedBytes, encoding: .utf8)
            
            decText?.removeAll(where: {
                $0 == "="
            })
            return decText ?? ""
        }catch {
            debugPrint(error)
            return ""
        }
    }
    
    func decryptData(at indexPath: IndexPath) -> DecryptedData {
        let name = getPlainData(from: walletModel.data.cards[indexPath.row].nameOnCard)
        let cardNumber = getPlainData(from: walletModel.data.cards[indexPath.row].encCardNumber)
        let cvc = getPlainData(from: walletModel.data.cards[indexPath.row].encCVC)
        let expiry = getPlainData(from: walletModel.data.cards[indexPath.row].encExpiry)
        let redeemCode = getPlainData(from: walletModel.data.cards[indexPath.row].redeemCode)
        let issueDate = getPlainData(from: walletModel.data.cards[indexPath.row].issueDate)
        
        return DecryptedData.init(name: name, cardNumber: cardNumber, cardExpiry: expiry, cvc: cvc, redeemCode: redeemCode, issueDate: issueDate)
        
    }
    
    
}
