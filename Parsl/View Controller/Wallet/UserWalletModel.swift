//
//  UserWalletModel.swift
//  AES
//
//  Created by broadstone on 08/03/2022.
//

import Foundation

struct UserWalletModel {
    let success: Bool
    let message: String
    var data: UserWalletData
}

struct UserWalletData {
    var cards: [UserWalletCards]
    let timestamp: Int64
    let checkSum: String
}

struct UserWalletCards {
    let nameOnCard: String
    let encCardNumber: String
    let encCVC: String
    let encExpiry: String
    let cardNetwork: String
    let encBalance: String
    let currency: String
    let redeemCode: String
    let issueDate: String
    let senderName: String
    var isSelected: Bool = false
}
