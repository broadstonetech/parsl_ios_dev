// Redeem Later.
// Redeem Now.
// This class allows the users to save PARSL otp for late redeem procedure as well as enable the user to do the redeem of the PARSL
/* always add strings in only constant file
 always seprate the urls in sperate file
 use alwasy debug print for testing purpose instead of the print statement.
 use dispatchqueue if using loading progress view etc.
 always make folder for view controller and make subfolder for its cell and extensions.
 always use enums in seprate file.
 add protocols always in last in the file.
 always use else if using if().
 */
import UIKit
import ContactsUI

class ReceiverModalViewController: BaseViewController,CNContactPickerDelegate, UITextFieldDelegate{
    
    // MARK: - Properties
    private let contactPicker = CNContactPickerViewController()
    var delegate : SendDataDelegate?
    let peoplePicker = CNContactPickerViewController()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var redeemLater: UIButton!
    @IBOutlet weak var redeemNow: UIButton! // if it is a button then add btn in last camel case
    @IBOutlet weak var innerStackView: UIStackView!
    @IBOutlet weak var receiverNameTF: UITextField!
    @IBOutlet weak var urlTF: UITextField!
    @IBOutlet weak var receiverContactTF: UITextField!
    
    //MARK: -ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        receiverNameTF.delegate = self
        receiverContactTF.delegate = self
        self.redeemNow.backgroundColor = UIColor.lightGray
        self.redeemLater.backgroundColor = UIColor.lightGray
        self.redeemNow.isUserInteractionEnabled = false
        self.redeemLater.isUserInteractionEnabled = false
        self.redeemNow.layer.cornerRadius = 21
        self.redeemLater.layer.cornerRadius = 21
        urlTF.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginDone(_:)), name: Notification.Name(rawValue: "RedeemLaterLogin"), object: nil)
        
        let screenTap = UITapGestureRecognizer(target: self, action: #selector(self.screenTapPressed(_:)))
        view.addGestureRecognizer(screenTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.innerStackView.isHidden = false
        setTheme()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        self.delegate?.mayBelater()
        self.delegate?.showActionButton()
    }
    
    // MARK: - IBActions
    @IBAction func cancelButtonTapped(_ sender: Any) {
        if ParslUtils.sharedInstance.getIfuserLogin() {
            if Reachability.isConnectedToNetwork() {
                self.saveParslOtp()
            }
        }
        else {
            navigateToLoginVc(whichButton: "RedeemLater")
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if urlTF.text == "" || urlTF.text!.isEmpty {
            self.view.makeToast(constantsStrings.enterOTP)
            return
        }
        receiverNameTF.text = ""
        receiverContactTF.text = ""
        
        
        if !urlTF.text!.contains("parsl.io") {
            let uppercasedText = urlTF.text?.uppercased()
            urlTF.text = uppercasedText
        }
        if Reachability.isConnectedToNetwork() {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            self.delegate?.sendData(self.receiverNameTF.text!,self.receiverContactTF.text!,self.urlTF.text!)
                ParslUtils.sharedInstance.eventsCallForFRAnalytics(envetName: "Redeem SuccessFully withOutSave", detailsOfEvent: ["Type": "Redeem Now", "Message": "Redeem Code is \(urlTF.text)", "DeviceId": ParslUtils.sharedInstance.DEVICE_TOKEN])
            dismiss(animated: true, completion: nil)
            } else {
                navigateToLoginVc(whichButton: "")
            }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    // MARK: - Class Funcions
    
    @objc func LoginDone (_ notification: NSNotification){
        saveParslOtp()
    }
    
    func navigateToLoginVc (whichButton: String) {
        let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
        let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.fromSendOrRedeem = whichButton
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func screenTapPressed(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    func saveParslOtp() {
        ProgressHUD.show()
        let url = constantsUrl.SAVE_PARSL_OTP_URL
        let Data = ["parsl_otp": urlTF.text!.uppercased()] as [String: AnyObject]
        let params = ["app_id": "parsl_ios", "namespace": ParslUtils.sharedInstance.getUserNameSpace(), "data": Data] as [String: AnyObject]
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.view.makeToast(message)
                        self.delegate?.mayBelater()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.showAlert(withMessage: message)
                    }
                }
                
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
}

