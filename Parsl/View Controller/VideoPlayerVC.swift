//
//  VideoPlayerVC.swift
//  Parsl
//
//  Created by broadstone on 25/05/2021.
// This class help the user to play a video inside the app.

import UIKit
import AVFoundation
class VideoPlayerVC: BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var videoView: VideoView!
    
    // MARK: - Properties
    var videoPath: String!
    var isArVideo = false
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareVideoView()
        NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.didEnterBackgroundNotification, object: nil)
//
//          // foreground event
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.willEnterForegroundNotification, object: nil)
//
//         // add these 2 notifications to prevent freeze on long Home button press and back
          NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.willResignActiveNotification, object: nil)
//
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    // MARK: - Methods
    private func prepareVideoView(){
            videoView.isHidden = false
            guard let videoPath = Bundle.main.path(forResource: videoPath!, ofType:"mp4") else {
                print("error return")
                return
            }
            let url = NSURL(fileURLWithPath: videoPath) as URL
            playVideo(url: url.absoluteString)
    }
   

    func playVideo(url: String){
        if let videoURL = URL(string: url) {
            player = AVPlayer(url: videoURL)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer!.frame = self.view.bounds
            playerLayer!.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(playerLayer!)
            
            player!.play()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
        }
    }
    
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func setPlayerLayerToNil(){
        // first pause the player before setting the playerLayer to nil. The pause works similar to a stop button
        player?.pause()

    }
//
//     // foreground event
    @objc fileprivate func reinitializePlayerLayer(){

        player?.play()
    }
}
