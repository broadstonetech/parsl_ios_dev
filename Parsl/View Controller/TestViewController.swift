//
//  TestViewController.swift
//  Parsl
//
//  Created by M Zaryab on 26/10/2021.
//

import UIKit
import AVKit
import MobileCoreServices
import PassKit
import Alamofire
import Lottie
import UnityFramework

struct ModelsQuantityStruct {
    var modelIndex: Int!
    var modelQuantity: Int!
    var modelPrice: Double!
    var totalPrice: Double!
   // var discountedPrice:Double!
    var isDiscounted:Bool!
}



class TestViewController: PaymentsBaseClass, UINavigationControllerDelegate, CartCellClickDelegate {
    
//    func sendMessage(toMobileApp message: String!) {
//
//    }
//
//    func destroyUnity(_ message: String!) {
//
//    }
//
//    func showContainerView(_ message: String!) {
//
//    }
//
//    func sendMessage(onReload message: String!) {
//
//    }
//    func modelProjected(_ message: Bool) {
//        print("model is projected....\(message)")
//    }
//    func modelDownloaded(_ message: Bool) {
//        print("message is .......\(message)")
//    }
//
//    func modelDownloadingDone(_ message: String!) {
//
//    }

//    func avatarModule(_ message1: String!, forRedeem message2: String!) {
//        print("Avatar module froom catview")
//        print(message1)
//        print(message2)
//        avatarCharacter = message1
//        avatarAnimation = message2
//
////        self.view.sendSubviewToBack(self.unityView!)
////        self.view.bringSubviewToFront(self.backgroundView)
//    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrColor = [(isSelected:Bool,color:UIColor)]()
    var couponResponse:                   CouponResponse!
    var taxPrice: Double            = 0.0
    var priceWithoutTax: Double     = 0.0
    var totalPrice: Double          = 0.0
    var senderName: String          = ""
    var senderEmail: String         = ""
    var senderAddress: String       = ""
    var senderNumber: String        = ""
    
    var receiverName: String        = ""
    var receiverEmail: String       = ""
    var receiverAddress: String     = ""
    var receiverNumber: String      = ""
    
    var deviceTokenString:String    = ""
    var pricesList                  = [Double]()
    var selectedItemsDataList       = [SubCategoryData]()
    //these arrays are used to zip into one dictionary
    var selectedItemsArray          = [String]()
    var quantity                    = [Int]()
    
    var videoURL: String            = ""
    var selectedModel:String        = ""
    var isVideo:Bool = false
    var selectedItem                = 0
    var selectIndex                 = 0
    var selectIndexSec              = 0
    var saveParsl                   : SaveParsl!
    var isVideoRecorded: Bool = false
    var extrasArray                     = [GiftModelMessage]()

    
    var animationView: AnimationView?
    
    var downloadedAnimationsFilePaths = [String]()
    
    var isAnyDiscount:Bool = false
    
    var avatarCharacter = ""
    var avatarAnimation = ""
    
    //@IBOutlet weak var animationView: AnimationView!
//    @IBOutlet var backgroundView: UIView!
//
//    //MARK: - IBOutlets
//
//    @IBOutlet weak var extraStackView: UIStackView!
   @IBOutlet weak var extraLabelView: UIView!
//    @IBOutlet weak var recordVideoView: UIView!
    @IBOutlet weak var personalizeView: UIView!
    @IBOutlet weak var mainAboveView: UIView!
//    @IBOutlet weak var scrollHeight: UIView!
//    @IBOutlet weak var textOuterView: IQPreviousNextView!
//    @IBOutlet weak var scrolView: UIView!
//    @IBOutlet weak var scroolView: UIScrollView!
//    @IBOutlet weak var animationCollectionView: UICollectionView!{
//        didSet {
//            animationCollectionView.delegate = self
//            animationCollectionView.dataSource = self
//        }
//    }
//    @IBOutlet weak var avatarCollectionView: UICollectionView!{
//        didSet {
//            avatarCollectionView.delegate = self
//            avatarCollectionView.dataSource = self
//
//        }
//    }
//    @IBOutlet weak var personalizeVM: UILabel!
//    @IBOutlet weak var applyBtnOutlet: RoundButton!
//
//    @IBOutlet weak var greetMsgOutlet: UIButton!
//    @IBAction func greetMsgOutlet(_ sender: Any) {
//    }
    @IBOutlet weak var personalizBtnOutlet: UIButton!
//
//
//    @IBOutlet weak var interactionModelsView: UIView!
//    @IBOutlet weak var avatarModelsView: UIView!
//    @IBOutlet weak var giftModelsView: UIView!
    
    @IBOutlet weak var extraBtnOutlet: UIButton!
    @IBOutlet weak var CTPView: UIView!
//
    @IBOutlet weak var emailTFView: UIView!
    @IBOutlet weak var nameTFView: UIView!
//    @IBOutlet weak var extraView: UIView!
//    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var ExtrasCollectionView: UICollectionView!
//
//    @IBOutlet weak var messageTextView:             UITextView!
    @IBOutlet weak var totalCharges:                UILabel!
    @IBOutlet weak var applyCopoun:                 UIButton!
    @IBOutlet weak var copounTF:                    UITextField!
//    @IBOutlet weak var addMessageButton:            RoundButton!
    @IBOutlet weak var paymentButton:               UIButton!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
//    @IBOutlet weak var recordVideoBtn: UIImageView!
    @IBOutlet weak var transactionalCostLabel: UILabel!
    
    var modelsInCart = [ModelDataStruct]()
    var modelsPrices = [ModelsQuantityStruct]()
    var giftWrapSelectedIndex = 0
    var animationSelectedIndex = -1
    var avatarSelectedIndex = -1
    
    var cartDelegate: CartEmptyDelegate?
    var unityView: UIView?
    
    
    //MARK: -View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
//        self.view.bringSubviewToFront(scrolView)
        self.navigationController?.isNavigationBarHidden = true
        
        paymentButton.layer.cornerRadius = paymentButton.frame.height / 2
        paymentButton.backgroundColor = UIColor(displayP3Red: 55/255, green: 176/255, blue: 131/255, alpha: 1)
//        FrameworkLibAPI.registerAPIforNativeCalls(self)
        self.view.backgroundColor = UIColor.appColor(.CartViewBackgroundColor)
        self.extraBtnOutlet.backgroundColor =  UIColor(named: "light-custom-blue")
        self.extraBtnOutlet.setTitleColor(.black, for: .normal)
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.personalizBtnOutlet.setTitleColor(.white, for: .normal)
//        self.extraView.isHidden = true
//        self.messageTextView.isHidden = true
//        self.recordVideoBtn.isHidden = true
//
        
//        addKeyboardListeners()
//        self.scrolView.layer.zPosition = 1
//        self.scroolView.layer.zPosition = 1
        
//        scrollHeight.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
    }
    
    
    private func calculateInitalModelPrices(){
        for i in 0..<modelsInCart.count {
            let modelIndex = i
            let modelQuantity = 1
            let modelPrice = modelsInCart[i].priceInfo.price ?? 0.0
            let finalPrice = modelPrice * Double(modelQuantity)
            
            let modelPriceStruct = ModelsQuantityStruct(modelIndex: modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice, totalPrice: finalPrice,isDiscounted: false)
            
            modelsPrices.append(modelPriceStruct)
            
            totalPrice += finalPrice
        }
        taxPrice = totalPrice * 0.15
        totalPrice = totalPrice + taxPrice
        totalCharges.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCostLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
        
        
    }
    
    private func recalculateFinalPrice(){
        totalPrice = 0
        for i in 0..<modelsPrices.count {
            totalPrice += modelsPrices[i].totalPrice
        }
        taxPrice = totalPrice * 0.15
        totalPrice = totalPrice + taxPrice
        totalCharges.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCostLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
    }
    
    
    var pricesListCount:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.personalizeView.addShadowToView()
        self.mainAboveView.addShadowToView()
        self.mainAboveView.layer.cornerRadius = 5
        self.mainAboveView.clipsToBounds = true
//        self.extraStackView.layer.cornerRadius = 5
//        self.extraStackView.clipsToBounds = true
        extraLabelView.roundCornerss([.topLeft, .topRight], radius: 5)
        personalizeView.roundCornerss([.bottomLeft, .bottomRight], radius: 5)
//        extraView.roundCornerss([.bottomLeft, .bottomRight], radius: 5)
//        giftModelsView.layer.cornerRadius = 5
//        giftModelsView.clipsToBounds = true
//        avatarModelsView.layer.cornerRadius = 5
//        avatarModelsView.clipsToBounds = true
//        interactionModelsView.layer.cornerRadius = 5
//        interactionModelsView.clipsToBounds = true
        calculateInitalModelPrices()
//        messageTextView.textColor = .lightGray
//        messageTextView.text = "Type your message here..."
//        messageTextView.delegate = self
        copounTF.cornerRadiuss(radius: copounTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        nameTF.cornerRadiuss(radius: nameTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        emailTF.cornerRadiuss(radius: emailTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        paymentButton.cornerRadiuss(radius: paymentButton.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
//        applyBtnOutlet.layer.cornerRadius = paymentButton.frame.size.height/2
//        applyBtnOutlet.clipsToBounds = true
        personalizBtnOutlet.roundCornerss([.bottomLeft, .topLeft], radius: personalizBtnOutlet.frame.size.height/2)
        extraBtnOutlet.roundCornerss([.bottomRight, .topRight], radius: extraBtnOutlet.frame.size.height/2)
//
//        let tapGestureRecoginzer = UITapGestureRecognizer(target: self, action: #selector(recordVideoBtnPressed))
//        //recordVideoBtn.isUserInteractionEnabled = true
//        //recordVideoBtn.addGestureRecognizer(tapGestureRecoginzer)
////        recordVMOutlet.isUserInteractionEnabled = true
////        recordVMOutlet.addGestureRecognizer(tapGestureRecoginzer)
////        recordVMOutlet.isHidden = true
//        personalizeVM.isUserInteractionEnabled = true
//        personalizeVM.addGestureRecognizer(tapGestureRecoginzer)
        
//        //to load the initial price list
//        priceWithoutTax = pricesList.reduce(0,+)
//        taxPrice = ( 15.0 / 100) * priceWithoutTax
//        totalPrice = priceWithoutTax + taxPrice
//        totalCharges.text = "$\(String(totalPrice))"
//        pricesListCount = pricesList.count
        //delegates
//        ExtrasCollectionView.delegate = self
//        ExtrasCollectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        
        //loading gift models
       
        DispatchQueue.main.async {
//            giftModels.data![0].isModelSelected = true
//            self.ExtrasCollectionView.reloadData()
//            self.avatarCollectionView.reloadData()
//            self.animationCollectionView.reloadData()
        }
        
        for model in modelsInCart{
            selectedItemsArray.append(model.productId)
        }
     
        
        let estimatedHeight = tableView.numberOfRows(inSection: 0) //You may need to modify as necessary
        let width = self.view.frame.size.width
        tableView.frame = CGRect(x: 0, y: 0, width: Int(width), height: estimatedHeight)
        
        //payment token
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
//        let avatarIndex = giftModels.avatars![avatarSelectedIndex].chracter_key
//        let aIndex = String(avatarIndex)
        // keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(TestViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(TestViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//
//                view.addGestureRecognizer(tap)
           
    }
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else {return}
            guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
            let keyboardFrame = keyboardSize.cgRectValue
            if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= keyboardFrame.height-60
            }
        }
        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//               textField.resignFirstResponder()
//               return true
//           }
           
//    @objc override func dismissKeyboard() {
//               //Causes the view (or one of its embedded text fields) to resign the first responder status.
//               view.endEditing(true)
//           }
    
    @objc private func onPaymentTokenGenerated(_ notification: NSNotification){
        print("Payment base class #17")
        ProgressHUD.dismiss()
        if let token = notification.userInfo?["token"] as? String {
            if let lastDigit = notification.userInfo?["lastFour"] as? String{
                if let cardType = notification.userInfo?["cardType"] as? String{
//                    self.saveParslDetail(token: token,lastFour: lastDigit,cardType: cardType)
                }
                
            }
        }
        
    }
    
    
    //MARK: -Button Actions
    
//    @IBAction func showMsgView(_ sender: Any) {
////        self.textOuterView.isHidden = false
////        self.messageTextView.isHidden = false
////        self.greetMsgOutlet.isHidden = true
//
//
//    }
    
    var isValidCoupon = false
    @IBAction func applyCopounTapped(_ sender: Any) {
        isAnyDiscount = false
        if self.copounTF.text != ""{
            self.getCopounDetails()
        }
        else
        {
            showAlert(withTitle: "Missing Field", withMessage: "Please Enter Copoun Code")
        }
    }
    @IBAction func addMessageTapped(_ sender: Any) {
        self.popupAlert(title: "Type Message or Record Message", message: nil, actionTitles: ["Type Message","Record Video","Cancel"], actionStyle: [.default,.default,.cancel],
        actions: [{ action in
//            self.messageTextView.isHidden = false


    },{ action in
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .camera)
    },nil], vc: self)
    }
    
    private var paymentRequest: PKPaymentRequest{
        let request = PKPaymentRequest()
        request.merchantIdentifier = ("")
        request.supportedCountries = ["US"]
        request.supportedNetworks = [.quicPay,.visa,.masterCard]
        request.merchantCapabilities = .capability3DS
        request.currencyCode = "USD"
        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Gift Item", amount: NSDecimalNumber(value: taxPrice))]
        
        return request
    }
    @IBAction func paymentTapped(_ sender: Any) {
        if nameTF.text!.isEmpty || nameTF.text == "" || nameTF.text == " " {
            self.view.makeToast("Please enter name")
            return
        }
        if emailTF.text!.isEmpty || emailTF.text == "" || emailTF.text == " " {
            self.view.makeToast("Please enter email")
            return
        }
        if !isValidEmail(emailTF.text!) {
            self.view.makeToast("Please enter valid email address")
            return
        }

        self.popupAlert(title: "Choose option", message: nil, actionTitles: ["Apple Pay","Pay with Stripe","Cancel"], actionStyle: [.default,.default,.cancel],
            actions: [
            {
            action in
                self.view.makeToast("Coming soon")
            },
            {
            action in
                let stripeVC = StipePaymentIntegration()
                self.navigationController?.present(stripeVC, animated: true, completion: nil)
            }
            ,nil], vc: self)
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showExtraView(_ sender: Any) {
        print("extra btn pressed")
        self.personalizeView.isHidden = true
//        self.extraView.isHidden = false
//
//        self.nameTFView.isHidden = true
//        self.emailTFView.isHidden = true
//        self.CTPView.isHidden = true
        self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
        self.personalizBtnOutlet.setTitleColor(.black, for: .normal)
        self.extraBtnOutlet.setTitleColor(.white, for: .normal)
//        if messageTextView.text == "" || messageTextView.text == "Type your message here..."{
//            messageTextView.isHidden = true
//            textOuterView.isHidden = true
//            greetMsgOutlet.isHidden = false
//        }
//        else{
//            messageTextView.isHidden = false
//            greetMsgOutlet.isHidden = true
//        }
        if isVideoRecorded == false{
//            self.recordVideoView.isHidden = true
        }
        else{
//            self.recordVideoView.isHidden = false
        }


    }
    
    @IBAction func showPDataView(_ sender: Any) {

        print("personalize btn pressed")
        self.personalizeView.isHidden = false
//        self.extraView.isHidden = true
//
        self.extraBtnOutlet.backgroundColor =  UIColor(named: "light-custom-blue")
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.extraBtnOutlet.setTitleColor(.black, for: .normal)
        self.personalizBtnOutlet.setTitleColor(.white, for: .normal)
       
    }
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK: -Functions
    
//    func addKeyboardListeners() {
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//    }
    
    @objc func video(
      _ videoPath: String,
      didFinishSavingWithError error: Error?,
      contextInfo info: AnyObject
    ) {
      let title = (error == nil) ? "Success" : "Error"
      let message = (error == nil) ? "Video was saved" : "Video failed to save"
      videoURL = videoPath
      let alert = UIAlertController(
        title: title,
        message: message,
        preferredStyle: .alert)
      alert.addAction(UIAlertAction(
        title: "OK",
        style: UIAlertAction.Style.cancel,
        handler: nil))
      present(alert, animated: true, completion: nil)
    }
    
    func dateTimeStatus(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy hh:mm:ss a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if let dt = dateFormatter.date(from: date) {
            let userFormatter = DateFormatter()
            userFormatter.dateStyle = .medium // Set as desired
            userFormatter.timeStyle = .medium // Set as desired

            return userFormatter.string(from: dt)
        } else {
            return "Unknown date"
        }
    }
    //device token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
    }
//    func saveParslDetail(token: String,lastFour:String,cardType:String) {
//        //Calculate tax price
//
//        ProgressHUD.show()
//        taxPrice = (totalPrice * 15) / 100
//        priceWithoutTax = totalPrice - taxPrice
//
//        //End Calculation of tax price
//
//        var isVideoRecorded = 0
//
//        if videoURL != "" {
//            isVideoRecorded = 1
//        }
//
//
//        //let selectedItemsDict = zip(modelsInCart, modelsPrices).map{["product_id":$0,"quantity":$1]}
//
//        var itemDict = [AnyObject]()
//        for i in 0..<modelsInCart.count {
//            let item = ["product_id": modelsInCart[i].productId as AnyObject,
//                        "quantity": modelsPrices[i].modelQuantity as AnyObject]
//
//            itemDict.append(item as AnyObject)
//        }
//        print(itemDict)
//        let timestamp = Date().currentTimeMillis()
//        let senderData       = ["card_number":lastFour,
//                                "card_type":cardType,
//                                "sender_device_token": ParslUtils.sharedInstance.DEVICE_TOKEN,
//                                "sender_device_id": deviceID,
//                                "email":emailTF.text ?? "",
//                                "sender_name": nameTF.text ?? ""
//                               ] as [String: Any]
//
//        let priceRelated    = ["price":priceWithoutTax,
//                               "currency":"usd",
//                               "total_price":totalPrice] as [String : Any]
//
//
//        if messageTextView.text == "Type your message here..." || messageTextView.text == "" {
//            messageTextView.text = "Hey, there is parsl for you "
//        }else{
//            messageTextView.text  = messageTextView.text
//        }
//        var characterkey = ""
//        var animationKey = ""
//        if avatarSelectedIndex != -1{
//            characterkey = giftModels.avatars![avatarSelectedIndex].chracter_key!
//        }
//        if animationSelectedIndex != -1{
//            animationKey = giftModels.animations[animationSelectedIndex].animation_key!
//        }
//
////        let characterkey = giftModels.avatars![avatarSelectedIndex].chracter_key
////        let animationKey = giftModels.animations[animationSelectedIndex].animation_key
//
//        let data :Dictionary<String,Any>           = ["selected_products":itemDict,
//                               "gift_model":giftModels.data![giftWrapSelectedIndex].productID,
//                               "sender_data":senderData,
//                               "text_msg":messageTextView.text ?? "",
//                               "parsl_timestamp":timestamp,
//                               "parsl_stripe_token":token,
//                               "have_video":isVideoRecorded,
//                               "avatar_chracter": characterkey as String ,
//                               "avatar_animation": animationKey as String,
//                               "price_related": priceRelated] as [String : Any]
//        let parameters      = ["app_id": "parsl_ios", "data": data] as [String : Any]
//        print(parameters)
//        let url = URL(string: "https://api.parsl.io/save/parsl/details")!
//        let session = URLSession.shared
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        do {
//
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
//            guard error == nil else {
//                return
//            }
//
//            guard let data = data else {
//                return
//            }
//            //applinks:parsl.io/604e127f66c4bc23831f1fcc
//            do {
//                ProgressHUD.dismiss()
//                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
//                    saveParsl = try JSONDecoder().decode(SaveParsl.self, from: data)
//                    print(saveParsl!)
//                    if self.videoURL != ""{
//                        DispatchQueue.main.async {
//                            self.uploadParslVideo()
//                        }
//                    }
//                    DispatchQueue.main.async {
//                        print(parameters)
//                        print(saveParsl!)
//                        guard let message = saveParsl.message else {
//                            return
//                        }
//                        if message.contains("Price cannot be zero"){
//                            showAlert(withTitle: "PARSL", withMessage: "Transaction unsuccessful. Please try again")
//                        }else{
//                            isOrderFlowCompleted = true
//                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
//                            vc.amount = "$" + String(saveParsl.data!.totalPrice!)
//                            vc.parslOTP = saveParsl.data!.parslOtp!
//                            vc.parslURL = saveParsl.data!.parslURL!
//                            vc.transaction = saveParsl.data!.transectionID!
//                            vc.cardNumber = saveParsl.data!.cardNumber!
//                            vc.transactionCost = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
//                            vc.modelsPrices = modelsPrices
//                            vc.modelsInCart = modelsInCart
//                            if messageTextView.text == "Type your message here..." || messageTextView.text == "" {
//                                vc.msgText = "Hey, here is a PARSL from " + nameTF.text! + "."
//                            }else{
//                                vc.msgText = messageTextView.text
//                            }
//
//                            self.navigationController?.pushViewController(vc, animated: true)
//                        }
//                    }
//
//                }
//            } catch let error {
//                ProgressHUD.dismiss()
//                print(error)
//                showAlert(withTitle: "PARSL", withMessage: "Transaction unsuccessful. Please try again")
//            }
//        })
//        task.resume()
//    }

    func uploadParslVideo(){
        let url = URL(string: "https://api.parsl.io/save/parsl/video/msg")!
        print(saveParsl.data!.parslID!)
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(NSURL(fileURLWithPath: self.videoURL) as URL, withName: "video_msg")

            multipart.append("\(self.saveParsl.data!.parslID!)".data(using: .utf8)!, withName :"parsl_id")
        }, to: url, method: .post, headers: nil) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response { answer in
                    print(answer.response!)
                    print("statusCode: \(String(describing: answer.response?.statusCode))")
                }
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                    print(progress)
                }
             //   DispatchQueue.main.async {
//                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShareViewController") as? ShareViewController
//                    //vc?.urlText = self.saveParsl!.data!.parslURL!
//                   // vc?.transactionID.text = self.saveParsl!.data?.parslID
//                    self.navigationController?.present(vc!, animated: true, completion: nil)
                //}
            case .failure(let encodingError):
                print("multipart upload encodingError: \(encodingError)")
            }
        }
    }
    
    func getCopounDetails(){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)

        let data = ["coupon_code":self.copounTF.text!,
                    "selected_products":selectedItemsArray,
                    "date_time":dateString] as [String : Any]
        let parameters = ["app_id": "parsl_ios",
                          "data": data] as [String : Any]

        print(parameters)
        let url = URL(string: "https://api.parsl.io/check/coupon/details")!
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"


        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {
                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {

                    let decoder = JSONDecoder()
                    self.couponResponse = try? decoder.decode(CouponResponse.self, from: data)
                    dump(self.couponResponse)
                    let relief = couponResponse.data!.copounRelief
                    var discountedPrice = 0.0
                    var nonDiscountedPrice = 0.0
                    if couponResponse.data?.copounStatus == true{

                        self.isAnyDiscount = true //coupon is valid


                            let item = couponResponse.data!.approvedProducts!
                            if item.count > 0 {

                                //discounted price

                                for product in item{
                                    let index = modelsInCart.firstIndex(where: {$0.productId == product})
                                    var totalPrice = modelsPrices[index!].totalPrice

                                    if couponResponse.data?.copounReliefType == "price_relief"{
                                        totalPrice = totalPrice! -  (Double(relief!))

                                    }else{

                                        totalPrice = totalPrice! - (( (Double(relief!)) / 100) * totalPrice!)
                                    }

                                    modelsPrices[index!].totalPrice = totalPrice
                                    modelsPrices[index!].isDiscounted = true
                                    discountedPrice = totalPrice! + discountedPrice
                                   // self.calculateDiscountedPrice(indexPath: index!, discountedPrice:totalPrice!)

                                }
                            }

                        //denied products
                        let deniedItems = couponResponse.data!.deniedProducts!
                        if deniedItems.count > 0{

                            for product in deniedItems{
                                let index = modelsInCart.firstIndex(where: {$0.productId == product})
                                let totalPrice = modelsPrices[index!].totalPrice
                                modelsPrices[index!].isDiscounted = false
                                nonDiscountedPrice = totalPrice! + nonDiscountedPrice

                            }



                        }


                                    priceWithoutTax = discountedPrice + nonDiscountedPrice
                                   // priceWithoutTax = pricesList.reduce(0,+)
                                    taxPrice = ( 15.0 / 100) * priceWithoutTax
                                    totalPrice = priceWithoutTax + taxPrice
                                    DispatchQueue.main.async {
                                        self.transactionalCostLabel.text = "$\(String(format: "%.2f",taxPrice))"
                                        self.totalCharges.text = "$\(String(format: "%.2f",totalPrice))"
                                    }

                                }else{
                                    showAlert(withTitle: "Coupon Invalid", withMessage: "Try Valid Coupon")
                                }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }


                        }

                    else{
                        showAlert(withTitle: "Coupon Invalid", withMessage: "Try Valid Coupon")
                    }

                }
         catch let  error {
                print(error)
            }
        })
        task.resume()
    }
    
}

//@objc private extension MainViewController {
//
//    func keyboardWillShow(_ notification: Notification) {
//        collectionViewBottomConstraint.constant = 0
//    }
//
//    func keyboardWillHide(_ notification: Notification) {
//        collectionViewBottomConstraint.constant = 20
//    }
//}
    
    





    //MARK: -TableView
protocol CartCellClickDelegate: class {
    func plusButtonTapped(cell: CartTableViewCell, sender : Any)
    func minusButtonTapped(cell: CartTableViewCell, sender : Any)
    func deleteButtonTapped(cell: CartTableViewCell, sender : Any)
    
}
extension TestViewController: UITableViewDelegate, UITableViewDataSource{
    //Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return selectedItemsDataList.count
        return 1 + modelsInCart.count + modelsPrices.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell")!
            return cell
        }
        
        if indexPath.row < 1 + modelsInCart.count {
            let item = modelsInCart[indexPath.row - 1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as? CartTableViewCell
            cell?.delegate = self
            cell?.plusButton.tag = indexPath.row - 1
            cell?.minusButton.tag = indexPath.row - 1
            cell?.titleLabel.text = item.basicInfo!.name!
            cell?.itemsCountLabel.text = String(modelsPrices[indexPath.row - 1].modelQuantity)
            cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)
            cell?.subtotalLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item.priceInfo.price * Double(modelsPrices[indexPath.row - 1].modelQuantity)))
            let url = URL(string: modelsInCart[indexPath.row - 1].modelFile.thumbnail)
            cell?.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
    

            
            if modelsPrices[indexPath.row-1].isDiscounted {
                //discounted
                cell!.discountedPrice.isHidden = false
                cell!.discountedPrice.text = "$\(String(format: "%.2f",modelsPrices[indexPath.row-1].totalPrice))"
                cell!.discountedCutView.isHidden = false
            }else{
                cell!.discountedPrice.isHidden = true
                cell!.discountedCutView.isHidden = true
                
            }
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartDetailTableViewCell") as? CartDetailTableViewCell
            let index = indexPath.row - modelsInCart.count - 1
            cell?.titleLabel.text = modelsInCart[index].basicInfo.name
            cell?.quantityLabel.text = "\(String(modelsPrices[index].modelQuantity))"
            cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
            
            if modelsPrices[index].isDiscounted{
                cell?.priceLabel.text = "$\(String(format: "%.2f",modelsPrices[index].totalPrice))"
            }

            return cell!
        }
    }
    //Protocol Functions
    func deleteButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        modelsInCart.remove(at: indexPath.row - 1)
        modelsPrices.remove(at: indexPath.row - 1)
        DispatchQueue.main.async {
            if self.modelsInCart.count == 0{
                
                let alert = UIAlertController(title: "Cart is Empty", message: "Add Items to Cart", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                    self.cartDelegate?.cartHasBeenCleared()
                    if let navigator = self.navigationController {
                        navigator.popViewController(animated: true)
                    }
                }))
               
                self.present(alert, animated: true, completion: nil)
            }
            self.tableView.reloadData()
        }
        recalculateFinalPrice()
        
    }
    func plusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }

        let modelPrice = modelsPrices[indexPath.row - 1]
        if modelPrice.modelQuantity < 100 {
            let modelQuantity = modelPrice.modelQuantity + 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted:modelPrice.isDiscounted)
            
            modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
           if isAnyDiscount{
            self.getCopounDetails()
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Total quantity should be less than 100")
        }
        
    }
    func minusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        
        let modelPrice = modelsPrices[indexPath.row - 1]
        
        if modelPrice.modelQuantity > 1 {
            let modelQuantity = modelPrice.modelQuantity - 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted: modelPrice.isDiscounted)
            
            modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
            if isAnyDiscount{
             self.getCopounDetails()
             }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Quantity cannot be less than 1")
        }
        
    }
}


//MARK: Collection View
//extension TestViewController:UICollectionViewDelegate, UICollectionViewDataSource {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let returnValue = 0
//        if collectionView == ExtrasCollectionView{
//            return giftModels.data!.count
//        }
//        else if collectionView == avatarCollectionView{
//            return giftModels.avatars!.count
//        }
//        else if collectionView == animationCollectionView{
//            return giftModels.animations.count
//        }
////        return giftModels.data!.count
//        return returnValue
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        if collectionView == ExtrasCollectionView{
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
//        let item = giftModels.data![indexPath.row]
//        cell.imagePath = item.modelFiles!.thumbnail!
//        let url = URL(string: cell.imagePath)
//        cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
//        cell.title.text = item.basicInfo!.name!
//        print("boxes are ",item.basicInfo?.name,item.productID)
//            cell.mainView.layer.cornerRadius = 7
//            cell.mainView.clipsToBounds = true
//            cell.thumbnail.layer.cornerRadius = 7
//            cell.thumbnail.clipsToBounds = true
//
//        if item.isModelSelected == true{
//            cell.SelectedImage.isHidden = false
//        }
//        else{
//            cell.SelectedImage.isHidden = true
//        }
//
//        return cell
//        }
//        else if collectionView == avatarCollectionView{
//            let avatarCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCollectionViewCell", for: indexPath) as! AvatarCollectionViewCell
//            let avatarItem = giftModels.avatars![indexPath.row]
//            avatarCell.imagePath = avatarItem.chracter_thumbnail!
//            let url = URL(string: avatarCell.imagePath)
//            avatarCell.characterThumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
//            if avatarItem.isAvatarSelected == true{
//                avatarCell.avatarselectedImg.isHidden = false
//            }
//            else{
//                avatarCell.avatarselectedImg.isHidden = true
//            }
//            return avatarCell
//        }
//        else if collectionView == animationCollectionView{
//            let animationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnimationCollectionViewCell", for: indexPath) as! AnimationCollectionViewCell
//            let animationitem = giftModels.animations[indexPath.row]
//            animationCell.imagePath = animationitem.animation_thumbnail!
//            let url = URL(string: animationCell.imagePath)
//            animationCell.animatiomThumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
//            if animationitem.isAnimationSelected == true{
//                animationCell.animationSelectedImg.isHidden = false
//            }
//            else{
//                animationCell.animationSelectedImg.isHidden = true
//            }
//            return animationCell
//        }
//       return UICollectionViewCell()
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if collectionView == ExtrasCollectionView{
//            print("gift box selected............")
//        var item = giftModels.data!
//        for i in 0..<giftModels.data!.count{
//            giftModels.data![i].isModelSelected = false
//        }
//        giftWrapSelectedIndex = indexPath.row
//        giftModels.data![indexPath.row].isModelSelected = true
//        //scrollToCell(index: indexPath.row)
////        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
////            //self.ExtrasCollectionView.reloadData()
//            self.ExtrasCollectionView.reloadSections(IndexSet(integer: 0))
////        }
//        }
//        else if collectionView == animationCollectionView{
//            for j in 0..<giftModels.animations.count{
//                giftModels.animations[j].isAnimationSelected = false
//            }
//            animationSelectedIndex = indexPath.row
//            giftModels.animations[indexPath.row].isAnimationSelected = true
////            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
//                self.animationCollectionView.reloadSections(IndexSet(integer: 0))
////            }
//        }
//        else if collectionView == avatarCollectionView{
//            for k in 0..<giftModels.avatars!.count{
//                giftModels.avatars![k].isAvatarSelected = false
//            }
//            avatarSelectedIndex = indexPath.row
//            giftModels.avatars![indexPath.row].isAvatarSelected = true
////            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
//                self.avatarCollectionView.reloadSections(IndexSet(integer: 0))
////            }
//        }
//    }
//
//    func previewBtnPressed(itemIndex: Int) {
//        let fileName = giftModels.data![itemIndex].basicInfo!.name!
//        let fileUrl = giftModels.data![itemIndex].modelFiles!.previewVideo ?? ""
//        if fileUrl == "" {
//            self.view.makeToast("No preview available")
//            return
//        }
//        let index = isAnimationAlreadyDownloaded(fileName: fileName)
//        if index == -1 {
//            downloadGiftBoxPreviewVideo(animationFileUrl: fileUrl, animationFileName: fileName)
//        }else {
//            playGiftBoxVideo(filePath: downloadedAnimationsFilePaths[index])
//        }
//    }
//
//    @objc private func giftBoxDoubleTapped() {
//        let fileName = giftModels.data![giftWrapSelectedIndex].basicInfo!.name!
//        let fileUrl = giftModels.data![giftWrapSelectedIndex].modelFiles!.previewVideo ?? ""
//        if fileUrl == "" {
//            self.view.makeToast("No preview available")
//            return
//        }
//        let index = isAnimationAlreadyDownloaded(fileName: fileName)
//        if index == -1 {
//            downloadGiftBoxPreviewVideo(animationFileUrl: fileUrl, animationFileName: fileName)
//        }else {
//            playGiftBoxVideo(filePath: downloadedAnimationsFilePaths[index])
//        }
//    }
//
//    private func isAnimationAlreadyDownloaded(fileName: String) -> Int {
//
//        for i in 0..<downloadedAnimationsFilePaths.count {
//            if downloadedAnimationsFilePaths[i].lowercased().contains(fileName.lowercased()) {
//                return i
//            }
//        }
//
//        return -1
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////        let width = 0
////        let height = 0
//
//         if collectionView == ExtrasCollectionView{
//            let width = ExtrasCollectionView.frame.size.width/3
//            let height = ExtrasCollectionView.frame.size.height/1.3
//            return CGSize(width: width, height: height)
//        }
//        else if collectionView == avatarCollectionView{
//            let width = animationCollectionView.frame.size.width/3
//            let height = animationCollectionView.frame.size.height/1.5
//            return CGSize(width: width, height: height)
//        }
//
//        else if collectionView == animationCollectionView{
//            let width = animationCollectionView.frame.size.width/3
//            let height = animationCollectionView.frame.size.height/1.5
//            return CGSize(width: width, height: height)
//        }
//
//
//        return collectionView.frame.size
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//}



//video Message
extension TestViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(
      _ picker: UIImagePickerController,
      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
      dismiss(animated: true, completion: nil)

      guard
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
        mediaType == (kUTTypeMovie as String),
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
        UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
        else { return }

      // Handle a movie capture
//      UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(video(_:didFinishSavingWithError:contextInfo:)), nil)
        
        guard let data = try? Data(contentsOf: url) else {
            return
        }

        print("File size before compression: \(Double(data.count / 1048576)) mb")
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
        compressVideo(inputURL: url as URL,
                      outputURL: compressedURL) { exportSession in
            guard let session = exportSession else {
                return
            }
//            let url = NSURL(string: compressedURL)
            let urll = compressedURL
            let parhUrl = urll.path
            if parhUrl == ""{
                print("url is empty......")
            }
            else{
                print("url path is \(parhUrl)")
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                    return
                }
                self.isVideoRecorded = true
                self.videoURL = compressedURL.path
                DispatchQueue.main.async {
//                    self.extraView.isHidden = false
//                    self.recordVideoBtn.isHidden = false
//                    self.recordVideoView.isHidden = false
                    self.isVideoRecorded = true
                    self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
                    self.extraBtnOutlet.setTitleColor(.white, for: .normal)
                    self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
                    self.personalizBtnOutlet.setTitleColor(.black, for: .normal)
                    self.personalizeView.isHidden = true
                    self.CTPView.isHidden = false
                    self.nameTFView.isHidden = false
                    self.emailTFView.isHidden = false
//                    if self.messageTextView.text == "" || self.messageTextView.text == "Type your message here..."{
//                        self.messageTextView.isHidden = true
////                        self.textOuterView.isHidden = true
//                        self.greetMsgOutlet.isHidden = false
//                    }
//                    else{
//                        self.messageTextView.isHidden = false
////                        self.textOuterView.isHidden = false
//                    }
                }
                print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                DispatchQueue.main.async {
//                    self.extraView.isHidden = false
//                    self.recordVideoBtn.isHidden = true
//                    self.recordVideoView.isHidden = true
                    self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
                    self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
                }
                break
            @unknown default:
                print("video cancelled...........")
                break
            }
        }
        
    }
    
}
//extension TestViewController: UITextViewDelegate {
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        let ph_TextColor: UIColor = .lightGray
//        if messageTextView.textColor == ph_TextColor && messageTextView.isFirstResponder {
//                self.messageTextView.text = nil
//                self.messageTextView.textColor = .black
//            }
//    }
//
//    func textViewDidEndEditing (_ textView: UITextView) {
//        if messageTextView.text.isEmpty || messageTextView.text == "" {
//            messageTextView.textColor = .lightGray
//            messageTextView.text = "Type your message here..."
//        }
//    }
//
//    @objc private func recordVideoBtnPressed(){
//        VideoHelper.startMediaBrowser(delegate: self, sourceType: .camera)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//            self.personalizeView.isHidden = false
//            self.nameTFView.isHidden = false
//            self.emailTFView.isHidden = false
//            self.CTPView.isHidden = false
//        })
//    }
//}
extension TestViewController: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput,
                        didFinishRecordingTo outputFileURL: URL,
                        from connections: [AVCaptureConnection],
                        error: Error?) {
            guard let data = try? Data(contentsOf: outputFileURL) else {
                return
            }

            print("File size before compression: \(data.count)")
            print("File size before compression: \(Double(data.count / 1048576)) mb")

            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
            compressVideo(inputURL: outputFileURL as URL,
                          outputURL: compressedURL) { exportSession in
                guard let session = exportSession else {
                    return
                }

                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = try? Data(contentsOf: compressedURL) else {
                        return
                    }
                    
                    print("File size after compression: \(compressedData.count)")

                    print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
                case .failed:
                    break
                case .cancelled:
                   
                    break
                @unknown default:
                    break
                }
            }
        }


        func compressVideo(inputURL: URL,
                           outputURL: URL,
                           handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
            let urlAsset = AVURLAsset(url: inputURL, options: nil)
            guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                           presetName: AVAssetExportPresetMediumQuality) else {
                handler(nil)

                return
            }

            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4
            exportSession.exportAsynchronously {
                handler(exportSession)
            }
        }
}
extension TestViewController {
    
    func downloadGiftBoxPreviewVideo(animationFileUrl : String, animationFileName: String) {
        ProgressHUD.show("Downloading")
        let url = URL(string: animationFileUrl)
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                print("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                print("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    print("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(animationFileName).mp4"
                        let fileManager = FileManager.default
                        if (fileManager.fileExists(atPath: documentsDestPath)){
                            try! fileManager.removeItem(atPath: documentsDestPath)
                        }
                        try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                        //get stored file path and url
                        let fileName = "\(animationFileName).mp4"
                        let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                        print("File path \(filePath)")
                        
                        
                        DispatchQueue.main.async { [self] in
                            ProgressHUD.dismiss()
                            self.downloadedAnimationsFilePaths.append(filePath)
                            self.playGiftBoxVideo(filePath: filePath)
                            //animationView!.play()
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: "PARSL", withMessage: "Unable to download gift box animation, please try again later.")
                        }
                    }
                }else {
                    ProgressHUD.dismiss()
                    print("downlaoding failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    
    private func playGiftBoxVideo(filePath: String) {
//        animationView = .init(filePath: filePath)
//        animationView!.frame = view.bounds
//        animationView!.contentMode = .scaleAspectFit
//        animationView!.loopMode = .playOnce
//        animationView?.tag = 10001
//        self.view.addSubview(animationView!)
//        self.animationView?.play(completion: {_ in
//            DispatchQueue.main.async { [self] in
//                print("Is user interaction enabled \(self.view.isUserInteractionEnabled)")
//
//                self.animationView!.removeFromSuperview()
//
//                for subView in self.view.subviews {
//                    if subView.tag == 10001 {
//                        subView.removeFromSuperview()
//                    }
//
//                }
//            }
//        })
        let url = URL(fileURLWithPath: filePath)
        
        let player = AVPlayer(url: url)
        
        let vc = AVPlayerViewController()
        vc.player = player
        
        self.present(vc, animated: true) {
            vc.player?.play()
            
        }
    }
}
extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
    
    var appDelegate : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var sceneDelegate: SceneDelegate {
        return UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate
    }
}
