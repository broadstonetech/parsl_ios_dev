//
//  VideoControlelrViewController.swift
//  Parsl
//
//  Created by M Zaryab on 17/10/2021.
//

import UIKit
import AVKit
import MediaPlayer
import AudioToolbox
import Lottie

class VideoControlelrViewController: UIViewController {

//    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    @IBOutlet weak var videoView: VideoView!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let videoPath = Bundle.main.path(forResource: "parsl_video", ofType:"mp4") else {
            print("error return")
            return
        }
        let url = NSURL(fileURLWithPath: videoPath) as URL
        playVideo(url: url.absoluteString)
    }
    func playVideo(url: String){
        if let videoURL = URL(string: url) {
          player = AVPlayer(url: videoURL)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer!.frame = self.view.bounds
            playerLayer!.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(playerLayer!)
            player!.play()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
        }
    }
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        print("VideoEnded")
        playerLayer?.player = nil
        playerLayer?.removeFromSuperlayer()
    }
}
