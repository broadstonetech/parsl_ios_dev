//
//  RedeemCustomModalViewController.swift
//  Parsl
//
//  Created by Mufaza on 04/08/2021.
//

import Foundation
import UIKit
import FlagPhoneNumber



class RedeemCustomModalViewController: BaseViewController, UITextFieldDelegate,FPNTextFieldDelegate{
  
    var delegate : SendDataDelegate?
   var dialCountryCode = ""
    var receiverContactNo = ""
    var trimmed = ""

    @IBOutlet weak var receiverNameTF: UITextField!
    @IBOutlet weak var urlTF: UITextField!
    
    @IBOutlet weak var receiverContactTF: FPNTextField!
    
    var isValid = false
    
    
    //MARK: -ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        receiverNameTF.delegate = self
        receiverContactTF.delegate = self
        urlTF.delegate = self
       
        receiverContactTF.setFlag(key: .US)
         
        let signOut = UITapGestureRecognizer(target: self, action: #selector(self.screenTapPressed(_:)))
        view.addGestureRecognizer(signOut)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
    }
    
    @objc func screenTapPressed(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
 
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    func fpnDisplayCountryList() {
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCountryCode = dialCode
        print(name, dialCode, code, self.dialCountryCode)
        // Output "France", "+33", "FR"
        
    }

    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
           
            self.isValid = true
       } else {
        if textField.text != "" {
            self.isValid = false
        }
       
       }
    
 }

    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {

        
        if receiverNameTF.text == "" || receiverNameTF.text!.isEmpty {
            self.view.makeToast(constantsStrings.enterReceiverName)
            return
        }
        if receiverContactTF.text == "" || receiverContactTF.text!.isEmpty {
            self.view.makeToast(constantsStrings.enterReceiverNumber)
            return
        }

        if isValid {
            self.receiverContactNo = self.dialCountryCode + receiverContactTF.text!
           debugPrint("reciever contact no with coutry code is \(receiverContactNo)")
            self.trimmed = self.receiverContactNo.trimmingCharacters(in: .whitespacesAndNewlines)
            debugPrint("reciever contact no with coutry code is \(trimmed)")
            ParslUtils.sharedInstance.saveUserNumber(trimmed)
            self.delegate?.sendRedeemData(self.receiverNameTF.text!,self.trimmed)
                dismiss(animated: true, completion: nil)
        }else{
            self.view.makeToast(constantsStrings.enterValidNumber)
        }

    }
}

