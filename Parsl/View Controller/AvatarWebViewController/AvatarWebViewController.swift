//
//  AvatarWebViewController.swift
//  Parsl
//
//  Created by M Zaryab on 12/01/2022.
// This class open the webview and let the user to design and request for a new avatar

import UIKit
import WebKit
class AvatarWebViewController: UIViewController, WebViewDelegate {

    // MARK: - Properties
    var webView: WKWebView!
    let webViewControllerTag = 100
    let webViewIdentifier = "WebViewController"
    var webViewController = WebViewController()
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()

        title = constantsStrings.createAvatarScreenTitle
//        let url = URL(string: "https://parsl.readyplayer.me/avatar")!
//        avatarWebView.load(URLRequest(url: url))
        createWebView()
        webViewController.view.isHidden = true
        destroyWebView()
        createWebView()
        webViewController.reloadPage(clearHistory: true)
        webViewController.view.isHidden = false

    }
    

    // MARK: - Methods
    
    
    func createWebView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: webViewIdentifier) as UIViewController

        guard let viewController = controller as? WebViewController else {
            return
        }
        webViewController = viewController
        webViewController.avatarUrlDelegate = self
        
        addChild(controller)

        self.view.addSubview(controller.view)
        controller.view.frame = view.safeAreaLayoutGuide.layoutFrame
        controller.view.tag = webViewControllerTag
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controller.didMove(toParent: self)
    }
    
    func avatarUrlCallback(url: String){
        showAlert(message: url)
        webViewController.view.isHidden = true
//        editAvatarButton?.isHidden = false
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: constantsStrings.avatarUrlGenerated, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
             })
             alert.addAction(okButton)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func destroyWebView(){
        if let viewWithTag = self.view.viewWithTag(webViewControllerTag) {
            
            webViewController.dismiss(animated: true, completion: nil)
            viewWithTag.removeFromSuperview()
        }else{
            print("No WebView to destroy!")
        }
    }
    // MARK: - IBActions

}
