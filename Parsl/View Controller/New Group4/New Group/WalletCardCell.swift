//
//  WalletCardCell.swift
//  AES
//
//  Created by broadstone on 08/03/2022.
//

import UIKit

class WalletCardCell: UICollectionViewCell {
    
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var cardImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardImageWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cardImg: UIImageView!
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    } 
    func configureCell() {
        cardImageHeightConstraint.constant = self.frame.height
        cardImageWidthConstraint.constant = self.frame.width
        cardImageHeightConstraint.isActive = true
        cardImageWidthConstraint.isActive = true
    }
}
