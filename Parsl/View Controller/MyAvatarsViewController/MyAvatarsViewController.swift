//
//  MyAvatarsViewController.swift
//  Parsl
//
//  Created by M Zaryab on 28/12/2021.
// This class shows the saved avatars along the status of the avatar. It helps the user to request, edit and delete the avatar.

import UIKit
import WebKit
import Toast_Swift

class MyAvatarsViewController: BaseViewController, WebViewDelegate{

    //MARK: - IBOutlets
  
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var swiptLeftOutlet: UILabel!
    @IBOutlet weak var avatarsTableView: UITableView!
    var dataArray = [AvatarsData]()
    
    //MARK: - Properties
    var webView: WKWebView!
    let webViewControllerTag = 100
    let webViewIdentifier = "WebViewController"
    var webViewController = WebViewController()
    var pasteboardString: String? = UIPasteboard.general.string
    var isAddPressed = false
    var avatarId = ""
    var avatarindex = 0
    
    var style = ToastStyle()
    
    
    
    //MARK: - lifeCycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTheme()
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
 
        setRightBarButton()
        avatarsTableView.delegate = self
        avatarsTableView.dataSource = self
        avatarsTableView.rowHeight = UITableView.automaticDimension
        avatarsTableView.reloadData()
        title = constantsStrings.myAvatarScreenTitle
        swiptLeftOutlet.isHidden = true
        centerLabel.isHidden = true
        avatarsTableView.isHidden = true
        getListofMyAvatar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(avatarUpdated(_:)), name: Notification.Name(rawValue: "AvatarUpdated"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(avatarSubmitted(_:)), name: Notification.Name(rawValue: "AvatarSubmitted"), object: nil)
        
    }
    //MARK: - Methods

    func setRightBarButton() {
        let add = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(addTapped))
        add.setBackgroundImage(UIImage(named: "addBtn"), for: .normal, barMetrics: .default)
        navigationItem.rightBarButtonItems = [add]
    }
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    @objc func avatarUpdated (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.title = constantsStrings.myAvatarScreenTitle
            self.getListofMyAvatar()
        }
        
    }
    
    @objc func avatarSubmitted (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setRightBarButton()
            self.title = constantsStrings.myAvatarScreenTitle
            self.getListofMyAvatar()
        }
        
    }
    
    @objc func addTapped() {
        print("add tapped")
        
        if isAddPressed == false {
            title = constantsStrings.createAvatarScreenTitle
            destroyWebView()
            createWebView()
            webViewController.reloadPage(clearHistory: true)
            webViewController.view.isHidden = false
            navigationItem.rightBarButtonItem = nil
            isAddPressed = true
        }
        else {
            isAddPressed = false
            webViewController.view.isHidden = true
            title = constantsStrings.myAvatarScreenTitle
        }
    }
    
    func getListofMyAvatar() {
        ProgressHUD.show()
        let url = constantsUrl.GET_DETAILS_AVATAR_URL
        let params = ["app_id": "parsl_ios", "namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                
                print("...count is \(json)")
                let dictonary = json as [String: Any]
                let obj = Avatars(fromDictionary: dictonary)
                self.dataArray = obj.data
                self.dataArray.reverse()
                if obj.message == "Success" {
                    if obj.data.count != 0 {
                        DispatchQueue.main.async{
                            self.avatarsTableView.isHidden = false
                            self.avatarsTableView.reloadData()
                            self.swiptLeftOutlet.isHidden = false
                            self.centerLabel.isHidden = true
                            ProgressHUD.dismiss()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.centerLabel.isHidden = false
                            self.swiptLeftOutlet.isHidden = true
                            self.avatarsTableView.isHidden = true
                            ProgressHUD.dismiss()
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.centerLabel.isHidden = false
                        self.swiptLeftOutlet.isHidden = true
                        self.avatarsTableView.isHidden = true
                        ProgressHUD.dismiss()
                    }
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func createWebView(){
        let storyboard = UIStoryboard(name: "MyAvatarStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: webViewIdentifier) as UIViewController

        guard let viewController = controller as? WebViewController else {
            return
        }
        webViewController = viewController
        webViewController.avatarUrlDelegate = self
        
        addChild(controller)

        self.view.addSubview(controller.view)
        controller.view.frame = view.safeAreaLayoutGuide.layoutFrame
        controller.view.tag = webViewControllerTag
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controller.didMove(toParent: self)
    }
    
    func avatarUrlCallback(url: String){
        
        webViewController.view.isHidden = true
        let pasteboardString: String? = UIPasteboard.general.string
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubmitAvatarViewController")as! SubmitAvatarViewController
        vc.avatarUrl = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: constantsStrings.avatarUrlGenerated, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
             })
             alert.addAction(okButton)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func destroyWebView(){
        if let viewWithTag = self.view.viewWithTag(webViewControllerTag) {
            
            webViewController.dismiss(animated: true, completion: nil)
            viewWithTag.removeFromSuperview()
        }else{
            print("No WebView to destroy!")
        }
    }
    
    func editUserAvatar () {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SubmitAvatarViewController") as! SubmitAvatarViewController
        vc.isfrom = "edit"
        vc.avatarId = avatarId
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteUserAvatar () {
        
        ProgressHUD.show()
        let url = constantsUrl.DELETE_AVATAR_URL
        
        let params = ["namespace": ParslUtils.sharedInstance.getUserNameSpace(), "avatar_id": self.avatarId] as [String: AnyObject]
        print(params)
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]

                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        self.view.makeToast(constantsStrings.deleted, duration: 1.0, position: .bottom, style: self.self.style)
                        ProgressHUD.dismiss()
                        self.getListofMyAvatar()
                    }
                }
                else {
                    ProgressHUD.dismiss()
                    self.showAlert(withMessage: message)
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
    //MARK: - IBActions

}
