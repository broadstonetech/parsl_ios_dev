//
//  MyAvatarsTableViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 28/12/2021.
//

import UIKit

class MyAvatarsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var avatarTitle: UILabel!
    @IBOutlet weak var avatarStatus: UILabel!
    @IBOutlet weak var avatarThumbNail: UIImageView!
    
    @IBOutlet weak var avatarDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainView.layer.cornerRadius = 7
        self.mainView.clipsToBounds = true
    }


}
