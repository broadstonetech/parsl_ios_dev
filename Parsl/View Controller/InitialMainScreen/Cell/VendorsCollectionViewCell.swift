//
//  VendorsCollectionViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 01/11/2021.
//

import UIKit

class VendorsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!    //Only for Vendors Collection view, donot use this in category list.
    
    var imagePath: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
