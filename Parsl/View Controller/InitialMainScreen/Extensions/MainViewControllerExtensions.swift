//
//  MainViewControllerExtensions.swift
//  Parsl
//
//  Created by M Zaryab on 23/02/2022.
//

import Foundation
import UIKit
import AVKit
import StoreKit

// MARK: - Extensions
//MARK: -CollectionViews
extension MainViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == mainCategoryCollectionView{
            return 1
        }
        else if collectionView == vendorsCollectionView{
            return 1
        }
        else if collectionView == subCategoryCollectionView{
            return 1
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainCategoryCollectionView {
            
            if mainCategoryList == nil {
                return 0
            }
            else {
                print("maincategory count....\(mainCategoryList.data!.count)")
                return mainCategoryList.data!.count
            }
        } else if collectionView == subCategoryCollectionView {
            if isRedeem == false {
                if isSearching {
                    return searchResult.count
                }else {
                    if vendorSelectedIndex == -1 {
                        return 0
                    }else {
                        if self.isMyAvatarSelected == false{
                        return modelDataSturctArray.count
                        } else {
                            self.isMyAvatarSelected = false
                            return liveAvatarData.count
                            
                        }
                    }
                    
                }
            }
            //for redeem process
            else{
                return parslRedeemModel.data.parslModels.count
            }
        } else if collectionView == vendorsCollectionView {
            if vendersList != nil {
                print("Vendors count \(vendersList.data.vendorsList.count)")
                return vendersList.data.vendorsList.count
            } else {
                return 0
            }
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainCategoryCollectionView {
            let cell = mainCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "MainCategoryCollectionViewCell", for: indexPath) as! MainCategoryCollectionViewCell
            cell.titleLabel.text = "  \(mainCategoryList.data![indexPath.row].categoryTitle!)  "
            
            cell.selectedView.isHidden = true
            
            if selectedIndex == indexPath.row
            {
                cell.parentView.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.6666666667, blue: 0.7647058824, alpha: 1)
                cell.titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            else
            {
                cell.parentView.backgroundColor = UIColor(named: "CardDarkMode")!
                cell.titleLabel.textColor = UIColor(named: "BlackWhiteColor")!
            }
            return cell
        }
         else if collectionView == subCategoryCollectionView {
            let cell = subCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoriesCollectionViewCell", for: indexPath) as! SubCategoriesCollectionViewCell

            cell.sybCategoriesView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMinXMinYCorner], radius: 8)
            cell.thumbnail.roundCorners(corners: [.layerMinXMaxYCorner, .layerMinXMinYCorner], radius: 5)
            if isRedeem == false {
                
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    var item: ModelDataStruct
                    if isSearching {
                        item = searchResult[indexPath.row]
                    }else {
                        item = modelDataSturctArray[indexPath.row]
                    }
                    cell.imagePath = item.modelFile.thumbnail
                    let url = URL(string: cell.imagePath)
                    cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                    cell.titleLabel.text = String(item.basicInfo.name)
                    cell.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)

                    return cell
                } else {
                    var item: LiveAvatarData
                    if isSearching {
                        item = searchForLiveAvatar[indexPath.row]
                    }else {
                        item = liveAvatarData[indexPath.row]
                    }
                    cell.imagePath = item.thumbnails
                    let url = URL(string: cell.imagePath)
                    cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                    cell.titleLabel.text = item.title
                    cell.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: Double(item.priceRelated.price ?? 0))
                    return cell
                }
            }
            else {

                let item = parslRedeemModel.data.parslModels[indexPath.row]
                cell.imagePath = item.modelFile.thumbnail
                let url = URL(string: cell.imagePath)
                cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                cell.titleLabel.text = String(item.basicInfo.name)
                cell.selectedImage.isHidden = true
                cell.priceLabel.isHidden = true
                return cell
            }
        }
         else if collectionView == vendorsCollectionView {
            let cell = vendorsCollectionView.dequeueReusableCell(withReuseIdentifier: "VendorsCollectionViewCell", for: indexPath) as! VendorsCollectionViewCell
            cell.titleLabel.text = vendersList.data.vendorsList[indexPath.row].vendorName
//             cell.titleLabel.text = self.vendors[indexPath.row].vendorName!
//            cell.selectedView.isHidden = true
            
            let url = URL(string: vendersList.data.vendorsList[indexPath.row].otherInfo.logoURL)
            cell.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            
             if vendorSelectedIndex == indexPath.row && self.subcategoriesHeight.constant != 0{
                cell.parentView.backgroundColor = #colorLiteral(red: 0.6505359792, green: 0.5087047048, blue: 1, alpha: 1)
                cell.titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            else{
                cell.parentView.backgroundColor = UIColor(named: "CardDarkMode")!
                cell.titleLabel.textColor = UIColor(named: "BlackWhiteColor")!
                
                
            }
            let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(vendorLongTouched))
            cell.parentView.isUserInteractionEnabled = true
            cell.parentView.addGestureRecognizer(longTapGesture)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    @objc private func vendorLongTouched(){
        if vendorSelectedIndex == -1 {
            return
        }
        let vendorInfo = vendersList.data.vendorsList[vendorSelectedIndex].otherInfo
        vendorDetailDialog.vendorTitle.text = vendersList.data.vendorsList[vendorSelectedIndex].vendorName
        vendorDetailDialog.vendorSlogan.text = vendorInfo.slogan
        vendorDetailDialog.vendorDesc.text = vendorInfo.otherInfoDescription
        
        let url = URL(string: vendorInfo.logoURL)
        vendorDetailDialog.vendorLogo.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        
        
        blurView.isHidden = false
        vendorDetailDialog.isHidden = false
        
    }
     
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == mainCategoryCollectionView {
            self.isSearching = false
            self.searchBar.searchTextField.text = ""
            selectedIndex = indexPath.row
            self.isfromMain = true
            self.subcategoriesHeight.constant = 90
            getDownloadedVendorListOrCallAPI(categoryIndex: selectedIndex)
            
            UIView.animate(withDuration: 1, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else if collectionView == vendorsCollectionView {
            vendorSelectedIndex = indexPath.row
            self.isSearching = false
            self.searchBar.searchTextField.text = ""
            self.isfromMain = false
            getDownloadedModelsListOrCallAPI(isCallforVendor: true)
            self.subcategoriesHeight.constant = 90
            self.isSearchBarShow = true
            self.searchBar.isHidden = false
            actionButton.removeFromSuperview()
            view.addSubview(actionButton)
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -8).isActive = true

            UIView.animate(withDuration: 1, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
                       
            
        }
            else if collectionView == subCategoryCollectionView {

            if isRedeem == false {
                if !(self.vendersList.data.vendorsList.isEmpty) {
                self.mainPresentedView.isHidden = true
                var item: ModelDataStruct
                if isSearching {
                    item = searchResult[indexPath.row]
                }else {
                    item = modelDataSturctArray[indexPath.row]
                }

                prices = item.priceInfo.price
                let defaultShadows = item.defaultShadow!
                selectedModelIndex = indexPath.row
                modelID = item.productId // hasFace/productId
                ParslUtils.sharedInstance.eventsCallForFRAnalytics(envetName: "Selected Model for send", detailsOfEvent: ["Type": "Send Parsl", "Message": "Models Id \(String(describing: modelID)) , Model Name  \(String(item.basicInfo.name))", "DeviceId": ParslUtils.sharedInstance.DEVICE_TOKEN as Any])
                    
                    
                    self.sendMSg(ID: modelID!, defaultShadows: defaultShadows, face: "noFace")
                    let result = modelID?.contains("has")
                    debugPrint(result ?? false)
                    
                    if result == true {
                        modelIDWithoutHas = modelID ?? ""
                        while true {
                            modelIDWithoutHas = String(modelIDWithoutHas.dropFirst())
                            if String(modelIDWithoutHas.stringAt(0))  == "/" {
                                modelIDWithoutHas = String(modelIDWithoutHas.dropFirst())
                                modelID = modelIDWithoutHas
                                break
                            }
                        }
                    }
                    debugPrint(modelID as Any)
                    modelsInView[modelID!] = item
                    presentedTextView.text = item.description
                } else {
                    self.mainPresentedView.isHidden = true
                    var item: LiveAvatarData
                    if isSearching {
                        item = searchForLiveAvatar[indexPath.row]
                    }else {
                        item = liveAvatarData[indexPath.row]
                    }
                    
                    var id = item.url
                    var type = item.whoami
                    selectedModelIndex = indexPath.row
                    ParslUtils.sharedInstance.eventsCallForFRAnalytics(envetName: "Selected Model for send", detailsOfEvent: ["Type": "Send Parsl", "Message": "Models Id \(String(describing: id)) , Model Name  \(String(item.title))", "DeviceId": ParslUtils.sharedInstance.DEVICE_TOKEN as Any])
//                    modelsInView[modelID!] = item
                    projectionForJLB(ID: id, defaultShadows: false, whoAmI: type)
                    presentedTextView.text = item.datumDescription
                }
            } else {
                selectedIndex = indexPath.row
                if selectedIndex != 0{
                    let redeemModelID = parslRedeemModel.data.parslModels[selectedIndex].productId
                    self.sendMSg(ID: redeemModelID!, defaultShadows: parslRedeemModel.data.parslModels[selectedIndex].defaultShadow, face: "noface")

                }
            }
        }
    }
    
    private func getDownloadedModelIndexForRedeem(_ model: String) -> Int {
        var index = -1
        for i in 0..<downloadedModelsForRedeem.count {
            if downloadedModelsForRedeem[i]._3dModelFile == model {
                index = i
                break
            }
        }

        return index
    }
     
    private func getDownloadedVendorListOrCallAPI(categoryIndex: Int) {
        var isFound = false
        for i in 0..<downloadedVendors.count {
            if downloadedVendors[i].categoryIndex == categoryIndex {
                isFound = true
                self.vendersList = downloadedVendors[i].vendorsList
                vendorSelectedIndex = 0
                break
            }
        }
        
        if isFound {
            // get already downloaded list
            DispatchQueue.main.async {
                self.getDownloadedModelsListOrCallAPI(isCallforVendor: false)
            }
        }else {
            DispatchQueue.main.async {
            ProgressHUD.animationType = .circleRotateChase
            ProgressHUD.show("Loading")
            
                let selectedItem = (self.mainCategoryList.data?[categoryIndex].categoryKey)!
            print("selected item is ....\(selectedItem)")
                // call method vendors api.....
                self.getCategoryVendors(categoryName: selectedItem)
        }
        }
    }
    
    private func getDownloadedModelsListOrCallAPI(isCallforVendor: Bool) {
        var isFound = false
        
        for i in 0..<downloadedData.count {
            if downloadedData[i].categoryIndex == selectedIndex && downloadedData[i].vendorIndex == vendorSelectedIndex {
                isFound = true
                
                self.modelDataSturctArray = downloadedData[i].modelsData
                print(".......\(vendorSelectedIndex)")
                break
            }
        }
        
        if isFound {
            DispatchQueue.main.async {
                if !isCallforVendor {
                    self.vendorSelectedIndex = 0
                }
                
                
                self.mainCategoryCollectionView.reloadData()
                if self.vendersList.data.vendorsList.count == 1 {
                    self.vendorsCollectionView.reloadSections(IndexSet.init(integer: 0))
                }
                else {
                    self.vendorsCollectionView.reloadData()
                }
                self.subCategoryCollectionView.reloadData()
            }
        }else {
            ProgressHUD.animationType = .circleRotateChase
            ProgressHUD.show("Loading")
            let categoryKey = (mainCategoryList.data?[selectedIndex].categoryKey)!
            let vendorKey = self.vendersList.data.vendorsList[vendorSelectedIndex].vendorID
            getSubCategories(key: categoryKey, vendor: vendorKey!)
            
//            DispatchQueue.main.async {
//                self.view.isUserInteractionEnabled = false
//            }
        }
    }
    
    private func getIndexIfModelAlreadyDownloaded(modelId: String) -> Int {
        var index = -1
        for i in 0..<modelsDownloadedForSend.count {
            if modelId == modelsDownloadedForSend[i]._3dModelFile {
                index = i
                break
            }
        }
        
        return index
    }
    
}



//MARK: -TableViews
extension MainViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        if isVideoMessage{
            return 5
        }else
        {

            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isRedeem && isVideoMessage {
            
            if section == 3{
                return parslRedeemModel.data.parslModels.count
            }else{
                return 1
            }
       
        } else if isRedeem && !isVideoMessage {
            
            if section == 2 {
                return parslRedeemModel.data.parslModels.count
            }else{
     
                return 1
            }
    
        } else {
            return 0
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("section and row is\(indexPath.section)and \(indexPath.row)")
        if isVideoMessage {
            if indexPath.section == 3 {
                var isFound:Bool = false
                var buttonTag :String = ""
                let index = indexPath.row
                buttonTag = parslRedeemModel.data.parslModels[index].productId
                for id in selectedModelForDownload{
                    if id == buttonTag{
                      //model already projected
                        isFound = true
                        
                        break
                    }else{
                        isFound = false
                        
                    }

                }
                self.RedeemHalfView.isHidden = true
                sendRedeemMsg(ID: buttonTag)
                self.redeemAnimatingView.isHidden = false
            }
        }
        if isVideoMessage == false {
            if indexPath.section == 2 {
                var isFound:Bool = false
                var buttonTag :String = ""
                let index = indexPath.row
                buttonTag = parslRedeemModel.data.parslModels[index].productId
                for id in selectedModelForDownload{
                    if id == buttonTag{
                      //model already projected
                        isFound = true
                        
                        break
                    }else{
                        isFound = false
                        
                    }

                }
                self.RedeemHalfView.isHidden = true
                sendRedeemMsg(ID: buttonTag)
                self.redeemAnimatingView.isHidden = false
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "virtualCardCell", for: indexPath) as! RedeemTableViewCell
        cell.redeemgiftAmountLabel.text = redeemgiftAmount ?? ""
        
          if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "redeemMessageCell", for: indexPath) as! RedeemTableViewCell
            cell.messageTextView.text =  redeemMessage
            
            return cell
            
        }
        else if indexPath.section == 1 {
            
            if isVideoMessage {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "redeemVideoCell", for: indexPath) as! RedeemTableViewCell
                cell.downloadedImage.addTarget(self, action: #selector(self.playRedeemVideoTapped(sender:)), for: .touchUpInside)
                cell.downloadedImage.tag = indexPath.row
                return cell
            } else {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "RightLabelCell", for: indexPath) as! RedeemTableViewCell
                
                return cell
                //model table view
                /*cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell*/
            }
        }
        else if indexPath.section == 2 {
            
            
            
            if isVideoMessage {
                cell = tableView.dequeueReusableCell(withIdentifier: "RightLabelCell", for: indexPath) as! RedeemTableViewCell
                
                return cell
                
                //model table view
                /*cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell*/
            } else {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell
                /*//add to wallet
                cell = tableView.dequeueReusableCell(withIdentifier: "redeemAddWalletCell", for: indexPath) as! RedeemTableViewCell
                cell.downloadedImage.layer.cornerRadius = 10
                cell.downloadedImage.clipsToBounds = true
                cell.downloadedImage.addTarget(self, action: #selector(self.addToWalletBtnTapped(sender:)), for: .touchUpInside)
                cell.downloadedImage.tag = indexPath.row
//                cell.downloadedImage.backgroundColor =  UIColor(named: "#35AAC3" )
//                    cell.downloadedImage.setTitle("Add to Walled", for: .normal)*/
//                return cell
                
            }
            
        } else if indexPath.section == 3 {
            if isVideoMessage {
                cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "virtualCardCell", for: indexPath) as! RedeemTableViewCell
                cell.redeemgiftAmountLabel.text = redeemgiftAmount ?? ""
                return cell
            }
            
        } else {
        return cell
        }
    }
                
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 4 {
//            return 140
//        } else {
        return UITableView.automaticDimension
//            }
    }
    
    @objc func modelDownloadTapped(sender: UIButton) {
        var isFound:Bool = false
        let index = sender.tag
        var buttonTag :String = ""
        
        buttonTag = parslRedeemModel.data.parslModels[index].productId
        for id in selectedModelForDownload{
            if id == buttonTag{
              //model already projected
                isFound = true
                
                break
            }else{
                isFound = false
                
            }

        }
        
        self.RedeemHalfView.isHidden = true
        sendRedeemMsg(ID: buttonTag)
        self.redeemAnimatingView.isHidden = false
    }
    
    @objc func playRedeemVideoTapped(sender: UIButton){
        let url = URL(string: parslRedeemModel.data.receiverData.videoMsg)!
        playServerVideo(url: url)
    }
    
    @objc func addToWalletBtnTapped(sender: UIButton){
    
    let vc  = UIStoryboard(name: "RedeemCardStoryBoard", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
    vc?.delegate = self
    self.navigationController?.present(vc!, animated: true, completion: nil)
    }
}




//MARK: -Extensions

extension MainViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search(searchBar)
        self.searchBar.endEditing(true)
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        search(searchBar)
    }
    
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchResult.removeAll()
        isSearching = false
       // self.searchBar.isHidden = true
        DispatchQueue.main.async {
            self.subCategoryCollectionView.reloadData()
        }
    }
    
    func search(_ searchBar: UISearchBar) {
        let searchText = searchBar.text?.lowercased()
        if searchText?.count ?? 0 > 2 {
            isSearching = true
            searchResult = modelDataSturctArray.filter({$0.basicInfo.name.lowercased().contains(searchText!) }) 
            print(searchResult)
            if searchResult.isEmpty {
                self.view.makeToast("Model not found", duration: 0.5, position: .center, completion: nil)
            } else {
                self.view.hideToast()
            }
            
            
        }else {
            isSearching = false
        }
        
        DispatchQueue.main.async {
            self.subCategoryCollectionView.reloadData()
        }
        
    }
}

//extension float4x4 {
//    var translation: simd_float3 {
//        let translation = self.columns.3
//        return simd_float3(translation.x, translation.y, translation.z)
//    }
//}
extension UIColor {
    open class var transparentLightBlue: UIColor {
        return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.50)
    }
}


extension MainViewController {
     func fetchModelsApiData(data: [AnyObject]){
        modelDataSturctArray.removeAll()
        for i in 0..<data.count {
            print(i)
            let dataObj = data[i] as! [String: AnyObject]
            let modelDataStruct = fetchSingleModel(dataObj: dataObj)
            modelDataSturctArray.append(modelDataStruct)
        }
        
        let downloadedDataStruct = DownloadedDataForCatAndVendors(categoryIndex: selectedIndex, vendorIndex: vendorSelectedIndex, modelsData: modelDataSturctArray)
        
        downloadedData.append(downloadedDataStruct)
    }
    
     func fetchSingleModel(dataObj: [String: AnyObject]) -> ModelDataStruct {
        let modelFile = getModelFile(modelFile: dataObj["model_files"] as! [String: AnyObject])
        let basicInfo = getModelBasicInfo(basicInfo: dataObj["basic_info"] as! [String: AnyObject])
        let priceInfo = getModelPriceInfo(priceInfo: dataObj["price_related"] as! [String: AnyObject])
        let scalingInfo = getModelScalingInfo(scalingInfo: dataObj["scaling"] as! [String: AnyObject])
        
        let hyperlink = dataObj["hyperlink"] as! String
        let document = dataObj["document"] as! String
        let sampleVideo = dataObj["sample_video"] as! String
        let promoVideo = dataObj["promotional_video"] as! String
        let images = dataObj["images"] as! [String]
        let multiTexture = dataObj["multi_texture"] as! Int
        let licenceSource = dataObj["license_source"] as! String
        
        let action = getModelActions(actions: dataObj["actions"] as! [String: String])
        
        let video = dataObj["video"] as! String
        let analytics = getModelAnalytics(analytics: dataObj["analytics"] as! [String: AnyObject])
        let category = dataObj["category"] as! String
        let manufacturingDate = dataObj["manufacturing_date"] as! String
        let ownerId = dataObj["owner_id"] as! String
        let purchaseStatus = dataObj["purchase_status"] as! Int
        let license = dataObj["license"] as! String
        let description = dataObj["description"] as! String
        let addlInfo = dataObj["additional_info"] as! String
        let multinode = dataObj["multi_node"] as! Int
        let questionnaire = dataObj["questionnaire"] as! [String]
        let comments = dataObj["comment_feed"] as! [String]
        let metaTags = dataObj["meta_tags"] as! String
        let visionMessage = dataObj["vision_message"] as! String
        let subCategory = dataObj["sub_category"] as! [String]
        let testedStatus = dataObj["tested_status"] as! Double
        let modelIndex = dataObj["model_index"] as! Int
        let cryptoInfo = getModelCryptoInfo(cryptoInfo: dataObj["cryptographic_info"] as! [String: AnyObject])
        let format = dataObj["format"] as! String
        let productId = dataObj["product_id"] as! String
        let defaultShadow = (dataObj["default_shadows"] as? Bool) ?? true
        let modelId = dataObj["model_id"] as! String
        let isFavourite = dataObj["isFavorite"] as! Int
        let textureFile = getModelTextureFile(textureFile: dataObj["texture_file"] as! [AnyObject])
        
        let modelDataStruct = ModelDataStruct(modelFile: modelFile, basicInfo: basicInfo, priceInfo: priceInfo, scalingInfo: scalingInfo, hyperlink: hyperlink, document: document, sampleVideo: sampleVideo, promoVideo: promoVideo, images: images, multiTexture: multiTexture, licenseSource: licenceSource, modelActions: action, video: video, analytics: analytics, category: category, manufacturingDate: manufacturingDate, ownerId: ownerId, purchaseStatus: purchaseStatus, license: license, description: description, additionalInfo: addlInfo, multiNode: multinode, questionnaire: questionnaire, commentFeed: comments, metaTags: metaTags, visionMessage: visionMessage, subCategory: subCategory, testedStatus: testedStatus, modelIndex: modelIndex, cryptographicsInfo: cryptoInfo, format: format, productId: productId, defaultShadow: defaultShadow,  modelId: modelId, isFavourite: isFavourite, textureFiles: textureFile, isSelectedByUser: false)
        print(".......\(modelDataStruct)")
        return modelDataStruct
    }
    
     func getModelFile(modelFile: [String: AnyObject]) -> ModelFileStruct {
        let thumbnail = modelFile["thumbnail"] as! String
        let audioFile = modelFile["audio_file"] as! String
        let textureType = modelFile["textures_type"] as! String
        let textures = modelFile["textures"] as! [String]
        let multiModelTextureList = modelFile["multitexture_models_list"] as! [String]
        let _3dModelFile = modelFile["3d_model_file"] as! String
        
        return ModelFileStruct(thumbnail: thumbnail, audioFile: audioFile, textureType: textureType, textures: textures, multiTexturesModelFile: multiModelTextureList, _3dModelFile: _3dModelFile)
    }
    
     func getModelBasicInfo(basicInfo: [String: AnyObject]) -> ModelBasicInfoStruct {
        let name = basicInfo["name"] as! String
        let manufacturer = basicInfo["manufacturer"] as! String
        let modelType = basicInfo["model"] as! String
        
        return ModelBasicInfoStruct(name: name, manufacturer: manufacturer, modelType: modelType)
    }
    
     func getModelPriceInfo(priceInfo: [String: AnyObject]) -> ModelPriceInfoStruct {
        let price = priceInfo["price"] as! Double
        let currency = priceInfo["currency"] as! String
        let priceStatus = priceInfo["price_status"] as! String
        
        return ModelPriceInfoStruct(price: price, currency: currency, priceStatus: priceStatus)
    }
    
     func getModelScalingInfo(scalingInfo: [String: AnyObject]) -> ModelScalingStruct {
        let scaleUnit = scalingInfo["scale_unit"] as! String
        let minScale = getScaling(scalingInfo: scalingInfo["min_scale"] as! [String: Double])
        let maxScale = getScaling(scalingInfo: scalingInfo["max_scale"] as! [String: Double])
        
        return ModelScalingStruct(scaleUnit: scaleUnit, minScale: minScale, maxScale: maxScale)
    }
    
     func getScaling(scalingInfo: [String: Double]) -> ScalingAxisStruct {
        let xAxis = scalingInfo["x-axis"]
        let yAxis = scalingInfo["y-axis"]
        let zAxis = scalingInfo["z-axis"]
        
        return ScalingAxisStruct(xAxis: xAxis, yAxis: yAxis, zAxis: zAxis)
    }
    
     func getModelActions(actions: [String: String]) -> ModelActionStruct {
        let actionType = actions["action_type"]
        let actionTitle = actions["action_title"]
        let actionData = actions["action_data"]
        
        return ModelActionStruct(actionType: actionType, actionTitle: actionTitle, actionData: actionData)
    }
    
     func getModelAnalytics(analytics: [String: AnyObject]) -> ModelAnalyticsStruct {
        let shareCount = analytics["shares_count"] as! Int
        let downloadCount = analytics["download_count"] as! Int
        let searchCount = analytics["search_appearances"] as! Int
        let survey = analytics["survey"] as! [String]
        
        return ModelAnalyticsStruct(shareCount: shareCount, downloadCount: downloadCount, searchCount: searchCount, questions: survey)
    }
    
     func  getModelCryptoInfo(cryptoInfo: [String: AnyObject]) -> ModelCryptoStruct {
        let digitalSig = cryptoInfo["digital_sig"] as! [Int]
        let publicKey = cryptoInfo["public_key"] as! [Int]
        let modelSha = cryptoInfo["model_sha1"] as! String
        let unzippedSize = cryptoInfo["unzipped_size"] as! Int64
        let zippedSize = cryptoInfo["zipped_size"] as! Int64
        
        return ModelCryptoStruct(digitalSig: digitalSig, publicKeys: publicKey, modelSHA: modelSha, unzippedSize: unzippedSize, zippedSize: zippedSize)
     }
    
     func getModelTextureFile(textureFile: [AnyObject]) -> [ModelTextureFileStruct] {
        var textures = [ModelTextureFileStruct]()
        for i in 0..<textureFile.count {
            let texture = textureFile[i] as! [String: AnyObject]
            
            let title = texture["title"] as! String
            let defaultTexture = texture["default"] as! Int
            let texturesList = texture["texturesList"] as! [String]
            
            textures.append(ModelTextureFileStruct(title: title, defaultTextures: defaultTexture, texturesList: texturesList))
            
        }
        
        
        return textures
    }
    
    
    //Redeem Parsl API parser methods
    
     func
    fetchRedeemModelStruct(response: [String: AnyObject]) -> RedeemModelStruct {
        let message = response["message"] as! String
        if message.contains("Success") {
            let data = fetchRedeemModelDataStruct(dataObj: response["data"] as! [String: AnyObject])
            return RedeemModelStruct(message: message, data: data)
        }
        
        return RedeemModelStruct(message: message, data: nil)
        
    }
    
     func fetchRedeemModelDataStruct(dataObj: [String: AnyObject]) -> RedeemModelData {
        let giftModel = fetchSingleModel(dataObj: dataObj["gift_model"] as! [String: AnyObject])
        let stripeToken = dataObj["parsl_stripe_token"] as! String
        let senderData = fetchSenderData(senderData: dataObj["sender_data"] as! [String: String])
        let receiverData = fetchReceiverData(receiverData: dataObj["reciever_data"] as! [String: AnyObject])
        let timestamp = dataObj["parsl_timestamp"] as? Int64 ?? 0
        let otp = dataObj["parsl_otp"] as! String
        let parslAvailableFlag = dataObj["parsl_available_flag"] as! Bool
        //let parslRedeemed = dataObj["parsl_redeemed"] as! Bool
        let parslRedeemed = false
        let parslId = dataObj["parsl_id"] as! String
        
        let avatarCharacter = dataObj["avatar_chracter"] as! String
        let avatarAnimation = dataObj["avatar_animation"] as! String
        
        var parslModelsArray = [ModelDataStruct]()
        
        let parslModels = dataObj["parsl_models"] as! [AnyObject]
        
        for i in 0..<parslModels.count {
            let parslModel = fetchSingleModel(dataObj: parslModels[i] as! [String: AnyObject])
            parslModelsArray.append(parslModel)
        }
        
        return RedeemModelData(giftModel: giftModel, stripeToken: stripeToken, senderData: senderData, receiverData: receiverData, timestamp: timestamp, otp: otp, availableFlag: parslAvailableFlag, parslRedeemed: parslRedeemed, parslId: parslId, parslModels: parslModelsArray,avatarCharacter: avatarCharacter,avatarAnimation: avatarAnimation)
    }
    
     func fetchSenderData(senderData: [String: String]) -> SenderDataStruct {
        let senderName = senderData["sender_name"]
        let cardNumber = senderData["card_number"]
        let cardType = senderData["card_type"]
        let senderEmail = senderData["sender_email"]
        let transactionId = senderData["transection_id"]
        
        return SenderDataStruct(senderName: senderName,cardNumber: cardNumber, cardType: cardType, senderEmail: senderEmail, transactionId: transactionId)
    }
    
     func fetchReceiverData(receiverData: [String: AnyObject]) -> ReceiverDataStruct {
        let textMsg = receiverData["text_msg"] as? String
        let videoMsg = receiverData["video_msg"] as? String
        let isRedeemed = receiverData["redeemed_by_reciever"] as? Bool
        
        return ReceiverDataStruct(textMsg: textMsg, videoMsg: videoMsg, isRedeemedByUser: isRedeemed)
        
    }
}

extension MainViewController {
     func openBottomActionSheetForDataSorting() {
        let optionMenu = UIAlertController(title: nil, message: "Sort by", preferredStyle: .actionSheet)
        
        
        let priceHighLow = UIAlertAction(title: "Price (High to Low)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "H_L"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.priceInfo.price > $1.priceInfo.price
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        
        
        let priceLowHigh = UIAlertAction(title: "Price (Low to High)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "L_H"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.priceInfo.price < $1.priceInfo.price
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        
        let alphaAZ = UIAlertAction(title: "Alphabetically (A - Z)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "A_Z"
            
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.basicInfo.name < $1.basicInfo.name
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        let alphaZA = UIAlertAction(title: "Alphabetically (Z - A)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "Z_A"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.basicInfo.name > $1.basicInfo.name
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
         
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        if selectedFilter == "H_L" {
            setSelectedCheckmark(action: priceHighLow)
        }else if selectedFilter == "L_H" {
            setSelectedCheckmark(action: priceLowHigh)
        } else if selectedFilter == "A_Z" {
            setSelectedCheckmark(action: alphaAZ)
        } else if selectedFilter == "Z_A" {
            setSelectedCheckmark(action: alphaZA)
        }
        
        
        optionMenu.addAction(priceHighLow)
        optionMenu.addAction(priceLowHigh)
        optionMenu.addAction(alphaAZ)
        optionMenu.addAction(alphaZA)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func setSelectedCheckmark(action: UIAlertAction){
        
        if #available(iOS 13.0, *) {
            action.setValue(UIImage(systemName: "checkmark"), forKey: "image")
        } else {
            // Fallback on earlier versions
        }
        
    }
}
@objc  extension MainViewController {

    func keyboardWillShow(_ notification: Notification) {
        DispatchQueue.main.async {
            self.bottomViewBottomMargin.constant = 320
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async {
            self.bottomViewBottomMargin.constant = 0
        }
    }
}

extension MainViewController {
    
    func getVirtualGiftCard(otp: String, receiverName: String, phoneNumber: String) {
        let data = ["parsl":otp as AnyObject,
                    "reciever_name":receiverName as AnyObject,
                    "reciever_phone_no":phoneNumber as AnyObject,
                    "redeemed_timestamp": Date().currentTimeMillis() as AnyObject]
        let parameters = ["namespace":ParslUtils.sharedInstance.getUserNameSpace(),"data": data ] as [String : AnyObject]
        ProgressHUD.show()
        
        print(parameters)
        let url = "https://api.parsl.io/get/virtual/card/details"
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                self.virtualCardModel = try JSONDecoder().decode(VirtualCard.self, from: response)
//                self.data = response["data"] as! [String : Any]
                DispatchQueue.main.async {
                    
                    if virtualCardModel.message == "Success"{
                        ProgressHUD.dismiss()
                        let redeemBy : String!
                        redeemBy = virtualCardModel.data!.cardholder!.name
                        if virtualCardModel.redeemed_by_reciever == true {
                            showAlert(withMessage: "\(constantsStrings.alreadyRedeemed) \(redeemBy ?? "")")
                            
                        } else {
                            let vc = UIStoryboard.init(name: "RedeemCardStoryBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "VirtualCardViewController") as! VirtualCardViewController
                            vc.virtualCardModel = self.virtualCardModel
                            vc.message =   redeemBy!
                            vc.isCardRedeemed = virtualCardModel.redeemed_by_reciever!
                            self.navigationController?.present(vc, animated: true, completion: nil)
                        }
                    }
                    else {
                        ProgressHUD.dismiss()
                        self.showAlert(withMessage: virtualCardModel.message!)
                    }
                }

                
            } catch let error {
                print(error)
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                self.showAlert(withMessage: "Unable to fetch data. Please try again later")
            }
        })
    }
    
}
extension MainViewController: SKStoreProductViewControllerDelegate {
     func getAppVersionApi() {
//        let url = "https://api.parsl.io/check/version"
//        postRequest(serviceName: url, sendData: AppVersionCheck.sharedInstance.getCheckVersionApiParams(), success: { (response) in
//            print(response)
//            do {
//                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: Any]
//
//                let message = (jsonResponse["message"] ?? "") as! String
//                let status = (jsonResponse["status"] ?? true) as!Bool
//                print("VersionAPI response: \(jsonResponse)")
//                if status == false {
//                    if message.contains("App does not exisit") {
//                        print("App does not exist")
//                    }else if message.contains("Invalid app type") {
//                        print("Invalid app type")
//                    }
//                    else{
//                        DispatchQueue.main.async {
//                            self.showAlertViewForVersionCheckAPI()
//                        }
//                    }
//                } else {
//                    self.showAlertViewForVersionCheckAPI()
//                }
//
//                if message.contains("up to date") {
//                    print("App is upto date")
//                }else if message.contains("latest version available") {
//                    print("App latest version available")
//
//                }else {
//                    print("Undetermined result")
//                    print(message)
//                }
//
//
//            } catch let error {
//                print(error)
//            }
//
//        }, failure: { (data) in
//            print(data)
//
//            DispatchQueue.main.async {
//                print("Unable to fetch data of check version api. Please try again later")
//            }
//        })
    }
    
    private func showAlertViewForVersionCheckAPI() {
        let optionMenu = UIAlertController(title: "PARSL", message: "New version of app is available. Kindly update it from App Store.", preferredStyle: .alert)

        let update = UIAlertAction(title: "Update", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
//            self.openStoreProductWithiTunesItemIdentifier("1558433040")   //Parsl identifier
        })

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
        
        optionMenu.addAction(update)
        optionMenu.addAction(cancel)
        self.navigationController?.present(optionMenu, animated: true, completion: nil)
    }

//    private func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
//        let storeViewController = SKStoreProductViewController()
//        storeViewController.delegate = self
//
//        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
//        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
//            if loaded {
//                // Parent class of self is UIViewContorller
//                self?.navigationController?.present(storeViewController, animated: true, completion: nil)
//            }
//        }
//    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}
extension MainViewController: CartEmptyDelegate {
    func cartHasBeenCleared() {
        self.modelsInCart.removeAll()
        self.cartCountLabel.text = "\(modelsInCart.count)"
    }
}


extension AVPlayer{

    var isPlaying: Bool{
        return rate != 0 && error == nil
    }
}
