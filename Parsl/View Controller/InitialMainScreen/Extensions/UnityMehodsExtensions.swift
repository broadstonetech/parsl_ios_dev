//
//  UnityMehodsExtensions.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

// This extensios has methods written at native side by are called by unity.
import Foundation
import UIKit
import Toast_Swift

extension MainViewController {
    
    
    func modelDownloadingDone(_ message: String!) {
        //only when avatar downloading has complete
        if isAvatarDownloaded == true{
//            self.view.makeToast(constantsStrings.viewAvatar)
            self.modelStatusLabel.text = "Tap to view avatar"
            isAvatarDownloaded = false
        }
        
        if isRedeem{
            selectedModelForDownload.append(message)
            tableView.reloadData()
        }
    }
    
    func avatarProjected(_ message: Bool){
        self.modelStatusLabel.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.redeemAnimatingView.isHidden = false
            self.createPulse()
            self.scaleInOutAnimation()
        }
        
    }
    
    func modelProjected(_ message: Bool) { // calls when gift box projected.
        debugPrint("model is projected from redeem screen")
        print(message)
        self.giftShowed = true
        if isModelProjected == false {
            isModelProjected = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if self.parslRedeemModel.data.avatarCharacter != "" {
                    self.modelStatusLabel.text = "Downloading Avatar"
                    self.loadAvatar()
                    self.isAvatarDownloaded = true
                }
                else{
                    self.modelStatusLabel.isHidden = true
                    self.redeemAnimatingView.isHidden = false
                    self.createPulse()
                    self.scaleInOutAnimation()
                }
               }
        }
    }
    
    func modelDownloaded(_ message: Bool) {  // also when gift model is downloaded
        print(message)
        debugPrint("projection done in model downloaded")
        if !animatingCardView{
            self.view.makeToast(constantsStrings.viewModel)
        }
        else {
            self.modelStatusLabel.text = "Tap to the view gift"
            
//            self.view.makeToast(constantsStrings.viewParsl)
        }
        if isRedeem{
            tableView.reloadData()
            if giftShowed == true {
                self.view.makeToast(constantsStrings.viewModel)
                
            }
        }
    }
    
    
    
    // Calls by unity when projection of a model completed
    func modelProjectionDone(_ message: String!){
        print("model projection done",message)
    }
    // Calls by unity to show a view present on native side
    func showContainerView(_ message: String!) {
        self.mainPresentedView.isHidden = true
        self.view.sendSubviewToBack(self.unityView!)
      
    }
    
    func avatarModule(_ message1: String!,forRedeem message2: String!) {        self.view.sendSubviewToBack(self.unityView!)
    }
    
    
    func sendMessage(onReload message: String!) {
        self.redeemAnimatingView.isHidden = true
        modelsInCart.removeAll()
        modelsInView.removeAll()
        self.mainPresentedView.isHidden = true
        cartCountLabel.text = String(modelsInCart.count)
        self.view.sendSubviewToBack(self.unityView!)
    }
    
    // model description will be shown.
    func longTap(_ message: String!) {
       
//        if message == "AvatarM" || message == "AvatarF" {
            for i in 0 ..< liveAvatarData.count {
                if message == liveAvatarData[i].url {
                    self.view.makeToast("\(liveAvatarData[i].title) is selected")
                    return
                }
            }
        
        if !isRedeem {
            self.view.sendSubviewToBack(self.unityView!)
            self.mainPresentedView.isHidden = false
            tappedItem = modelsInView[message]!
            if tappedItem.defaultShadow == false {
                shadowBtn.isUserInteractionEnabled = true
            }
            else {
                shadowBtn.isUserInteractionEnabled = true
            }
            itemNameLabel.text = tappedItem.basicInfo.name
            let imagePath = tappedItem.modelFile.thumbnail
            let url = URL(string: imagePath!)
            
            presentedModelImage.sd_setImage(with: url)
            presentedTextView.text = tappedItem.description
        }
    }
    
    func doubleProjectionState(_ message: String!) {
        print(message as Any)
    }
 
    // close the unitview
    func closeUnityView(_ message: String!) {
        
        self.view.sendSubviewToBack(self.unityView!)
    }
    
    // load and initiliza when first video ends.
    func setUpUnityView() { 
        //load unity
        UnityEmbeddedSwift.showUnity()
        unityView = UnityEmbeddedSwift.getUnityView()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.view.insertSubview(self.unityView!, at: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                if self.isfrom == "MyAvatars"{
                    
                }
                else {
                self.setUpFloatingActionbutton()
                self.getCategory()
                }
            })
        })
    }
    
    // projects the avatar
    func loadAvatar() {
        let avatarCharacter = parslRedeemModel.data.avatarCharacter
        print("avatar character is \(avatarCharacter)")
        let avatarString = avatarCharacter! + "," + parslRedeemModel.data.avatarAnimation
        print("avaatar string is ...\(avatarString)")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "RedeemCharacter", message: avatarString)
    }
    // reload unity in native side
    func reloadUnity() { 
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ReLoadUnity", message: "")
    }
    
    func liveAvatarDownloaded(_ message: Bool) {
        ProgressHUD.dismiss()
        self.view.makeToast(constantsStrings.tapToSceeLiveAvatar, duration: 3.0, position: .bottom, completion: nil)
    }
    func screenTap(_ message: Bool) {
        self.mainPresentedView.isHidden = true
    }
}
