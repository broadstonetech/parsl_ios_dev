//
//  MyParslsTableViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 19/01/2022.
//
// This class designs the cell of the tableview used in my parsl view controller.
import UIKit

class MyParslsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView! 
    @IBOutlet weak var timeStatus: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainView.layer.cornerRadius = 7
        self.mainView.clipsToBounds = true
    }
    
}
