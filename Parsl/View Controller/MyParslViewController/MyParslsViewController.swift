//
//  MyParslsViewController.swift
//  Parsl
//
//  Created by M Zaryab on 19/01/2022.
// this class is used to view the saved parsls otp and select parsls to redeem the
// parsls

import UIKit
import SwiftUI

class MyParslsViewController: BaseViewController {

    // MARK: - IBoutelts
    
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var parslsTableview: UITableView!
    
    // MARK: - Properties
    var dataArray = [MyParslsData]()
    var delegate : SendDataDelegate?
    var parslData = [MyParslsData]()
    
    // MARK: - lifecycles
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarUi()
        setTheme()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        parslsTableview.delegate = self
        parslsTableview.dataSource = self
        parslsTableview.rowHeight = UITableView.automaticDimension
        parslsTableview.reloadData()
        title = constantsStrings.myParslScreenTitle
        notificationNav.goToMyParslScreen = false
        centerLabel.isHidden = true
        self.getListofMyParsls()
 
    }
    // MARK: - Methods
    func setTheme() {
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
        }
        else {
            self.overrideUserInterfaceStyle = .light
        }
    }
    
    func setNavigationBarUi() {
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func getListofMyParsls() {
        ProgressHUD.show()
        let url = constantsUrl.MY_PARSAL_URL
        let Data = ["namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        let params = ["app_id": "parsl_ios", "data": Data] as [String: AnyObject]
        print(params)
        if Reachability.isConnectedToNetwork() {
            postRequest(serviceName: url, sendData: params, success: { (response) in
                do {
                    let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                let message = json["message"] as? String
                if message == "Success" {
                    DispatchQueue.main.async{
                        self.fetchData(response: json["data"] as? [AnyObject] ?? [AnyObject]())
                        self.centerLabel.isHidden = true
                        self.parslData.reverse()
                        self.parslsTableview.reloadData()
                        ProgressHUD.dismiss()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.parslsTableview.isHidden = true
                        self.centerLabel.isHidden = false
                        ProgressHUD.dismiss()
                    }
                    
                }

            } catch {
                ProgressHUD.dismiss()
            }
             
        }, failure: {(error) in
           print(error)
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func fetchData(response: [AnyObject]) {
        for i in 0..<response.count {
            let dataObj = response[i] as! [String: AnyObject]
            let data = fetchModelDataStruct(dataObj: dataObj)
            self.parslData.append(data)
        }
    }

    func fetchModelDataStruct(dataObj: [String: AnyObject]) -> MyParslsData {
        let amount = (dataObj["amount"] as? Float) ?? Float("0")!
        let appId = (dataObj["app_id"] as? String) ?? ""
        let namespace = dataObj["namespace"] as! String
        let timestamp = (dataObj["timestamp"] as? Int64) ?? Int64("1648555642")!
        let parslOtp = dataObj["parsl_otp"] as! String
        let redeemedStatus = dataObj["redeemed_status"] as! Bool
        let senderName = dataObj["sender_name"] as! String
        return MyParslsData(amount: amount,  appId: appId, namespace: namespace, timestamp: timestamp, parslOtp: parslOtp, redeemedStatus: redeemedStatus, senderName: senderName)
    }
}


