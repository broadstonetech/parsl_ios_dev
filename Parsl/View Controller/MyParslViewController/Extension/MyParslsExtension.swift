//
//  Extension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit


// MARK: - Extensions
// MARK: - TableView Delegates
extension MyParslsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parslData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyParslsTableViewCell", for: indexPath) as! MyParslsTableViewCell
        cell.descriptionLbl.text = "\(parslData[indexPath.row].senderName ) sent you a PARSL '\(parslData[indexPath.row].parslOtp )'"
        let status = parslData[indexPath.row].redeemedStatus
        var availStatus = ""
        if status == true {
            availStatus = "Availed"
        }
        else {
            availStatus = "Not availed"
        }
//        let date = NSDate(timeIntervalSince1970: dataArray[indexPath.row].timestamp ?? Double("1643195387")!)
        print("timestamp is \(parslData[indexPath.row].timestamp) \(parslData[indexPath.row].parslOtp)")
        let date = NSDate(timeIntervalSince1970: TimeInterval(parslData[indexPath.row].timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.string(from: date as Date)
        cell.timeStatus.text = "\(availStatus)\n\(strDate)\n..."
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let otp = parslData[indexPath.row].parslOtp 
        ParslUtils.sharedInstance.parslOtp = otp
        debugPrint("parsl otp is \(String(describing: ParslUtils.sharedInstance.parslOtp).debugDescription)")
            ParslUtils.sharedInstance.eventsCallForFRAnalytics(envetName: "Redeem SuccessFully through SaveParsal", detailsOfEvent: ["Type": "Redeem Now", "Message": "Redeem Code is \(ParslUtils.sharedInstance.parslOtp)", "DeviceId": ParslUtils.sharedInstance.DEVICE_TOKEN])
 
        if let navigator = navigationController{
            navigator.popViewController(animated: true)
            navigator.popViewController(animated: false)
        }
        
    }
     
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
