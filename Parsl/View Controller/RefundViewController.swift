//
//  RefundViewController.swift
//  Parsl
//
//  Created by Billal on 22/03/2021.
//

import Foundation
import UIKit
class SupportViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func refundTapped(_ sender: Any) {
        let email =  "refund@parsl.io"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    @IBAction func generalTapped(_ sender: Any) {
        let email = "support@parsl.io"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
}
