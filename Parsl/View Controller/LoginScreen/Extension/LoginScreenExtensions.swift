//
//  LoginScreenExtensions.swift
//  Parsl
//
//  Created by M Zaryab on 23/02/2022.
//

import Foundation
import UIKit
import AuthenticationServices


// MARK: - Extensions
// Related to the apple login.
extension LoginViewController: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    switch authorization.credential {
    case let appleIDCredential as ASAuthorizationAppleIDCredential:
        let user = AppleUserData(credentials: appleIDCredential)
        print(user.debugDescription)
    let userIdentifier = appleIDCredential.user
    let fullName = appleIDCredential.fullName
    let email = appleIDCredential.email ?? ""
    let tokenString = NSString(data: user.token!, encoding: String.Encoding.utf8.rawValue)! 
        self.loginUser(socialLogin: tokenString as String, email: String(describing: email))
    default: break
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
        print(error)
    }
  }
}
