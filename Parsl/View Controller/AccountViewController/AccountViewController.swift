//
//  AccountViewController.swift
//  Parsl
//
//  Created by M Zaryab on 13/12/2021.
// This class gives access the users to visit multiple options of PARSL app to enhance user experience. Let user to change password, let user to watch notifications, let user to create, view avatars and parsls.

import UIKit
import StoreKit
import Toast_Swift
import SwiftUI

class AccountViewController:  BaseViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var themeBtnOutlet: UISwitch!
    @IBOutlet weak var deleteAccountView: UIView!
    @IBOutlet weak var createModelView: UIView!
    @IBOutlet weak var myParslsView: UIView!
    @IBOutlet weak var notificationsCountLbl: UILabel!
    @IBOutlet weak var termsAndConditions: UIView!
    @IBOutlet weak var legalHeight: NSLayoutConstraint!
    @IBOutlet weak var legalView: UIView!
    @IBOutlet weak var privacyView: UIView!
    @IBOutlet weak var registeredEmail: UILabel!
    @IBOutlet weak var signOutIcon: UIImageView!
    @IBOutlet weak var signOutLabel: UILabel!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var checkUpdateView: UIView!
    @IBOutlet weak var appSettingView: UIView!
    @IBOutlet weak var appSettingHeight: NSLayoutConstraint!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var clearCacheView: UIView!
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var qvHeight: NSLayoutConstraint!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var queriesView: UIView!
    @IBOutlet weak var pViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemParslView: UIView!
    @IBOutlet weak var senfParslview: UIView!
    @IBOutlet weak var generalQueryView: UIView!
    @IBOutlet weak var refundView: UIView!
    @IBOutlet weak var liveSupportView: UIView!
    @IBOutlet weak var signOutView: UIView!
    
    
    // MARK: - Properties
    var userEmail :String? = ""
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    var initialheightForIphoneOrIpad = 0
    var heightForiphoneOrIpadBeforeLogin = 0
    
    let method = ImageUPloadMethods()
    
    // MARK: - LifeCycles
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarUI()
        countParslNotifications()
        setTheme()
        method.deleteImagesFolder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersion =  Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String? ?? "x.x"
        let buildNo = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        self.versionLabel.text = "Version: \(appVersion) (\(buildNo))"
        scrollView.layer.cornerRadius = 15
        scrollView.clipsToBounds = true
        setUp()
        makeViewsTabable()
        createNotifications()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setUp()
        } 
    }
    // MARK: - IBactions
    
    @IBAction func setAppTheme(_ sender: Any) {
        self.scrollView.isHidden = true
        self.registeredEmail.isHidden = true
        self.bottomView.isHidden = true
        self.versionLabel.isHidden = true
        ProgressHUD.show()
        self.view.backgroundColor = UIColor.white
        if themeBtnOutlet.isOn {
            if #available(iOS 13.0, *) {
                self.overrideUserInterfaceStyle = .unspecified
                ParslUtils.sharedInstance.saveAutoThemeEnable(true)
            }
            debugPrint("theme button on")
        } else {
            if #available(iOS 13.0, *) {
                self.overrideUserInterfaceStyle = .light
                ParslUtils.sharedInstance.saveAutoThemeEnable(false)
            }
            debugPrint("theme button off")
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            ProgressHUD.dismiss()
            self.scrollView.isHidden = false
            self.registeredEmail.isHidden = true
            self.bottomView.isHidden = false
            self.versionLabel.isHidden = false
            self.view.backgroundColor = UIColor(named: "toastBackColor")
        }
    }
    
    // MARK: - Methods
    
    func deletePreviousFolders() {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: .skipsHiddenFiles)
            print(fileURLs)
//            for fileURL in fileURLs {
//                try FileManager.default.removeItem(at: fileURL)
//                break
//            }
        } catch  { print(error) }
    }
    
    func setTheme() {
        self.scrollView.isHidden = true
        self.registeredEmail.isHidden = true
        self.bottomView.isHidden = true
        self.versionLabel.isHidden = true
        self.view.backgroundColor = UIColor.white
        ProgressHUD.show()
        if ParslUtils.sharedInstance.getAutoThemeEnable() == true {
            self.overrideUserInterfaceStyle = .unspecified
            self.themeBtnOutlet.isOn = true
            
        }
        else {
            self.overrideUserInterfaceStyle = .light
            self.themeBtnOutlet.isOn = false
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            ProgressHUD.dismiss()
            self.scrollView.isHidden = false
            self.registeredEmail.isHidden = false
            self.bottomView.isHidden = false
            self.versionLabel.isHidden = false
            self.view.backgroundColor = UIColor(named: "toastBackColor")
        }
    }
    
    func deleteUser() {
            ProgressHUD.show()
            let url = constantsUrl.Delete_Account
            let params = [ "namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
            print(params)
            if Reachability.isConnectedToNetwork() {
                postRequest(serviceName: url, sendData: params, success: { (response) in
                    DispatchQueue.main.sync {
                        ProgressHUD.dismiss()
                    }
                    do {
                        let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                    let message = json["message"] as? String
                        if message == "User deleted" {
                            DispatchQueue.main.sync {
                                self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.requestForwarded)
                            }
                        }
                } catch {
                    ProgressHUD.dismiss()
                }
                 
            }, failure: {(error) in
                DispatchQueue.main.sync {
                    ProgressHUD.dismiss()
                }
               print(error)
            })
            } else {
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
                }
            }
    }
    
    func countParslNotifications() {
        
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
                    DispatchQueue.main.async { [self] in
                        if notifications.count > 0 {
                            notificationsCountLbl.isHidden = false
                            notificationsCountLbl.text = String(notifications.count)
                        }else {
                            notificationsCountLbl.isHidden = true
                            notificationsCountLbl.text = ""
                        }
                    }
                }
    }
    
    func navigationBarUI() {
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func createNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(takeToAvatarScreen(_:)), name: Notification.Name(rawValue: "MyAvatarLoginCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(joinNowToSignOut(_:)), name: Notification.Name(rawValue: "joinNowLoginCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MyParslLoginPressed(_:)), name: Notification.Name(rawValue: "MyParslLogin"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateModelLoginPressed(_:)), name: Notification.Name(rawValue: "CreateModelLogin"), object: nil)
    }
    func makeViewsTabable () {
        let tapPersonal = UITapGestureRecognizer(target: self, action: #selector(self.showHidePersonalView(_:)))
        personalView.addGestureRecognizer(tapPersonal)
        let tapQuery = UITapGestureRecognizer(target: self, action: #selector(self.showQuesriesView(_:)))
        queriesView.addGestureRecognizer(tapQuery)
        let tapSetting = UITapGestureRecognizer(target: self, action: #selector(self.showAppSetting(_:)))
        appSettingView.addGestureRecognizer(tapSetting)
        let myAvatar = UITapGestureRecognizer(target: self, action: #selector(self.myAvatarPressed(_:)))
        avatarView.addGestureRecognizer(myAvatar)
        let notificationSummary = UITapGestureRecognizer(target: self, action: #selector(self.notificationSummaryPressed(_:)))
        notificationView.addGestureRecognizer(notificationSummary)
        let refund = UITapGestureRecognizer(target: self, action: #selector(self.refundpressed(_:)))
        refundView.addGestureRecognizer(refund)
        let query = UITapGestureRecognizer(target: self, action: #selector(self.queryPressed(_:)))
        generalQueryView.addGestureRecognizer(query)
        let sendParsl = UITapGestureRecognizer(target: self, action: #selector(self.sendParslPressed(_:)))
        senfParslview.addGestureRecognizer(sendParsl)
        let redeemparsl = UITapGestureRecognizer(target: self, action: #selector(self.redeemparslPressed(_:)))
        redeemParslView.addGestureRecognizer(redeemparsl)
        let liveSupport = UITapGestureRecognizer(target: self, action: #selector(self.liveSupportPressed(_:)))
        liveSupportView.addGestureRecognizer(liveSupport)
        let clearCache = UITapGestureRecognizer(target: self, action: #selector(self.clearCachePressed(_:)))
        clearCacheView.addGestureRecognizer(clearCache)
        let update = UITapGestureRecognizer(target: self, action: #selector(self.updatePressed(_:)))
        checkUpdateView.addGestureRecognizer(update)
        let changePassword = UITapGestureRecognizer(target: self, action: #selector(self.changePasswordPressed(_:)))
        changePasswordView.addGestureRecognizer(changePassword)
        let signOut = UITapGestureRecognizer(target: self, action: #selector(self.signOutPressed(_:)))
        signOutView.addGestureRecognizer(signOut)
        let legalTap = UITapGestureRecognizer(target: self, action: #selector(self.legalViewPressed(_:)))
        legalView.addGestureRecognizer(legalTap)
        let privacyTap = UITapGestureRecognizer(target: self, action: #selector(self.privacyTapPressed(_:)))
        privacyView.addGestureRecognizer(privacyTap)
        let tcTap = UITapGestureRecognizer(target: self, action: #selector(self.termsAndConditionsPressed(_:)))
        termsAndConditions.addGestureRecognizer(tcTap)
        let myParslTap = UITapGestureRecognizer(target: self, action: #selector(self.myParslsPressed(_:)))
        myParslsView.addGestureRecognizer(myParslTap)
        let createModel = UITapGestureRecognizer(target: self, action: #selector(self.createModelPressed(_:)))
        createModelView.addGestureRecognizer(createModel)
        let deleteYourAccount = UITapGestureRecognizer(target: self, action: #selector(self.deleteYourAccountPressed(_:)))
        deleteAccountView.addGestureRecognizer(deleteYourAccount)
    }
    
    func signOut() {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
        ParslUtils.sharedInstance.saveUserNameSpace("")
        ParslUtils.sharedInstance.saveUserEmail("")
        ParslUtils.sharedInstance.saveIfLoginScreenShown(false)
        ParslUtils.sharedInstance.saveIfUserLogin(false)
        ParslUtils.sharedInstance.saveUserNumber("")
            DispatchQueue.main.async {
                self.view.makeToast("Signed out successfully")
                self.registeredEmail.text = ""
                self.signOutLabel.text = constantsStrings.signOutSignIn
                self.showHidePersonalView()
            }

        } else {
            let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
           vc.fromSendOrRedeem = "joinNow"
            self.present(vc, animated: true, completion: nil)
        }
    }
    func setUpForIphone() {
        pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        qvHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        appSettingHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        legalHeight.constant = CGFloat(initialheightForIphoneOrIpad)
    }
    
    func setUpForIpad() {
        pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        qvHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        appSettingHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        legalHeight.constant = CGFloat(initialheightForIphoneOrIpad)
    }
    
    func setUp() {
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch deviceIdiom {
        case .phone:
            debugPrint("Its iPhone ")
            initialheightForIphoneOrIpad = 40
            setUpForIphone()
        case .pad:
            debugPrint("Its iPod ")
            initialheightForIphoneOrIpad = 60
            setUpForIpad()
        default:
            debugPrint("Dont Know")
        }
        privacyView.isHidden = true
        avatarView.isHidden = true
        generalQueryView.isHidden = true
        refundView.isHidden = true
        senfParslview.isHidden = true
        redeemParslView.isHidden = true
        clearCacheView.isHidden = true
        checkUpdateView.isHidden = true
        liveSupportView.isHidden = true
        termsAndConditions.isHidden = true
        myParslsView.isHidden = true
        deleteAccountView.isHidden = true
        
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
            signOutLabel.text = constantsStrings.signOut
        }
        else {
            self.registeredEmail.text = ""
            signOutLabel.text = constantsStrings.signOutSignIn
        }
    }
    
    @objc func privacyTapPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
             let url = constantsWebsiteUrl.PRIVACY_POLICY_URL
            UIApplication.shared.open(URL(string: url)!)
        
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    @objc func termsAndConditionsPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
             let url = constantsWebsiteUrl.TERMS_CONDITION_URL
            UIApplication.shared.open(URL(string: url)!)
        
        }
    }
    
    
    @objc func myParslsPressed(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            moveToNextVC(storyBoardName: "AccountRelatedStoryBoard", identifierName: "MyParslsViewController")
        }
        else {
            let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            vc.fromSendOrRedeem = "MyParsls"
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @objc func deleteYourAccountPressed(_ sender: UITapGestureRecognizer? = nil) {
        let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.sureToDeleteAccount, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: constantsStrings.cancelLabel, style: .default, handler: { action in
            self.dismiss(animated: true)
        }))
        alert.addAction(UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
            self.deleteUser()
        }))
        self.present(alert, animated: true, completion: nil)
    }

    @objc func createModelPressed(_ sender: UITapGestureRecognizer? = nil) {
        @StateObject var model = CameraViewModel()
        if ParslUtils.sharedInstance.getifImagesUPloaded() == false {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
                let alertController = UIAlertController(title: constantsStrings.appName, message: constantsStrings.folderNamelabel, preferredStyle: UIAlertController.Style.alert)
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = constantsStrings.enterFolderName
                    }
                let saveAction = UIAlertAction(title: constantsStrings.saveTitle, style: UIAlertAction.Style.default, handler: { alert -> Void in
                        let firstTextField = alertController.textFields![0] as UITextField
                    if firstTextField.text == "" {
                        self.view.makeToast(constantsStrings.nameOfObject)
                        return
                    }
                    ParslUtils.sharedInstance.saveFolderName(firstTextField.text ?? "")
                    self.deletePreviousFolders()
                    let swiftUIView = ContentView(model: model)
                        let hostingController = UIHostingController(rootView: swiftUIView)
            //        navigationController?.pushViewController(hostingController, animated: false)
                    hostingController.modalPresentationStyle = .fullScreen
                    self.present(hostingController, animated: true)
                    
                    })
                let cancelAction = UIAlertAction(title: constantsStrings.cancelLabel, style: UIAlertAction.Style.default, handler: {
                        (action : UIAlertAction!) -> Void in })
                    alertController.addAction(cancelAction)
                    alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
                let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                vc.fromSendOrRedeem = "createModel"
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            showAlert(withMessage: constantsStrings.imagesUPloading)
        }
       
    }
    
    
    
    @objc func signOutPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
        signOut()
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
   
    @objc func legalViewPressed(_ sender: UITapGestureRecognizer? = nil) {
        if legalHeight.constant == CGFloat(initialheightForIphoneOrIpad) {
            legalHeight.constant = CGFloat(initialheightForIphoneOrIpad) * 3
            privacyView.isHidden = false
            termsAndConditions.isHidden = false
        } else {
            privacyView.isHidden = true
            termsAndConditions.isHidden = true
            legalHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        }
    }
    
    @objc func changePasswordPressed(_ sender: UITapGestureRecognizer? = nil) {
        self.moveToNextVC(storyBoardName: "OnBoardingStoryBoard", identifierName: "resetVC")
    }
    
    @objc func myAvatarPressed(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true{
            self.moveToNextVC(storyBoardName: "MyAvatarStoryBoard", identifierName: "MyAvatarsViewController")
        } else {
            let storyboard = UIStoryboard(name: "OnBoardingStoryBoard", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            vc.fromSendOrRedeem = "avatar"
            self.present(vc, animated: true, completion: nil)
        }

    }
    @objc func notificationSummaryPressed(_ sender: UITapGestureRecognizer? = nil) {
        
        self.moveToNextVC(storyBoardName: "AccountRelatedStoryBoard", identifierName: "NotificationsSummryViewController")
        
    }
    @objc func refundpressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            let email =  constantsWebsiteUrl.REFUND_EMAIL_URL
                            if let url = URL(string: "mailto:\(email)") {
                              if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url)
                              } else {
                                UIApplication.shared.openURL(url)
                              }
                            }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    @objc func queryPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            let email = constantsWebsiteUrl.SUPPORT_URL
                           if let url = URL(string: "mailto:\(email)") {
                             if #available(iOS 10.0, *) {
                               UIApplication.shared.open(url)
                             } else {
                               UIApplication.shared.openURL(url)
                             }
                           }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    @objc func sendParslPressed(_ sender: UITapGestureRecognizer? = nil) {
        let vc = UIStoryboard(name: "PlayVideosStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                           vc.videoPath = "send_parsl"
                           self.navigationController!.present(vc, animated: true, completion: nil)
    }
    @objc func redeemparslPressed(_ sender: UITapGestureRecognizer? = nil) {
         let vc = UIStoryboard(name: "PlayVideosStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                           vc.videoPath = "redeem_parsl"
                           self.navigationController!.present(vc, animated: true, completion: nil)

    }
    @objc func liveSupportPressed(_ sender: UITapGestureRecognizer? = nil) {
        self.userEmail = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
                                print("email is",self.userEmail)
                                if self.userEmail == nil  || self.userEmail == "" {
        
                                    let alertController = UIAlertController(title: constantsStrings.liveSupportTitle, message: constantsStrings.enterEmailtoTalkAgent, preferredStyle: .alert)
        
                                    alertController.addTextField { (textField : UITextField!) -> Void in
                                        textField.placeholder = constantsStrings.userEmail
                                    }
        
        
        
                                    let cancelAction = UIAlertAction(title: constantsStrings.cancelLabel, style: .default, handler: nil )
        
                                    alertController.addAction(cancelAction)
        
                                    let saveAction = UIAlertAction(title: constantsStrings.saveTitle, style: .default, handler: { alert -> Void in
                                        let firstTextField = alertController.textFields![0] as UITextField
                                        
                                        
                                        if !self.isValidEmail(firstTextField.text!) {
                                            self.view.makeToast(constantsStrings.enterValidEmail)
                                            return
                                        }else{
                                            UserDefaults.standard.set(firstTextField.text, forKey: "userEmail")
                                            self.userEmail = firstTextField.text
                                            if Reachability.isConnectedToNetwork() {
                                            self.openConversationWindow()
                                            } else {
                                                ProgressHUD.dismiss()
                                                self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
                                            }
                                        }
        
                                    })
                                    alertController.addAction(saveAction)
        
                                    self.present(alertController, animated: true, completion: nil)
                                } else {
                                    if Reachability.isConnectedToNetwork() {
                                    self.openConversationWindow()
                                    } else {
                                        ProgressHUD.dismiss()
                                        self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
                                    }
                                }
    }
    @objc func clearCachePressed(_ sender: UITapGestureRecognizer? = nil) {
        
        var style = ToastStyle()
        self.view.makeToast(constantsStrings.cacheCleared, duration: 3.0, position: .center, style: style)
        
        
    }
    @objc func updatePressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            let url = URL(string: constantsWebsiteUrl.CHECK_UPDATE_URL)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }

    }
    
    @objc func showHidePersonalView(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            if pViewHeight.constant == CGFloat(initialheightForIphoneOrIpad) * 5 {
                avatarView.isHidden = true
                myParslsView.isHidden = true
                pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad)
            } else {
                avatarView.isHidden = false
                myParslsView.isHidden = false
                pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad) * 5
            }
        } else {
            if pViewHeight.constant == CGFloat(initialheightForIphoneOrIpad) * 4
            {
                avatarView.isHidden = true
                myParslsView.isHidden = true
                pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad)
            } else {
                avatarView.isHidden = false
                myParslsView.isHidden = false
                pViewHeight.constant = CGFloat(initialheightForIphoneOrIpad) * 4
            }
        }
        
    }
    @objc func showQuesriesView(_ sender: UITapGestureRecognizer? = nil) {
        if qvHeight.constant == CGFloat(initialheightForIphoneOrIpad) * 6 {
            generalQueryView.isHidden = true
            refundView.isHidden = true
            senfParslview.isHidden = true
            redeemParslView.isHidden = true
            liveSupportView.isHidden = true
            deleteAccountView.isHidden = true
            qvHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        } else {
            generalQueryView.isHidden = false
            refundView.isHidden = false
            senfParslview.isHidden = false
            redeemParslView.isHidden = false
            deleteAccountView.isHidden = false
            liveSupportView.isHidden = true
            qvHeight.constant = CGFloat(initialheightForIphoneOrIpad) * 6
        }
    }
    @objc func showAppSetting(_ sender: UITapGestureRecognizer? = nil) {
        if appSettingHeight.constant == CGFloat(initialheightForIphoneOrIpad) * 4
        {
            clearCacheView.isHidden = true
            checkUpdateView.isHidden = true
            appSettingHeight.constant = CGFloat(initialheightForIphoneOrIpad)
        } else {
            appSettingHeight.constant = CGFloat(initialheightForIphoneOrIpad) * 4
            clearCacheView.isHidden = false
            checkUpdateView.isHidden = false
        }
    }
    @objc func takeToAvatarScreen (_ notification: NSNotification){
        if ParslUtils.sharedInstance.getIfuserLogin() {
            self.signOutLabel.text = constantsStrings.signOut
            self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let storyboard = UIStoryboard(name: "MyAvatarStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAvatarsViewController")as! MyAvatarsViewController
            self.navigationController?.pushViewController(vc, animated: true)
//            self.moveToNextVC(storyBoardName: "MyAvatarStoryBoard", identifier: "MyAvatarsViewController", VC: MyAvatarsViewController)
        }
        }
    }
    
    @objc func joinNowToSignOut (_ notification: NSNotification){
        
        self.signOutLabel.text = constantsStrings.signOut
        self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
        showHidePersonalView()
    }
    
    @objc func MyParslLoginPressed (_ notification: NSNotification){
        
        self.signOutLabel.text = constantsStrings.signOut
        self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
    }
    
    @objc func CreateModelLoginPressed (_ notification: NSNotification){
        
        self.signOutLabel.text = constantsStrings.signOut
        self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
    }
   
    func getAppVersionApi() {
        let url = constantsUrl.CHECK_VERSION_URL
        ProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: AppVersionCheck.sharedInstance.getCheckVersionApiParams(), success: { (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: Any]
                
                let message = (jsonResponse["message"] ?? "") as! String
                let status = (jsonResponse["status"] ?? true) as!Bool
                if status == false {
                    if message.contains("App does not exisit") {
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.appNotExist, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: constantsStrings.okLabel, style: .default) {
                                    UIAlertAction in
                                self.dismiss(animated: true, completion: nil)
                                }
                            alert.addAction(okAction)
                            self.present(alert, animated: true)
                        }
                    }else if message.contains("Invalid app type") {
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.invalidType, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: constantsStrings.okLabel, style: .default) {
                                    UIAlertAction in
                                self.dismiss(animated: true, completion: nil)
                                }
                            alert.addAction(okAction)
                            self.present(alert, animated: true)
                        }
                       
                    }
                    else{
                        DispatchQueue.main.async {
                            self.showAlertViewForVersionCheckAPI()
                        }
                    }
                }
            } catch let error {
                print(error)
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            ProgressHUD.dismiss()
             
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func showAlertViewForVersionCheckAPI() {
        let optionMenu = UIAlertController(title: constantsStrings.appName, message: constantsStrings.newVersionAvailable, preferredStyle: .alert)

        let update = UIAlertAction(title: constantsStrings.updateLabel, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openStoreProductWithiTunesItemIdentifier(constantsStrings.parslItunesIdentifier)   //Parsl identifier
        })

        let cancel = UIAlertAction(title: constantsStrings.cancelLabel, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
        
        optionMenu.addAction(update)
        optionMenu.addAction(cancel)
        self.navigationController?.present(optionMenu, animated: true, completion: nil)
    }
    
    private func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self

        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                // Parent class of self is UIViewContorller
                self?.navigationController?.present(storeViewController, animated: true, completion: nil)
            }
        }
    }
    
 
    
    func saveFirebaseid(_ firebaseId: String) {
            UserDefaults.standard.set(firebaseId, forKey: "firebaseId")
            UserDefaults.standard.synchronize()
    }
    
    func openConversationWindow() {
        //check if email exists
        
          
          let sentDate = Date()
          var userName:String
          var email:String
          var phone:String?
          var namespace:String

              phone = ""
              userName = userEmail!
              email = userEmail!
              namespace = deviceId!
          
          var device_info : Dictionary <String, String> = [:]
  //        var receiver_info : Dictionary <String, String> = [:]

           device_info = [
              "device_manufacturar": "Apple" as String,
            "device_name":UIDevice.current.name as String,
            "device_os":UIDevice.current.systemName as String,
              "device_type": "iPhone" as String,
            "device_version":UIDevice.current.systemVersion as String
          ]

          var data: Dictionary<String, Any>

              data =
                  ["creator" :deviceId! as String,
                  "creator_name": userEmail! as String,
                  "phone":phone as Any,
                  "creator_email":email as String,
                  "creator_namespace":namespace as String,
                  "creater_device_info":device_info as Any,
                  "app_id":"parsl",
                  "creator_device_token":deviceToken as Any,
                  "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
          

          print(data)
        if Reachability.isConnectedToNetwork() {
            postChatRequest(serviceName: constantsUrl.START_CHAT_URL, sendData: data as [String : AnyObject], success: { (response) in
              print(response)
              let defaults = UserDefaults.standard
              let statusCode = defaults.string(forKey: "statusCode")

              if ((statusCode?.range(of: "200")) != nil){
                  DispatchQueue.main.async {
                     // self.showAlert("Success")
                      let status = response["status"] as! Bool
                      if status{
                          let data = response["data"] as! NSDictionary
                          let firebaseId = data["firebase_id"] as! String
                          print("firebase id is ......\(firebaseId)")
                          self.saveFirebaseid(firebaseId)
                          
                          let channel = Channel(id: firebaseId)
                          let vc = ChatViewController(channel: channel)
                          self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else{
                          print(response["message"] as! String)
                      }
                      
                  }

              }
              
              DispatchQueue.main.async {
                  
              }

          }, failure: { (data) in
              print(data)
              
              DispatchQueue.main.async {
                  self.view.makeToast(constantsStrings.unableChat)
                  
              }
          })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }

        }

}


