//
//  FolderEditNameView.swift
//  Parsl
//
//  Created by M Zaryab on 18/02/2022.
//

import SwiftUI

struct FolderEditNameView: View {
    @State private var folderName: String = ""
    var body: some View {
        VStack() {
            HStack {
                Text("Enter new name of folder")
                    .padding()
            }
        }
        TextField("Folder name", text: $folderName)
            .padding()
        Button(action: {
            
            print("\(ParslUtils.sharedInstance.getIfDepthCameraAvailable())")
//            ParslUtils.sharedInstance.saveDepthCameraAvailable(folderName)
            
            let documentsPath = ParslUtils.sharedInstance.getDirectoryPath()
            print(documentsPath)
            var destinationURL = URL(fileURLWithPath: documentsPath)
            
            print("\(ParslUtils.sharedInstance.getIfDepthCameraAvailable())")
            // check if file path is empty or not
            let fileManagerD = FileManager.default
            if fileManagerD.fileExists(atPath: documentsPath) {
                print("FILE AVAILABLE")
                do {
                    let numberOfItems = try fileManagerD.contentsOfDirectory(atPath: documentsPath).count
                    print("no of items in path is \(numberOfItems)")
                    let ext = destinationURL.deletingLastPathComponent()
//                    let path  = destinationURL.appendingPathComponent(folderName)
                    let path  = destinationURL.appendingPathComponent(folderName, isDirectory: true)
                    print(ext.absoluteString)
                    print(path.absoluteString)
                     
                } catch {
                    
                }
            } else {
                print("FILE NOT AVAILABLE")
            }
        }) {
            Text("Done")
                .foregroundColor(.black)
                .background(Color.white)
                .cornerRadius(5)
                .padding()
                
        }

    }
}

struct FolderEditNameView_Previews: PreviewProvider {
    static var previews: some View {
        FolderEditNameView()
    }
}
