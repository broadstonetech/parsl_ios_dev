////
////  Channel.swift
////  BTech
////
////  Created by Mufaza Majeed on 5/26/21.
////  Copyright © 2021 Broadstone Technologies. All rights reserved.
////
//
//
//import FirebaseFirestore
//
//struct Channel {
//  
//  let id: String?
//  let name: String
//  let creator:String
//  let status:String
//  let welcomeMessage:String
//    
//    init(id:String)
//    {
//        self.id  = id
//        self.name = ""
//        self.creator = ""
//        self.status = "read"
//        self.welcomeMessage = "False"
//    }
//    
//    init(name: String,creator:String) {
//        id = nil
//        self.name = name
//        self.creator = creator
//        self.status = "Unread"
//        self.welcomeMessage = "False"
//    }
//    
//    init?(document: QueryDocumentSnapshot) {
//        let data = document.data()
//        
//        guard let name = data["name"] as? String else {
//            return nil
//        }
//        
//        guard let creator = data["creator"] as? String else {
//            return nil
//        }
//        
//        guard let status = data["status"] as? String else {
//            return nil
//        }
//        
//        guard let welcomeMessage = data["welcomeMessage"] as? String else {
//            return nil
//        }
//        
//        id = document.documentID
//        self.name = name
//        self.creator = creator
//        self.status = status
//        self.welcomeMessage = welcomeMessage
//    }
//    
//}
//
//extension Channel: DatabaseRepresentation {
//    
//    var representation: [String : Any] {
//        var rep = ["name": name]
//        rep["creator"] =  creator
//        rep["status"] = status
//        rep["welcomeMessage"] = welcomeMessage
//        
//        if let id = id {
//            rep["id"] = id
//        }
//        
//        return rep
//    }
//    
//}
//
//
//extension Channel: Comparable {
//  
//  static func == (lhs: Channel, rhs: Channel) -> Bool {
//    return lhs.id == rhs.id
//  }
//  
//  static func < (lhs: Channel, rhs: Channel) -> Bool {
//    return lhs.name < rhs.name
//  }
//
//}

//
//  Channel.swift
//  BTech
//
//  Created by Mufaza Majeed on 5/26/21.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//


import FirebaseFirestore

struct Channel {
  
  let id: String?
  let name: String
  let creator:String
  let status:String
  let welcomeMessage:String
    
    init(id:String)
    {
        self.id  = id
        self.name = ""
        self.creator = ""
        self.status = "read"
        self.welcomeMessage = "False"
    }
    
    init(name: String,creator:String) {
        id = nil
        self.name = name
        self.creator = creator
        self.status = "Unread"
        self.welcomeMessage = "False"
    }
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()
        
        guard let name = data["name"] as? String else {
            return nil
        }
        
        guard let creator = data["creator"] as? String else {
            return nil
        }
        
        guard let status = data["status"] as? String else {
            return nil
        }
        
        guard let welcomeMessage = data["welcomeMessage"] as? String else {
            return nil
        }
        
        id = document.documentID
        self.name = name
        self.creator = creator
        self.status = status
        self.welcomeMessage = welcomeMessage
    }
    
}

extension Channel: DatabaseRepresentation {
    
    var representation: [String : Any] {
        var rep = ["name": name]
        rep["creator"] =  creator
        rep["status"] = status
        rep["welcomeMessage"] = welcomeMessage
        
        if let id = id {
            rep["id"] = id
        }
        
        return rep
    }
    
}


extension Channel: Comparable {
  
  static func == (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func < (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.name < rhs.name
  }

}
