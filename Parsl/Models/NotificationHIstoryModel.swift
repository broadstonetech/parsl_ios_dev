//
//  NotificationHIstoryModel.swift
//  Parsl
//
//  Created by M Zaryab on 17/12/2021.
//

import Foundation

struct NotificationHistory{
    let message: String
    let data: [NotificationsData]
}
struct NotificationsData {
    let not_message: String
    let not_title: String
    let not_type: String
    let notificationRecieverName: String
    let sent_time: Double
}
