// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getVendorsList = try? newJSONDecoder().decode(GetVendorsList.self, from: jsonData)

import Foundation

// MARK: - GetVendorsList
struct GetVendorsList: Codable {
    let message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let vendorsList: [VendorsList]
    let vendorsDict: VendorsDict

    enum CodingKeys: String, CodingKey {
        case vendorsList = "vendors_list"
        case vendorsDict = "vendors_dict"
    }
}

// MARK: - VendorsDict
struct VendorsDict: Codable {
}

// MARK: - VendorsList
struct VendorsList: Codable {
    let vendorID: String!
    let vendorName: String!
    let otherInfo: OtherInfo

    enum CodingKeys: String, CodingKey {
        case vendorID = "vendor_id"
        case vendorName = "vendor_name"
        case otherInfo = "other_info"
    }
}

// MARK: - OtherInfo
struct OtherInfo: Codable {
    let slogan, otherInfoDescription: String
    let website: String
    let logoURL: String

    enum CodingKeys: String, CodingKey {
        case slogan
        case otherInfoDescription = "description"
        case website
        case logoURL = "logo_url"
    }
}
