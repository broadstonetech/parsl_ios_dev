import Foundation

// MARK: - Welcome
struct GetParsl: Codable {
    let message: String?
    let data: GetParslData!
}

// MARK: - Message
struct GetParslData: Codable {
    let giftModel: ParslGiftsModel?
    let senderNamespace, parslStripeToken: String?
    let senderData: SenderData?
    let recieverData: RecieverData?
    let priceRelated: PriceRelatedModel?
    let parslTimestamp: Int?
    let parslOtp: String?
    let parslAvailableFlag, parslRedeemed: Bool?
    let parslID: String?
    let parslModels: [ParslModels]?

    enum CodingKeys: String, CodingKey {
        case giftModel = "gift_model"
        case senderNamespace = "sender_namespace"
        case parslStripeToken = "parsl_stripe_token"
        case senderData = "sender_data"
        case recieverData = "reciever_data"
        case priceRelated = "price_related"
        case parslTimestamp = "parsl_timestamp"
        case parslOtp = "parsl_otp"
        case parslAvailableFlag = "parsl_available_flag"
        case parslRedeemed = "parsl_redeemed"
        case parslID = "parsl_id"
        case parslModels = "parsl_models"
    }
}

// MARK: - GiftModel
struct ParslGiftsModel: Codable {
    let basicInfo: BasicInfoModel?
    let priceRelated: PriceRelatedModel?
    let scaling: ModelScaling?
    let hyperlink, document, sampleVideo, promotionalVideo: String?
    let images: [String]?
    let multiTexture: Int?
    let licenseSource: String?
    let actions: ActionsModel?
    let languagesSupported: LanguagesSupportedModel?
    let modelFiles: FilesModel?
    let video: String?
    let cryptographicInfo: CryptographicInfoModel?
    let analytics: AnalyticsModel?
    let category, manufacturingDate, ownerID: String?
    let purchaseStatus: Int?
    let format, license, giftModelDescription, additionalInfo: String?
    let annotations: Annotations?
    let multiNode: Int?
    let questionnaire: [String]?
    let commentFeed: [String]?
    let metaTags, visionMessage: String?
    let subCategory: [String]?
    let testedStatus, modelIndex: Int?
    let productID, modelID: String?
    let isFavorite: Int?
    let textureFile: [TextureFileModel]?

    enum CodingKeys: String, CodingKey {
        case basicInfo = "basic_info"
        case priceRelated = "price_related"
        case scaling, hyperlink, document
        case sampleVideo = "sample_video"
        case promotionalVideo = "promotional_video"
        case images
        case multiTexture = "multi_texture"
        case licenseSource = "license_source"
        case actions
        case languagesSupported = "languages_supported"
        case modelFiles = "model_files"
        case video
        case cryptographicInfo = "cryptographic_info"
        case analytics, category
        case manufacturingDate = "manufacturing_date"
        case ownerID = "owner_id"
        case purchaseStatus = "purchase_status"
        case format, license
        case giftModelDescription = "description"
        case additionalInfo = "additional_info"
        case annotations
        case multiNode = "multi_node"
        case questionnaire
        case commentFeed = "comment_feed"
        case metaTags = "meta_tags"
        case visionMessage = "vision_message"
        case subCategory = "sub_category"
        case testedStatus = "tested_status"
        case modelIndex = "model_index"
        case productID = "product_id"
        case modelID = "model_id"
        case isFavorite
        case textureFile = "texture_file"
    }
}

// MARK: - All Models
struct ParslModels: Codable {
    let basicInfo: BasicInfoModel?
    let priceRelated: PriceRelatedModel?
    let scaling: ModelScaling?
    let testedStatus: Int?
    let hyperlink, document, sampleVideo, promotionalVideo: String?
    let images: [String]?
    let licenseSource: String?
    let multiTexture: Int?
    let actions: ActionsModel?
    let languagesSupported: LanguagesSupportedModel?
    let modelFiles: FilesModel?
    let video: String?
    let cryptographicInfo: CryptographicInfoModel?
    let analytics: AnalyticsModel?
    let category, manufacturingDate, ownerID: String?
    let purchaseStatus: Int?
    let format, license, fashionDescription, additionalInfo: String?
    let annotations: Annotations?
    let multiNode: Int?
    let questionnaire: [String]?
    let commentFeed: [String]?
    let metaTags: String?
    let modelIndex, cpgKey: Int?
    let visionMessage: String?
    let subCategory: [String]?
    let productID, modelID: String?
    let isFavorite: Int?
    let textureFile: [TextureFileModel]?

    enum CodingKeys: String, CodingKey {
        case basicInfo = "basic_info"
        case priceRelated = "price_related"
        case scaling
        case testedStatus = "tested_status"
        case hyperlink, document
        case sampleVideo = "sample_video"
        case promotionalVideo = "promotional_video"
        case images
        case licenseSource = "license_source"
        case multiTexture = "multi_texture"
        case actions
        case languagesSupported = "languages_supported"
        case modelFiles = "model_files"
        case video
        case cryptographicInfo = "cryptographic_info"
        case analytics, category
        case manufacturingDate = "manufacturing_date"
        case ownerID = "owner_id"
        case purchaseStatus = "purchase_status"
        case format, license
        case fashionDescription = "description"
        case additionalInfo = "additional_info"
        case annotations
        case multiNode = "multi_node"
        case questionnaire
        case commentFeed = "comment_feed"
        case metaTags = "meta_tags"
        case modelIndex = "model_index"
        case cpgKey = "cpg_key"
        case visionMessage = "vision_message"
        case subCategory = "sub_category"
        case productID = "product_id"
        case modelID = "model_id"
        case isFavorite
        case textureFile = "texture_file"
    }
}
// MARK: - Actions
struct ActionsModel: Codable {
    let actionType, actionTitle: String?
    let actionData: String?

    enum CodingKeys: String, CodingKey {
        case actionType = "action_type"
        case actionTitle = "action_title"
        case actionData = "action_data"
    }
}

// MARK: - Analytics
struct AnalyticsModel: Codable {
    let sharesCount, downloadCount, searchAppearances: Int?
    let survey: [String]?

    enum CodingKeys: String, CodingKey {
        case sharesCount = "shares_count"
        case downloadCount = "download_count"
        case searchAppearances = "search_appearances"
        case survey
    }
}

// MARK: - Annotations
struct AnnotationsModel: Codable {
}

// MARK: - BasicInfo
struct BasicInfoModel: Codable {
    let name, manufacturer, model: String?
}

// MARK: - CryptographicInfo
struct CryptographicInfoModel: Codable {
    let digitalSig, publicKey: [Int]?
    let modelSha1: String?
    let unzippedSize, zippedSize: Int?

    enum CodingKeys: String, CodingKey {
        case digitalSig = "digital_sig"
        case publicKey = "public_key"
        case modelSha1 = "model_sha1"
        case unzippedSize = "unzipped_size"
        case zippedSize = "zipped_size"
    }
}

// MARK: - ModelFiles
struct FilesModel: Codable {
    let audioFile, texturesType: String?
    let textures: [String]?
    let multitextureModelsList: [String]?
    let the3DModelFile: String?
    let thumbnail: String?

    enum CodingKeys: String, CodingKey {
        case audioFile = "audio_file"
        case texturesType = "textures_type"
        case textures
        case multitextureModelsList = "multitexture_models_list"
        case the3DModelFile = "3d_model_file"
        case thumbnail
    }
}

// MARK: - PriceRelated
struct PriceRelatedModel: Codable {
    let price: Double?
    let currency, priceStatus: String?

    enum CodingKeys: String, CodingKey {
        case price, currency
        case priceStatus = "price_status"
    }
}

// MARK: - GiftModelScaling
struct ModelScaling: Codable {
    let scaleUnit: String?
    let maxScale, minScale: ScaleModel?

    enum CodingKeys: String, CodingKey {
        case scaleUnit = "scale_unit"
        case maxScale = "max_scale"
        case minScale = "min_scale"
    }
}

// MARK: - Scale
struct ScaleModel: Codable {
    let xAxis, yAxis, zAxis: Double?

    enum CodingKeys: String, CodingKey {
        case xAxis = "x-axis"
        case yAxis = "y-axis"
        case zAxis = "z-axis"
    }
}

// MARK: - TextureFile
struct TextureFileModel: Codable {
    let title: String?
    let textureFileDefault: Int?
    let texturesList: [String]?

    enum CodingKeys: String, CodingKey {
        case title
        case textureFileDefault = "default"
        case texturesList
    }
}

// MARK: - LanguagesSupported
struct LanguagesSupportedModel: Codable {
    let english: EnglishLang?
}

// MARK: - English
struct EnglishLang: Codable {
    let title, englishDescription: String?

    enum CodingKeys: String, CodingKey {
        case title
        case englishDescription = "description"
    }
}

// MARK: - FashionScaling
struct FashionScaling: Codable {
    let scalingUnit: String?
    let maxScale, minScale: ScaleModel?

    enum CodingKeys: String, CodingKey {
        case scalingUnit = "scaling_unit"
        case maxScale = "max_scale"
        case minScale = "min_scale"
    }
}



// MARK: - RecieverData
struct RecieverData: Codable {
    let recieverEmail, recieverName,receiverPhoneNo,textMsg, videoMsg: String?
    let address: Address?
    enum CodingKeys: String, CodingKey {
        case recieverEmail = "reciever_email"
        case recieverName = "reciever_name"
        case receiverPhoneNo = "receiver_phone_no"
        case textMsg = "text_msg"
        case videoMsg = "video_msg"
        case address
    }
}
// MARK: - RecieverData
struct SenderData: Codable {
    let senderName,senderEmail,senderPhoneNo: String?
    let address: Address?
    enum CodingKeys: String, CodingKey {
        case senderName = "sender_name"
        case senderEmail = "sender_email"
        case senderPhoneNo = "sender_phone_no"
        case address
    }
}
struct Address: Codable {
    let line1,city,state: String?
    let postalCode: String?
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case line1,city,state
        case postalCode = "postal_code"
        case country
    }
}
