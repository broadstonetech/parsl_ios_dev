//
//  DataExtractor.swift
//  Parsl
//
//  Created by M Zaryab on 22/10/2021.
//

import Foundation
import UIKit

struct dataextractor: Codable {
    let data: dataDig
}
struct dataDig: Codable {
    let id: String
    let object: String
    let associated_objects: [associatedObjectsDig]
    let created: Int
    let expires: Int
    let livemode: Bool
    let secret: String
    
}
 
struct associatedObjectsDig: Codable {
    let type: String
    let id: String
}
