//
//  ParslRedeemModel.swift
//  Parsl
//
//  Created by broadstone on 23/04/2021.
//

import Foundation

struct RedeemModelStruct {
    var message: String!
    var data: RedeemModelData!
}

struct RedeemModelData {
    var giftModel: ModelDataStruct!
    var stripeToken: String!
    var senderData: SenderDataStruct!
    var receiverData: ReceiverDataStruct!
    var timestamp: Int64!
    var otp: String!
    var availableFlag: Bool!
    var parslRedeemed: Bool!
    var parslId: String!
    var parslModels: [ModelDataStruct]
    var avatarCharacter : String!
    var avatarAnimation :String!
}

struct SenderDataStruct {
    var senderName: String!
    var cardNumber: String!
    var cardType: String!
    var senderEmail: String!
    var transactionId: String!
}

struct ReceiverDataStruct {
    var textMsg: String!
    var videoMsg: String!
    var isRedeemedByUser: Bool!
}

struct ModelsDownloadedForRedeemStruct {
    var _3dModelFile: String!
    var destinationUrl: URL!
}
