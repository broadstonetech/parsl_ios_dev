//
//  CopounDetail.swift
//  Parsl
//
//  Created by Billal on 13/03/2021.
//

import Foundation

// MARK: - Welcome
struct CouponResponse: Codable {
    let message: String?
    let data: CouponResponseData?
}

// MARK: - DataClass
struct CouponResponseData: Codable {
    let approvedProducts: [String]?
    let deniedProducts: [String]?
    let copounStatus: Bool?
    let copounRelief: Int?
    let copounReliefType: String?

    enum CodingKeys: String, CodingKey {
        case approvedProducts = "approved_products"
        case deniedProducts = "denied_products"
        case copounStatus = "coupon_status"
        case copounRelief = "coupon_relief"
        case copounReliefType = "coupon_relief_type"
    }
    
    
    
    
}
