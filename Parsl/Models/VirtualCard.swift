//
//  VirtualCard.swift
//  Parsl
//
//  Created by Billal on 18/03/2021.
//


import Foundation

// MARK: - Welcome
struct VirtualCard: Codable {
    let message: String?
    let redeemed_by_reciever: Bool?
    let data: CardData?
}

// MARK: - DataClass
struct CardData: Codable {
    let id, object, brand: String?
   // let cancellationReason:
    let cardholder: Cardholder?
    let created: Int?
    let currency, cvc: String?
    let expMonth, expYear: Int?
    let last4: String?
    let livemode: Bool?
    let metadata: Metadata?
    let number: String?
//    let replacedBy, replacementFor, replacementReason, shipping: JSONNull?
    let spendingControls: SpendingControls?
    let status, type: String?

    enum CodingKeys: String, CodingKey {
        case id, object, brand
//        case cancellationReason = "cancellation_reason"
        case cardholder, created, currency, cvc
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case last4, livemode, metadata, number
//        case replacedBy = "replaced_by"
//        case replacementFor = "replacement_for"
//        case replacementReason = "replacement_reason"
//        case shipping
        case spendingControls = "spending_controls"
        case status, type
    }
}

// MARK: - Cardholder
struct Cardholder: Codable {
    let id, object: String?
    let billing: Billing?
//    let company: JSONNull?
    let created: Int?
    let email: String?
//    let individual: JSONNull?
    let livemode: Bool?
    let metadata: Metadata?
    let name, phoneNumber: String!
    let requirements: Requirements?
    let spendingControls: SpendingControls?
    let status, type: String?

    enum CodingKeys: String, CodingKey {
        case id, object, billing, created, email, livemode, metadata, name
//        case company,individual
        case phoneNumber = "phone_number"
        case requirements
        case spendingControls = "spending_controls"
        case status, type
    }
}

// MARK: - Billing
struct Billing: Codable {
    let address: CardHolderAddress?
}

// MARK: - Address
struct CardHolderAddress: Codable {
    let city, country, line1: String?
   // let line2: JSONNull?
    let postalCode, state: String?

    enum CodingKeys: String, CodingKey {
        case city, country, line1
//        case line2
        case postalCode = "postal_code"
        case state
    }
}

// MARK: - Metadata
struct Metadata: Codable {
}

// MARK: - Requirements
struct Requirements: Codable {
//    let disabledReason: JSONNull?
//    let pastDue: [JSONAny]?

  //  enum CodingKeys: String, CodingKey {
//        case disabledReason = "disabled_reason"
//        case pastDue = "past_due"
   // }
}

// MARK: - SpendingControls
struct SpendingControls: Codable {
    let allowedCategories, blockedCategories: [String]?
    let spendingLimits: [SpendingLimit]?
    let spendingLimitsCurrency: String?

    enum CodingKeys: String, CodingKey {
        case allowedCategories = "allowed_categories"
        case blockedCategories = "blocked_categories"
        case spendingLimits = "spending_limits"
        case spendingLimitsCurrency = "spending_limits_currency"
    }
}

// MARK: - SpendingLimit
struct SpendingLimit: Codable {
    let amount: Double?
    let categories: [String]?
    let interval: String?
}
