//
//  SubCategory.swift
//  Parsl
//
//  Created by Billal on 26/02/2021.
//
import Foundation

// MARK: - Welcome
class SubCategoryModel: Codable {
    let message: String?
    var data: [SubCategoryData]?
}

// MARK: - Message
class SubCategoryData: Codable {
    var selectedCategories: String = ""
    var isSelectedByUser: Bool = false
    var totalPrice: Double = 0.0
    var selectedCount: Int = 1
    var isDiscounted: Bool = false
    var updatedPrice: Double = 0.0
    var discountedPrice: Double = 0.0
    let category: String?
    let manufacturingDate, messageDescription: String?
    let license: String?
    let format: String?
    var totalDisc: Double = 0.0
    var priceRelated: PriceRelated?
    let basicInfo: BasicInfo?
    let analytics: ParslAnalytics?
    let commentFeed: [String]?
    let modelFiles: ModelFiles?
    let questionnaire: [Questionnaire]?
    let additionalInfo: String?
    let cryptographicInfo: CryptographicInfo?
    let ownerID: String?
    let appName: String?
    let scaling: Scaling?
    let purchaseStatus: Int?
    let promotionalVideo: String?
    let testedStatus: Int?
    let document: String?
    let hyperlink: String?
    let images: [String]?
    let licenseSource: LicenseSource?
    let multiTexture, multiNode: Int?
    let sampleVideo, video: String?
    let actions: Actions?
    let modelIndex: Int?
    let languagesSupported: LanguagesSupported?
    let metaTags: String?
    let visionMessage: VisionMessage?
    let subCategory: [String]?
    let modelID: String?
    let isFavorite: Int?
    let textureFile: [TextureFile]?
    let energy, energyKcal, fat, ofWhichSaturates: String?
    let carbohydrate, ofWhichSugars, protein, salt: String?
    let annotations: SubAnnotations?
    let cpgKey: Int?
    let productID: String?

    enum CodingKeys: String, CodingKey {
        case category
        case manufacturingDate = "manufacturing_date"
        case messageDescription = "description"
        case license, format
        case priceRelated = "price_related"
        case basicInfo = "basic_info"
        case analytics
        case commentFeed = "comment_feed"
        case modelFiles = "model_files"
        case questionnaire
        case additionalInfo = "additional_info"
        case cryptographicInfo = "cryptographic_info"
        case ownerID = "owner_id"
        case appName = "app_name"
        case scaling
        case purchaseStatus = "purchase_status"
        case promotionalVideo = "promotional_video"
        case testedStatus = "tested_status"
        case document, hyperlink, images
        case licenseSource = "license_source"
        case multiTexture = "multi_texture"
        case multiNode = "multi_node"
        case sampleVideo = "sample_video"
        case video, actions
        case modelIndex = "model_index"
        case languagesSupported = "languages_supported"
        case metaTags = "meta_tags"
        case visionMessage = "vision_message"
        case subCategory = "sub_category"
        case modelID = "model_id"
        case isFavorite
        case textureFile = "texture_file"
        case energy = "Energy"
        case energyKcal = "Energy kcal"
        case fat = "Fat"
        case ofWhichSaturates = "of which Saturates"
        case carbohydrate = "Carbohydrate"
        case ofWhichSugars = "of which Sugars"
        case protein = "Protein"
        case salt = "Salt"
        case annotations
        case cpgKey = "cpg_key"
        case productID = "product_id"
    }
}

// MARK: - Actions
class Actions: Codable {
    let actionTitle: String?
    let actionType: String?
    let actionData: String?

    enum CodingKeys: String, CodingKey {
        case actionTitle = "action_title"
        case actionType = "action_type"
        case actionData = "action_data"
    }
}
//
//class ActionTitle: String, Codable {
//    case empty = ""
//    case orderNow = "Order Now"
//    case readMore = "Read More"
//}

enum ActionType: String, Codable {
    case call = "call"
    case empty = ""
    case link = "link"
}

// MARK: - Analytics
class ParslAnalytics: Codable {
    let downloadCount: Int?
    let survey: [Questionnaire]?
    let searchAppearances, sharesCount: Int?

    enum CodingKeys: String, CodingKey {
        case downloadCount = "download_count"
        case survey
        case searchAppearances = "search_appearances"
        case sharesCount = "shares_count"
    }
}

enum Questionnaire: String, Codable {
    case howWouldYouLikeToRateOurServices = "How would you like to rate our services?"
    case whatYouWillRecommendUs = "What you will recommend us?"
}

// MARK: - Annotations
class SubAnnotations: Codable {
}

// MARK: - BasicInfo
class BasicInfo: Codable {
    let model, name: String?
    let manufacturer: String?
}

enum Manufacturer: String, Codable {
    case laPatisserie = "La Patisserie"
    case locusAR = "LocusAR"
    case pizzaParlour = "Pizza Parlour"
}

//enum CategoryName: String, Codable {
//    case food = "food"
//    case fashion = "fashion"
//    case interior = ""
//    case electronics = ""
//    case specialOccasions = ""
//}

// MARK: - CryptographicInfo
class CryptographicInfo: Codable {
    let publicKey, privateKey: [Int]?
    let zippedSize: Int?
    let digitalSig: [Int]?
    let modelSha1: String?
    let unzippedSize: Int?

    enum CodingKeys: String, CodingKey {
        case publicKey = "public_key"
        case privateKey = "private_key"
        case zippedSize = "zipped_size"
        case digitalSig = "digital_sig"
        case modelSha1 = "model_sha1"
        case unzippedSize = "unzipped_size"
    }
}

enum Format: String, Codable {
    case scn = ".scn"
    case usdz = ".usdz"
}

// MARK: - LanguagesSupported
class LanguagesSupported: Codable {
    let english: English?
}

// MARK: - English
class English: Codable {
    let title, englishDescription: String?

    enum CodingKeys: String, CodingKey {
        case title
        case englishDescription = "description"
    }
}

enum License: String, Codable {
    case licenseOpenSource = "Open source"
    case openSource = "open source"
}

enum LicenseSource: String, Codable {
    case free3DCOM = "free3d.com"
    case locusAR = "LocusAR"
}

// MARK: - ModelFiles
class ModelFiles: Codable {
    let textures: [String]?
    let the3DModelFile: String?
    let thumbnail: String?
    let audioFile: String?
    let multitextureModelsList: [String]?
    let texturesType: String?

    enum CodingKeys: String, CodingKey {
        case textures
        case the3DModelFile = "3d_model_file"
        case thumbnail
        case audioFile = "audio_file"
        case multitextureModelsList = "multitexture_models_list"
        case texturesType = "textures_type"
    }
}

enum SubCatOwnerID: String, Codable {
    case org4929 = "org4929"
    case org5503 = "org5503"
    case org6150 = "org6150"
}

// MARK: - PriceRelated
class PriceRelated: Codable {
    let currency: Currency?
    var price: Double?
    
    let priceStatus: String?

    enum CodingKeys: String, CodingKey {
        case currency, price 
        case priceStatus = "price_status"
    }
}

enum Currency: String, Codable {
    case empty = " "
    case usd = "USD"
}

// MARK: - Scaling
class Scaling: Codable {
    let scalingUnit: ScalingUnit?
    let maxScale, minScale: Scale?

    enum CodingKeys: String, CodingKey {
        case scalingUnit = "scaling_unit"
        case maxScale = "max_scale"
        case minScale = "min_scale"
    }
}

// MARK: - Scale
class Scale: Codable {
    let xAxis, yAxis, zAxis: Double?

    enum CodingKeys: String, CodingKey {
        case xAxis = "x-axis"
        case yAxis = "y-axis"
        case zAxis = "z-axis"
    }
}

enum ScalingUnit: String, Codable {
    case inches = "inches"
}

// MARK: - TextureFile
class TextureFile: Codable {
    let title: String?
    let textureFileDefault: Int?
    let texturesList: [String]?

    enum CodingKeys: String, CodingKey {
        case title
        case textureFileDefault = "default"
        case texturesList
    }
}

enum Title: String, Codable {
    case titleDefault = "default"
}

enum VisionMessage: String, Codable {
    case empty = ""
    case forOptimalVisionExperienceProjectThisObjectNear = "For optimal vision experience project this object near."
}
