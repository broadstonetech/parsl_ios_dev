//
//  GetCategoryModels.swift
//  Parsl
//
//  Created by broadstone on 19/04/2021.
//

import Foundation

struct GetCategoryModels {
    var message: String!
    var data: [ModelDataStruct]!
}

struct ModelDataStruct {
    var modelFile: ModelFileStruct!
    var basicInfo: ModelBasicInfoStruct!
    var priceInfo: ModelPriceInfoStruct!
    var scalingInfo: ModelScalingStruct!
    var hyperlink: String!
    var document: String!
    var sampleVideo: String!
    var promoVideo: String!
    var images: [String]!
    var multiTexture: Int!
    var licenseSource: String!
    var modelActions: ModelActionStruct!
    var video: String!
    var analytics: ModelAnalyticsStruct!
    var category: String!
    var manufacturingDate: String!
    var ownerId: String!
    var purchaseStatus: Int!
    var license: String!
    var description: String!
    var additionalInfo: String!
    var multiNode: Int!
    var questionnaire: [String]!
    var commentFeed: [String]!
    var metaTags: String!
    var visionMessage: String!
    var subCategory: [String]!
    var testedStatus: Double!
    var modelIndex: Int!
    var cryptographicsInfo: ModelCryptoStruct!
    var format: String!
    var productId: String!
    var defaultShadow: Bool!
    var modelId: String!
    var isFavourite: Int!
    var textureFiles: [ModelTextureFileStruct]!
    var isSelectedByUser: Bool!
    
}

struct ModelFileStruct {
    var thumbnail: String!
    var audioFile: String!
    var textureType: String!
    var textures: [String]!
    var multiTexturesModelFile: [String]!
    var _3dModelFile: String!
}

struct ModelBasicInfoStruct {
    var name: String!
    var manufacturer: String!
    var modelType: String!
}

struct ModelPriceInfoStruct {
    var price: Double!
    var currency: String!
    var priceStatus: String!
}

struct ModelScalingStruct {
    var scaleUnit: String!
    var minScale: ScalingAxisStruct!
    var maxScale: ScalingAxisStruct!
}

struct ScalingAxisStruct {
    var xAxis: Double!
    var yAxis: Double!
    var zAxis: Double!
}

struct ModelActionStruct {
    var actionType: String!
    var actionTitle: String!
    var actionData: String!
}

struct ModelAnalyticsStruct {
    var shareCount: Int!
    var downloadCount: Int!
    var searchCount: Int!
    var questions: [String]!
}

struct ModelCryptoStruct {
    var digitalSig: [Int]!
    var publicKeys: [Int]!
    var modelSHA: String!
    var unzippedSize: Int64!
    var zippedSize: Int64!
}

struct ModelTextureFileStruct {
    var title: String!
    var defaultTextures: Int!
    var texturesList: [String]!
}

struct DownloadedDataForVendors {
    var categoryIndex: Int!
    var vendorsList: GetVendorsList!
    
}

struct DownloadedDataForCatAndVendors {
    var categoryIndex: Int!
    var vendorIndex: Int!
    var modelsData: [ModelDataStruct]
}

struct ModelsDownloadedForSendStruct {
    var _3dModelFile: String!
    var destinationUrl: URL!
}
