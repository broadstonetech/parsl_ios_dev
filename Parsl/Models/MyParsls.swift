////
////    RootClass.swift
////    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport
//
//import Foundation
//
//struct MyParsls{
//
//    var data : [MyParslsData]!
//    var message : String!
//    var status : Bool!
//
//
//    /**
//     * Instantiate the instance using the passed dictionary values to set the properties values
//     */
//    init(fromDictionary dictionary: [String:Any]){
//        data = [MyParslsData]()
//        if let dataArray = dictionary["data"] as? [[String:Any]]{
//            for dic in dataArray{
//                let value = MyParslsData(fromDictionary: dic)
//                data.append(value)
//            }
//        }
//        message = dictionary["message"] as? String
//        status = dictionary["status"] as? Bool
//    }
//
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
//        if data != nil{
//            var dictionaryElements = [[String:Any]]()
//            for dataElement in data {
//                dictionaryElements.append(dataElement.toDictionary())
//            }
//            dictionary["data"] = dictionaryElements
//        }
//        if message != nil{
//            dictionary["message"] = message
//        }
//        if status != nil{
//            dictionary["status"] = status
//        }
//        return dictionary
//    }
//
//}
//
//
//import Foundation
//
//struct MyParslsData{
//
//    var amount : Float!
//    var appId : String!
//    var namespace : String!
//    var timestamp : Double!
//    var parslOtp : String!
//    var redeemedStatus : Bool!
//    var senderName : String!
//
//
//
//    /**
//     * Instantiate the instance using the passed dictionary values to set the properties values
//     */
//    init(fromDictionary dictionary: [String:Any]){
//        amount = dictionary["amount"] as? Float
//        appId = dictionary["app_id"] as? String
//        namespace = dictionary["namespace"] as? String
//        timestamp = dictionary["timestamp"] as? Double
//        parslOtp = dictionary["parsl_otp"] as? String
//        redeemedStatus = dictionary["redeemed_status"] as? Bool
//        senderName = dictionary["sender_name"] as? String
//
//    }
//
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
//        if amount != nil{
//            dictionary["amount"] = amount
//        }
//        if appId != nil{
//            dictionary["app_id"] = appId
//        }
//        if namespace != nil{
//            dictionary["namespace"] = namespace
//        }
//        if timestamp != nil{
//            dictionary["timestamp"] = timestamp
//        }
//        if parslOtp != nil{
//            dictionary["parsl_otp"] = parslOtp
//        }
//        if redeemedStatus != nil{
//            dictionary["redeemed_status"] = redeemedStatus
//        }
//        if senderName != nil{
//            dictionary["sender_name"] = senderName
//        }
//
//        return dictionary
//    }
//
//}

struct MyPARSLModel{
    var message : String
    var status : Bool
    var data : [MyParslsData]
    
}
struct MyParslsData {
    var amount : Float
    var appId : String
    var namespace : String
    var timestamp : Int64
    var parslOtp : String
    var redeemedStatus : Bool
    var senderName : String
}
