
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let message: String
    let data: [LiveAvatarData]
}

// MARK: - Datum
struct LiveAvatarData: Codable {
    let title, datumDescription: String
    let url: String
    let whoami: String
    let thumbnails: String
    let priceRelated: LiveAvatarPriceRelated

    enum CodingKeys: String, CodingKey {
        case title
        case datumDescription = "description"
        case url, whoami, thumbnails
        case priceRelated = "price_related"
    }
}

// MARK: - PriceRelated
struct LiveAvatarPriceRelated: Codable {
    let price: Int
    let currency, priceStatus: String

    enum CodingKeys: String, CodingKey {
        case price, currency
        case priceStatus = "price_status"
    }
}
