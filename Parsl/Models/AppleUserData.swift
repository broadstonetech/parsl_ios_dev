//
//  AppleUserData.swift
//  Parsl
//
//  Created by M Zaryab on 09/05/2022.
//
 
import Foundation
import AuthenticationServices
@available(iOS 13.0, *)
struct AppleUserData {
    let id: String
    let firstName: String
    let lastName: String
    let email: String
    let token : Data?

    @available(iOS 13.0, *)
    init(credentials: ASAuthorizationAppleIDCredential) {
        self.id = credentials.user
        self.firstName = credentials.fullName?.givenName ?? ""
        self.lastName = credentials.fullName?.familyName ?? ""
        self.email = credentials.email ?? ""
        self.token = credentials.identityToken
    }
}
@available(iOS 13.0, *)
extension AppleUserData: CustomDebugStringConvertible {
    var debugDescription: String {
        return """
        ID: \(id)
        First Name: \(firstName)
        Last Name: \(lastName)
        Email: \(email)
        Token: \(String(describing: token))
        """
    }
}
