//
//  PaymentParentClass.swift
//  Fintech
//
//  Created by broadstone on 08/01/2021.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
// this class is being used for stripe payment method, enable to user in custom way.

import Foundation
import UIKit
import Stripe

class StipePaymentIntegration: PaymentsBaseClass {
    
    // MARK: - Properties
    var payBtnTappedCount = 0
    
    lazy var cardTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        return cardTextField
    }()
    let payView = UIView()
    lazy var payButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor(displayP3Red: 55/255, green: 176/255, blue: 131/255, alpha: 255/255)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        button.setTitle("Pay", for: .normal)
        button.addTarget(self, action: #selector(pay), for: .touchUpInside)
        return button
    }()
     // MARK: - LifeCycles
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        payView.backgroundColor = .systemGray6
        payView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(payView)
        payView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        payView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        payView.topAnchor.constraint(equalTo: view.topAnchor, constant: 400).isActive = true
        payView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        view.layoutIfNeeded()
        
        let stackView = UIStackView(arrangedSubviews: [cardTextField, payButton])
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.translatesAutoresizingMaskIntoConstraints = false
        payView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leftAnchor.constraint(equalTo: payView.leftAnchor, constant: 20),
            stackView.rightAnchor.constraint(equalTo: payView.rightAnchor, constant: -20),
            stackView.topAnchor.constraint(equalTo: payView.topAnchor, constant: 40),
        ])
    }
    // MARK: - Methods
    //MARK: -  Open Stripe View
    
    func openStripePaymentView(){
        
    }
    
    @objc func pay() {
        if payBtnTappedCount != 0 {
            return
        }
        ProgressHUD.show()
        payBtnTappedCount += 1
        // Create an STPCardParams instance
        let cardParams = STPCardParams()
        cardParams.number = cardTextField.cardNumber
        cardParams.expMonth = UInt(cardTextField.expirationMonth)
        cardParams.expYear = UInt(cardTextField.expirationYear)
        cardParams.cvc = cardTextField.cvc
        
        // Pass it to STPAPIClient to create a Token
        STPAPIClient.shared().createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                // Handle the error
                return
            }
            
            // Send the token identifier to your server
            let tokenID = token.tokenId
            let lastFour = cardParams.last4()!
            let cardType = STPCard.string(from: token.card!.brand)
            print(cardType)
            print(cardParams.last4()!)
            print(tokenID)
            ParslUtils.sharedInstance.saveCardLastDigitsAndType(cardParams.last4()!, cardType)
//            let dataDict: [String: String] = ["token": tokenID]
            //self.navigationController?.popViewController(animated: true)
            
            self.dismiss(animated: true, completion: nil)
            
            let data:[String: String] = ["token": tokenID,"lastFour":lastFour,"cardType":cardType]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentObservers.paymentCompleteNotificationKey), object: nil, userInfo: data)
        }
    }
}

// MARK: - Extensions
extension StipePaymentIntegration: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}
