﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1, typename T2, typename T3>
struct VirtualActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<System.Exception>
struct Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C;
// System.Func`2<UnityEngine.BoneWeight,UniGLTF.UShort4>
struct Func_2_t4AB38DE49E02197EC72C8A674B7F54DC4752F8B8;
// System.Func`2<UnityEngine.BoneWeight,UnityEngine.Vector4>
struct Func_2_t4C4B7B18D0F36E20937C252FF9DBFE003D92E320;
// System.Func`2<System.Byte,System.Boolean>
struct Func_2_tC801BC5CCF689A4C07A1A655BBFE80597F30B0DC;
// System.Func`2<System.Byte,System.Int32>
struct Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8;
// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>
struct Func_2_t68523D24AC42F29095D04644A96099DD5CDE5041;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA;
// System.Func`2<System.Int32,System.UInt32>
struct Func_2_t22229F56B3F44A8D9155EEF00ECBA1525996620B;
// System.Func`2<UnityEngine.Material,System.Collections.Generic.IEnumerable`1<UniGLTF.TextureIO/TextureExportItem>>
struct Func_2_t1D84F5542D491C0E19A8C6A84CC279594E29C53D;
// System.Func`2<UnityEngine.Material,System.Boolean>
struct Func_2_t436C5731F14D84E722C51EB29BECA68D03730F16;
// System.Func`2<UnityEngine.Matrix4x4,UnityEngine.Matrix4x4>
struct Func_2_t95B7B47280CD23C9B92163ACAAE5AECBD026FFAD;
// System.Func`2<UnityEngine.SkinnedMeshRenderer,System.Boolean>
struct Func_2_tCA03099759F7D272679D353D795BDD5165E4FF1A;
// System.Func`2<UnityEngine.Transform,System.Collections.Generic.IEnumerable`1<UnityEngine.Material>>
struct Func_2_t676BF602111DE1959C332255273200551A4B53D2;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t0CD378E6B47E8AE92216FCBB5D1D50088AE581B6;
// System.Func`2<UnityEngine.Transform,UniGLTF.PosRot>
struct Func_2_t5643C0D56F133CC02F63A07E0876D242C49B8CDF;
// System.Func`2<UnityEngine.Transform,UnityEngine.SkinnedMeshRenderer>
struct Func_2_t836CCD5FBC280423E6C8739A810EF16A73E4A607;
// System.Func`2<UnityEngine.Transform,UnityEngine.Transform>
struct Func_2_tE0D10069F0F7283489CA64A1F676EBFF7CCC51CC;
// System.Func`2<UnityEngine.Transform,UniGLTF.gltfExporter/MeshWithRenderer>
struct Func_2_t89BF755511E60DE2F3EA83866C5F5AFB8C6D2C1C;
// System.Func`2<System.UInt16,System.Int32>
struct Func_2_t7096C463F0A4CAD550F6B27787048967060427B4;
// System.Func`2<System.UInt32,System.Int32>
struct Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct Func_2_tB3D19331301201246DE473D608119BB47D418EB9;
// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441;
// System.Func`2<UnityEngine.Vector4,UnityEngine.Vector3>
struct Func_2_tA6B20794E71B01DBB95C2366481D0913D3A5B34D;
// System.Func`2<UniGLTF.glTFMaterial,System.Boolean>
struct Func_2_tD172912E8F9D7E046D8A0963F2555EF077A12769;
// System.Func`2<UniGLTF.glTFMaterial,System.String>
struct Func_2_t72575C47441411C4C896DA47097F85CA1CC008F0;
// System.Func`2<UniGLTF.glTFMesh,System.Collections.Generic.IEnumerable`1<UniGLTF.glTFPrimitives>>
struct Func_2_tF90FD14D03F69B666B16A6918C5E3FF21F3F5443;
// System.Func`2<UniGLTF.glTFTextureInfo,System.Boolean>
struct Func_2_t5EDF16696B7D6B94D7F8B6E0419EF1334715876A;
// System.Func`2<UniGLTF.TextureIO/TextureExportItem,System.Boolean>
struct Func_2_t561EBC4E8BCE588881F403D12167916A1466E4DA;
// System.Func`2<UniGLTF.TextureIO/TextureExportItem,UnityEngine.Texture>
struct Func_2_t77880818CC9186B40F0FB2E9A5E2CA690C47DB98;
// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,System.Boolean>
struct Func_2_t17DE6DEF9F74C5B25D45FAEB597033B28EA32AC0;
// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh>
struct Func_2_t08105548739575F9FE3A646B6888D9042E732055;
// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,System.Object>
struct Func_2_tC9460EE39F336D0F5EF3B94D7189D15A4C827951;
// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t22A35158F9E40077A7147A082319C0D1DFFBE2FD;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>>
struct IEnumerable_1_t2CF0B4ADCA07D41DCC2B5AD279327261793FA4B1;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Material>
struct IEnumerable_1_tC56A812DB62E3B254ED968BE9E6EE9AD8151B7BB;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh>
struct IEnumerable_1_t35BB7C30D129B44CFC76622A3B7D17CA9132D587;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
struct IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792;
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>
struct IEnumerable_1_tC110BB72C4D6ACD305C792639F4DB4A5660C9C1E;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_tF9FFC7B635421ED6396ABF58E4F5831F13B2C61F;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_tDBC849B8248C833C53F1762E771EFC477EB8AF18;
// System.Collections.Generic.IEnumerable`1<UniGLTF.glTFPrimitives>
struct IEnumerable_1_t294F8BDD387E938C7DD824497A7A112211670427;
// System.Collections.Generic.IEnumerable`1<UniGLTF.TextureIO/TextureExportItem>
struct IEnumerable_1_t9E3CE452FC632805324CD53B64D9FF2DC2920DA0;
// System.Collections.Generic.IEnumerable`1<UniGLTF.gltfExporter/MeshWithRenderer>
struct IEnumerable_1_tE49815BBD7ECABEF397D7DF488A63479C43B6E56;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode>>
struct IEnumerator_1_tF19E0E0C3FDB4C1F99A9E818C22712C8D2038A43;
// System.Collections.Generic.IEnumerator`1<DepthFirstScheduler.ISchedulable>
struct IEnumerator_1_t107932B0602FBF3CE7191C8A4C2836FC3E7A5774;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Texture2D>
struct IEnumerator_1_tC5FFA28E1AA5DDCC928C145299C89EFA35256160;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
struct IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150;
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>
struct IEnumerator_1_t532DE44511E4D7A7C2E9CD3ECBBA02853A6D6C1F;
// System.Collections.Generic.IEnumerator`1<UniGLTF.TextureIO/TextureExportItem>
struct IEnumerator_1_tE8368157A6F097E34E6915C3E56CAF47B3039D5C;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.Texture2D>
struct KeyCollection_t5BF010F61696931AD604E5661D57B0C3E75F3F89;
// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B;
// System.Collections.Generic.List`1<UniGLTF.MeshWithMaterials>
struct List_1_tD032E454558CC6C4697AB61EF2BA00FABEDDCF5C;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>
struct List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D;
// System.Collections.Generic.List`1<UniGLTF.TextureItem>
struct List_1_tC25A21969032076B8172539FE2918D3280CF2290;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UniGLTF.glTFAccessor>
struct List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC;
// System.Collections.Generic.List`1<UniGLTF.glTFAnimation>
struct List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5;
// System.Collections.Generic.List`1<UniGLTF.glTFBuffer>
struct List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38;
// System.Collections.Generic.List`1<UniGLTF.glTFBufferView>
struct List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89;
// System.Collections.Generic.List`1<UniGLTF.glTFCamera>
struct List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C;
// System.Collections.Generic.List`1<UniGLTF.glTFImage>
struct List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD;
// System.Collections.Generic.List`1<UniGLTF.glTFMaterial>
struct List_1_tBF560A6A99F5B82BE164503467D8684D672FC119;
// System.Collections.Generic.List`1<UniGLTF.glTFMesh>
struct List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C;
// System.Collections.Generic.List`1<UniGLTF.glTFNode>
struct List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853;
// System.Collections.Generic.List`1<UniGLTF.glTFPrimitives>
struct List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB;
// System.Collections.Generic.List`1<UniGLTF.glTFSkin>
struct List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065;
// System.Collections.Generic.List`1<UniGLTF.glTFTexture>
struct List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0;
// System.Collections.Generic.List`1<UniGLTF.glTFTextureSampler>
struct List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90;
// System.Collections.Generic.List`1<UniGLTF.gltfMorphTarget>
struct List_1_t9173FC3611CD124BBEF5351E7ECE265EE0563E32;
// System.Collections.Generic.List`1<UniGLTF.gltfScene>
struct List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7;
// System.Collections.Generic.List`1<UniGLTF.ImporterContext/KeyElapsed>
struct List_1_tD482B8C4B716F19365ED58E20462BE0764683827;
// System.Collections.Generic.List`1<UniGLTF.gltfExporter/MeshWithRenderer>
struct List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.Texture2D>
struct ValueCollection_tE5FE75610567CA88CAC35F1A7E298EC8ED244E01;
// System.Collections.Generic.Dictionary`2/Entry<System.String,UnityEngine.Texture2D>[]
struct EntryU5BU5D_tEBC229867AD1A3EE9B8B6A34263272DECBFE37FA;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UniGLTF.ShaderPropExporter.ShaderProperty[]
struct ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// UnityEngine.SkinnedMeshRenderer[]
struct SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UniGLTF.glTFPrimitives[]
struct glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA;
// UniGLTF.gltfExporter/MeshWithRenderer[]
struct MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89;
// UnityEngine.AnimationClip
struct AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UniGLTF.Zip.CentralDirectoryFileHeader
struct CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7;
// UniGLTF.Zip.CommonHeader
struct CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.IO.FileStream
struct FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UniGLTF.IMaterialExporter
struct IMaterialExporter_tFA443B0F21B6D006C58F8A9ECEE40C81A4FEB9D3;
// UniGLTF.IMaterialImporter
struct IMaterialImporter_t25AF3CABA2259D038D6BE45BC63FC11676333B74;
// UniGLTF.IShaderStore
struct IShaderStore_t1DD2B20B30362225F29D017C31D678BE9D708D5B;
// UniGLTF.IStorage
struct IStorage_tD6BC4074CAACD06D3F1AA1F5A6545389FA232B40;
// UniGLTF.ITextureLoader
struct ITextureLoader_tECE164CB7ADED7358A0798D40319C3C90ED2BE03;
// UniGLTF.ImporterContext
struct ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_tC77A9860A03C31DC46AD2C08EC10EACDC3B7A662;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39;
// UniGLTF.ShaderPropExporter.ShaderProps
struct ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496;
// System.String
struct String_t;
// DepthFirstScheduler.TaskChain
struct TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UniGLTF.TextureExportManager
struct TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A;
// UniGLTF.TextureItem
struct TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA;
// UniGLTF.TextureLoader
struct TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// UniGLTF.UnityWebRequestTextureLoader
struct UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WWW
struct WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2;
// UniGLTF.glTF
struct glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17;
// UniGLTF.glTFAnimationChannel
struct glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B;
// UniGLTF.glTFAnimationTarget
struct glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A;
// UniGLTF.glTFAssets
struct glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986;
// UniGLTF.glTFAttributes
struct glTFAttributes_tFA2177D23DBBDB60ECB8AF3D090825EA9779C9C9;
// UniGLTF.glTFMaterial
struct glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC;
// UniGLTF.glTFMaterialEmissiveTextureInfo
struct glTFMaterialEmissiveTextureInfo_t2F9A3AFEB20366CBA1E180C0414525BA75B8681B;
// UniGLTF.glTFMaterialNormalTextureInfo
struct glTFMaterialNormalTextureInfo_t6D150E15CEA24273D7B0C80E66F9B22107970019;
// UniGLTF.glTFMaterialOcclusionTextureInfo
struct glTFMaterialOcclusionTextureInfo_t69A60A1A8A9C1781D618D4AFA7B2B5487E8B6B4D;
// UniGLTF.glTFMaterial_extensions
struct glTFMaterial_extensions_t7A04698CBFFFCEC246DD6B72454299069EBD22FF;
// UniGLTF.glTFMesh
struct glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755;
// UniGLTF.glTFNode
struct glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480;
// UniGLTF.glTFNode_extensions
struct glTFNode_extensions_tB678A11BB0F4B5CC110B19B53F17E8E91B3714A1;
// UniGLTF.glTFNode_extra
struct glTFNode_extra_t6BB9D43654E287FD54522BC0C85EF018F3C398B3;
// UniGLTF.glTFPbrMetallicRoughness
struct glTFPbrMetallicRoughness_t8D5AB37B5167EAB04B72D54E7BFE73CA384618EA;
// UniGLTF.glTFPrimitives
struct glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598;
// UniGLTF.glTFPrimitives_extensions
struct glTFPrimitives_extensions_t39BDF61B91F413D69BE59CA7131F06ED12324B56;
// UniGLTF.glTFPrimitives_extras
struct glTFPrimitives_extras_tB25723016CB0805B401E71CAC783352FE92CAEF4;
// UniGLTF.glTFTextureInfo
struct glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F;
// UniGLTF.glTFTextureSampler
struct glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496;
// UniGLTF.glTF_extensions
struct glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985;
// UniGLTF.gltfExporter
struct gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174;
// UniGLTF.gltf_extras
struct gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94;
// UniGLTF.StaticMeshIntegrator/Integrator
struct Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974;
// UniGLTF.TextureConverter/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t9116E9331CE9FA0B41D192076C87ED0195257A1D;
// UniGLTF.TextureConverter/ColorConversion
struct ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99;
// UniGLTF.TextureIO/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5;
// UniGLTF.TextureIO/<GetTextures>d__4
struct U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651;
// UniGLTF.TextureItem/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tDF0015826BF1D71934A4988CBA6A8B3956DF6C79;
// UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11
struct U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594;
// UniGLTF.TextureItem/<ProcessCoroutine>d__15
struct U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17;
// UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17
struct U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6;
// UniGLTF.TextureLoader/<ProcessOnMainThread>d__11
struct U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC;
// UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2
struct U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339;
// TriangleUtil/<>c
struct U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721;
// TriangleUtil/<FlipTriangle>d__3
struct U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640;
// UniGLTF.UnityExtensions/<>c
struct U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C;
// UniGLTF.UnityExtensions/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t641D155E04F0EFA0F3C94152F3D4A62E35FBDC4C;
// UniGLTF.UnityExtensions/<Ancestors>d__18
struct U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232;
// UniGLTF.UnityExtensions/<GetChildren>d__15
struct U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D;
// UniGLTF.UnityExtensions/<Traverse>d__16
struct U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C;
// UniHumanoid.UnityExtensions/<GetChildren>d__1
struct U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21;
// UniHumanoid.UnityExtensions/<Traverse>d__2
struct U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE;
// UniGLTF.UnityPath/<TravserseDir>d__38
struct U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1;
// UniGLTF.UnityPath/<get_ChildDirs>d__40
struct U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D;
// UniGLTF.UnityPath/<get_ChildFiles>d__42
struct U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872;
// UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12
struct U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED;
// UniGLTF.UnityWebRequestTextureLoader/Deleter
struct Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D;
// UniGLTF.Zip.ZipArchiveStorage/<>c
struct U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215;
// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t47560D8B7EC8B115B6DE13A1B94A6659521D6499;
// UniGLTF.glTF/<>c
struct U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7;
// UniGLTF.glTF/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t36893A0919E05A0D1E8591BF6B047DAF5EB6A18B;
// UniGLTF.glTFAnimation/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9;
// UniGLTF.glbImporter/<>c
struct U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD;
// UniGLTF.gltfExporter/<>c
struct U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5;
// UniGLTF.gltfExporter/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_tD105192BEFDF5A92F21A1211C6994A11ECF88B07;
// UniGLTF.gltfExporter/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3;
// UniGLTF.gltfExporter/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83;
// UniGLTF.gltfExporter/<>c__DisplayClass35_1
struct U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC;
// UniGLTF.gltfImporter/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16;
// DepthFirstScheduler.Scheduler/ThreadPoolScheduler/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tE82D0128C1DFC11ACA808215BDEB39E3AB8B3FDD;
// UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA;

IL2CPP_EXTERN_C RuntimeClass* Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t08105548739575F9FE3A646B6888D9042E732055_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_tC110BB72C4D6ACD305C792639F4DB4A5660C9C1E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t532DE44511E4D7A7C2E9CD3ECBBA02853A6D6C1F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMaterialExporter_tFA443B0F21B6D006C58F8A9ECEE40C81A4FEB9D3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ITextureLoader_tECE164CB7ADED7358A0798D40319C3C90ED2BE03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PreShaderPropExporter_t79A6BBD54954D59BB1305B9C955B2D873D13AFC1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40;
IL2CPP_EXTERN_C String_t* _stringLiteral849D9073DD7D266CCFFD877500171B0CB572D56C;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral876BA9D37F5B3B86B1953A81D0C931AE6AFB2BED;
IL2CPP_EXTERN_C String_t* _stringLiteralE5D65FF0775E5025619AE4F4CCD3E1536D94EF55;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mD4DFE26268AFEE6CA1AEA70EE0FEB61E599DC3E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mB89D75983F403B440947CE6FB264503618F5B951_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m1E9C5D6F74CD0F5F21102B83360E41013884A501_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_GetEnumerator_m1889B555EFCAE12D382994CEB941D4D74FAA2AAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Any_TisKeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986_mE8485C294FD1E6A3BFF22E28FA961C3F91860100_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_m49DF9F51864902059B935A4953F3F4FCF61AA3BE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToList_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_mA9B87D5951385CA98350A1CE27AEB321CEFF0989_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m8464D1A0162AE786225345C59D904340E92B15A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m14D37307E545E87355A7180B90F7EC67DEA34B16_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m04294D9E012BC7B9B153DC5438BAAA0F2C7BA0A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m7126A380EC86400442C76F6E88B55A2ABEF146FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_mEF264B1DD704042A07FA7767D09CD9B140608045_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_m453802FFD6BD090185B8F8E655E241D04E070D49_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m8FA5E9A771A18C54032AE5470895574151D55083_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m13C775A28676EED93E673F62FCA6AB4610B99257_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_IndexOf_m066AEE31C72DF470B7FD54374C7ADED0C43D199B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m0657384C4E785AFC6CCF0F8E525311EAC9E129A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m497DA7006D664307F20A0546E5B717AA4B5E8A86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m9E6B743F729F390A508DDCD477B81957DF20DB4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CAncestorsU3Ed__18_System_Collections_IEnumerator_Reset_m3B2E397EF34A70954CA020CC657A61EFF7D077EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CFlipTriangleU3Ed__3_System_Collections_IEnumerator_Reset_mB05A7E73BBBD6E4B337283236D579F70AF7ABAE2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetChildrenU3Ed__15_System_Collections_IEnumerator_Reset_mA1AD165672DC79E503BA338C9E4A3F92573B7C6A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetChildrenU3Ed__1_System_Collections_IEnumerator_Reset_mB868A68C8D7C10108CD73B8B0860B50316DB4580_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_IEnumerator_Reset_m726C217B282E1AEF2532204BB07EC3824CFF3719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetTexturesU3Ed__4_System_Collections_IEnumerator_Reset_m0BD99F097ACD0B9701333239E43EC436CE93BBF1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetUnityWrapModeU3Ed__2_MoveNext_m1F4CDED5BB8276B27000FF325324F0AC5D0F22D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetUnityWrapModeU3Ed__2_System_Collections_IEnumerator_Reset_m8509F993ADB3C1420AB6C5FB5A4A0D4D9C9CE5B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_m192C28FBDF022A6BE8FD77C512C965FDC7E622D6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessOnMainThreadCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m9AA9A7D54AF56F2165145AA44550CDFF8D5796EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessOnMainThreadU3Ed__11_System_Collections_IEnumerator_Reset_mB7FE99FC411C77A74C3F1ABBFC39D8A5B1122C00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessOnMainThreadU3Ed__12_System_Collections_IEnumerator_Reset_m26884EB48C1754801435A50E4F68913F2DD6B56A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTraverseU3Ed__16_System_Collections_IEnumerator_Reset_mA5653B0EF412BEE75E76905BC9D71CFBC9A9242F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTraverseU3Ed__2_System_Collections_IEnumerator_Reset_m76C3ED0186EF3D29822CC189DE8CC6D1E8CDC044_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_m35BD4F36C2376A3A570E4CF234060A207AC38BA3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CFromGameObjectU3Eb__35_12_mFB03ABF6DE33058C0F475D4F0ECBA395FD08BCDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__0_m94EDED9E92F81857DF1B8E1559724DFDF3623C9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__1_m3ECB93AF7BD11AC361B644E8B6A18E119EEF6DF6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__2_m99207BBFCAF1F67ED747E7FE3C70957DBFC9B08F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mC236479C9A834B44352D9158BC0B52ACFFEEF302_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m0C8D5063B2894A80CC9E2385D69B640EAFA87852_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityExtensions_Has_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_mEA1212D8AD20345E735606E08B43969F5D89B606_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tEBC229867AD1A3EE9B8B6A34263272DECBFE37FA* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t5BF010F61696931AD604E5661D57B0C3E75F3F89 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tE5FE75610567CA88CAC35F1A7E298EC8ED244E01 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___entries_1)); }
	inline EntryU5BU5D_tEBC229867AD1A3EE9B8B6A34263272DECBFE37FA* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tEBC229867AD1A3EE9B8B6A34263272DECBFE37FA** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tEBC229867AD1A3EE9B8B6A34263272DECBFE37FA* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___keys_7)); }
	inline KeyCollection_t5BF010F61696931AD604E5661D57B0C3E75F3F89 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t5BF010F61696931AD604E5661D57B0C3E75F3F89 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t5BF010F61696931AD604E5661D57B0C3E75F3F89 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ___values_8)); }
	inline ValueCollection_tE5FE75610567CA88CAC35F1A7E298EC8ED244E01 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tE5FE75610567CA88CAC35F1A7E298EC8ED244E01 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tE5FE75610567CA88CAC35F1A7E298EC8ED244E01 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98, ____items_1)); }
	inline Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* get__items_1() const { return ____items_1; }
	inline Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5DU5BU5D_t104DBF1B996084AA19567FD32B02EDF88D044FAF* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4, ____items_1)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get__items_1() const { return ____items_1; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4_StaticFields, ____emptyArray_5)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B, ____items_1)); }
	inline MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* get__items_1() const { return ____items_1; }
	inline MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B_StaticFields, ____emptyArray_5)); }
	inline MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>
struct List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40, ____items_1)); }
	inline SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* get__items_1() const { return ____items_1; }
	inline SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40_StaticFields, ____emptyArray_5)); }
	inline SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SkinnedMeshRendererU5BU5D_t5E45BBCBFA792B65D947C1BE81E777D0FF78FEED* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____items_1)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____items_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UniGLTF.glTFPrimitives>
struct List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB, ____items_1)); }
	inline glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* get__items_1() const { return ____items_1; }
	inline glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB_StaticFields, ____emptyArray_5)); }
	inline glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(glTFPrimitivesU5BU5D_tC77E6927E8D9D332AA64DF892F07DCFF15BE3DFA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UniGLTF.gltfExporter/MeshWithRenderer>
struct List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063, ____items_1)); }
	inline MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* get__items_1() const { return ____items_1; }
	inline MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063_StaticFields, ____emptyArray_5)); }
	inline MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MeshWithRendererU5BU5D_t6DEB69079BD1C98CFE4EBC4F14621D08E5F37D89* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7  : public RuntimeObject
{
public:

public:
};


// UniGLTF.ImporterContext
struct ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UniGLTF.ImporterContext/KeyElapsed> UniGLTF.ImporterContext::m_speedReports
	List_1_tD482B8C4B716F19365ED58E20462BE0764683827 * ___m_speedReports_0;
	// UniGLTF.IShaderStore UniGLTF.ImporterContext::m_shaderStore
	RuntimeObject* ___m_shaderStore_1;
	// UniGLTF.IMaterialImporter UniGLTF.ImporterContext::m_materialImporter
	RuntimeObject* ___m_materialImporter_2;
	// System.String UniGLTF.ImporterContext::Json
	String_t* ___Json_3;
	// UniGLTF.glTF UniGLTF.ImporterContext::GLTF
	glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___GLTF_4;
	// UniGLTF.IStorage UniGLTF.ImporterContext::Storage
	RuntimeObject* ___Storage_5;
	// UnityEngine.GameObject UniGLTF.ImporterContext::Root
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Root_6;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UniGLTF.ImporterContext::Nodes
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___Nodes_7;
	// System.Collections.Generic.List`1<UniGLTF.TextureItem> UniGLTF.ImporterContext::m_textures
	List_1_tC25A21969032076B8172539FE2918D3280CF2290 * ___m_textures_8;
	// System.Collections.Generic.List`1<UnityEngine.Material> UniGLTF.ImporterContext::m_materials
	List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * ___m_materials_9;
	// System.Collections.Generic.List`1<UniGLTF.MeshWithMaterials> UniGLTF.ImporterContext::Meshes
	List_1_tD032E454558CC6C4697AB61EF2BA00FABEDDCF5C * ___Meshes_10;
	// UnityEngine.AnimationClip UniGLTF.ImporterContext::Animation
	AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * ___Animation_11;

public:
	inline static int32_t get_offset_of_m_speedReports_0() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___m_speedReports_0)); }
	inline List_1_tD482B8C4B716F19365ED58E20462BE0764683827 * get_m_speedReports_0() const { return ___m_speedReports_0; }
	inline List_1_tD482B8C4B716F19365ED58E20462BE0764683827 ** get_address_of_m_speedReports_0() { return &___m_speedReports_0; }
	inline void set_m_speedReports_0(List_1_tD482B8C4B716F19365ED58E20462BE0764683827 * value)
	{
		___m_speedReports_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_speedReports_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_shaderStore_1() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___m_shaderStore_1)); }
	inline RuntimeObject* get_m_shaderStore_1() const { return ___m_shaderStore_1; }
	inline RuntimeObject** get_address_of_m_shaderStore_1() { return &___m_shaderStore_1; }
	inline void set_m_shaderStore_1(RuntimeObject* value)
	{
		___m_shaderStore_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_shaderStore_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_materialImporter_2() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___m_materialImporter_2)); }
	inline RuntimeObject* get_m_materialImporter_2() const { return ___m_materialImporter_2; }
	inline RuntimeObject** get_address_of_m_materialImporter_2() { return &___m_materialImporter_2; }
	inline void set_m_materialImporter_2(RuntimeObject* value)
	{
		___m_materialImporter_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_materialImporter_2), (void*)value);
	}

	inline static int32_t get_offset_of_Json_3() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Json_3)); }
	inline String_t* get_Json_3() const { return ___Json_3; }
	inline String_t** get_address_of_Json_3() { return &___Json_3; }
	inline void set_Json_3(String_t* value)
	{
		___Json_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Json_3), (void*)value);
	}

	inline static int32_t get_offset_of_GLTF_4() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___GLTF_4)); }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * get_GLTF_4() const { return ___GLTF_4; }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 ** get_address_of_GLTF_4() { return &___GLTF_4; }
	inline void set_GLTF_4(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * value)
	{
		___GLTF_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GLTF_4), (void*)value);
	}

	inline static int32_t get_offset_of_Storage_5() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Storage_5)); }
	inline RuntimeObject* get_Storage_5() const { return ___Storage_5; }
	inline RuntimeObject** get_address_of_Storage_5() { return &___Storage_5; }
	inline void set_Storage_5(RuntimeObject* value)
	{
		___Storage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Storage_5), (void*)value);
	}

	inline static int32_t get_offset_of_Root_6() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Root_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Root_6() const { return ___Root_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Root_6() { return &___Root_6; }
	inline void set_Root_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Root_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Root_6), (void*)value);
	}

	inline static int32_t get_offset_of_Nodes_7() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Nodes_7)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_Nodes_7() const { return ___Nodes_7; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_Nodes_7() { return &___Nodes_7; }
	inline void set_Nodes_7(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___Nodes_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nodes_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_textures_8() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___m_textures_8)); }
	inline List_1_tC25A21969032076B8172539FE2918D3280CF2290 * get_m_textures_8() const { return ___m_textures_8; }
	inline List_1_tC25A21969032076B8172539FE2918D3280CF2290 ** get_address_of_m_textures_8() { return &___m_textures_8; }
	inline void set_m_textures_8(List_1_tC25A21969032076B8172539FE2918D3280CF2290 * value)
	{
		___m_textures_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textures_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_materials_9() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___m_materials_9)); }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * get_m_materials_9() const { return ___m_materials_9; }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 ** get_address_of_m_materials_9() { return &___m_materials_9; }
	inline void set_m_materials_9(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * value)
	{
		___m_materials_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_materials_9), (void*)value);
	}

	inline static int32_t get_offset_of_Meshes_10() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Meshes_10)); }
	inline List_1_tD032E454558CC6C4697AB61EF2BA00FABEDDCF5C * get_Meshes_10() const { return ___Meshes_10; }
	inline List_1_tD032E454558CC6C4697AB61EF2BA00FABEDDCF5C ** get_address_of_Meshes_10() { return &___Meshes_10; }
	inline void set_Meshes_10(List_1_tD032E454558CC6C4697AB61EF2BA00FABEDDCF5C * value)
	{
		___Meshes_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Meshes_10), (void*)value);
	}

	inline static int32_t get_offset_of_Animation_11() { return static_cast<int32_t>(offsetof(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9, ___Animation_11)); }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * get_Animation_11() const { return ___Animation_11; }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 ** get_address_of_Animation_11() { return &___Animation_11; }
	inline void set_Animation_11(AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * value)
	{
		___Animation_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animation_11), (void*)value);
	}
};


// UniGLTF.JsonSerializableBase
struct JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F  : public RuntimeObject
{
public:

public:
};


// System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// UniGLTF.ShaderPropExporter.ShaderProps
struct ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4  : public RuntimeObject
{
public:
	// UniGLTF.ShaderPropExporter.ShaderProperty[] UniGLTF.ShaderPropExporter.ShaderProps::Properties
	ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* ___Properties_0;

public:
	inline static int32_t get_offset_of_Properties_0() { return static_cast<int32_t>(offsetof(ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4, ___Properties_0)); }
	inline ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* get_Properties_0() const { return ___Properties_0; }
	inline ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503** get_address_of_Properties_0() { return &___Properties_0; }
	inline void set_Properties_0(ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* value)
	{
		___Properties_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Properties_0), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UniGLTF.TextureExportManager
struct TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture> UniGLTF.TextureExportManager::m_textures
	List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * ___m_textures_0;
	// System.Collections.Generic.List`1<UnityEngine.Texture> UniGLTF.TextureExportManager::m_exportTextures
	List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * ___m_exportTextures_1;

public:
	inline static int32_t get_offset_of_m_textures_0() { return static_cast<int32_t>(offsetof(TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A, ___m_textures_0)); }
	inline List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * get_m_textures_0() const { return ___m_textures_0; }
	inline List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D ** get_address_of_m_textures_0() { return &___m_textures_0; }
	inline void set_m_textures_0(List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * value)
	{
		___m_textures_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textures_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_exportTextures_1() { return static_cast<int32_t>(offsetof(TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A, ___m_exportTextures_1)); }
	inline List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * get_m_exportTextures_1() const { return ___m_exportTextures_1; }
	inline List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D ** get_address_of_m_exportTextures_1() { return &___m_exportTextures_1; }
	inline void set_m_exportTextures_1(List_1_t692E82B1A375B23285D8EAC6AA53F573315C6C3D * value)
	{
		___m_exportTextures_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_exportTextures_1), (void*)value);
	}
};


// UniGLTF.TextureItem
struct TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureItem::m_textureIndex
	int32_t ___m_textureIndex_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> UniGLTF.TextureItem::m_converts
	Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * ___m_converts_1;
	// System.Boolean UniGLTF.TextureItem::<IsAsset>k__BackingField
	bool ___U3CIsAssetU3Ek__BackingField_2;
	// UniGLTF.ITextureLoader UniGLTF.TextureItem::m_textureLoader
	RuntimeObject* ___m_textureLoader_3;

public:
	inline static int32_t get_offset_of_m_textureIndex_0() { return static_cast<int32_t>(offsetof(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA, ___m_textureIndex_0)); }
	inline int32_t get_m_textureIndex_0() const { return ___m_textureIndex_0; }
	inline int32_t* get_address_of_m_textureIndex_0() { return &___m_textureIndex_0; }
	inline void set_m_textureIndex_0(int32_t value)
	{
		___m_textureIndex_0 = value;
	}

	inline static int32_t get_offset_of_m_converts_1() { return static_cast<int32_t>(offsetof(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA, ___m_converts_1)); }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * get_m_converts_1() const { return ___m_converts_1; }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C ** get_address_of_m_converts_1() { return &___m_converts_1; }
	inline void set_m_converts_1(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * value)
	{
		___m_converts_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_converts_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsAssetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA, ___U3CIsAssetU3Ek__BackingField_2)); }
	inline bool get_U3CIsAssetU3Ek__BackingField_2() const { return ___U3CIsAssetU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsAssetU3Ek__BackingField_2() { return &___U3CIsAssetU3Ek__BackingField_2; }
	inline void set_U3CIsAssetU3Ek__BackingField_2(bool value)
	{
		___U3CIsAssetU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_m_textureLoader_3() { return static_cast<int32_t>(offsetof(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA, ___m_textureLoader_3)); }
	inline RuntimeObject* get_m_textureLoader_3() const { return ___m_textureLoader_3; }
	inline RuntimeObject** get_address_of_m_textureLoader_3() { return &___m_textureLoader_3; }
	inline void set_m_textureLoader_3(RuntimeObject* value)
	{
		___m_textureLoader_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textureLoader_3), (void*)value);
	}
};


// UniGLTF.TextureLoader
struct TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureLoader::m_textureIndex
	int32_t ___m_textureIndex_0;
	// UnityEngine.Texture2D UniGLTF.TextureLoader::<Texture>k__BackingField
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___U3CTextureU3Ek__BackingField_1;
	// System.Byte[] UniGLTF.TextureLoader::m_imageBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_imageBytes_2;
	// System.String UniGLTF.TextureLoader::m_textureName
	String_t* ___m_textureName_3;

public:
	inline static int32_t get_offset_of_m_textureIndex_0() { return static_cast<int32_t>(offsetof(TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA, ___m_textureIndex_0)); }
	inline int32_t get_m_textureIndex_0() const { return ___m_textureIndex_0; }
	inline int32_t* get_address_of_m_textureIndex_0() { return &___m_textureIndex_0; }
	inline void set_m_textureIndex_0(int32_t value)
	{
		___m_textureIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA, ___U3CTextureU3Ek__BackingField_1)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_U3CTextureU3Ek__BackingField_1() const { return ___U3CTextureU3Ek__BackingField_1; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_U3CTextureU3Ek__BackingField_1() { return &___U3CTextureU3Ek__BackingField_1; }
	inline void set_U3CTextureU3Ek__BackingField_1(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___U3CTextureU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTextureU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_imageBytes_2() { return static_cast<int32_t>(offsetof(TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA, ___m_imageBytes_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_imageBytes_2() const { return ___m_imageBytes_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_imageBytes_2() { return &___m_imageBytes_2; }
	inline void set_m_imageBytes_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_imageBytes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_imageBytes_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_textureName_3() { return static_cast<int32_t>(offsetof(TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA, ___m_textureName_3)); }
	inline String_t* get_m_textureName_3() const { return ___m_textureName_3; }
	inline String_t** get_address_of_m_textureName_3() { return &___m_textureName_3; }
	inline void set_m_textureName_3(String_t* value)
	{
		___m_textureName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textureName_3), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UniGLTF.gltfExporter
struct gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174  : public RuntimeObject
{
public:
	// UniGLTF.glTF UniGLTF.gltfExporter::glTF
	glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___glTF_1;
	// System.Boolean UniGLTF.gltfExporter::<UseSparseAccessorForBlendShape>k__BackingField
	bool ___U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2;
	// UnityEngine.GameObject UniGLTF.gltfExporter::<Copy>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CCopyU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> UniGLTF.gltfExporter::<Meshes>k__BackingField
	List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * ___U3CMeshesU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UniGLTF.gltfExporter::<Nodes>k__BackingField
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___U3CNodesU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<UnityEngine.Material> UniGLTF.gltfExporter::<Materials>k__BackingField
	List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * ___U3CMaterialsU3Ek__BackingField_6;
	// UniGLTF.TextureExportManager UniGLTF.gltfExporter::TextureManager
	TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A * ___TextureManager_7;

public:
	inline static int32_t get_offset_of_glTF_1() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___glTF_1)); }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * get_glTF_1() const { return ___glTF_1; }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 ** get_address_of_glTF_1() { return &___glTF_1; }
	inline void set_glTF_1(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * value)
	{
		___glTF_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___glTF_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2)); }
	inline bool get_U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2() const { return ___U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2() { return &___U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2; }
	inline void set_U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2(bool value)
	{
		___U3CUseSparseAccessorForBlendShapeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCopyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___U3CCopyU3Ek__BackingField_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CCopyU3Ek__BackingField_3() const { return ___U3CCopyU3Ek__BackingField_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CCopyU3Ek__BackingField_3() { return &___U3CCopyU3Ek__BackingField_3; }
	inline void set_U3CCopyU3Ek__BackingField_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CCopyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCopyU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMeshesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___U3CMeshesU3Ek__BackingField_4)); }
	inline List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * get_U3CMeshesU3Ek__BackingField_4() const { return ___U3CMeshesU3Ek__BackingField_4; }
	inline List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B ** get_address_of_U3CMeshesU3Ek__BackingField_4() { return &___U3CMeshesU3Ek__BackingField_4; }
	inline void set_U3CMeshesU3Ek__BackingField_4(List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * value)
	{
		___U3CMeshesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMeshesU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNodesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___U3CNodesU3Ek__BackingField_5)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_U3CNodesU3Ek__BackingField_5() const { return ___U3CNodesU3Ek__BackingField_5; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_U3CNodesU3Ek__BackingField_5() { return &___U3CNodesU3Ek__BackingField_5; }
	inline void set_U3CNodesU3Ek__BackingField_5(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___U3CNodesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNodesU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMaterialsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___U3CMaterialsU3Ek__BackingField_6)); }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * get_U3CMaterialsU3Ek__BackingField_6() const { return ___U3CMaterialsU3Ek__BackingField_6; }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 ** get_address_of_U3CMaterialsU3Ek__BackingField_6() { return &___U3CMaterialsU3Ek__BackingField_6; }
	inline void set_U3CMaterialsU3Ek__BackingField_6(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * value)
	{
		___U3CMaterialsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMaterialsU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_TextureManager_7() { return static_cast<int32_t>(offsetof(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174, ___TextureManager_7)); }
	inline TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A * get_TextureManager_7() const { return ___TextureManager_7; }
	inline TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A ** get_address_of_TextureManager_7() { return &___TextureManager_7; }
	inline void set_TextureManager_7(TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A * value)
	{
		___TextureManager_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextureManager_7), (void*)value);
	}
};


// UniGLTF.StaticMeshIntegrator/Integrator
struct Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UniGLTF.StaticMeshIntegrator/Integrator::m_positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UniGLTF.StaticMeshIntegrator/Integrator::m_normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_normals_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UniGLTF.StaticMeshIntegrator/Integrator::m_uv
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___m_uv_2;
	// System.Collections.Generic.List`1<System.Int32[]> UniGLTF.StaticMeshIntegrator/Integrator::m_subMeshes
	List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * ___m_subMeshes_3;
	// System.Collections.Generic.List`1<UnityEngine.Material> UniGLTF.StaticMeshIntegrator/Integrator::m_materials
	List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * ___m_materials_4;

public:
	inline static int32_t get_offset_of_m_positions_0() { return static_cast<int32_t>(offsetof(Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79, ___m_positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_positions_0() const { return ___m_positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_positions_0() { return &___m_positions_0; }
	inline void set_m_positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_normals_1() { return static_cast<int32_t>(offsetof(Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79, ___m_normals_1)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_normals_1() const { return ___m_normals_1; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_normals_1() { return &___m_normals_1; }
	inline void set_m_normals_1(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_normals_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_normals_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_uv_2() { return static_cast<int32_t>(offsetof(Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79, ___m_uv_2)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_m_uv_2() const { return ___m_uv_2; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_m_uv_2() { return &___m_uv_2; }
	inline void set_m_uv_2(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___m_uv_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_uv_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_subMeshes_3() { return static_cast<int32_t>(offsetof(Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79, ___m_subMeshes_3)); }
	inline List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * get_m_subMeshes_3() const { return ___m_subMeshes_3; }
	inline List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 ** get_address_of_m_subMeshes_3() { return &___m_subMeshes_3; }
	inline void set_m_subMeshes_3(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * value)
	{
		___m_subMeshes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_subMeshes_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_materials_4() { return static_cast<int32_t>(offsetof(Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79, ___m_materials_4)); }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * get_m_materials_4() const { return ___m_materials_4; }
	inline List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 ** get_address_of_m_materials_4() { return &___m_materials_4; }
	inline void set_m_materials_4(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * value)
	{
		___m_materials_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_materials_4), (void*)value);
	}
};


// UniGLTF.TextureConverter/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t9116E9331CE9FA0B41D192076C87ED0195257A1D  : public RuntimeObject
{
public:
	// UniGLTF.TextureConverter/ColorConversion UniGLTF.TextureConverter/<>c__DisplayClass1_0::colorConversion
	ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * ___colorConversion_0;

public:
	inline static int32_t get_offset_of_colorConversion_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9116E9331CE9FA0B41D192076C87ED0195257A1D, ___colorConversion_0)); }
	inline ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * get_colorConversion_0() const { return ___colorConversion_0; }
	inline ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 ** get_address_of_colorConversion_0() { return &___colorConversion_0; }
	inline void set_colorConversion_0(ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * value)
	{
		___colorConversion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorConversion_0), (void*)value);
	}
};


// UniGLTF.TextureIO/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureIO/<>c__DisplayClass2_0::textureIndex
	int32_t ___textureIndex_0;
	// System.Func`2<UniGLTF.glTFTextureInfo,System.Boolean> UniGLTF.TextureIO/<>c__DisplayClass2_0::<>9__0
	Func_2_t5EDF16696B7D6B94D7F8B6E0419EF1334715876A * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_textureIndex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5, ___textureIndex_0)); }
	inline int32_t get_textureIndex_0() const { return ___textureIndex_0; }
	inline int32_t* get_address_of_textureIndex_0() { return &___textureIndex_0; }
	inline void set_textureIndex_0(int32_t value)
	{
		___textureIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5, ___U3CU3E9__0_1)); }
	inline Func_2_t5EDF16696B7D6B94D7F8B6E0419EF1334715876A * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Func_2_t5EDF16696B7D6B94D7F8B6E0419EF1334715876A ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Func_2_t5EDF16696B7D6B94D7F8B6E0419EF1334715876A * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_1), (void*)value);
	}
};


// UniGLTF.TextureItem/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tDF0015826BF1D71934A4988CBA6A8B3956DF6C79  : public RuntimeObject
{
public:
	// System.String UniGLTF.TextureItem/<>c__DisplayClass6_0::prop
	String_t* ___prop_0;

public:
	inline static int32_t get_offset_of_prop_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tDF0015826BF1D71934A4988CBA6A8B3956DF6C79, ___prop_0)); }
	inline String_t* get_prop_0() const { return ___prop_0; }
	inline String_t** get_address_of_prop_0() { return &___prop_0; }
	inline void set_prop_0(String_t* value)
	{
		___prop_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prop_0), (void*)value);
	}
};


// UniGLTF.TextureItem/<ProcessCoroutine>d__15
struct U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureItem/<ProcessCoroutine>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UniGLTF.TextureItem/<ProcessCoroutine>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UniGLTF.TextureItem UniGLTF.TextureItem/<ProcessCoroutine>d__15::<>4__this
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * ___U3CU3E4__this_2;
	// UniGLTF.glTF UniGLTF.TextureItem/<ProcessCoroutine>d__15::gltf
	glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf_3;
	// UniGLTF.IStorage UniGLTF.TextureItem/<ProcessCoroutine>d__15::storage
	RuntimeObject* ___storage_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17, ___U3CU3E4__this_2)); }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_gltf_3() { return static_cast<int32_t>(offsetof(U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17, ___gltf_3)); }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * get_gltf_3() const { return ___gltf_3; }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 ** get_address_of_gltf_3() { return &___gltf_3; }
	inline void set_gltf_3(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * value)
	{
		___gltf_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gltf_3), (void*)value);
	}

	inline static int32_t get_offset_of_storage_4() { return static_cast<int32_t>(offsetof(U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17, ___storage_4)); }
	inline RuntimeObject* get_storage_4() const { return ___storage_4; }
	inline RuntimeObject** get_address_of_storage_4() { return &___storage_4; }
	inline void set_storage_4(RuntimeObject* value)
	{
		___storage_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storage_4), (void*)value);
	}
};


// UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17
struct U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UniGLTF.TextureItem UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>4__this
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * ___U3CU3E4__this_2;
	// UniGLTF.glTF UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::gltf
	glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf_3;
	// UniGLTF.ITextureLoader UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6, ___U3CU3E4__this_2)); }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_gltf_3() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6, ___gltf_3)); }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * get_gltf_3() const { return ___gltf_3; }
	inline glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 ** get_address_of_gltf_3() { return &___gltf_3; }
	inline void set_gltf_3(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * value)
	{
		___gltf_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gltf_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_4), (void*)value);
	}
};


// UniGLTF.TextureLoader/<ProcessOnMainThread>d__11
struct U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UniGLTF.TextureLoader UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::<>4__this
	TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * ___U3CU3E4__this_2;
	// System.Boolean UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::isLinear
	bool ___isLinear_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC, ___U3CU3E4__this_2)); }
	inline TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_isLinear_3() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC, ___isLinear_3)); }
	inline bool get_isLinear_3() const { return ___isLinear_3; }
	inline bool* get_address_of_isLinear_3() { return &___isLinear_3; }
	inline void set_isLinear_3(bool value)
	{
		___isLinear_3 = value;
	}
};


// TriangleUtil/<>c
struct U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields
{
public:
	// TriangleUtil/<>c TriangleUtil/<>c::<>9
	U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * ___U3CU3E9_0;
	// System.Func`2<System.Byte,System.Int32> TriangleUtil/<>c::<>9__0_0
	Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * ___U3CU3E9__0_0_1;
	// System.Func`2<System.UInt16,System.Int32> TriangleUtil/<>c::<>9__1_0
	Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * ___U3CU3E9__1_0_2;
	// System.Func`2<System.UInt32,System.Int32> TriangleUtil/<>c::<>9__2_0
	Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * ___U3CU3E9__2_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields, ___U3CU3E9__1_0_2)); }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * get_U3CU3E9__1_0_2() const { return ___U3CU3E9__1_0_2; }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 ** get_address_of_U3CU3E9__1_0_2() { return &___U3CU3E9__1_0_2; }
	inline void set_U3CU3E9__1_0_2(Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * value)
	{
		___U3CU3E9__1_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields, ___U3CU3E9__2_0_3)); }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * get_U3CU3E9__2_0_3() const { return ___U3CU3E9__2_0_3; }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 ** get_address_of_U3CU3E9__2_0_3() { return &___U3CU3E9__2_0_3; }
	inline void set_U3CU3E9__2_0_3(Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * value)
	{
		___U3CU3E9__2_0_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_0_3), (void*)value);
	}
};


// TriangleUtil/<FlipTriangle>d__3
struct U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640  : public RuntimeObject
{
public:
	// System.Int32 TriangleUtil/<FlipTriangle>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Int32 TriangleUtil/<FlipTriangle>d__3::<>2__current
	int32_t ___U3CU3E2__current_1;
	// System.Int32 TriangleUtil/<FlipTriangle>d__3::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<System.Int32> TriangleUtil/<FlipTriangle>d__3::src
	RuntimeObject* ___src_3;
	// System.Collections.Generic.IEnumerable`1<System.Int32> TriangleUtil/<FlipTriangle>d__3::<>3__src
	RuntimeObject* ___U3CU3E3__src_4;
	// System.Collections.Generic.IEnumerator`1<System.Int32> TriangleUtil/<FlipTriangle>d__3::<it>5__2
	RuntimeObject* ___U3CitU3E5__2_5;
	// System.Int32 TriangleUtil/<FlipTriangle>d__3::<i0>5__3
	int32_t ___U3Ci0U3E5__3_6;
	// System.Int32 TriangleUtil/<FlipTriangle>d__3::<i1>5__4
	int32_t ___U3Ci1U3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3CU3E2__current_1)); }
	inline int32_t get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline int32_t* get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(int32_t value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_src_3() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___src_3)); }
	inline RuntimeObject* get_src_3() const { return ___src_3; }
	inline RuntimeObject** get_address_of_src_3() { return &___src_3; }
	inline void set_src_3(RuntimeObject* value)
	{
		___src_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___src_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__src_4() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3CU3E3__src_4)); }
	inline RuntimeObject* get_U3CU3E3__src_4() const { return ___U3CU3E3__src_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__src_4() { return &___U3CU3E3__src_4; }
	inline void set_U3CU3E3__src_4(RuntimeObject* value)
	{
		___U3CU3E3__src_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__src_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CitU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3CitU3E5__2_5)); }
	inline RuntimeObject* get_U3CitU3E5__2_5() const { return ___U3CitU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CitU3E5__2_5() { return &___U3CitU3E5__2_5; }
	inline void set_U3CitU3E5__2_5(RuntimeObject* value)
	{
		___U3CitU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CitU3E5__2_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3Ci0U3E5__3_6() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3Ci0U3E5__3_6)); }
	inline int32_t get_U3Ci0U3E5__3_6() const { return ___U3Ci0U3E5__3_6; }
	inline int32_t* get_address_of_U3Ci0U3E5__3_6() { return &___U3Ci0U3E5__3_6; }
	inline void set_U3Ci0U3E5__3_6(int32_t value)
	{
		___U3Ci0U3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3Ci1U3E5__4_7() { return static_cast<int32_t>(offsetof(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640, ___U3Ci1U3E5__4_7)); }
	inline int32_t get_U3Ci1U3E5__4_7() const { return ___U3Ci1U3E5__4_7; }
	inline int32_t* get_address_of_U3Ci1U3E5__4_7() { return &___U3Ci1U3E5__4_7; }
	inline void set_U3Ci1U3E5__4_7(int32_t value)
	{
		___U3Ci1U3E5__4_7 = value;
	}
};


// UniGLTF.UnityExtensions/<>c
struct U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_StaticFields
{
public:
	// UniGLTF.UnityExtensions/<>c UniGLTF.UnityExtensions/<>c::<>9
	U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Transform,UnityEngine.Transform> UniGLTF.UnityExtensions/<>c::<>9__23_0
	Func_2_tE0D10069F0F7283489CA64A1F676EBFF7CCC51CC * ___U3CU3E9__23_0_1;
	// System.Func`2<UnityEngine.Transform,UniGLTF.PosRot> UniGLTF.UnityExtensions/<>c::<>9__23_1
	Func_2_t5643C0D56F133CC02F63A07E0876D242C49B8CDF * ___U3CU3E9__23_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Func_2_tE0D10069F0F7283489CA64A1F676EBFF7CCC51CC * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Func_2_tE0D10069F0F7283489CA64A1F676EBFF7CCC51CC ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Func_2_tE0D10069F0F7283489CA64A1F676EBFF7CCC51CC * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__23_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_StaticFields, ___U3CU3E9__23_1_2)); }
	inline Func_2_t5643C0D56F133CC02F63A07E0876D242C49B8CDF * get_U3CU3E9__23_1_2() const { return ___U3CU3E9__23_1_2; }
	inline Func_2_t5643C0D56F133CC02F63A07E0876D242C49B8CDF ** get_address_of_U3CU3E9__23_1_2() { return &___U3CU3E9__23_1_2; }
	inline void set_U3CU3E9__23_1_2(Func_2_t5643C0D56F133CC02F63A07E0876D242C49B8CDF * value)
	{
		___U3CU3E9__23_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__23_1_2), (void*)value);
	}
};


// UniGLTF.UnityExtensions/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t641D155E04F0EFA0F3C94152F3D4A62E35FBDC4C  : public RuntimeObject
{
public:
	// System.String UniGLTF.UnityExtensions/<>c__DisplayClass17_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t641D155E04F0EFA0F3C94152F3D4A62E35FBDC4C, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}
};


// UniGLTF.UnityExtensions/<Ancestors>d__18
struct U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityExtensions/<Ancestors>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Ancestors>d__18::<>2__current
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityExtensions/<Ancestors>d__18::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Ancestors>d__18::t
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t_3;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Ancestors>d__18::<>3__t
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E3__t_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Ancestors>d__18::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___U3CU3E2__current_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_t_3() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___t_3)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_t_3() const { return ___t_3; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_t_3() { return &___t_3; }
	inline void set_t_3(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___t_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__t_4() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___U3CU3E3__t_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E3__t_4() const { return ___U3CU3E3__t_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E3__t_4() { return &___U3CU3E3__t_4; }
	inline void set_U3CU3E3__t_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E3__t_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__t_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}
};


// UniGLTF.UnityExtensions/<GetChildren>d__15
struct U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityExtensions/<GetChildren>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<GetChildren>d__15::<>2__current
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityExtensions/<GetChildren>d__15::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<GetChildren>d__15::self
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___self_3;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<GetChildren>d__15::<>3__self
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E3__self_4;
	// System.Collections.IEnumerator UniGLTF.UnityExtensions/<GetChildren>d__15::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___U3CU3E2__current_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___self_3)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_self_3() const { return ___self_3; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___self_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___self_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___U3CU3E3__self_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E3__self_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__self_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}
};


// UniGLTF.UnityExtensions/<Traverse>d__16
struct U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityExtensions/<Traverse>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Traverse>d__16::<>2__current
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityExtensions/<Traverse>d__16::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Traverse>d__16::t
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t_3;
	// UnityEngine.Transform UniGLTF.UnityExtensions/<Traverse>d__16::<>3__t
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E3__t_4;
	// System.Collections.IEnumerator UniGLTF.UnityExtensions/<Traverse>d__16::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Traverse>d__16::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3E2__current_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_t_3() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___t_3)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_t_3() const { return ___t_3; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_t_3() { return &___t_3; }
	inline void set_t_3(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___t_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__t_4() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3E3__t_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E3__t_4() const { return ___U3CU3E3__t_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E3__t_4() { return &___U3CU3E3__t_4; }
	inline void set_U3CU3E3__t_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E3__t_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__t_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C, ___U3CU3E7__wrap2_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap2_6), (void*)value);
	}
};


// UniHumanoid.UnityExtensions/<GetChildren>d__1
struct U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21  : public RuntimeObject
{
public:
	// System.Int32 UniHumanoid.UnityExtensions/<GetChildren>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<GetChildren>d__1::<>2__current
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E2__current_1;
	// System.Int32 UniHumanoid.UnityExtensions/<GetChildren>d__1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<GetChildren>d__1::parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_3;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<GetChildren>d__1::<>3__parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E3__parent_4;
	// System.Collections.IEnumerator UniHumanoid.UnityExtensions/<GetChildren>d__1::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___U3CU3E2__current_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___parent_3)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parent_3() const { return ___parent_3; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__parent_4() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___U3CU3E3__parent_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E3__parent_4() const { return ___U3CU3E3__parent_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E3__parent_4() { return &___U3CU3E3__parent_4; }
	inline void set_U3CU3E3__parent_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E3__parent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__parent_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}
};


// UniHumanoid.UnityExtensions/<Traverse>d__2
struct U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE  : public RuntimeObject
{
public:
	// System.Int32 UniHumanoid.UnityExtensions/<Traverse>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<Traverse>d__2::<>2__current
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E2__current_1;
	// System.Int32 UniHumanoid.UnityExtensions/<Traverse>d__2::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<Traverse>d__2::parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_3;
	// UnityEngine.Transform UniHumanoid.UnityExtensions/<Traverse>d__2::<>3__parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CU3E3__parent_4;
	// System.Collections.IEnumerator UniHumanoid.UnityExtensions/<Traverse>d__2::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniHumanoid.UnityExtensions/<Traverse>d__2::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3E2__current_1)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___parent_3)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parent_3() const { return ___parent_3; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__parent_4() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3E3__parent_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CU3E3__parent_4() const { return ___U3CU3E3__parent_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CU3E3__parent_4() { return &___U3CU3E3__parent_4; }
	inline void set_U3CU3E3__parent_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CU3E3__parent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__parent_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE, ___U3CU3E7__wrap2_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap2_6), (void*)value);
	}
};


// UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12
struct U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UniGLTF.UnityWebRequestTextureLoader UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>4__this
	UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * ___U3CU3E4__this_2;
	// UniGLTF.UnityWebRequestTextureLoader/Deleter UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<d>5__2
	Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * ___U3CdU3E5__2_3;
	// UnityEngine.WWW UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<m_uwr>5__3
	WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * ___U3Cm_uwrU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED, ___U3CU3E4__this_2)); }
	inline UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED, ___U3CdU3E5__2_3)); }
	inline Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * get_U3CdU3E5__2_3() const { return ___U3CdU3E5__2_3; }
	inline Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D ** get_address_of_U3CdU3E5__2_3() { return &___U3CdU3E5__2_3; }
	inline void set_U3CdU3E5__2_3(Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * value)
	{
		___U3CdU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cm_uwrU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED, ___U3Cm_uwrU3E5__3_4)); }
	inline WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * get_U3Cm_uwrU3E5__3_4() const { return ___U3Cm_uwrU3E5__3_4; }
	inline WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 ** get_address_of_U3Cm_uwrU3E5__3_4() { return &___U3Cm_uwrU3E5__3_4; }
	inline void set_U3Cm_uwrU3E5__3_4(WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * value)
	{
		___U3Cm_uwrU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cm_uwrU3E5__3_4), (void*)value);
	}
};


// UniGLTF.UnityWebRequestTextureLoader/Deleter
struct Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D  : public RuntimeObject
{
public:
	// System.String UniGLTF.UnityWebRequestTextureLoader/Deleter::m_path
	String_t* ___m_path_0;

public:
	inline static int32_t get_offset_of_m_path_0() { return static_cast<int32_t>(offsetof(Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D, ___m_path_0)); }
	inline String_t* get_m_path_0() const { return ___m_path_0; }
	inline String_t** get_address_of_m_path_0() { return &___m_path_0; }
	inline void set_m_path_0(String_t* value)
	{
		___m_path_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_path_0), (void*)value);
	}
};


// UniGLTF.Zip.ZipArchiveStorage/<>c
struct U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_StaticFields
{
public:
	// UniGLTF.Zip.ZipArchiveStorage/<>c UniGLTF.Zip.ZipArchiveStorage/<>c::<>9
	U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * ___U3CU3E9_0;
	// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String> UniGLTF.Zip.ZipArchiveStorage/<>c::<>9__0_0
	Func_2_t68523D24AC42F29095D04644A96099DD5CDE5041 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t68523D24AC42F29095D04644A96099DD5CDE5041 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t68523D24AC42F29095D04644A96099DD5CDE5041 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t68523D24AC42F29095D04644A96099DD5CDE5041 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}
};


// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t47560D8B7EC8B115B6DE13A1B94A6659521D6499  : public RuntimeObject
{
public:
	// System.String UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::url
	String_t* ___url_0;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t47560D8B7EC8B115B6DE13A1B94A6659521D6499, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_0), (void*)value);
	}
};


// UniGLTF.glTF/<>c
struct U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields
{
public:
	// UniGLTF.glTF/<>c UniGLTF.glTF/<>c::<>9
	U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * ___U3CU3E9_0;
	// System.Func`2<System.Byte,System.Int32> UniGLTF.glTF/<>c::<>9__9_0
	Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * ___U3CU3E9__9_0_1;
	// System.Func`2<System.UInt16,System.Int32> UniGLTF.glTF/<>c::<>9__9_1
	Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * ___U3CU3E9__9_1_2;
	// System.Func`2<System.UInt32,System.Int32> UniGLTF.glTF/<>c::<>9__9_2
	Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * ___U3CU3E9__9_2_3;
	// System.Func`2<System.Byte,System.Int32> UniGLTF.glTF/<>c::<>9__10_0
	Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * ___U3CU3E9__10_0_4;
	// System.Func`2<System.UInt16,System.Int32> UniGLTF.glTF/<>c::<>9__10_1
	Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * ___U3CU3E9__10_1_5;
	// System.Func`2<System.UInt32,System.Int32> UniGLTF.glTF/<>c::<>9__10_2
	Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * ___U3CU3E9__10_2_6;
	// System.Func`2<UniGLTF.glTFMaterial,System.Boolean> UniGLTF.glTF/<>c::<>9__23_0
	Func_2_tD172912E8F9D7E046D8A0963F2555EF077A12769 * ___U3CU3E9__23_0_7;
	// System.Func`2<UniGLTF.glTFMaterial,System.String> UniGLTF.glTF/<>c::<>9__23_1
	Func_2_t72575C47441411C4C896DA47097F85CA1CC008F0 * ___U3CU3E9__23_1_8;
	// System.Func`2<UniGLTF.glTFMesh,System.Collections.Generic.IEnumerable`1<UniGLTF.glTFPrimitives>> UniGLTF.glTF/<>c::<>9__26_0
	Func_2_tF90FD14D03F69B666B16A6918C5E3FF21F3F5443 * ___U3CU3E9__26_0_9;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__9_0_1)); }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * get_U3CU3E9__9_0_1() const { return ___U3CU3E9__9_0_1; }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 ** get_address_of_U3CU3E9__9_0_1() { return &___U3CU3E9__9_0_1; }
	inline void set_U3CU3E9__9_0_1(Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * value)
	{
		___U3CU3E9__9_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__9_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__9_1_2)); }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * get_U3CU3E9__9_1_2() const { return ___U3CU3E9__9_1_2; }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 ** get_address_of_U3CU3E9__9_1_2() { return &___U3CU3E9__9_1_2; }
	inline void set_U3CU3E9__9_1_2(Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * value)
	{
		___U3CU3E9__9_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__9_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__9_2_3)); }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * get_U3CU3E9__9_2_3() const { return ___U3CU3E9__9_2_3; }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 ** get_address_of_U3CU3E9__9_2_3() { return &___U3CU3E9__9_2_3; }
	inline void set_U3CU3E9__9_2_3(Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * value)
	{
		___U3CU3E9__9_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__9_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__10_0_4)); }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * get_U3CU3E9__10_0_4() const { return ___U3CU3E9__10_0_4; }
	inline Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 ** get_address_of_U3CU3E9__10_0_4() { return &___U3CU3E9__10_0_4; }
	inline void set_U3CU3E9__10_0_4(Func_2_tC15EC5D4FBC78FCEBC4675F57F52CCD0638E1ED8 * value)
	{
		___U3CU3E9__10_0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_0_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__10_1_5)); }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * get_U3CU3E9__10_1_5() const { return ___U3CU3E9__10_1_5; }
	inline Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 ** get_address_of_U3CU3E9__10_1_5() { return &___U3CU3E9__10_1_5; }
	inline void set_U3CU3E9__10_1_5(Func_2_t7096C463F0A4CAD550F6B27787048967060427B4 * value)
	{
		___U3CU3E9__10_1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_2_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__10_2_6)); }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * get_U3CU3E9__10_2_6() const { return ___U3CU3E9__10_2_6; }
	inline Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 ** get_address_of_U3CU3E9__10_2_6() { return &___U3CU3E9__10_2_6; }
	inline void set_U3CU3E9__10_2_6(Func_2_tE9D4E2F30FE884904F3B9A65A46D1482DF827523 * value)
	{
		___U3CU3E9__10_2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_2_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__23_0_7)); }
	inline Func_2_tD172912E8F9D7E046D8A0963F2555EF077A12769 * get_U3CU3E9__23_0_7() const { return ___U3CU3E9__23_0_7; }
	inline Func_2_tD172912E8F9D7E046D8A0963F2555EF077A12769 ** get_address_of_U3CU3E9__23_0_7() { return &___U3CU3E9__23_0_7; }
	inline void set_U3CU3E9__23_0_7(Func_2_tD172912E8F9D7E046D8A0963F2555EF077A12769 * value)
	{
		___U3CU3E9__23_0_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__23_0_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_1_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__23_1_8)); }
	inline Func_2_t72575C47441411C4C896DA47097F85CA1CC008F0 * get_U3CU3E9__23_1_8() const { return ___U3CU3E9__23_1_8; }
	inline Func_2_t72575C47441411C4C896DA47097F85CA1CC008F0 ** get_address_of_U3CU3E9__23_1_8() { return &___U3CU3E9__23_1_8; }
	inline void set_U3CU3E9__23_1_8(Func_2_t72575C47441411C4C896DA47097F85CA1CC008F0 * value)
	{
		___U3CU3E9__23_1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__23_1_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields, ___U3CU3E9__26_0_9)); }
	inline Func_2_tF90FD14D03F69B666B16A6918C5E3FF21F3F5443 * get_U3CU3E9__26_0_9() const { return ___U3CU3E9__26_0_9; }
	inline Func_2_tF90FD14D03F69B666B16A6918C5E3FF21F3F5443 ** get_address_of_U3CU3E9__26_0_9() { return &___U3CU3E9__26_0_9; }
	inline void set_U3CU3E9__26_0_9(Func_2_tF90FD14D03F69B666B16A6918C5E3FF21F3F5443 * value)
	{
		___U3CU3E9__26_0_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__26_0_9), (void*)value);
	}
};


// UniGLTF.glTF/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t36893A0919E05A0D1E8591BF6B047DAF5EB6A18B  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.glTF/<>c__DisplayClass26_0::materialIndex
	int32_t ___materialIndex_0;

public:
	inline static int32_t get_offset_of_materialIndex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t36893A0919E05A0D1E8591BF6B047DAF5EB6A18B, ___materialIndex_0)); }
	inline int32_t get_materialIndex_0() const { return ___materialIndex_0; }
	inline int32_t* get_address_of_materialIndex_0() { return &___materialIndex_0; }
	inline void set_materialIndex_0(int32_t value)
	{
		___materialIndex_0 = value;
	}
};


// UniGLTF.glbImporter/<>c
struct U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_StaticFields
{
public:
	// UniGLTF.glbImporter/<>c UniGLTF.glbImporter/<>c::<>9
	U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * ___U3CU3E9_0;
	// System.Func`2<System.Byte,System.Boolean> UniGLTF.glbImporter/<>c::<>9__3_0
	Func_2_tC801BC5CCF689A4C07A1A655BBFE80597F30B0DC * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_tC801BC5CCF689A4C07A1A655BBFE80597F30B0DC * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_tC801BC5CCF689A4C07A1A655BBFE80597F30B0DC ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_tC801BC5CCF689A4C07A1A655BBFE80597F30B0DC * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// UniGLTF.gltfExporter/<>c
struct U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields
{
public:
	// UniGLTF.gltfExporter/<>c UniGLTF.gltfExporter/<>c::<>9
	U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__31_0
	Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * ___U3CU3E9__31_0_1;
	// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__31_1
	Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * ___U3CU3E9__31_1_2;
	// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__31_2
	Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * ___U3CU3E9__31_2_3;
	// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__31_3
	Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * ___U3CU3E9__31_3_4;
	// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2> UniGLTF.gltfExporter/<>c::<>9__31_4
	Func_2_tB3D19331301201246DE473D608119BB47D418EB9 * ___U3CU3E9__31_4_5;
	// System.Func`2<UnityEngine.BoneWeight,UnityEngine.Vector4> UniGLTF.gltfExporter/<>c::<>9__31_5
	Func_2_t4C4B7B18D0F36E20937C252FF9DBFE003D92E320 * ___U3CU3E9__31_5_6;
	// System.Func`2<UnityEngine.BoneWeight,UniGLTF.UShort4> UniGLTF.gltfExporter/<>c::<>9__31_6
	Func_2_t4AB38DE49E02197EC72C8A674B7F54DC4752F8B8 * ___U3CU3E9__31_6_7;
	// System.Func`2<System.Int32,System.UInt32> UniGLTF.gltfExporter/<>c::<>9__31_7
	Func_2_t22229F56B3F44A8D9155EEF00ECBA1525996620B * ___U3CU3E9__31_7_8;
	// System.Func`2<UnityEngine.Vector4,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__33_0
	Func_2_tA6B20794E71B01DBB95C2366481D0913D3A5B34D * ___U3CU3E9__33_0_9;
	// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__33_1
	Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * ___U3CU3E9__33_1_10;
	// System.Func`3<UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3> UniGLTF.gltfExporter/<>c::<>9__33_2
	Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * ___U3CU3E9__33_2_11;
	// System.Func`2<UnityEngine.Transform,System.Collections.Generic.IEnumerable`1<UnityEngine.Material>> UniGLTF.gltfExporter/<>c::<>9__35_0
	Func_2_t676BF602111DE1959C332255273200551A4B53D2 * ___U3CU3E9__35_0_12;
	// System.Func`2<UnityEngine.Material,System.Boolean> UniGLTF.gltfExporter/<>c::<>9__35_1
	Func_2_t436C5731F14D84E722C51EB29BECA68D03730F16 * ___U3CU3E9__35_1_13;
	// System.Func`2<UnityEngine.Material,System.Collections.Generic.IEnumerable`1<UniGLTF.TextureIO/TextureExportItem>> UniGLTF.gltfExporter/<>c::<>9__35_2
	Func_2_t1D84F5542D491C0E19A8C6A84CC279594E29C53D * ___U3CU3E9__35_2_14;
	// System.Func`2<UniGLTF.TextureIO/TextureExportItem,System.Boolean> UniGLTF.gltfExporter/<>c::<>9__35_3
	Func_2_t561EBC4E8BCE588881F403D12167916A1466E4DA * ___U3CU3E9__35_3_15;
	// System.Func`2<UniGLTF.TextureIO/TextureExportItem,UnityEngine.Texture> UniGLTF.gltfExporter/<>c::<>9__35_4
	Func_2_t77880818CC9186B40F0FB2E9A5E2CA690C47DB98 * ___U3CU3E9__35_4_16;
	// System.Func`2<UnityEngine.Transform,UniGLTF.gltfExporter/MeshWithRenderer> UniGLTF.gltfExporter/<>c::<>9__35_6
	Func_2_t89BF755511E60DE2F3EA83866C5F5AFB8C6D2C1C * ___U3CU3E9__35_6_17;
	// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,System.Boolean> UniGLTF.gltfExporter/<>c::<>9__35_7
	Func_2_t17DE6DEF9F74C5B25D45FAEB597033B28EA32AC0 * ___U3CU3E9__35_7_18;
	// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh> UniGLTF.gltfExporter/<>c::<>9__35_8
	Func_2_t08105548739575F9FE3A646B6888D9042E732055 * ___U3CU3E9__35_8_19;
	// System.Func`2<UnityEngine.Transform,UnityEngine.SkinnedMeshRenderer> UniGLTF.gltfExporter/<>c::<>9__35_9
	Func_2_t836CCD5FBC280423E6C8739A810EF16A73E4A607 * ___U3CU3E9__35_9_20;
	// System.Func`2<UnityEngine.SkinnedMeshRenderer,System.Boolean> UniGLTF.gltfExporter/<>c::<>9__35_10
	Func_2_tCA03099759F7D272679D353D795BDD5165E4FF1A * ___U3CU3E9__35_10_21;
	// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh> UniGLTF.gltfExporter/<>c::<>9__35_12
	Func_2_t08105548739575F9FE3A646B6888D9042E732055 * ___U3CU3E9__35_12_22;
	// System.Func`2<UnityEngine.Matrix4x4,UnityEngine.Matrix4x4> UniGLTF.gltfExporter/<>c::<>9__35_14
	Func_2_t95B7B47280CD23C9B92163ACAAE5AECBD026FFAD * ___U3CU3E9__35_14_23;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_1_2)); }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * get_U3CU3E9__31_1_2() const { return ___U3CU3E9__31_1_2; }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A ** get_address_of_U3CU3E9__31_1_2() { return &___U3CU3E9__31_1_2; }
	inline void set_U3CU3E9__31_1_2(Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * value)
	{
		___U3CU3E9__31_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_2_3)); }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * get_U3CU3E9__31_2_3() const { return ___U3CU3E9__31_2_3; }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A ** get_address_of_U3CU3E9__31_2_3() { return &___U3CU3E9__31_2_3; }
	inline void set_U3CU3E9__31_2_3(Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * value)
	{
		___U3CU3E9__31_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_3_4)); }
	inline Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * get_U3CU3E9__31_3_4() const { return ___U3CU3E9__31_3_4; }
	inline Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 ** get_address_of_U3CU3E9__31_3_4() { return &___U3CU3E9__31_3_4; }
	inline void set_U3CU3E9__31_3_4(Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * value)
	{
		___U3CU3E9__31_3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_4_5)); }
	inline Func_2_tB3D19331301201246DE473D608119BB47D418EB9 * get_U3CU3E9__31_4_5() const { return ___U3CU3E9__31_4_5; }
	inline Func_2_tB3D19331301201246DE473D608119BB47D418EB9 ** get_address_of_U3CU3E9__31_4_5() { return &___U3CU3E9__31_4_5; }
	inline void set_U3CU3E9__31_4_5(Func_2_tB3D19331301201246DE473D608119BB47D418EB9 * value)
	{
		___U3CU3E9__31_4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_5_6)); }
	inline Func_2_t4C4B7B18D0F36E20937C252FF9DBFE003D92E320 * get_U3CU3E9__31_5_6() const { return ___U3CU3E9__31_5_6; }
	inline Func_2_t4C4B7B18D0F36E20937C252FF9DBFE003D92E320 ** get_address_of_U3CU3E9__31_5_6() { return &___U3CU3E9__31_5_6; }
	inline void set_U3CU3E9__31_5_6(Func_2_t4C4B7B18D0F36E20937C252FF9DBFE003D92E320 * value)
	{
		___U3CU3E9__31_5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_6_7)); }
	inline Func_2_t4AB38DE49E02197EC72C8A674B7F54DC4752F8B8 * get_U3CU3E9__31_6_7() const { return ___U3CU3E9__31_6_7; }
	inline Func_2_t4AB38DE49E02197EC72C8A674B7F54DC4752F8B8 ** get_address_of_U3CU3E9__31_6_7() { return &___U3CU3E9__31_6_7; }
	inline void set_U3CU3E9__31_6_7(Func_2_t4AB38DE49E02197EC72C8A674B7F54DC4752F8B8 * value)
	{
		___U3CU3E9__31_6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_6_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__31_7_8)); }
	inline Func_2_t22229F56B3F44A8D9155EEF00ECBA1525996620B * get_U3CU3E9__31_7_8() const { return ___U3CU3E9__31_7_8; }
	inline Func_2_t22229F56B3F44A8D9155EEF00ECBA1525996620B ** get_address_of_U3CU3E9__31_7_8() { return &___U3CU3E9__31_7_8; }
	inline void set_U3CU3E9__31_7_8(Func_2_t22229F56B3F44A8D9155EEF00ECBA1525996620B * value)
	{
		___U3CU3E9__31_7_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__31_7_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__33_0_9)); }
	inline Func_2_tA6B20794E71B01DBB95C2366481D0913D3A5B34D * get_U3CU3E9__33_0_9() const { return ___U3CU3E9__33_0_9; }
	inline Func_2_tA6B20794E71B01DBB95C2366481D0913D3A5B34D ** get_address_of_U3CU3E9__33_0_9() { return &___U3CU3E9__33_0_9; }
	inline void set_U3CU3E9__33_0_9(Func_2_tA6B20794E71B01DBB95C2366481D0913D3A5B34D * value)
	{
		___U3CU3E9__33_0_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__33_0_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_1_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__33_1_10)); }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * get_U3CU3E9__33_1_10() const { return ___U3CU3E9__33_1_10; }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A ** get_address_of_U3CU3E9__33_1_10() { return &___U3CU3E9__33_1_10; }
	inline void set_U3CU3E9__33_1_10(Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * value)
	{
		___U3CU3E9__33_1_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__33_1_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_2_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__33_2_11)); }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * get_U3CU3E9__33_2_11() const { return ___U3CU3E9__33_2_11; }
	inline Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A ** get_address_of_U3CU3E9__33_2_11() { return &___U3CU3E9__33_2_11; }
	inline void set_U3CU3E9__33_2_11(Func_3_t4E0812D8B048478D8ADD983CA784A7940206E96A * value)
	{
		___U3CU3E9__33_2_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__33_2_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_0_12)); }
	inline Func_2_t676BF602111DE1959C332255273200551A4B53D2 * get_U3CU3E9__35_0_12() const { return ___U3CU3E9__35_0_12; }
	inline Func_2_t676BF602111DE1959C332255273200551A4B53D2 ** get_address_of_U3CU3E9__35_0_12() { return &___U3CU3E9__35_0_12; }
	inline void set_U3CU3E9__35_0_12(Func_2_t676BF602111DE1959C332255273200551A4B53D2 * value)
	{
		___U3CU3E9__35_0_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_0_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_1_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_1_13)); }
	inline Func_2_t436C5731F14D84E722C51EB29BECA68D03730F16 * get_U3CU3E9__35_1_13() const { return ___U3CU3E9__35_1_13; }
	inline Func_2_t436C5731F14D84E722C51EB29BECA68D03730F16 ** get_address_of_U3CU3E9__35_1_13() { return &___U3CU3E9__35_1_13; }
	inline void set_U3CU3E9__35_1_13(Func_2_t436C5731F14D84E722C51EB29BECA68D03730F16 * value)
	{
		___U3CU3E9__35_1_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_1_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_2_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_2_14)); }
	inline Func_2_t1D84F5542D491C0E19A8C6A84CC279594E29C53D * get_U3CU3E9__35_2_14() const { return ___U3CU3E9__35_2_14; }
	inline Func_2_t1D84F5542D491C0E19A8C6A84CC279594E29C53D ** get_address_of_U3CU3E9__35_2_14() { return &___U3CU3E9__35_2_14; }
	inline void set_U3CU3E9__35_2_14(Func_2_t1D84F5542D491C0E19A8C6A84CC279594E29C53D * value)
	{
		___U3CU3E9__35_2_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_2_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_3_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_3_15)); }
	inline Func_2_t561EBC4E8BCE588881F403D12167916A1466E4DA * get_U3CU3E9__35_3_15() const { return ___U3CU3E9__35_3_15; }
	inline Func_2_t561EBC4E8BCE588881F403D12167916A1466E4DA ** get_address_of_U3CU3E9__35_3_15() { return &___U3CU3E9__35_3_15; }
	inline void set_U3CU3E9__35_3_15(Func_2_t561EBC4E8BCE588881F403D12167916A1466E4DA * value)
	{
		___U3CU3E9__35_3_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_3_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_4_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_4_16)); }
	inline Func_2_t77880818CC9186B40F0FB2E9A5E2CA690C47DB98 * get_U3CU3E9__35_4_16() const { return ___U3CU3E9__35_4_16; }
	inline Func_2_t77880818CC9186B40F0FB2E9A5E2CA690C47DB98 ** get_address_of_U3CU3E9__35_4_16() { return &___U3CU3E9__35_4_16; }
	inline void set_U3CU3E9__35_4_16(Func_2_t77880818CC9186B40F0FB2E9A5E2CA690C47DB98 * value)
	{
		___U3CU3E9__35_4_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_4_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_6_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_6_17)); }
	inline Func_2_t89BF755511E60DE2F3EA83866C5F5AFB8C6D2C1C * get_U3CU3E9__35_6_17() const { return ___U3CU3E9__35_6_17; }
	inline Func_2_t89BF755511E60DE2F3EA83866C5F5AFB8C6D2C1C ** get_address_of_U3CU3E9__35_6_17() { return &___U3CU3E9__35_6_17; }
	inline void set_U3CU3E9__35_6_17(Func_2_t89BF755511E60DE2F3EA83866C5F5AFB8C6D2C1C * value)
	{
		___U3CU3E9__35_6_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_6_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_7_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_7_18)); }
	inline Func_2_t17DE6DEF9F74C5B25D45FAEB597033B28EA32AC0 * get_U3CU3E9__35_7_18() const { return ___U3CU3E9__35_7_18; }
	inline Func_2_t17DE6DEF9F74C5B25D45FAEB597033B28EA32AC0 ** get_address_of_U3CU3E9__35_7_18() { return &___U3CU3E9__35_7_18; }
	inline void set_U3CU3E9__35_7_18(Func_2_t17DE6DEF9F74C5B25D45FAEB597033B28EA32AC0 * value)
	{
		___U3CU3E9__35_7_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_7_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_8_19() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_8_19)); }
	inline Func_2_t08105548739575F9FE3A646B6888D9042E732055 * get_U3CU3E9__35_8_19() const { return ___U3CU3E9__35_8_19; }
	inline Func_2_t08105548739575F9FE3A646B6888D9042E732055 ** get_address_of_U3CU3E9__35_8_19() { return &___U3CU3E9__35_8_19; }
	inline void set_U3CU3E9__35_8_19(Func_2_t08105548739575F9FE3A646B6888D9042E732055 * value)
	{
		___U3CU3E9__35_8_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_8_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_9_20() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_9_20)); }
	inline Func_2_t836CCD5FBC280423E6C8739A810EF16A73E4A607 * get_U3CU3E9__35_9_20() const { return ___U3CU3E9__35_9_20; }
	inline Func_2_t836CCD5FBC280423E6C8739A810EF16A73E4A607 ** get_address_of_U3CU3E9__35_9_20() { return &___U3CU3E9__35_9_20; }
	inline void set_U3CU3E9__35_9_20(Func_2_t836CCD5FBC280423E6C8739A810EF16A73E4A607 * value)
	{
		___U3CU3E9__35_9_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_9_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_10_21() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_10_21)); }
	inline Func_2_tCA03099759F7D272679D353D795BDD5165E4FF1A * get_U3CU3E9__35_10_21() const { return ___U3CU3E9__35_10_21; }
	inline Func_2_tCA03099759F7D272679D353D795BDD5165E4FF1A ** get_address_of_U3CU3E9__35_10_21() { return &___U3CU3E9__35_10_21; }
	inline void set_U3CU3E9__35_10_21(Func_2_tCA03099759F7D272679D353D795BDD5165E4FF1A * value)
	{
		___U3CU3E9__35_10_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_10_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_12_22() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_12_22)); }
	inline Func_2_t08105548739575F9FE3A646B6888D9042E732055 * get_U3CU3E9__35_12_22() const { return ___U3CU3E9__35_12_22; }
	inline Func_2_t08105548739575F9FE3A646B6888D9042E732055 ** get_address_of_U3CU3E9__35_12_22() { return &___U3CU3E9__35_12_22; }
	inline void set_U3CU3E9__35_12_22(Func_2_t08105548739575F9FE3A646B6888D9042E732055 * value)
	{
		___U3CU3E9__35_12_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_12_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_14_23() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields, ___U3CU3E9__35_14_23)); }
	inline Func_2_t95B7B47280CD23C9B92163ACAAE5AECBD026FFAD * get_U3CU3E9__35_14_23() const { return ___U3CU3E9__35_14_23; }
	inline Func_2_t95B7B47280CD23C9B92163ACAAE5AECBD026FFAD ** get_address_of_U3CU3E9__35_14_23() { return &___U3CU3E9__35_14_23; }
	inline void set_U3CU3E9__35_14_23(Func_2_t95B7B47280CD23C9B92163ACAAE5AECBD026FFAD * value)
	{
		___U3CU3E9__35_14_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_14_23), (void*)value);
	}
};


// UniGLTF.gltfExporter/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_tD105192BEFDF5A92F21A1211C6994A11ECF88B07  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> UniGLTF.gltfExporter/<>c__DisplayClass30_0::nodes
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___nodes_0;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_tD105192BEFDF5A92F21A1211C6994A11ECF88B07, ___nodes_0)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_nodes_0() const { return ___nodes_0; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_0), (void*)value);
	}
};


// UniGLTF.gltfExporter/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3  : public RuntimeObject
{
public:
	// System.Boolean UniGLTF.gltfExporter/<>c__DisplayClass33_0::usePosition
	bool ___usePosition_0;
	// UnityEngine.Vector3[] UniGLTF.gltfExporter/<>c__DisplayClass33_0::blendShapeVertices
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___blendShapeVertices_1;
	// System.Boolean UniGLTF.gltfExporter/<>c__DisplayClass33_0::useNormal
	bool ___useNormal_2;
	// UnityEngine.Vector3[] UniGLTF.gltfExporter/<>c__DisplayClass33_0::blendShapeNormals
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___blendShapeNormals_3;
	// System.Boolean UniGLTF.gltfExporter/<>c__DisplayClass33_0::useTangent
	bool ___useTangent_4;
	// UnityEngine.Vector3[] UniGLTF.gltfExporter/<>c__DisplayClass33_0::blendShapeTangents
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___blendShapeTangents_5;

public:
	inline static int32_t get_offset_of_usePosition_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___usePosition_0)); }
	inline bool get_usePosition_0() const { return ___usePosition_0; }
	inline bool* get_address_of_usePosition_0() { return &___usePosition_0; }
	inline void set_usePosition_0(bool value)
	{
		___usePosition_0 = value;
	}

	inline static int32_t get_offset_of_blendShapeVertices_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___blendShapeVertices_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_blendShapeVertices_1() const { return ___blendShapeVertices_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_blendShapeVertices_1() { return &___blendShapeVertices_1; }
	inline void set_blendShapeVertices_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___blendShapeVertices_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blendShapeVertices_1), (void*)value);
	}

	inline static int32_t get_offset_of_useNormal_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___useNormal_2)); }
	inline bool get_useNormal_2() const { return ___useNormal_2; }
	inline bool* get_address_of_useNormal_2() { return &___useNormal_2; }
	inline void set_useNormal_2(bool value)
	{
		___useNormal_2 = value;
	}

	inline static int32_t get_offset_of_blendShapeNormals_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___blendShapeNormals_3)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_blendShapeNormals_3() const { return ___blendShapeNormals_3; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_blendShapeNormals_3() { return &___blendShapeNormals_3; }
	inline void set_blendShapeNormals_3(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___blendShapeNormals_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blendShapeNormals_3), (void*)value);
	}

	inline static int32_t get_offset_of_useTangent_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___useTangent_4)); }
	inline bool get_useTangent_4() const { return ___useTangent_4; }
	inline bool* get_address_of_useTangent_4() { return &___useTangent_4; }
	inline void set_useTangent_4(bool value)
	{
		___useTangent_4 = value;
	}

	inline static int32_t get_offset_of_blendShapeTangents_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3, ___blendShapeTangents_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_blendShapeTangents_5() const { return ___blendShapeTangents_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_blendShapeTangents_5() { return &___blendShapeTangents_5; }
	inline void set_blendShapeTangents_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___blendShapeTangents_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blendShapeTangents_5), (void*)value);
	}
};


// UniGLTF.gltfExporter/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83  : public RuntimeObject
{
public:
	// UniGLTF.IMaterialExporter UniGLTF.gltfExporter/<>c__DisplayClass35_0::materialExporter
	RuntimeObject* ___materialExporter_0;
	// System.Collections.Generic.List`1<UniGLTF.gltfExporter/MeshWithRenderer> UniGLTF.gltfExporter/<>c__DisplayClass35_0::unityMeshes
	List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * ___unityMeshes_1;
	// System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer> UniGLTF.gltfExporter/<>c__DisplayClass35_0::unitySkins
	List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 * ___unitySkins_2;
	// UniGLTF.gltfExporter UniGLTF.gltfExporter/<>c__DisplayClass35_0::<>4__this
	gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_materialExporter_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83, ___materialExporter_0)); }
	inline RuntimeObject* get_materialExporter_0() const { return ___materialExporter_0; }
	inline RuntimeObject** get_address_of_materialExporter_0() { return &___materialExporter_0; }
	inline void set_materialExporter_0(RuntimeObject* value)
	{
		___materialExporter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialExporter_0), (void*)value);
	}

	inline static int32_t get_offset_of_unityMeshes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83, ___unityMeshes_1)); }
	inline List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * get_unityMeshes_1() const { return ___unityMeshes_1; }
	inline List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 ** get_address_of_unityMeshes_1() { return &___unityMeshes_1; }
	inline void set_unityMeshes_1(List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * value)
	{
		___unityMeshes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unityMeshes_1), (void*)value);
	}

	inline static int32_t get_offset_of_unitySkins_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83, ___unitySkins_2)); }
	inline List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 * get_unitySkins_2() const { return ___unitySkins_2; }
	inline List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 ** get_address_of_unitySkins_2() { return &___unitySkins_2; }
	inline void set_unitySkins_2(List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 * value)
	{
		___unitySkins_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unitySkins_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83, ___U3CU3E4__this_3)); }
	inline gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// UniGLTF.gltfExporter/<>c__DisplayClass35_1
struct U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC  : public RuntimeObject
{
public:
	// UnityEngine.SkinnedMeshRenderer UniGLTF.gltfExporter/<>c__DisplayClass35_1::x
	SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___x_0;
	// System.Func`2<UnityEngine.Transform,System.Boolean> UniGLTF.gltfExporter/<>c__DisplayClass35_1::<>9__16
	Func_2_t0CD378E6B47E8AE92216FCBB5D1D50088AE581B6 * ___U3CU3E9__16_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC, ___x_0)); }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * get_x_0() const { return ___x_0; }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___x_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC, ___U3CU3E9__16_1)); }
	inline Func_2_t0CD378E6B47E8AE92216FCBB5D1D50088AE581B6 * get_U3CU3E9__16_1() const { return ___U3CU3E9__16_1; }
	inline Func_2_t0CD378E6B47E8AE92216FCBB5D1D50088AE581B6 ** get_address_of_U3CU3E9__16_1() { return &___U3CU3E9__16_1; }
	inline void set_U3CU3E9__16_1(Func_2_t0CD378E6B47E8AE92216FCBB5D1D50088AE581B6 * value)
	{
		___U3CU3E9__16_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_1), (void*)value);
	}
};


// UniGLTF.gltfImporter/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16  : public RuntimeObject
{
public:
	// System.Boolean UniGLTF.gltfImporter/<>c__DisplayClass3_0::show
	bool ___show_0;
	// UniGLTF.ImporterContext UniGLTF.gltfImporter/<>c__DisplayClass3_0::context
	ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * ___context_1;
	// System.Action`1<UnityEngine.GameObject> UniGLTF.gltfImporter/<>c__DisplayClass3_0::onLoaded
	Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D * ___onLoaded_2;

public:
	inline static int32_t get_offset_of_show_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16, ___show_0)); }
	inline bool get_show_0() const { return ___show_0; }
	inline bool* get_address_of_show_0() { return &___show_0; }
	inline void set_show_0(bool value)
	{
		___show_0 = value;
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16, ___context_1)); }
	inline ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * get_context_1() const { return ___context_1; }
	inline ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___context_1), (void*)value);
	}

	inline static int32_t get_offset_of_onLoaded_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16, ___onLoaded_2)); }
	inline Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D * get_onLoaded_2() const { return ___onLoaded_2; }
	inline Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D ** get_address_of_onLoaded_2() { return &___onLoaded_2; }
	inline void set_onLoaded_2(Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D * value)
	{
		___onLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onLoaded_2), (void*)value);
	}
};


// DepthFirstScheduler.Scheduler/ThreadPoolScheduler/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tE82D0128C1DFC11ACA808215BDEB39E3AB8B3FDD  : public RuntimeObject
{
public:
	// DepthFirstScheduler.TaskChain DepthFirstScheduler.Scheduler/ThreadPoolScheduler/<>c__DisplayClass0_0::item
	TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tE82D0128C1DFC11ACA808215BDEB39E3AB8B3FDD, ___item_0)); }
	inline TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * get_item_0() const { return ___item_0; }
	inline TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}
};


// System.ArraySegment`1<System.Byte>
struct ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____array_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>
struct KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986, ___value_1)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_value_1() const { return ___value_1; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// UnityEngine.BoneWeight
struct BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 
{
public:
	// System.Single UnityEngine.BoneWeight::m_Weight0
	float ___m_Weight0_0;
	// System.Single UnityEngine.BoneWeight::m_Weight1
	float ___m_Weight1_1;
	// System.Single UnityEngine.BoneWeight::m_Weight2
	float ___m_Weight2_2;
	// System.Single UnityEngine.BoneWeight::m_Weight3
	float ___m_Weight3_3;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex0
	int32_t ___m_BoneIndex0_4;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex1
	int32_t ___m_BoneIndex1_5;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex2
	int32_t ___m_BoneIndex2_6;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex3
	int32_t ___m_BoneIndex3_7;

public:
	inline static int32_t get_offset_of_m_Weight0_0() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_Weight0_0)); }
	inline float get_m_Weight0_0() const { return ___m_Weight0_0; }
	inline float* get_address_of_m_Weight0_0() { return &___m_Weight0_0; }
	inline void set_m_Weight0_0(float value)
	{
		___m_Weight0_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight1_1() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_Weight1_1)); }
	inline float get_m_Weight1_1() const { return ___m_Weight1_1; }
	inline float* get_address_of_m_Weight1_1() { return &___m_Weight1_1; }
	inline void set_m_Weight1_1(float value)
	{
		___m_Weight1_1 = value;
	}

	inline static int32_t get_offset_of_m_Weight2_2() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_Weight2_2)); }
	inline float get_m_Weight2_2() const { return ___m_Weight2_2; }
	inline float* get_address_of_m_Weight2_2() { return &___m_Weight2_2; }
	inline void set_m_Weight2_2(float value)
	{
		___m_Weight2_2 = value;
	}

	inline static int32_t get_offset_of_m_Weight3_3() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_Weight3_3)); }
	inline float get_m_Weight3_3() const { return ___m_Weight3_3; }
	inline float* get_address_of_m_Weight3_3() { return &___m_Weight3_3; }
	inline void set_m_Weight3_3(float value)
	{
		___m_Weight3_3 = value;
	}

	inline static int32_t get_offset_of_m_BoneIndex0_4() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_BoneIndex0_4)); }
	inline int32_t get_m_BoneIndex0_4() const { return ___m_BoneIndex0_4; }
	inline int32_t* get_address_of_m_BoneIndex0_4() { return &___m_BoneIndex0_4; }
	inline void set_m_BoneIndex0_4(int32_t value)
	{
		___m_BoneIndex0_4 = value;
	}

	inline static int32_t get_offset_of_m_BoneIndex1_5() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_BoneIndex1_5)); }
	inline int32_t get_m_BoneIndex1_5() const { return ___m_BoneIndex1_5; }
	inline int32_t* get_address_of_m_BoneIndex1_5() { return &___m_BoneIndex1_5; }
	inline void set_m_BoneIndex1_5(int32_t value)
	{
		___m_BoneIndex1_5 = value;
	}

	inline static int32_t get_offset_of_m_BoneIndex2_6() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_BoneIndex2_6)); }
	inline int32_t get_m_BoneIndex2_6() const { return ___m_BoneIndex2_6; }
	inline int32_t* get_address_of_m_BoneIndex2_6() { return &___m_BoneIndex2_6; }
	inline void set_m_BoneIndex2_6(int32_t value)
	{
		___m_BoneIndex2_6 = value;
	}

	inline static int32_t get_offset_of_m_BoneIndex3_7() { return static_cast<int32_t>(offsetof(BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8, ___m_BoneIndex3_7)); }
	inline int32_t get_m_BoneIndex3_7() const { return ___m_BoneIndex3_7; }
	inline int32_t* get_address_of_m_BoneIndex3_7() { return &___m_BoneIndex3_7; }
	inline void set_m_BoneIndex3_7(int32_t value)
	{
		___m_BoneIndex3_7 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeReadWriteTask_2), (void*)value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncActiveSemaphore_3), (void*)value);
	}
};

struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields, ___Null_1)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_Null_1() const { return ___Null_1; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}
};


// System.UInt16
struct UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UniGLTF.UShort4
#pragma pack(push, tp, 1)
struct UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB 
{
public:
	// System.UInt16 UniGLTF.UShort4::x
	uint16_t ___x_0;
	// System.UInt16 UniGLTF.UShort4::y
	uint16_t ___y_1;
	// System.UInt16 UniGLTF.UShort4::z
	uint16_t ___z_2;
	// System.UInt16 UniGLTF.UShort4::w
	uint16_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___x_0)); }
	inline uint16_t get_x_0() const { return ___x_0; }
	inline uint16_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint16_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___y_1)); }
	inline uint16_t get_y_1() const { return ___y_1; }
	inline uint16_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint16_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___z_2)); }
	inline uint16_t get_z_2() const { return ___z_2; }
	inline uint16_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint16_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___w_3)); }
	inline uint16_t get_w_3() const { return ___w_3; }
	inline uint16_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(uint16_t value)
	{
		___w_3 = value;
	}
};
#pragma pack(pop, tp)


// DepthFirstScheduler.Unit
struct Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D__padding[1];
	};

public:
};

struct Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D_StaticFields
{
public:
	// DepthFirstScheduler.Unit DepthFirstScheduler.Unit::default
	Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D  ___default_0;

public:
	inline static int32_t get_offset_of_default_0() { return static_cast<int32_t>(offsetof(Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D_StaticFields, ___default_0)); }
	inline Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D  get_default_0() const { return ___default_0; }
	inline Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D * get_address_of_default_0() { return &___default_0; }
	inline void set_default_0(Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D  value)
	{
		___default_0 = value;
	}
};


// UniGLTF.UnityPath
struct UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 
{
public:
	// System.String UniGLTF.UnityPath::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0, ___U3CValueU3Ek__BackingField_0)); }
	inline String_t* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(String_t* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CValueU3Ek__BackingField_0), (void*)value);
	}
};

struct UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_StaticFields
{
public:
	// System.Char[] UniGLTF.UnityPath::EscapeChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___EscapeChars_1;
	// System.String UniGLTF.UnityPath::s_basePath
	String_t* ___s_basePath_2;

public:
	inline static int32_t get_offset_of_EscapeChars_1() { return static_cast<int32_t>(offsetof(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_StaticFields, ___EscapeChars_1)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_EscapeChars_1() const { return ___EscapeChars_1; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_EscapeChars_1() { return &___EscapeChars_1; }
	inline void set_EscapeChars_1(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___EscapeChars_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EscapeChars_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_basePath_2() { return static_cast<int32_t>(offsetof(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_StaticFields, ___s_basePath_2)); }
	inline String_t* get_s_basePath_2() const { return ___s_basePath_2; }
	inline String_t** get_address_of_s_basePath_2() { return &___s_basePath_2; }
	inline void set_s_basePath_2(String_t* value)
	{
		___s_basePath_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_basePath_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.UnityPath
struct UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_marshaled_pinvoke
{
	char* ___U3CValueU3Ek__BackingField_0;
};
// Native definition for COM marshalling of UniGLTF.UnityPath
struct UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_marshaled_com
{
	Il2CppChar* ___U3CValueU3Ek__BackingField_0;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WWW
struct WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2, ____uwr_0)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____uwr_0), (void*)value);
	}
};


// UniGLTF.glTF
struct glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// UniGLTF.glTFAssets UniGLTF.glTF::asset
	glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * ___asset_0;
	// System.Collections.Generic.List`1<UniGLTF.glTFBuffer> UniGLTF.glTF::buffers
	List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * ___buffers_1;
	// System.Collections.Generic.List`1<UniGLTF.glTFBufferView> UniGLTF.glTF::bufferViews
	List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * ___bufferViews_2;
	// System.Collections.Generic.List`1<UniGLTF.glTFAccessor> UniGLTF.glTF::accessors
	List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * ___accessors_3;
	// System.Collections.Generic.List`1<UniGLTF.glTFTexture> UniGLTF.glTF::textures
	List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * ___textures_4;
	// System.Collections.Generic.List`1<UniGLTF.glTFTextureSampler> UniGLTF.glTF::samplers
	List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * ___samplers_5;
	// System.Collections.Generic.List`1<UniGLTF.glTFImage> UniGLTF.glTF::images
	List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * ___images_6;
	// System.Collections.Generic.List`1<UniGLTF.glTFMaterial> UniGLTF.glTF::materials
	List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * ___materials_7;
	// System.Collections.Generic.List`1<UniGLTF.glTFMesh> UniGLTF.glTF::meshes
	List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * ___meshes_8;
	// System.Collections.Generic.List`1<UniGLTF.glTFNode> UniGLTF.glTF::nodes
	List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * ___nodes_9;
	// System.Collections.Generic.List`1<UniGLTF.glTFSkin> UniGLTF.glTF::skins
	List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * ___skins_10;
	// System.Int32 UniGLTF.glTF::scene
	int32_t ___scene_11;
	// System.Collections.Generic.List`1<UniGLTF.gltfScene> UniGLTF.glTF::scenes
	List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * ___scenes_12;
	// System.Collections.Generic.List`1<UniGLTF.glTFAnimation> UniGLTF.glTF::animations
	List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * ___animations_13;
	// System.Collections.Generic.List`1<UniGLTF.glTFCamera> UniGLTF.glTF::cameras
	List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * ___cameras_14;
	// System.Collections.Generic.List`1<System.String> UniGLTF.glTF::extensionsUsed
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___extensionsUsed_15;
	// System.Collections.Generic.List`1<System.String> UniGLTF.glTF::extensionsRequired
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___extensionsRequired_16;
	// UniGLTF.glTF_extensions UniGLTF.glTF::extensions
	glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * ___extensions_17;
	// UniGLTF.gltf_extras UniGLTF.glTF::extras
	gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * ___extras_18;

public:
	inline static int32_t get_offset_of_asset_0() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___asset_0)); }
	inline glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * get_asset_0() const { return ___asset_0; }
	inline glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 ** get_address_of_asset_0() { return &___asset_0; }
	inline void set_asset_0(glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * value)
	{
		___asset_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asset_0), (void*)value);
	}

	inline static int32_t get_offset_of_buffers_1() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___buffers_1)); }
	inline List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * get_buffers_1() const { return ___buffers_1; }
	inline List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 ** get_address_of_buffers_1() { return &___buffers_1; }
	inline void set_buffers_1(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * value)
	{
		___buffers_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffers_1), (void*)value);
	}

	inline static int32_t get_offset_of_bufferViews_2() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___bufferViews_2)); }
	inline List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * get_bufferViews_2() const { return ___bufferViews_2; }
	inline List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 ** get_address_of_bufferViews_2() { return &___bufferViews_2; }
	inline void set_bufferViews_2(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * value)
	{
		___bufferViews_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bufferViews_2), (void*)value);
	}

	inline static int32_t get_offset_of_accessors_3() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___accessors_3)); }
	inline List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * get_accessors_3() const { return ___accessors_3; }
	inline List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC ** get_address_of_accessors_3() { return &___accessors_3; }
	inline void set_accessors_3(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * value)
	{
		___accessors_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___accessors_3), (void*)value);
	}

	inline static int32_t get_offset_of_textures_4() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___textures_4)); }
	inline List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * get_textures_4() const { return ___textures_4; }
	inline List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 ** get_address_of_textures_4() { return &___textures_4; }
	inline void set_textures_4(List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * value)
	{
		___textures_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textures_4), (void*)value);
	}

	inline static int32_t get_offset_of_samplers_5() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___samplers_5)); }
	inline List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * get_samplers_5() const { return ___samplers_5; }
	inline List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 ** get_address_of_samplers_5() { return &___samplers_5; }
	inline void set_samplers_5(List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * value)
	{
		___samplers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplers_5), (void*)value);
	}

	inline static int32_t get_offset_of_images_6() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___images_6)); }
	inline List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * get_images_6() const { return ___images_6; }
	inline List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD ** get_address_of_images_6() { return &___images_6; }
	inline void set_images_6(List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * value)
	{
		___images_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___images_6), (void*)value);
	}

	inline static int32_t get_offset_of_materials_7() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___materials_7)); }
	inline List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * get_materials_7() const { return ___materials_7; }
	inline List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 ** get_address_of_materials_7() { return &___materials_7; }
	inline void set_materials_7(List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * value)
	{
		___materials_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materials_7), (void*)value);
	}

	inline static int32_t get_offset_of_meshes_8() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___meshes_8)); }
	inline List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * get_meshes_8() const { return ___meshes_8; }
	inline List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C ** get_address_of_meshes_8() { return &___meshes_8; }
	inline void set_meshes_8(List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * value)
	{
		___meshes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshes_8), (void*)value);
	}

	inline static int32_t get_offset_of_nodes_9() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___nodes_9)); }
	inline List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * get_nodes_9() const { return ___nodes_9; }
	inline List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 ** get_address_of_nodes_9() { return &___nodes_9; }
	inline void set_nodes_9(List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * value)
	{
		___nodes_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_9), (void*)value);
	}

	inline static int32_t get_offset_of_skins_10() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___skins_10)); }
	inline List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * get_skins_10() const { return ___skins_10; }
	inline List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 ** get_address_of_skins_10() { return &___skins_10; }
	inline void set_skins_10(List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * value)
	{
		___skins_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skins_10), (void*)value);
	}

	inline static int32_t get_offset_of_scene_11() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___scene_11)); }
	inline int32_t get_scene_11() const { return ___scene_11; }
	inline int32_t* get_address_of_scene_11() { return &___scene_11; }
	inline void set_scene_11(int32_t value)
	{
		___scene_11 = value;
	}

	inline static int32_t get_offset_of_scenes_12() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___scenes_12)); }
	inline List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * get_scenes_12() const { return ___scenes_12; }
	inline List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 ** get_address_of_scenes_12() { return &___scenes_12; }
	inline void set_scenes_12(List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * value)
	{
		___scenes_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scenes_12), (void*)value);
	}

	inline static int32_t get_offset_of_animations_13() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___animations_13)); }
	inline List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * get_animations_13() const { return ___animations_13; }
	inline List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 ** get_address_of_animations_13() { return &___animations_13; }
	inline void set_animations_13(List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * value)
	{
		___animations_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animations_13), (void*)value);
	}

	inline static int32_t get_offset_of_cameras_14() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___cameras_14)); }
	inline List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * get_cameras_14() const { return ___cameras_14; }
	inline List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C ** get_address_of_cameras_14() { return &___cameras_14; }
	inline void set_cameras_14(List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * value)
	{
		___cameras_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameras_14), (void*)value);
	}

	inline static int32_t get_offset_of_extensionsUsed_15() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensionsUsed_15)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_extensionsUsed_15() const { return ___extensionsUsed_15; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_extensionsUsed_15() { return &___extensionsUsed_15; }
	inline void set_extensionsUsed_15(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___extensionsUsed_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensionsUsed_15), (void*)value);
	}

	inline static int32_t get_offset_of_extensionsRequired_16() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensionsRequired_16)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_extensionsRequired_16() const { return ___extensionsRequired_16; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_extensionsRequired_16() { return &___extensionsRequired_16; }
	inline void set_extensionsRequired_16(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___extensionsRequired_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensionsRequired_16), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_17() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensions_17)); }
	inline glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * get_extensions_17() const { return ___extensions_17; }
	inline glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 ** get_address_of_extensions_17() { return &___extensions_17; }
	inline void set_extensions_17(glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * value)
	{
		___extensions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_17), (void*)value);
	}

	inline static int32_t get_offset_of_extras_18() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extras_18)); }
	inline gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * get_extras_18() const { return ___extras_18; }
	inline gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 ** get_address_of_extras_18() { return &___extras_18; }
	inline void set_extras_18(gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * value)
	{
		___extras_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_18), (void*)value);
	}
};


// UniGLTF.glTFAnimationChannel
struct glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFAnimationChannel::sampler
	int32_t ___sampler_0;
	// UniGLTF.glTFAnimationTarget UniGLTF.glTFAnimationChannel::target
	glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A * ___target_1;
	// System.Object UniGLTF.glTFAnimationChannel::extensions
	RuntimeObject * ___extensions_2;
	// System.Object UniGLTF.glTFAnimationChannel::extras
	RuntimeObject * ___extras_3;

public:
	inline static int32_t get_offset_of_sampler_0() { return static_cast<int32_t>(offsetof(glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B, ___sampler_0)); }
	inline int32_t get_sampler_0() const { return ___sampler_0; }
	inline int32_t* get_address_of_sampler_0() { return &___sampler_0; }
	inline void set_sampler_0(int32_t value)
	{
		___sampler_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B, ___target_1)); }
	inline glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A * get_target_1() const { return ___target_1; }
	inline glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B, ___extensions_2)); }
	inline RuntimeObject * get_extensions_2() const { return ___extensions_2; }
	inline RuntimeObject ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(RuntimeObject * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_2), (void*)value);
	}

	inline static int32_t get_offset_of_extras_3() { return static_cast<int32_t>(offsetof(glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B, ___extras_3)); }
	inline RuntimeObject * get_extras_3() const { return ___extras_3; }
	inline RuntimeObject ** get_address_of_extras_3() { return &___extras_3; }
	inline void set_extras_3(RuntimeObject * value)
	{
		___extras_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_3), (void*)value);
	}
};


// UniGLTF.glTFAnimationTarget
struct glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFAnimationTarget::node
	int32_t ___node_0;
	// System.String UniGLTF.glTFAnimationTarget::path
	String_t* ___path_1;
	// System.Object UniGLTF.glTFAnimationTarget::extensions
	RuntimeObject * ___extensions_2;
	// System.Object UniGLTF.glTFAnimationTarget::extras
	RuntimeObject * ___extras_3;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A, ___node_0)); }
	inline int32_t get_node_0() const { return ___node_0; }
	inline int32_t* get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(int32_t value)
	{
		___node_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_1), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A, ___extensions_2)); }
	inline RuntimeObject * get_extensions_2() const { return ___extensions_2; }
	inline RuntimeObject ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(RuntimeObject * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_2), (void*)value);
	}

	inline static int32_t get_offset_of_extras_3() { return static_cast<int32_t>(offsetof(glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A, ___extras_3)); }
	inline RuntimeObject * get_extras_3() const { return ___extras_3; }
	inline RuntimeObject ** get_address_of_extras_3() { return &___extras_3; }
	inline void set_extras_3(RuntimeObject * value)
	{
		___extras_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_3), (void*)value);
	}
};


// UniGLTF.glTFMaterial
struct glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.String UniGLTF.glTFMaterial::name
	String_t* ___name_0;
	// UniGLTF.glTFPbrMetallicRoughness UniGLTF.glTFMaterial::pbrMetallicRoughness
	glTFPbrMetallicRoughness_t8D5AB37B5167EAB04B72D54E7BFE73CA384618EA * ___pbrMetallicRoughness_1;
	// UniGLTF.glTFMaterialNormalTextureInfo UniGLTF.glTFMaterial::normalTexture
	glTFMaterialNormalTextureInfo_t6D150E15CEA24273D7B0C80E66F9B22107970019 * ___normalTexture_2;
	// UniGLTF.glTFMaterialOcclusionTextureInfo UniGLTF.glTFMaterial::occlusionTexture
	glTFMaterialOcclusionTextureInfo_t69A60A1A8A9C1781D618D4AFA7B2B5487E8B6B4D * ___occlusionTexture_3;
	// UniGLTF.glTFMaterialEmissiveTextureInfo UniGLTF.glTFMaterial::emissiveTexture
	glTFMaterialEmissiveTextureInfo_t2F9A3AFEB20366CBA1E180C0414525BA75B8681B * ___emissiveTexture_4;
	// System.Single[] UniGLTF.glTFMaterial::emissiveFactor
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___emissiveFactor_5;
	// System.String UniGLTF.glTFMaterial::alphaMode
	String_t* ___alphaMode_6;
	// System.Single UniGLTF.glTFMaterial::alphaCutoff
	float ___alphaCutoff_7;
	// System.Boolean UniGLTF.glTFMaterial::doubleSided
	bool ___doubleSided_8;
	// UniGLTF.glTFMaterial_extensions UniGLTF.glTFMaterial::extensions
	glTFMaterial_extensions_t7A04698CBFFFCEC246DD6B72454299069EBD22FF * ___extensions_9;
	// System.Object UniGLTF.glTFMaterial::extras
	RuntimeObject * ___extras_10;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_pbrMetallicRoughness_1() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___pbrMetallicRoughness_1)); }
	inline glTFPbrMetallicRoughness_t8D5AB37B5167EAB04B72D54E7BFE73CA384618EA * get_pbrMetallicRoughness_1() const { return ___pbrMetallicRoughness_1; }
	inline glTFPbrMetallicRoughness_t8D5AB37B5167EAB04B72D54E7BFE73CA384618EA ** get_address_of_pbrMetallicRoughness_1() { return &___pbrMetallicRoughness_1; }
	inline void set_pbrMetallicRoughness_1(glTFPbrMetallicRoughness_t8D5AB37B5167EAB04B72D54E7BFE73CA384618EA * value)
	{
		___pbrMetallicRoughness_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pbrMetallicRoughness_1), (void*)value);
	}

	inline static int32_t get_offset_of_normalTexture_2() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___normalTexture_2)); }
	inline glTFMaterialNormalTextureInfo_t6D150E15CEA24273D7B0C80E66F9B22107970019 * get_normalTexture_2() const { return ___normalTexture_2; }
	inline glTFMaterialNormalTextureInfo_t6D150E15CEA24273D7B0C80E66F9B22107970019 ** get_address_of_normalTexture_2() { return &___normalTexture_2; }
	inline void set_normalTexture_2(glTFMaterialNormalTextureInfo_t6D150E15CEA24273D7B0C80E66F9B22107970019 * value)
	{
		___normalTexture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalTexture_2), (void*)value);
	}

	inline static int32_t get_offset_of_occlusionTexture_3() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___occlusionTexture_3)); }
	inline glTFMaterialOcclusionTextureInfo_t69A60A1A8A9C1781D618D4AFA7B2B5487E8B6B4D * get_occlusionTexture_3() const { return ___occlusionTexture_3; }
	inline glTFMaterialOcclusionTextureInfo_t69A60A1A8A9C1781D618D4AFA7B2B5487E8B6B4D ** get_address_of_occlusionTexture_3() { return &___occlusionTexture_3; }
	inline void set_occlusionTexture_3(glTFMaterialOcclusionTextureInfo_t69A60A1A8A9C1781D618D4AFA7B2B5487E8B6B4D * value)
	{
		___occlusionTexture_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___occlusionTexture_3), (void*)value);
	}

	inline static int32_t get_offset_of_emissiveTexture_4() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___emissiveTexture_4)); }
	inline glTFMaterialEmissiveTextureInfo_t2F9A3AFEB20366CBA1E180C0414525BA75B8681B * get_emissiveTexture_4() const { return ___emissiveTexture_4; }
	inline glTFMaterialEmissiveTextureInfo_t2F9A3AFEB20366CBA1E180C0414525BA75B8681B ** get_address_of_emissiveTexture_4() { return &___emissiveTexture_4; }
	inline void set_emissiveTexture_4(glTFMaterialEmissiveTextureInfo_t2F9A3AFEB20366CBA1E180C0414525BA75B8681B * value)
	{
		___emissiveTexture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emissiveTexture_4), (void*)value);
	}

	inline static int32_t get_offset_of_emissiveFactor_5() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___emissiveFactor_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_emissiveFactor_5() const { return ___emissiveFactor_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_emissiveFactor_5() { return &___emissiveFactor_5; }
	inline void set_emissiveFactor_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___emissiveFactor_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emissiveFactor_5), (void*)value);
	}

	inline static int32_t get_offset_of_alphaMode_6() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___alphaMode_6)); }
	inline String_t* get_alphaMode_6() const { return ___alphaMode_6; }
	inline String_t** get_address_of_alphaMode_6() { return &___alphaMode_6; }
	inline void set_alphaMode_6(String_t* value)
	{
		___alphaMode_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alphaMode_6), (void*)value);
	}

	inline static int32_t get_offset_of_alphaCutoff_7() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___alphaCutoff_7)); }
	inline float get_alphaCutoff_7() const { return ___alphaCutoff_7; }
	inline float* get_address_of_alphaCutoff_7() { return &___alphaCutoff_7; }
	inline void set_alphaCutoff_7(float value)
	{
		___alphaCutoff_7 = value;
	}

	inline static int32_t get_offset_of_doubleSided_8() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___doubleSided_8)); }
	inline bool get_doubleSided_8() const { return ___doubleSided_8; }
	inline bool* get_address_of_doubleSided_8() { return &___doubleSided_8; }
	inline void set_doubleSided_8(bool value)
	{
		___doubleSided_8 = value;
	}

	inline static int32_t get_offset_of_extensions_9() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___extensions_9)); }
	inline glTFMaterial_extensions_t7A04698CBFFFCEC246DD6B72454299069EBD22FF * get_extensions_9() const { return ___extensions_9; }
	inline glTFMaterial_extensions_t7A04698CBFFFCEC246DD6B72454299069EBD22FF ** get_address_of_extensions_9() { return &___extensions_9; }
	inline void set_extensions_9(glTFMaterial_extensions_t7A04698CBFFFCEC246DD6B72454299069EBD22FF * value)
	{
		___extensions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_9), (void*)value);
	}

	inline static int32_t get_offset_of_extras_10() { return static_cast<int32_t>(offsetof(glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC, ___extras_10)); }
	inline RuntimeObject * get_extras_10() const { return ___extras_10; }
	inline RuntimeObject ** get_address_of_extras_10() { return &___extras_10; }
	inline void set_extras_10(RuntimeObject * value)
	{
		___extras_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_10), (void*)value);
	}
};


// UniGLTF.glTFMesh
struct glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.String UniGLTF.glTFMesh::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<UniGLTF.glTFPrimitives> UniGLTF.glTFMesh::primitives
	List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB * ___primitives_1;
	// System.Single[] UniGLTF.glTFMesh::weights
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___weights_2;
	// System.Object UniGLTF.glTFMesh::extensions
	RuntimeObject * ___extensions_3;
	// System.Object UniGLTF.glTFMesh::extras
	RuntimeObject * ___extras_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_primitives_1() { return static_cast<int32_t>(offsetof(glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755, ___primitives_1)); }
	inline List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB * get_primitives_1() const { return ___primitives_1; }
	inline List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB ** get_address_of_primitives_1() { return &___primitives_1; }
	inline void set_primitives_1(List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB * value)
	{
		___primitives_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___primitives_1), (void*)value);
	}

	inline static int32_t get_offset_of_weights_2() { return static_cast<int32_t>(offsetof(glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755, ___weights_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_weights_2() const { return ___weights_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_weights_2() { return &___weights_2; }
	inline void set_weights_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___weights_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weights_2), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755, ___extensions_3)); }
	inline RuntimeObject * get_extensions_3() const { return ___extensions_3; }
	inline RuntimeObject ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(RuntimeObject * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_3), (void*)value);
	}

	inline static int32_t get_offset_of_extras_4() { return static_cast<int32_t>(offsetof(glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755, ___extras_4)); }
	inline RuntimeObject * get_extras_4() const { return ___extras_4; }
	inline RuntimeObject ** get_address_of_extras_4() { return &___extras_4; }
	inline void set_extras_4(RuntimeObject * value)
	{
		___extras_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_4), (void*)value);
	}
};


// UniGLTF.glTFNode
struct glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.String UniGLTF.glTFNode::name
	String_t* ___name_0;
	// System.Int32[] UniGLTF.glTFNode::children
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___children_1;
	// System.Single[] UniGLTF.glTFNode::matrix
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___matrix_2;
	// System.Single[] UniGLTF.glTFNode::translation
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___translation_3;
	// System.Single[] UniGLTF.glTFNode::rotation
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___rotation_4;
	// System.Single[] UniGLTF.glTFNode::scale
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___scale_5;
	// System.Int32 UniGLTF.glTFNode::mesh
	int32_t ___mesh_6;
	// System.Int32 UniGLTF.glTFNode::skin
	int32_t ___skin_7;
	// System.Single[] UniGLTF.glTFNode::weights
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___weights_8;
	// System.Int32 UniGLTF.glTFNode::camera
	int32_t ___camera_9;
	// UniGLTF.glTFNode_extensions UniGLTF.glTFNode::extensions
	glTFNode_extensions_tB678A11BB0F4B5CC110B19B53F17E8E91B3714A1 * ___extensions_10;
	// UniGLTF.glTFNode_extra UniGLTF.glTFNode::extras
	glTFNode_extra_t6BB9D43654E287FD54522BC0C85EF018F3C398B3 * ___extras_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_children_1() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___children_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_children_1() const { return ___children_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_children_1() { return &___children_1; }
	inline void set_children_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___children_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___children_1), (void*)value);
	}

	inline static int32_t get_offset_of_matrix_2() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___matrix_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_matrix_2() const { return ___matrix_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_matrix_2() { return &___matrix_2; }
	inline void set_matrix_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___matrix_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___matrix_2), (void*)value);
	}

	inline static int32_t get_offset_of_translation_3() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___translation_3)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_translation_3() const { return ___translation_3; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_translation_3() { return &___translation_3; }
	inline void set_translation_3(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___translation_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___translation_3), (void*)value);
	}

	inline static int32_t get_offset_of_rotation_4() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___rotation_4)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_rotation_4() const { return ___rotation_4; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_rotation_4() { return &___rotation_4; }
	inline void set_rotation_4(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___rotation_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotation_4), (void*)value);
	}

	inline static int32_t get_offset_of_scale_5() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___scale_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_scale_5() const { return ___scale_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_scale_5() { return &___scale_5; }
	inline void set_scale_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___scale_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scale_5), (void*)value);
	}

	inline static int32_t get_offset_of_mesh_6() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___mesh_6)); }
	inline int32_t get_mesh_6() const { return ___mesh_6; }
	inline int32_t* get_address_of_mesh_6() { return &___mesh_6; }
	inline void set_mesh_6(int32_t value)
	{
		___mesh_6 = value;
	}

	inline static int32_t get_offset_of_skin_7() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___skin_7)); }
	inline int32_t get_skin_7() const { return ___skin_7; }
	inline int32_t* get_address_of_skin_7() { return &___skin_7; }
	inline void set_skin_7(int32_t value)
	{
		___skin_7 = value;
	}

	inline static int32_t get_offset_of_weights_8() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___weights_8)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_weights_8() const { return ___weights_8; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_weights_8() { return &___weights_8; }
	inline void set_weights_8(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___weights_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weights_8), (void*)value);
	}

	inline static int32_t get_offset_of_camera_9() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___camera_9)); }
	inline int32_t get_camera_9() const { return ___camera_9; }
	inline int32_t* get_address_of_camera_9() { return &___camera_9; }
	inline void set_camera_9(int32_t value)
	{
		___camera_9 = value;
	}

	inline static int32_t get_offset_of_extensions_10() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___extensions_10)); }
	inline glTFNode_extensions_tB678A11BB0F4B5CC110B19B53F17E8E91B3714A1 * get_extensions_10() const { return ___extensions_10; }
	inline glTFNode_extensions_tB678A11BB0F4B5CC110B19B53F17E8E91B3714A1 ** get_address_of_extensions_10() { return &___extensions_10; }
	inline void set_extensions_10(glTFNode_extensions_tB678A11BB0F4B5CC110B19B53F17E8E91B3714A1 * value)
	{
		___extensions_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_10), (void*)value);
	}

	inline static int32_t get_offset_of_extras_11() { return static_cast<int32_t>(offsetof(glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480, ___extras_11)); }
	inline glTFNode_extra_t6BB9D43654E287FD54522BC0C85EF018F3C398B3 * get_extras_11() const { return ___extras_11; }
	inline glTFNode_extra_t6BB9D43654E287FD54522BC0C85EF018F3C398B3 ** get_address_of_extras_11() { return &___extras_11; }
	inline void set_extras_11(glTFNode_extra_t6BB9D43654E287FD54522BC0C85EF018F3C398B3 * value)
	{
		___extras_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_11), (void*)value);
	}
};


// UniGLTF.glTFPrimitives
struct glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFPrimitives::mode
	int32_t ___mode_0;
	// System.Int32 UniGLTF.glTFPrimitives::indices
	int32_t ___indices_1;
	// UniGLTF.glTFAttributes UniGLTF.glTFPrimitives::attributes
	glTFAttributes_tFA2177D23DBBDB60ECB8AF3D090825EA9779C9C9 * ___attributes_2;
	// System.Int32 UniGLTF.glTFPrimitives::material
	int32_t ___material_3;
	// System.Collections.Generic.List`1<UniGLTF.gltfMorphTarget> UniGLTF.glTFPrimitives::targets
	List_1_t9173FC3611CD124BBEF5351E7ECE265EE0563E32 * ___targets_4;
	// UniGLTF.glTFPrimitives_extras UniGLTF.glTFPrimitives::extras
	glTFPrimitives_extras_tB25723016CB0805B401E71CAC783352FE92CAEF4 * ___extras_5;
	// UniGLTF.glTFPrimitives_extensions UniGLTF.glTFPrimitives::extensions
	glTFPrimitives_extensions_t39BDF61B91F413D69BE59CA7131F06ED12324B56 * ___extensions_6;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_indices_1() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___indices_1)); }
	inline int32_t get_indices_1() const { return ___indices_1; }
	inline int32_t* get_address_of_indices_1() { return &___indices_1; }
	inline void set_indices_1(int32_t value)
	{
		___indices_1 = value;
	}

	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___attributes_2)); }
	inline glTFAttributes_tFA2177D23DBBDB60ECB8AF3D090825EA9779C9C9 * get_attributes_2() const { return ___attributes_2; }
	inline glTFAttributes_tFA2177D23DBBDB60ECB8AF3D090825EA9779C9C9 ** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(glTFAttributes_tFA2177D23DBBDB60ECB8AF3D090825EA9779C9C9 * value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributes_2), (void*)value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___material_3)); }
	inline int32_t get_material_3() const { return ___material_3; }
	inline int32_t* get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(int32_t value)
	{
		___material_3 = value;
	}

	inline static int32_t get_offset_of_targets_4() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___targets_4)); }
	inline List_1_t9173FC3611CD124BBEF5351E7ECE265EE0563E32 * get_targets_4() const { return ___targets_4; }
	inline List_1_t9173FC3611CD124BBEF5351E7ECE265EE0563E32 ** get_address_of_targets_4() { return &___targets_4; }
	inline void set_targets_4(List_1_t9173FC3611CD124BBEF5351E7ECE265EE0563E32 * value)
	{
		___targets_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targets_4), (void*)value);
	}

	inline static int32_t get_offset_of_extras_5() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___extras_5)); }
	inline glTFPrimitives_extras_tB25723016CB0805B401E71CAC783352FE92CAEF4 * get_extras_5() const { return ___extras_5; }
	inline glTFPrimitives_extras_tB25723016CB0805B401E71CAC783352FE92CAEF4 ** get_address_of_extras_5() { return &___extras_5; }
	inline void set_extras_5(glTFPrimitives_extras_tB25723016CB0805B401E71CAC783352FE92CAEF4 * value)
	{
		___extras_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_5), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_6() { return static_cast<int32_t>(offsetof(glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598, ___extensions_6)); }
	inline glTFPrimitives_extensions_t39BDF61B91F413D69BE59CA7131F06ED12324B56 * get_extensions_6() const { return ___extensions_6; }
	inline glTFPrimitives_extensions_t39BDF61B91F413D69BE59CA7131F06ED12324B56 ** get_address_of_extensions_6() { return &___extensions_6; }
	inline void set_extensions_6(glTFPrimitives_extensions_t39BDF61B91F413D69BE59CA7131F06ED12324B56 * value)
	{
		___extensions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_6), (void*)value);
	}
};


// UniGLTF.glTFTextureInfo
struct glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFTextureInfo::index
	int32_t ___index_0;
	// System.Int32 UniGLTF.glTFTextureInfo::texCoord
	int32_t ___texCoord_1;
	// System.Object UniGLTF.glTFTextureInfo::extensions
	RuntimeObject * ___extensions_2;
	// System.Object UniGLTF.glTFTextureInfo::extras
	RuntimeObject * ___extras_3;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_texCoord_1() { return static_cast<int32_t>(offsetof(glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F, ___texCoord_1)); }
	inline int32_t get_texCoord_1() const { return ___texCoord_1; }
	inline int32_t* get_address_of_texCoord_1() { return &___texCoord_1; }
	inline void set_texCoord_1(int32_t value)
	{
		___texCoord_1 = value;
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F, ___extensions_2)); }
	inline RuntimeObject * get_extensions_2() const { return ___extensions_2; }
	inline RuntimeObject ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(RuntimeObject * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_2), (void*)value);
	}

	inline static int32_t get_offset_of_extras_3() { return static_cast<int32_t>(offsetof(glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F, ___extras_3)); }
	inline RuntimeObject * get_extras_3() const { return ___extras_3; }
	inline RuntimeObject ** get_address_of_extras_3() { return &___extras_3; }
	inline void set_extras_3(RuntimeObject * value)
	{
		___extras_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_3), (void*)value);
	}
};


// UniGLTF.StaticMeshIntegrator/MeshWithMaterials
struct MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8 
{
public:
	// UnityEngine.Mesh UniGLTF.StaticMeshIntegrator/MeshWithMaterials::Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	// UnityEngine.Material[] UniGLTF.StaticMeshIntegrator/MeshWithMaterials::Materials
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___Materials_1;

public:
	inline static int32_t get_offset_of_Mesh_0() { return static_cast<int32_t>(offsetof(MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8, ___Mesh_0)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_Mesh_0() const { return ___Mesh_0; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_Mesh_0() { return &___Mesh_0; }
	inline void set_Mesh_0(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___Mesh_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Mesh_0), (void*)value);
	}

	inline static int32_t get_offset_of_Materials_1() { return static_cast<int32_t>(offsetof(MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8, ___Materials_1)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get_Materials_1() const { return ___Materials_1; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of_Materials_1() { return &___Materials_1; }
	inline void set_Materials_1(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		___Materials_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Materials_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.StaticMeshIntegrator/MeshWithMaterials
struct MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_pinvoke
{
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___Materials_1;
};
// Native definition for COM marshalling of UniGLTF.StaticMeshIntegrator/MeshWithMaterials
struct MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_com
{
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___Materials_1;
};

// UniGLTF.TextureIO/BytesWithMime
struct BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC 
{
public:
	// System.Byte[] UniGLTF.TextureIO/BytesWithMime::Bytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___Bytes_0;
	// System.String UniGLTF.TextureIO/BytesWithMime::Mime
	String_t* ___Mime_1;

public:
	inline static int32_t get_offset_of_Bytes_0() { return static_cast<int32_t>(offsetof(BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC, ___Bytes_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_Bytes_0() const { return ___Bytes_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_Bytes_0() { return &___Bytes_0; }
	inline void set_Bytes_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___Bytes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bytes_0), (void*)value);
	}

	inline static int32_t get_offset_of_Mime_1() { return static_cast<int32_t>(offsetof(BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC, ___Mime_1)); }
	inline String_t* get_Mime_1() const { return ___Mime_1; }
	inline String_t** get_address_of_Mime_1() { return &___Mime_1; }
	inline void set_Mime_1(String_t* value)
	{
		___Mime_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Mime_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.TextureIO/BytesWithMime
struct BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___Bytes_0;
	char* ___Mime_1;
};
// Native definition for COM marshalling of UniGLTF.TextureIO/BytesWithMime
struct BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___Bytes_0;
	Il2CppChar* ___Mime_1;
};

// UniGLTF.TextureItem/ColorSpaceScope
struct ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 
{
public:
	// System.Boolean UniGLTF.TextureItem/ColorSpaceScope::m_sRGBWrite
	bool ___m_sRGBWrite_0;

public:
	inline static int32_t get_offset_of_m_sRGBWrite_0() { return static_cast<int32_t>(offsetof(ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668, ___m_sRGBWrite_0)); }
	inline bool get_m_sRGBWrite_0() const { return ___m_sRGBWrite_0; }
	inline bool* get_address_of_m_sRGBWrite_0() { return &___m_sRGBWrite_0; }
	inline void set_m_sRGBWrite_0(bool value)
	{
		___m_sRGBWrite_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.TextureItem/ColorSpaceScope
struct ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_pinvoke
{
	int32_t ___m_sRGBWrite_0;
};
// Native definition for COM marshalling of UniGLTF.TextureItem/ColorSpaceScope
struct ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_com
{
	int32_t ___m_sRGBWrite_0;
};

// UniGLTF.gltfExporter/MeshWithRenderer
struct MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709 
{
public:
	// UnityEngine.Mesh UniGLTF.gltfExporter/MeshWithRenderer::Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	// UnityEngine.Renderer UniGLTF.gltfExporter/MeshWithRenderer::Rendererer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___Rendererer_1;

public:
	inline static int32_t get_offset_of_Mesh_0() { return static_cast<int32_t>(offsetof(MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709, ___Mesh_0)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_Mesh_0() const { return ___Mesh_0; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_Mesh_0() { return &___Mesh_0; }
	inline void set_Mesh_0(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___Mesh_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Mesh_0), (void*)value);
	}

	inline static int32_t get_offset_of_Rendererer_1() { return static_cast<int32_t>(offsetof(MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709, ___Rendererer_1)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get_Rendererer_1() const { return ___Rendererer_1; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of_Rendererer_1() { return &___Rendererer_1; }
	inline void set_Rendererer_1(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		___Rendererer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Rendererer_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.gltfExporter/MeshWithRenderer
struct MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_pinvoke
{
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___Rendererer_1;
};
// Native definition for COM marshalling of UniGLTF.gltfExporter/MeshWithRenderer
struct MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_com
{
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___Mesh_0;
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___Rendererer_1;
};

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0, ___dictionary_0)); }
	inline Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0, ___current_3)); }
	inline KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>
struct Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9, ___dictionary_0)); }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9, ___current_3)); }
	inline KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// DepthFirstScheduler.ChainStatus
struct ChainStatus_tBAB7DB6FCF51D049661BF0ECFA23AEB57C62F82E 
{
public:
	// System.Int32 DepthFirstScheduler.ChainStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ChainStatus_tBAB7DB6FCF51D049661BF0ECFA23AEB57C62F82E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.Zip.CompressionMethod
struct CompressionMethod_t7A5D7F3F701F1ADA10014B9CF0145C2425EC75A4 
{
public:
	// System.UInt16 UniGLTF.Zip.CompressionMethod::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMethod_t7A5D7F3F701F1ADA10014B9CF0145C2425EC75A4, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// DepthFirstScheduler.ExecutionStatus
struct ExecutionStatus_tF529CBE903D0D2FE91F8D1ECB999FDD935EE6E64 
{
public:
	// System.Int32 DepthFirstScheduler.ExecutionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExecutionStatus_tF529CBE903D0D2FE91F8D1ECB999FDD935EE6E64, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileAccess
struct FileAccess_t09E176678AB8520C44024354E0DB2F01D40A2F5B 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t09E176678AB8520C44024354E0DB2F01D40A2F5B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileMode
struct FileMode_t7AB84351F909CC2A0F99B798E50C6E8610994336 
{
public:
	// System.Int32 System.IO.FileMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileMode_t7AB84351F909CC2A0F99B798E50C6E8610994336, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.MeshTopology
struct MeshTopology_tF37D1A0C174D5906B715580E7318A21B4263C1A6 
{
public:
	// System.Int32 UnityEngine.MeshTopology::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MeshTopology_tF37D1A0C174D5906B715580E7318A21B4263C1A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UniGLTF.PosRot
struct PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4 
{
public:
	// UnityEngine.Vector3 UniGLTF.PosRot::Position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___Position_0;
	// UnityEngine.Quaternion UniGLTF.PosRot::Rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___Rotation_1;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4, ___Position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_Position_0() const { return ___Position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4, ___Rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___Rotation_1 = value;
	}
};


// UnityEngine.RenderTextureReadWrite
struct RenderTextureReadWrite_t4F64C0CC7097707282602ADD52760C1A86552580 
{
public:
	// System.Int32 UnityEngine.RenderTextureReadWrite::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureReadWrite_t4F64C0CC7097707282602ADD52760C1A86552580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.ShaderPropExporter.ShaderPropertyType
struct ShaderPropertyType_t13932E481029687997AB68D05F687C755050154A 
{
public:
	// System.Int32 UniGLTF.ShaderPropExporter.ShaderPropertyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShaderPropertyType_t13932E481029687997AB68D05F687C755050154A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureWrapMode
struct TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.UnityWebRequestTextureLoader
struct UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D UniGLTF.UnityWebRequestTextureLoader::<Texture>k__BackingField
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___U3CTextureU3Ek__BackingField_0;
	// System.Int32 UniGLTF.UnityWebRequestTextureLoader::m_textureIndex
	int32_t ___m_textureIndex_1;
	// UnityEngine.Networking.UnityWebRequest UniGLTF.UnityWebRequestTextureLoader::m_uwr
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___m_uwr_2;
	// System.ArraySegment`1<System.Byte> UniGLTF.UnityWebRequestTextureLoader::m_segments
	ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  ___m_segments_3;
	// System.String UniGLTF.UnityWebRequestTextureLoader::m_textureName
	String_t* ___m_textureName_4;

public:
	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347, ___U3CTextureU3Ek__BackingField_0)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_U3CTextureU3Ek__BackingField_0() const { return ___U3CTextureU3Ek__BackingField_0; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_U3CTextureU3Ek__BackingField_0() { return &___U3CTextureU3Ek__BackingField_0; }
	inline void set_U3CTextureU3Ek__BackingField_0(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___U3CTextureU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTextureU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_textureIndex_1() { return static_cast<int32_t>(offsetof(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347, ___m_textureIndex_1)); }
	inline int32_t get_m_textureIndex_1() const { return ___m_textureIndex_1; }
	inline int32_t* get_address_of_m_textureIndex_1() { return &___m_textureIndex_1; }
	inline void set_m_textureIndex_1(int32_t value)
	{
		___m_textureIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_uwr_2() { return static_cast<int32_t>(offsetof(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347, ___m_uwr_2)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_m_uwr_2() const { return ___m_uwr_2; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_m_uwr_2() { return &___m_uwr_2; }
	inline void set_m_uwr_2(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___m_uwr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_uwr_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_segments_3() { return static_cast<int32_t>(offsetof(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347, ___m_segments_3)); }
	inline ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  get_m_segments_3() const { return ___m_segments_3; }
	inline ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * get_address_of_m_segments_3() { return &___m_segments_3; }
	inline void set_m_segments_3(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  value)
	{
		___m_segments_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_segments_3))->____array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_textureName_4() { return static_cast<int32_t>(offsetof(UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347, ___m_textureName_4)); }
	inline String_t* get_m_textureName_4() const { return ___m_textureName_4; }
	inline String_t** get_address_of_m_textureName_4() { return &___m_textureName_4; }
	inline void set_m_textureName_4(String_t* value)
	{
		___m_textureName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textureName_4), (void*)value);
	}
};


// UniGLTF.glComponentType
struct glComponentType_tAAD2C29320D53B232DA1C6BA61D10A90816E3BE0 
{
public:
	// System.Int32 UniGLTF.glComponentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glComponentType_tAAD2C29320D53B232DA1C6BA61D10A90816E3BE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.glFilter
struct glFilter_t4248FC9EEC2B4ABC51699292F52A3312FF641A9B 
{
public:
	// System.Int32 UniGLTF.glFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glFilter_t4248FC9EEC2B4ABC51699292F52A3312FF641A9B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.glTFTextureTypes
struct glTFTextureTypes_tDBA4B00B1E7E60A27DA273A8814554C87462CD93 
{
public:
	// System.Int32 UniGLTF.glTFTextureTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glTFTextureTypes_tDBA4B00B1E7E60A27DA273A8814554C87462CD93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.glWrap
struct glWrap_tD71E83D6B504B7845C8ACFAEBE3D9C702EF4A6BE 
{
public:
	// System.Int32 UniGLTF.glWrap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glWrap_tD71E83D6B504B7845C8ACFAEBE3D9C702EF4A6BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.TextureSamplerUtil/TextureWrapType
struct TextureWrapType_t29F77B8F51FB0403C3A248133CB0F76B694934B1 
{
public:
	// System.Int32 UniGLTF.TextureSamplerUtil/TextureWrapType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapType_t29F77B8F51FB0403C3A248133CB0F76B694934B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.UnityPath/<TravserseDir>d__38
struct U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityPath/<TravserseDir>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>2__current
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<TravserseDir>d__38::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>3__<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E3__U3CU3E4__this_4;
	// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_5;
	// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E2__current_1)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E4__this_3)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E3__U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E3__U3CU3E4__this_4)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E3__U3CU3E4__this_4() const { return ___U3CU3E3__U3CU3E4__this_4; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E3__U3CU3E4__this_4() { return &___U3CU3E3__U3CU3E4__this_4; }
	inline void set_U3CU3E3__U3CU3E4__this_4(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E3__U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E7__wrap1_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1, ___U3CU3E7__wrap2_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap2_6), (void*)value);
	}
};


// UniGLTF.UnityPath/<get_ChildDirs>d__40
struct U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>2__current
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>3__<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E3__U3CU3E4__this_4;
	// System.String[] UniGLTF.UnityPath/<get_ChildDirs>d__40::<>7__wrap1
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CU3E7__wrap1_5;
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E2__current_1)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E4__this_3)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E3__U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E3__U3CU3E4__this_4)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E3__U3CU3E4__this_4() const { return ___U3CU3E3__U3CU3E4__this_4; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E3__U3CU3E4__this_4() { return &___U3CU3E3__U3CU3E4__this_4; }
	inline void set_U3CU3E3__U3CU3E4__this_4(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E3__U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E7__wrap1_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}
};


// UniGLTF.UnityPath/<get_ChildFiles>d__42
struct U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>2__current
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>3__<>4__this
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  ___U3CU3E3__U3CU3E4__this_4;
	// System.String[] UniGLTF.UnityPath/<get_ChildFiles>d__42::<>7__wrap1
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CU3E7__wrap1_5;
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E2__current_1)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E4__this_3)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E3__U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E3__U3CU3E4__this_4)); }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  get_U3CU3E3__U3CU3E4__this_4() const { return ___U3CU3E3__U3CU3E4__this_4; }
	inline UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * get_address_of_U3CU3E3__U3CU3E4__this_4() { return &___U3CU3E3__U3CU3E4__this_4; }
	inline void set_U3CU3E3__U3CU3E4__this_4(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  value)
	{
		___U3CU3E3__U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E7__wrap1_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}
};


// UniGLTF.glTFAnimationTarget/AnimationPropertys
struct AnimationPropertys_t0921B26CA88547EFE8959C7FE5204CE731B701A8 
{
public:
	// System.Int32 UniGLTF.glTFAnimationTarget/AnimationPropertys::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationPropertys_t0921B26CA88547EFE8959C7FE5204CE731B701A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.glTFAnimationTarget/Interpolations
struct Interpolations_t0AFD84669253486F88F20DF5C404110C6ABEB54D 
{
public:
	// System.Int32 UniGLTF.glTFAnimationTarget/Interpolations::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Interpolations_t0AFD84669253486F88F20DF5C404110C6ABEB54D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniJSON.JSONNode/Enumerator/Type
struct Type_t82E20427E3CFA40ED0C2C65B9831C750C47F916A 
{
public:
	// System.Int32 UniJSON.JSONNode/Enumerator/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t82E20427E3CFA40ED0C2C65B9831C750C47F916A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::localToRoot
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___localToRoot_0;
	// System.Int32 UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::offset
	int32_t ___offset_1;
	// System.Func`2<System.Int32,System.Int32> UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::<>9__2
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___U3CU3E9__2_2;

public:
	inline static int32_t get_offset_of_localToRoot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA, ___localToRoot_0)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_localToRoot_0() const { return ___localToRoot_0; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_localToRoot_0() { return &___localToRoot_0; }
	inline void set_localToRoot_0(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___localToRoot_0 = value;
	}

	inline static int32_t get_offset_of_offset_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA, ___offset_1)); }
	inline int32_t get_offset_1() const { return ___offset_1; }
	inline int32_t* get_address_of_offset_1() { return &___offset_1; }
	inline void set_offset_1(int32_t value)
	{
		___offset_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA, ___U3CU3E9__2_2)); }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * get_U3CU3E9__2_2() const { return ___U3CU3E9__2_2; }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA ** get_address_of_U3CU3E9__2_2() { return &___U3CU3E9__2_2; }
	inline void set_U3CU3E9__2_2(Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * value)
	{
		___U3CU3E9__2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_2), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode>
struct KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};


// UniGLTF.Zip.CommonHeader
struct CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED  : public RuntimeObject
{
public:
	// System.Text.Encoding UniGLTF.Zip.CommonHeader::Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___Encoding_0;
	// System.Byte[] UniGLTF.Zip.CommonHeader::Bytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___Bytes_1;
	// System.Int32 UniGLTF.Zip.CommonHeader::Offset
	int32_t ___Offset_2;
	// System.UInt16 UniGLTF.Zip.CommonHeader::VersionNeededToExtract
	uint16_t ___VersionNeededToExtract_3;
	// System.UInt16 UniGLTF.Zip.CommonHeader::GeneralPurposeBitFlag
	uint16_t ___GeneralPurposeBitFlag_4;
	// UniGLTF.Zip.CompressionMethod UniGLTF.Zip.CommonHeader::CompressionMethod
	uint16_t ___CompressionMethod_5;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileLastModificationTime
	uint16_t ___FileLastModificationTime_6;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileLastModificationDate
	uint16_t ___FileLastModificationDate_7;
	// System.Int32 UniGLTF.Zip.CommonHeader::CRC32
	int32_t ___CRC32_8;
	// System.Int32 UniGLTF.Zip.CommonHeader::CompressedSize
	int32_t ___CompressedSize_9;
	// System.Int32 UniGLTF.Zip.CommonHeader::UncompressedSize
	int32_t ___UncompressedSize_10;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileNameLength
	uint16_t ___FileNameLength_11;
	// System.UInt16 UniGLTF.Zip.CommonHeader::ExtraFieldLength
	uint16_t ___ExtraFieldLength_12;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___Encoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Encoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_Bytes_1() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___Bytes_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_Bytes_1() const { return ___Bytes_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_Bytes_1() { return &___Bytes_1; }
	inline void set_Bytes_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___Bytes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bytes_1), (void*)value);
	}

	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___Offset_2)); }
	inline int32_t get_Offset_2() const { return ___Offset_2; }
	inline int32_t* get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(int32_t value)
	{
		___Offset_2 = value;
	}

	inline static int32_t get_offset_of_VersionNeededToExtract_3() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___VersionNeededToExtract_3)); }
	inline uint16_t get_VersionNeededToExtract_3() const { return ___VersionNeededToExtract_3; }
	inline uint16_t* get_address_of_VersionNeededToExtract_3() { return &___VersionNeededToExtract_3; }
	inline void set_VersionNeededToExtract_3(uint16_t value)
	{
		___VersionNeededToExtract_3 = value;
	}

	inline static int32_t get_offset_of_GeneralPurposeBitFlag_4() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___GeneralPurposeBitFlag_4)); }
	inline uint16_t get_GeneralPurposeBitFlag_4() const { return ___GeneralPurposeBitFlag_4; }
	inline uint16_t* get_address_of_GeneralPurposeBitFlag_4() { return &___GeneralPurposeBitFlag_4; }
	inline void set_GeneralPurposeBitFlag_4(uint16_t value)
	{
		___GeneralPurposeBitFlag_4 = value;
	}

	inline static int32_t get_offset_of_CompressionMethod_5() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___CompressionMethod_5)); }
	inline uint16_t get_CompressionMethod_5() const { return ___CompressionMethod_5; }
	inline uint16_t* get_address_of_CompressionMethod_5() { return &___CompressionMethod_5; }
	inline void set_CompressionMethod_5(uint16_t value)
	{
		___CompressionMethod_5 = value;
	}

	inline static int32_t get_offset_of_FileLastModificationTime_6() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___FileLastModificationTime_6)); }
	inline uint16_t get_FileLastModificationTime_6() const { return ___FileLastModificationTime_6; }
	inline uint16_t* get_address_of_FileLastModificationTime_6() { return &___FileLastModificationTime_6; }
	inline void set_FileLastModificationTime_6(uint16_t value)
	{
		___FileLastModificationTime_6 = value;
	}

	inline static int32_t get_offset_of_FileLastModificationDate_7() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___FileLastModificationDate_7)); }
	inline uint16_t get_FileLastModificationDate_7() const { return ___FileLastModificationDate_7; }
	inline uint16_t* get_address_of_FileLastModificationDate_7() { return &___FileLastModificationDate_7; }
	inline void set_FileLastModificationDate_7(uint16_t value)
	{
		___FileLastModificationDate_7 = value;
	}

	inline static int32_t get_offset_of_CRC32_8() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___CRC32_8)); }
	inline int32_t get_CRC32_8() const { return ___CRC32_8; }
	inline int32_t* get_address_of_CRC32_8() { return &___CRC32_8; }
	inline void set_CRC32_8(int32_t value)
	{
		___CRC32_8 = value;
	}

	inline static int32_t get_offset_of_CompressedSize_9() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___CompressedSize_9)); }
	inline int32_t get_CompressedSize_9() const { return ___CompressedSize_9; }
	inline int32_t* get_address_of_CompressedSize_9() { return &___CompressedSize_9; }
	inline void set_CompressedSize_9(int32_t value)
	{
		___CompressedSize_9 = value;
	}

	inline static int32_t get_offset_of_UncompressedSize_10() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___UncompressedSize_10)); }
	inline int32_t get_UncompressedSize_10() const { return ___UncompressedSize_10; }
	inline int32_t* get_address_of_UncompressedSize_10() { return &___UncompressedSize_10; }
	inline void set_UncompressedSize_10(int32_t value)
	{
		___UncompressedSize_10 = value;
	}

	inline static int32_t get_offset_of_FileNameLength_11() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___FileNameLength_11)); }
	inline uint16_t get_FileNameLength_11() const { return ___FileNameLength_11; }
	inline uint16_t* get_address_of_FileNameLength_11() { return &___FileNameLength_11; }
	inline void set_FileNameLength_11(uint16_t value)
	{
		___FileNameLength_11 = value;
	}

	inline static int32_t get_offset_of_ExtraFieldLength_12() { return static_cast<int32_t>(offsetof(CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED, ___ExtraFieldLength_12)); }
	inline uint16_t get_ExtraFieldLength_12() const { return ___ExtraFieldLength_12; }
	inline uint16_t* get_address_of_ExtraFieldLength_12() { return &___ExtraFieldLength_12; }
	inline void set_ExtraFieldLength_12(uint16_t value)
	{
		___ExtraFieldLength_12 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.IO.FileStream
struct FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26  : public Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buf_6;
	// System.String System.IO.FileStream::name
	String_t* ___name_7;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_tC77A9860A03C31DC46AD2C08EC10EACDC3B7A662 * ___safeHandle_8;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_9;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_10;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_11;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_12;
	// System.Boolean System.IO.FileStream::async
	bool ___async_13;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_14;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_15;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_16;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_17;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_18;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_19;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_20;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_6)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_6), (void*)value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_7), (void*)value);
	}

	inline static int32_t get_offset_of_safeHandle_8() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___safeHandle_8)); }
	inline SafeFileHandle_tC77A9860A03C31DC46AD2C08EC10EACDC3B7A662 * get_safeHandle_8() const { return ___safeHandle_8; }
	inline SafeFileHandle_tC77A9860A03C31DC46AD2C08EC10EACDC3B7A662 ** get_address_of_safeHandle_8() { return &___safeHandle_8; }
	inline void set_safeHandle_8(SafeFileHandle_tC77A9860A03C31DC46AD2C08EC10EACDC3B7A662 * value)
	{
		___safeHandle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___safeHandle_8), (void*)value);
	}

	inline static int32_t get_offset_of_isExposed_9() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___isExposed_9)); }
	inline bool get_isExposed_9() const { return ___isExposed_9; }
	inline bool* get_address_of_isExposed_9() { return &___isExposed_9; }
	inline void set_isExposed_9(bool value)
	{
		___isExposed_9 = value;
	}

	inline static int32_t get_offset_of_append_startpos_10() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___append_startpos_10)); }
	inline int64_t get_append_startpos_10() const { return ___append_startpos_10; }
	inline int64_t* get_address_of_append_startpos_10() { return &___append_startpos_10; }
	inline void set_append_startpos_10(int64_t value)
	{
		___append_startpos_10 = value;
	}

	inline static int32_t get_offset_of_access_11() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___access_11)); }
	inline int32_t get_access_11() const { return ___access_11; }
	inline int32_t* get_address_of_access_11() { return &___access_11; }
	inline void set_access_11(int32_t value)
	{
		___access_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___owner_12)); }
	inline bool get_owner_12() const { return ___owner_12; }
	inline bool* get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(bool value)
	{
		___owner_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_canseek_14() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___canseek_14)); }
	inline bool get_canseek_14() const { return ___canseek_14; }
	inline bool* get_address_of_canseek_14() { return &___canseek_14; }
	inline void set_canseek_14(bool value)
	{
		___canseek_14 = value;
	}

	inline static int32_t get_offset_of_anonymous_15() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___anonymous_15)); }
	inline bool get_anonymous_15() const { return ___anonymous_15; }
	inline bool* get_address_of_anonymous_15() { return &___anonymous_15; }
	inline void set_anonymous_15(bool value)
	{
		___anonymous_15 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_16() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_dirty_16)); }
	inline bool get_buf_dirty_16() const { return ___buf_dirty_16; }
	inline bool* get_address_of_buf_dirty_16() { return &___buf_dirty_16; }
	inline void set_buf_dirty_16(bool value)
	{
		___buf_dirty_16 = value;
	}

	inline static int32_t get_offset_of_buf_size_17() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_size_17)); }
	inline int32_t get_buf_size_17() const { return ___buf_size_17; }
	inline int32_t* get_address_of_buf_size_17() { return &___buf_size_17; }
	inline void set_buf_size_17(int32_t value)
	{
		___buf_size_17 = value;
	}

	inline static int32_t get_offset_of_buf_length_18() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_length_18)); }
	inline int32_t get_buf_length_18() const { return ___buf_length_18; }
	inline int32_t* get_address_of_buf_length_18() { return &___buf_length_18; }
	inline void set_buf_length_18(int32_t value)
	{
		___buf_length_18 = value;
	}

	inline static int32_t get_offset_of_buf_offset_19() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_offset_19)); }
	inline int32_t get_buf_offset_19() const { return ___buf_offset_19; }
	inline int32_t* get_address_of_buf_offset_19() { return &___buf_offset_19; }
	inline void set_buf_offset_19(int32_t value)
	{
		___buf_offset_19 = value;
	}

	inline static int32_t get_offset_of_buf_start_20() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26, ___buf_start_20)); }
	inline int64_t get_buf_start_20() const { return ___buf_start_20; }
	inline int64_t* get_address_of_buf_start_20() { return &___buf_start_20; }
	inline void set_buf_start_20(int64_t value)
	{
		___buf_start_20 = value;
	}
};

struct FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buf_recycle_4;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_5;

public:
	inline static int32_t get_offset_of_buf_recycle_4() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_StaticFields, ___buf_recycle_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buf_recycle_4() const { return ___buf_recycle_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buf_recycle_4() { return &___buf_recycle_4; }
	inline void set_buf_recycle_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buf_recycle_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_recycle_4), (void*)value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_5() { return static_cast<int32_t>(offsetof(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_StaticFields, ___buf_recycle_lock_5)); }
	inline RuntimeObject * get_buf_recycle_lock_5() const { return ___buf_recycle_lock_5; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_5() { return &___buf_recycle_lock_5; }
	inline void set_buf_recycle_lock_5(RuntimeObject * value)
	{
		___buf_recycle_lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_recycle_lock_5), (void*)value);
	}
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30 
{
public:
	// System.String UniGLTF.ShaderPropExporter.ShaderProperty::Key
	String_t* ___Key_0;
	// UniGLTF.ShaderPropExporter.ShaderPropertyType UniGLTF.ShaderPropExporter.ShaderProperty::ShaderPropertyType
	int32_t ___ShaderPropertyType_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Key_0), (void*)value);
	}

	inline static int32_t get_offset_of_ShaderPropertyType_1() { return static_cast<int32_t>(offsetof(ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30, ___ShaderPropertyType_1)); }
	inline int32_t get_ShaderPropertyType_1() const { return ___ShaderPropertyType_1; }
	inline int32_t* get_address_of_ShaderPropertyType_1() { return &___ShaderPropertyType_1; }
	inline void set_ShaderPropertyType_1(int32_t value)
	{
		___ShaderPropertyType_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30_marshaled_pinvoke
{
	char* ___Key_0;
	int32_t ___ShaderPropertyType_1;
};
// Native definition for COM marshalling of UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30_marshaled_com
{
	Il2CppChar* ___Key_0;
	int32_t ___ShaderPropertyType_1;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// DepthFirstScheduler.TaskChain
struct TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<DepthFirstScheduler.ISchedulable> DepthFirstScheduler.TaskChain::Enumerator
	RuntimeObject* ___Enumerator_0;
	// System.Action`1<System.Exception> DepthFirstScheduler.TaskChain::OnError
	Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * ___OnError_1;
	// DepthFirstScheduler.ChainStatus DepthFirstScheduler.TaskChain::ChainStatus
	int32_t ___ChainStatus_2;

public:
	inline static int32_t get_offset_of_Enumerator_0() { return static_cast<int32_t>(offsetof(TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809, ___Enumerator_0)); }
	inline RuntimeObject* get_Enumerator_0() const { return ___Enumerator_0; }
	inline RuntimeObject** get_address_of_Enumerator_0() { return &___Enumerator_0; }
	inline void set_Enumerator_0(RuntimeObject* value)
	{
		___Enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Enumerator_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnError_1() { return static_cast<int32_t>(offsetof(TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809, ___OnError_1)); }
	inline Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * get_OnError_1() const { return ___OnError_1; }
	inline Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 ** get_address_of_OnError_1() { return &___OnError_1; }
	inline void set_OnError_1(Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * value)
	{
		___OnError_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnError_1), (void*)value);
	}

	inline static int32_t get_offset_of_ChainStatus_2() { return static_cast<int32_t>(offsetof(TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809, ___ChainStatus_2)); }
	inline int32_t get_ChainStatus_2() const { return ___ChainStatus_2; }
	inline int32_t* get_address_of_ChainStatus_2() { return &___ChainStatus_2; }
	inline void set_ChainStatus_2(int32_t value)
	{
		___ChainStatus_2 = value;
	}
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UniGLTF.glTFTextureSampler
struct glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// UniGLTF.glFilter UniGLTF.glTFTextureSampler::magFilter
	int32_t ___magFilter_0;
	// UniGLTF.glFilter UniGLTF.glTFTextureSampler::minFilter
	int32_t ___minFilter_1;
	// UniGLTF.glWrap UniGLTF.glTFTextureSampler::wrapS
	int32_t ___wrapS_2;
	// UniGLTF.glWrap UniGLTF.glTFTextureSampler::wrapT
	int32_t ___wrapT_3;
	// System.Object UniGLTF.glTFTextureSampler::extensions
	RuntimeObject * ___extensions_4;
	// System.Object UniGLTF.glTFTextureSampler::extras
	RuntimeObject * ___extras_5;
	// System.String UniGLTF.glTFTextureSampler::name
	String_t* ___name_6;

public:
	inline static int32_t get_offset_of_magFilter_0() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___magFilter_0)); }
	inline int32_t get_magFilter_0() const { return ___magFilter_0; }
	inline int32_t* get_address_of_magFilter_0() { return &___magFilter_0; }
	inline void set_magFilter_0(int32_t value)
	{
		___magFilter_0 = value;
	}

	inline static int32_t get_offset_of_minFilter_1() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___minFilter_1)); }
	inline int32_t get_minFilter_1() const { return ___minFilter_1; }
	inline int32_t* get_address_of_minFilter_1() { return &___minFilter_1; }
	inline void set_minFilter_1(int32_t value)
	{
		___minFilter_1 = value;
	}

	inline static int32_t get_offset_of_wrapS_2() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___wrapS_2)); }
	inline int32_t get_wrapS_2() const { return ___wrapS_2; }
	inline int32_t* get_address_of_wrapS_2() { return &___wrapS_2; }
	inline void set_wrapS_2(int32_t value)
	{
		___wrapS_2 = value;
	}

	inline static int32_t get_offset_of_wrapT_3() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___wrapT_3)); }
	inline int32_t get_wrapT_3() const { return ___wrapT_3; }
	inline int32_t* get_address_of_wrapT_3() { return &___wrapT_3; }
	inline void set_wrapT_3(int32_t value)
	{
		___wrapT_3 = value;
	}

	inline static int32_t get_offset_of_extensions_4() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___extensions_4)); }
	inline RuntimeObject * get_extensions_4() const { return ___extensions_4; }
	inline RuntimeObject ** get_address_of_extensions_4() { return &___extensions_4; }
	inline void set_extensions_4(RuntimeObject * value)
	{
		___extensions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_4), (void*)value);
	}

	inline static int32_t get_offset_of_extras_5() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___extras_5)); }
	inline RuntimeObject * get_extras_5() const { return ___extras_5; }
	inline RuntimeObject ** get_address_of_extras_5() { return &___extras_5; }
	inline void set_extras_5(RuntimeObject * value)
	{
		___extras_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_5), (void*)value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_6), (void*)value);
	}
};


// UniGLTF.TextureIO/TextureExportItem
struct TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 
{
public:
	// UnityEngine.Texture UniGLTF.TextureIO/TextureExportItem::Texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___Texture_0;
	// UniGLTF.glTFTextureTypes UniGLTF.TextureIO/TextureExportItem::TextureType
	int32_t ___TextureType_1;

public:
	inline static int32_t get_offset_of_Texture_0() { return static_cast<int32_t>(offsetof(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9, ___Texture_0)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_Texture_0() const { return ___Texture_0; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_Texture_0() { return &___Texture_0; }
	inline void set_Texture_0(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___Texture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Texture_0), (void*)value);
	}

	inline static int32_t get_offset_of_TextureType_1() { return static_cast<int32_t>(offsetof(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9, ___TextureType_1)); }
	inline int32_t get_TextureType_1() const { return ___TextureType_1; }
	inline int32_t* get_address_of_TextureType_1() { return &___TextureType_1; }
	inline void set_TextureType_1(int32_t value)
	{
		___TextureType_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UniGLTF.TextureIO/TextureExportItem
struct TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_pinvoke
{
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___Texture_0;
	int32_t ___TextureType_1;
};
// Native definition for COM marshalling of UniGLTF.TextureIO/TextureExportItem
struct TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_com
{
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___Texture_0;
	int32_t ___TextureType_1;
};

// UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11
struct U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Texture2D UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>2__current
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.TextureItem UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>4__this
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * ___U3CU3E4__this_3;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D> UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>7__wrap1
	Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594, ___U3CU3E2__current_1)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594, ___U3CU3E4__this_3)); }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594, ___U3CU3E7__wrap1_4)); }
	inline Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_4))->___dictionary_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3E7__wrap1_4))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3E7__wrap1_4))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// UniGLTF.glTFAnimation/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.glTFAnimation/<>c__DisplayClass6_0::nodeIndex
	int32_t ___nodeIndex_0;
	// UniGLTF.glTFAnimationTarget/AnimationPropertys UniGLTF.glTFAnimation/<>c__DisplayClass6_0::property
	int32_t ___property_1;

public:
	inline static int32_t get_offset_of_nodeIndex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9, ___nodeIndex_0)); }
	inline int32_t get_nodeIndex_0() const { return ___nodeIndex_0; }
	inline int32_t* get_address_of_nodeIndex_0() { return &___nodeIndex_0; }
	inline void set_nodeIndex_0(int32_t value)
	{
		___nodeIndex_0 = value;
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9, ___property_1)); }
	inline int32_t get_property_1() const { return ___property_1; }
	inline int32_t* get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(int32_t value)
	{
		___property_1 = value;
	}
};


// UniGLTF.glTFExtensions/ComponentVec
struct ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 
{
public:
	// UniGLTF.glComponentType UniGLTF.glTFExtensions/ComponentVec::ComponentType
	int32_t ___ComponentType_0;
	// System.Int32 UniGLTF.glTFExtensions/ComponentVec::ElementCount
	int32_t ___ElementCount_1;

public:
	inline static int32_t get_offset_of_ComponentType_0() { return static_cast<int32_t>(offsetof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33, ___ComponentType_0)); }
	inline int32_t get_ComponentType_0() const { return ___ComponentType_0; }
	inline int32_t* get_address_of_ComponentType_0() { return &___ComponentType_0; }
	inline void set_ComponentType_0(int32_t value)
	{
		___ComponentType_0 = value;
	}

	inline static int32_t get_offset_of_ElementCount_1() { return static_cast<int32_t>(offsetof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33, ___ElementCount_1)); }
	inline int32_t get_ElementCount_1() const { return ___ElementCount_1; }
	inline int32_t* get_address_of_ElementCount_1() { return &___ElementCount_1; }
	inline void set_ElementCount_1(int32_t value)
	{
		___ElementCount_1 = value;
	}
};


// System.Action`1<UnityEngine.GameObject>
struct Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh>
struct Func_2_t08105548739575F9FE3A646B6888D9042E732055  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UniGLTF.Zip.CentralDirectoryFileHeader
struct CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7  : public CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED
{
public:
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::VersionMadeBy
	uint16_t ___VersionMadeBy_13;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::FileCommentLength
	uint16_t ___FileCommentLength_14;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::DiskNumberWhereFileStarts
	uint16_t ___DiskNumberWhereFileStarts_15;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::InternalFileAttributes
	uint16_t ___InternalFileAttributes_16;
	// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::ExternalFileAttributes
	int32_t ___ExternalFileAttributes_17;
	// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::RelativeOffsetOfLocalFileHeader
	int32_t ___RelativeOffsetOfLocalFileHeader_18;

public:
	inline static int32_t get_offset_of_VersionMadeBy_13() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___VersionMadeBy_13)); }
	inline uint16_t get_VersionMadeBy_13() const { return ___VersionMadeBy_13; }
	inline uint16_t* get_address_of_VersionMadeBy_13() { return &___VersionMadeBy_13; }
	inline void set_VersionMadeBy_13(uint16_t value)
	{
		___VersionMadeBy_13 = value;
	}

	inline static int32_t get_offset_of_FileCommentLength_14() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___FileCommentLength_14)); }
	inline uint16_t get_FileCommentLength_14() const { return ___FileCommentLength_14; }
	inline uint16_t* get_address_of_FileCommentLength_14() { return &___FileCommentLength_14; }
	inline void set_FileCommentLength_14(uint16_t value)
	{
		___FileCommentLength_14 = value;
	}

	inline static int32_t get_offset_of_DiskNumberWhereFileStarts_15() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___DiskNumberWhereFileStarts_15)); }
	inline uint16_t get_DiskNumberWhereFileStarts_15() const { return ___DiskNumberWhereFileStarts_15; }
	inline uint16_t* get_address_of_DiskNumberWhereFileStarts_15() { return &___DiskNumberWhereFileStarts_15; }
	inline void set_DiskNumberWhereFileStarts_15(uint16_t value)
	{
		___DiskNumberWhereFileStarts_15 = value;
	}

	inline static int32_t get_offset_of_InternalFileAttributes_16() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___InternalFileAttributes_16)); }
	inline uint16_t get_InternalFileAttributes_16() const { return ___InternalFileAttributes_16; }
	inline uint16_t* get_address_of_InternalFileAttributes_16() { return &___InternalFileAttributes_16; }
	inline void set_InternalFileAttributes_16(uint16_t value)
	{
		___InternalFileAttributes_16 = value;
	}

	inline static int32_t get_offset_of_ExternalFileAttributes_17() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___ExternalFileAttributes_17)); }
	inline int32_t get_ExternalFileAttributes_17() const { return ___ExternalFileAttributes_17; }
	inline int32_t* get_address_of_ExternalFileAttributes_17() { return &___ExternalFileAttributes_17; }
	inline void set_ExternalFileAttributes_17(int32_t value)
	{
		___ExternalFileAttributes_17 = value;
	}

	inline static int32_t get_offset_of_RelativeOffsetOfLocalFileHeader_18() { return static_cast<int32_t>(offsetof(CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7, ___RelativeOffsetOfLocalFileHeader_18)); }
	inline int32_t get_RelativeOffsetOfLocalFileHeader_18() const { return ___RelativeOffsetOfLocalFileHeader_18; }
	inline int32_t* get_address_of_RelativeOffsetOfLocalFileHeader_18() { return &___RelativeOffsetOfLocalFileHeader_18; }
	inline void set_RelativeOffsetOfLocalFileHeader_18(int32_t value)
	{
		___RelativeOffsetOfLocalFileHeader_18 = value;
	}
};


// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UniGLTF.TextureConverter/ColorConversion
struct ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99  : public MulticastDelegate_t
{
public:

public:
};


// UniGLTF.TextureIO/<GetTextures>d__4
struct U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureIO/<GetTextures>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.TextureIO/TextureExportItem UniGLTF.TextureIO/<GetTextures>d__4::<>2__current
	TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.TextureIO/<GetTextures>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Material UniGLTF.TextureIO/<GetTextures>d__4::m
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_3;
	// UnityEngine.Material UniGLTF.TextureIO/<GetTextures>d__4::<>3__m
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___U3CU3E3__m_4;
	// UniGLTF.ShaderPropExporter.ShaderProps UniGLTF.TextureIO/<GetTextures>d__4::<props>5__2
	ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * ___U3CpropsU3E5__2_5;
	// UniGLTF.ShaderPropExporter.ShaderProperty[] UniGLTF.TextureIO/<GetTextures>d__4::<>7__wrap2
	ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* ___U3CU3E7__wrap2_6;
	// System.Int32 UniGLTF.TextureIO/<GetTextures>d__4::<>7__wrap3
	int32_t ___U3CU3E7__wrap3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3E2__current_1)); }
	inline TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E2__current_1))->___Texture_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_m_3() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___m_3)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_3() const { return ___m_3; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_3() { return &___m_3; }
	inline void set_m_3(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__m_4() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3E3__m_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_U3CU3E3__m_4() const { return ___U3CU3E3__m_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_U3CU3E3__m_4() { return &___U3CU3E3__m_4; }
	inline void set_U3CU3E3__m_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___U3CU3E3__m_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__m_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpropsU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CpropsU3E5__2_5)); }
	inline ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * get_U3CpropsU3E5__2_5() const { return ___U3CpropsU3E5__2_5; }
	inline ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 ** get_address_of_U3CpropsU3E5__2_5() { return &___U3CpropsU3E5__2_5; }
	inline void set_U3CpropsU3E5__2_5(ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * value)
	{
		___U3CpropsU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpropsU3E5__2_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3E7__wrap2_6)); }
	inline ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503** get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* value)
	{
		___U3CU3E7__wrap2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap2_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651, ___U3CU3E7__wrap3_7)); }
	inline int32_t get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline int32_t* get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(int32_t value)
	{
		___U3CU3E7__wrap3_7 = value;
	}
};


// UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2
struct U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339  : public RuntimeObject
{
public:
	// System.Int32 UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode> UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::<>2__current
	KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.glTFTextureSampler UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::sampler
	glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * ___sampler_3;
	// UniGLTF.glTFTextureSampler UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::<>3__sampler
	glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * ___U3CU3E3__sampler_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_sampler_3() { return static_cast<int32_t>(offsetof(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339, ___sampler_3)); }
	inline glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * get_sampler_3() const { return ___sampler_3; }
	inline glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 ** get_address_of_sampler_3() { return &___sampler_3; }
	inline void set_sampler_3(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * value)
	{
		___sampler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sampler_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E3__sampler_4() { return static_cast<int32_t>(offsetof(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339, ___U3CU3E3__sampler_4)); }
	inline glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * get_U3CU3E3__sampler_4() const { return ___U3CU3E3__sampler_4; }
	inline glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 ** get_address_of_U3CU3E3__sampler_4() { return &___U3CU3E3__sampler_4; }
	inline void set_U3CU3E3__sampler_4(glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * value)
	{
		___U3CU3E3__sampler_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E3__sampler_4), (void*)value);
	}
};


// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_t8927C00353A72755313F046D0CE85178AE8218EE * m_Items[1];

public:
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UniGLTF.ShaderPropExporter.ShaderProperty[]
struct ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  m_Items[1];

public:
	inline ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Key_0), (void*)NULL);
	}
	inline ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Key_0), (void*)NULL);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * m_Items[1];

public:
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_gshared (Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_gshared (RuntimeObject* ___source0, Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * ___selector1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1_gshared (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Int32,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6_gshared (RuntimeObject* ___source0, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m0D27CDCE1DE6E1E9BB00B10E6C47205041209B3F_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_gshared (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_mEFB776105F87A4EAB1CAC3F0C96C4D0B79F3F03D_gshared_inline (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * __this, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_Any_TisKeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625_m92149F166EBF244F84BB874A124D86B8115B3882_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0  Dictionary_2_GetEnumerator_mD80BA563C32BF7C1EE95C9FC1BE3B423716CCE68_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  Enumerator_get_Current_m39BB9CD07FEC0DBEDFE938630364A23C9A87FC3F_gshared_inline (Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8425596BB4249956819960EC76E618357F573E76_gshared_inline (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mCAD84084129516BD41DE5CC3E1FABA5A8DF836D0_gshared (Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m85CA135BAB22C9F0C87C84AB90FF6740D1859279_gshared (Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0 * __this, const RuntimeMethod* method);
// !0[] System.ArraySegment`1<System.Byte>::get_Array()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_IndexOf_mD3FF9D1890AD7EA2EA64E96EEFDF3035710B21AC_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m969D913F54135A789F9DD08639C3F42AA93ADD81_gshared (Func_2_tC9460EE39F336D0F5EF3B94D7189D15A4C827951 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UniGLTF.gltfExporter/MeshWithRenderer,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisRuntimeObject_m828ED764953A26E02F83A197A8930ECA62E6AAC8_gshared (RuntimeObject* ___source0, Func_2_tC9460EE39F336D0F5EF3B94D7189D15A4C827951 * ___selector1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * Enumerable_ToList_TisRuntimeObject_m3AB0AB30DAC385C2DF8A16D5CB8D3D41F62C751F_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Boolean UniGLTF.UnityExtensions::Has<System.Object>(UnityEngine.Transform,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityExtensions_Has_TisRuntimeObject_mCBBD06E70F74C699078DDEA6E8D87A1A42039B07_gshared (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, RuntimeObject * ___t1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mAAE01A16F138CEC8E1965D322EFB6A7045FE76F2_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);

// System.Void UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_m69978FB15E83BA5D9C45321DEBF82798C3B0A8CA (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
inline int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline)(__this, method);
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546 (Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631 (RuntimeObject* ___source0, Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 *, const RuntimeMethod*))Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_gshared)(___source0, ___selector1, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_gshared)(__this, ___collection0, method);
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::get_vertexCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_gshared)(__this, ___item0, method);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
inline void List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413_gshared)(__this, ___collection0, method);
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* Mesh_GetIndices_m8C8D25ABFA9D8A7AE23DAEB6FD7142E6BB46C49D (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, int32_t ___submesh0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1 (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Int32,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6 (RuntimeObject* ___source0, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, const RuntimeMethod*))Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6_gshared)(___source0, ___selector1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7_gshared)(___source0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32[]>::Add(!0)
inline void List_1_Add_m13C775A28676EED93E673F62FCA6AB4610B99257 (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 *, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mesh_get_subMeshCount_m60E2BCBFEEF21260C70D06EAEC3A2A51D80796FF (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m8FA5E9A771A18C54032AE5470895574151D55083 (List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m0D27CDCE1DE6E1E9BB00B10E6C47205041209B3F_gshared)(__this, ___collection0, method);
}
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_normals_m3D06E214B63B49788710672B71C99F2365A83130 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___value0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	return ((  Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32[]>::get_Count()
inline int32_t List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_inline (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Mesh::set_subMeshCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_subMeshCount_mF6F2199AE4FA096C1AE0CAD02E13B6FEA38C6283 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, int32_t ___value0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32[]>::get_Item(System.Int32)
inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* List_1_get_Item_m9E6B743F729F390A508DDCD477B81957DF20DB4F_inline (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* (*) (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 *, int32_t, const RuntimeMethod*))List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_SetIndices_mCD0377083E978A3FF806CFCCD28410C042A77ECD (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___indices0, int32_t ___topology1, int32_t ___submesh2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
inline void List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53 (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32[]>::.ctor()
inline void List_1__ctor_m0657384C4E785AFC6CCF0F8E525311EAC9E129A6 (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::.ctor()
inline void List_1__ctor_m497DA7006D664307F20A0546E5B717AA4B5E8A86 (List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Color32 UniGLTF.TextureConverter/ColorConversion::Invoke(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ColorConversion_Invoke_mE66566BF85440B31F587541C0571CDEE6B51413E (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color0, const RuntimeMethod* method);
// System.Int32 System.Environment::get_CurrentManagedThreadId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D (const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Material::get_shader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// UniGLTF.ShaderPropExporter.ShaderProps UniGLTF.ShaderPropExporter.PreShaderPropExporter::GetPropsForSupportedShader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * PreShaderPropExporter_GetPropsForSupportedShader_m8202FDE799152768A0FE7AFE9F710FA44FED0051 (String_t* ___shaderName0, const RuntimeMethod* method);
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, const RuntimeMethod* method);
// System.Void UniGLTF.TextureIO/TextureExportItem::.ctor(UnityEngine.Texture,UniGLTF.glTFTextureTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD (TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___texture0, int32_t ___textureType1, const RuntimeMethod* method);
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, const RuntimeMethod* method);
// UniGLTF.glTFTextureTypes UniGLTF.TextureIO::GetglTFTextureType(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextureIO_GetglTFTextureType_mB87796412802D917D5BCF531BB79A724F6713FB4 (String_t* ___shaderName0, String_t* ___propName1, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.TextureIO/<GetTextures>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesU3Ed__4__ctor_m7C53D342D4981F54636407A409EDC7CFBBDEAE9C (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UniGLTF.TextureIO/TextureExportItem> UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.Generic.IEnumerable<UniGLTF.TextureIO.TextureExportItem>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesU3Ed__4_System_Collections_Generic_IEnumerableU3CUniGLTF_TextureIO_TextureExportItemU3E_GetEnumerator_m0832FF1FBAEFBFA6C318A24A76678350890877D7 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>::get_Key()
inline String_t* KeyValuePair_2_get_Key_mEF264B1DD704042A07FA7767D09CD9B140608045_inline (KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 *, const RuntimeMethod*))KeyValuePair_2_get_Key_mEFB776105F87A4EAB1CAC3F0C96C4D0B79F3F03D_gshared_inline)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11_U3CU3Em__Finally1_m5CFC8C292059B7CD3ECE6042A50CC0BFDE149E5F (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method);
// System.Boolean UniGLTF.TextureItem::get_IsAsset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextureItem_get_IsAsset_mC52E33C68ECA2AC51D86432383C1CF1A5B64A995_inline (TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * __this, const RuntimeMethod* method);
// UnityEngine.Texture2D UniGLTF.TextureItem::get_Texture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * TextureItem_get_Texture_mF74878F920E1EDFB48735231F73E5AF3D4E742AF (TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * __this, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>>(System.Collections.Generic.IEnumerable`1<!!0>)
inline bool Enumerable_Any_TisKeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986_mE8485C294FD1E6A3BFF22E28FA961C3F91860100 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisKeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625_m92149F166EBF244F84BB874A124D86B8115B3882_gshared)(___source0, method);
}
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>::GetEnumerator()
inline Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  Dictionary_2_GetEnumerator_m1889B555EFCAE12D382994CEB941D4D74FAA2AAD (Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  (*) (Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C *, const RuntimeMethod*))Dictionary_2_GetEnumerator_mD80BA563C32BF7C1EE95C9FC1BE3B423716CCE68_gshared)(__this, method);
}
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::get_Current()
inline KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  Enumerator_get_Current_m04294D9E012BC7B9B153DC5438BAAA0F2C7BA0A2_inline (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  (*) (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *, const RuntimeMethod*))Enumerator_get_Current_m39BB9CD07FEC0DBEDFE938630364A23C9A87FC3F_gshared_inline)(__this, method);
}
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>::get_Value()
inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * KeyValuePair_2_get_Value_m453802FFD6BD090185B8F8E655E241D04E070D49_inline (KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 * __this, const RuntimeMethod* method)
{
	return ((  Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * (*) (KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m8425596BB4249956819960EC76E618357F573E76_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::MoveNext()
inline bool Enumerator_MoveNext_m14D37307E545E87355A7180B90F7EC67DEA34B16 (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *, const RuntimeMethod*))Enumerator_MoveNext_mCAD84084129516BD41DE5CC3E1FABA5A8DF836D0_gshared)(__this, method);
}
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11_System_IDisposable_Dispose_m5558065F4017EF9AEA6CB5B2D8E230A59DC9B6D7 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::Dispose()
inline void Enumerator_Dispose_m8464D1A0162AE786225345C59D904340E92B15A4 (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *, const RuntimeMethod*))Enumerator_Dispose_m85CA135BAB22C9F0C87C84AB90FF6740D1859279_gshared)(__this, method);
}
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11__ctor_mFE512298EE902D29A2D71175B94100FD2501E843 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Texture2D> UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.Generic.IEnumerable<UnityEngine.Texture2D>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_Texture2DU3E_GetEnumerator_m68F26E45B906DDE0C5997DB88729E75B577F5D35 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem::ProcessOnAnyThread(UniGLTF.glTF,UniGLTF.IStorage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureItem_ProcessOnAnyThread_mDF4F0FA7A98DCD6A3D9DDAC1C903BA6979F3B3FB (TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * __this, glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, RuntimeObject* ___storage1, const RuntimeMethod* method);
// System.Collections.IEnumerator UniGLTF.TextureItem::ProcessOnMainThreadCoroutine(UniGLTF.glTF)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TextureItem_ProcessOnMainThreadCoroutine_m8D62A9EF877DF6A709F29F3E06FA6BFAADE133C5 (TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * __this, glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17_U3CU3Em__Finally1_mD5FCAE594A726A3B440FE32B53282D40C0CAAF9E (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method);
// UniGLTF.glTFTextureTypes UniGLTF.TextureIO::GetglTFTextureType(UniGLTF.glTF,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextureIO_GetglTFTextureType_m62835FF9380D4C9AABB1C880187D64F625E1C052 (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___glTf0, int32_t ___textureIndex1, const RuntimeMethod* method);
// UnityEngine.RenderTextureReadWrite UniGLTF.TextureIO::GetColorSpace(UniGLTF.glTFTextureTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextureIO_GetColorSpace_m91BFB360A44169A562FCCB0A7DA236144207049E (int32_t ___textureType0, const RuntimeMethod* method);
// UniGLTF.glTFTextureSampler UniGLTF.glTF::GetSamplerFromTextureIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * glTF_GetSamplerFromTextureIndex_m86AA8B70E28301607E5EDE4F40902409BFA1FE7D (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * __this, int32_t ___textureIndex0, const RuntimeMethod* method);
// System.Void UniGLTF.TextureSamplerUtil::SetSampler(UnityEngine.Texture2D,UniGLTF.glTFTextureSampler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureSamplerUtil_SetSampler_mE631879D75B949273A8758A1F36C60458B29BD25 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * ___sampler1, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17_System_IDisposable_Dispose_m928FDBCB96655E9F1563E79EF775D68E75441529 (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GL::get_sRGBWrite()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GL_get_sRGBWrite_m34F6CD683D0C47990DB25093C4AE18CB7611BE86 (const RuntimeMethod* method);
// System.Void UnityEngine.GL::set_sRGBWrite(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GL_set_sRGBWrite_mA2EF1B4BE399C97E9BFB0FA582D09646F7D95629 (bool ___value0, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/ColorSpaceScope::.ctor(UnityEngine.RenderTextureReadWrite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope__ctor_m077D9D9306479598498C8550932C37B33D37670D (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, int32_t ___colorSpace0, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/ColorSpaceScope::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope__ctor_mFA842FB8D9838E2269AD999DD7BAD3C92A355D8A (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, bool ___sRGBWrite0, const RuntimeMethod* method);
// System.Void UniGLTF.TextureItem/ColorSpaceScope::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope_Dispose_m8F11E9022EAE905929A0C4AE6E00655136E72931 (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, bool ___linear4, const RuntimeMethod* method);
// System.Void UniGLTF.TextureLoader::set_Texture(UnityEngine.Texture2D)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TextureLoader_set_Texture_mD1454DFCCC19367D3F475C05E4EAE7145E111938_inline (TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___value0, const RuntimeMethod* method);
// UnityEngine.Texture2D UniGLTF.TextureLoader::get_Texture()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * TextureLoader_get_Texture_m20A25069C9C86C6ED9DD9E46EDDD15694AF8EBF8_inline (TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___tex0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode> UniGLTF.TextureSamplerUtil::TypeWithMode(UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD (int32_t ___type0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83 (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetUnityWrapModeU3Ed__2__ctor_m43955C6CA3EC7DAEB78C49355EA8B52988516C83 (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode>> UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<UniGLTF.TextureSamplerUtil.TextureWrapType,UnityEngine.TextureWrapMode>>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetUnityWrapModeU3Ed__2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CUniGLTF_TextureSamplerUtil_TextureWrapTypeU2CUnityEngine_TextureWrapModeU3EU3E_GetEnumerator_m3A1DBB56807857C178368865E2D0CCC86A86502B (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method);
// System.Void TriangleUtil/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6DFF3D22470F722AE56EE9EEF7F7C01C3C2AF83D (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * __this, const RuntimeMethod* method);
// System.Void TriangleUtil/<FlipTriangle>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipTriangleU3Ed__3__ctor_mA0A1E7FD9D802F18B71C5AF348E08BD918579F34 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Int32> TriangleUtil/<FlipTriangle>d__3::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CFlipTriangleU3Ed__3_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m1CE18EEBC190F67FE9C390B162831BFFFC07C18B (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m626309C50812FC25705BCB786A53C4C35026CA35 (U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * __this, const RuntimeMethod* method);
// UniGLTF.PosRot UniGLTF.PosRot::FromGlobalTransform(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4  PosRot_FromGlobalTransform_m78E34B88E24EFF2EC01A5B93C78CFD824FD30156 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t0, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18_U3CU3Em__Finally1_m0C4FA0C1CD5BD57AF660BC46A9E3CABE10238022 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform> UniGLTF.UnityExtensions::Ancestors(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityExtensions_Ancestors_m378E737E6047E14CB95F88AD5CC3B5CCD47CB80C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t0, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18_System_IDisposable_Dispose_m92FA03456F1597B41BB6872242E6C92673D6C135 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18__ctor_m025FE1617B5206D83BFDCA46C51FA31986A8017C (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CAncestorsU3Ed__18_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m3D43D4A8C5EE2D723C43A0E029FBE44F3853CE36 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15_U3CU3Em__Finally1_mD287C3FAD9B8FC7589C3879518B1931EC758AF27 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15_System_IDisposable_Dispose_m9179915A2581C26DCC68E28B33966E27EF495245 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15__ctor_m4D33DE3ED8C86FEFC24593760FF19CC933031F63 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__15_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mBC7B2CE354CE73EA3776D981E7521580F5AD5403 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_U3CU3Em__Finally2_mBE81E429EACDA457E04B1BA2AA26572D9D9458DE (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_U3CU3Em__Finally1_m3E4E7206D320303533A27F78181CF7EF2D403B86 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform> UniGLTF.UnityExtensions::Traverse(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityExtensions_Traverse_m8EE8F917FF6348D2FE8C29671A5DD40797CC9FC7 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t0, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_System_IDisposable_Dispose_m63AC1B9B2306930A993CDE46593963FABFC0C4D2 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16__ctor_mA430402B6A443F4987D0BD109D3857C86F165CE7 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__16_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mFE22F7E9DDF8BB14CF22F5922CFBE2047EB3C689 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1_U3CU3Em__Finally1_m101F8977A5C05E58E8BB61FCF22381162324D398 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1_System_IDisposable_Dispose_m19124E00BC17283674FBBB346E952D8FC0E72AF9 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1__ctor_m8C97A9196FC9472495BF20093F78AC045D966FA5 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__1_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m014CB9794B4D01FB5452561C6FE8FCC04E44A06A (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_U3CU3Em__Finally2_mEC2B9792DA117D865CE44A3545BE04E6F86BE07B (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_U3CU3Em__Finally1_m89BFF8CC58BB7E79B14B123CB8E598F30E0BC9D5 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform> UniHumanoid.UnityExtensions::Traverse(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityExtensions_Traverse_m47E2CE7236D50DBEDB2D8F2C183F141B0D1BECBD (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent0, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_System_IDisposable_Dispose_m0D56D8F1A83F3BB32FD3D4771D5BB1F8A9593F70 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method);
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2__ctor_mCCC31783688C9F2ADE0A999E8AC201054F2880BE (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__2_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mD771DF30A8558B87DAB20E2A5C7297A9DCA7C214 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m2AB2E5BE5DB44EE6BA61E91B7C4CA9BC09B69D89 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_mAA7C19E69E95CD26FF1FA925D21A0BBB78984177 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method);
// System.Boolean UniGLTF.UnityPath::get_IsDirectoryExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsDirectoryExists_mA8D3ADDD40998DC76BEB9BB25AA9DA09B023430F (UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::get_ChildDirs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_get_ChildDirs_mF40B2AA1B6D32227A6358AE26060BDE21B87DE66 (UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::TravserseDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_TravserseDir_m293B9AC260627603FCB50777CBD01224439FC242 (UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_m7E8148B3070FA5E174A5F6FB907DE23A0B0A79D4 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38__ctor_m0F8CB11CDEEB9A2122DB966A8494DF2DA361ACE8 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mB6850E495BD9BA243C03DB4080D674777FDD5385 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method);
// System.String UniGLTF.UnityPath::get_FullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_FullPath_m775FE6912534B097964B340558FB2598C3631D63 (UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * __this, const RuntimeMethod* method);
// System.String[] System.IO.Directory::GetDirectories(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Directory_GetDirectories_mACD36932126E03B5983AE0AE438B516B1CDDD7B7 (String_t* ___path0, const RuntimeMethod* method);
// UniGLTF.UnityPath UniGLTF.UnityPath::FromFullpath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  UnityPath_FromFullpath_mE2D127BEF63CCA22B3EE92DB7A6C5805BE897097 (String_t* ___fullPath0, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40__ctor_m00CEF083D5ECEE8135AB2F44FB34156AC402DC30 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m8BD83073BDFEC08A32FAE4234DD77EAF8E813274 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method);
// System.String[] System.IO.Directory::GetFiles(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Directory_GetFiles_m832F37C2ABC12EEAB3B1736C5196C50C0B65C1DC (String_t* ___path0, const RuntimeMethod* method);
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42__ctor_m09184C98C0D1E5C6B64B15A4BAC2392D388384B5 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mC9570B670C3C240E62A695D22695F3BF2F5707D6 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally2_mC47713BD6B4401CEF1B89EB169202144EECFA2D6 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally1_mB54C15DA0BAFC35C7C63051BB36E24D810F807A1 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method);
// System.String System.IO.Path::GetTempFileName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetTempFileName_mD4D7B9D2329A6D5FF86A05EFDD67E08E3150B34C (const RuntimeMethod* method);
// System.Void System.IO.FileStream::.ctor(System.String,System.IO.FileMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileStream__ctor_mDC286819520925AB5873921EFFEADA1EC1648CFB (FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * __this, String_t* ___path0, int32_t ___mode1, const RuntimeMethod* method);
// !0[] System.ArraySegment`1<System.Byte>::get_Array()
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_gshared_inline)(__this, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
inline int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline)(__this, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
inline int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline)(__this, method);
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/Deleter::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deleter__ctor_m4B8FC64020C404285F8D737E244827AA0F8753A3 (Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * __this, String_t* ___path0, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6 (String_t* ___format0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Void UnityEngine.WWW::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5 (WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Boolean UnityEngine.WWW::get_isDone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WWW_get_isDone_m916B54D53395990DB59C64413798FBCAFB08E0E3 (WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * __this, const RuntimeMethod* method);
// System.String UnityEngine.WWW::get_error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC (WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.WWW::get_textureNonReadable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * WWW_get_textureNonReadable_m5656C1450CDA1CF1A55B8BC1EBBE21CCCE9FDD0C (WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityWebRequestTextureLoader::set_Texture(UnityEngine.Texture2D)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityWebRequestTextureLoader_set_Texture_m38A430E5B42A5AFDD7B8DBDF5AA1329C6DD2A5A8_inline (UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___value0, const RuntimeMethod* method);
// UnityEngine.Texture2D UniGLTF.UnityWebRequestTextureLoader::get_Texture()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * UnityWebRequestTextureLoader_get_Texture_m7B2713B4793DD204D1BCFB369982E16E5D5ADBA3_inline (UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_System_IDisposable_Dispose_m6E8904532AB281C3748F2FCCC7D29CC0692ED043 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_mDAEBF2732BC830270FD98346F069B04E97BB1D5B (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.IO.File::Delete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Delete_m82FE53535A3911380F7E4C8AD44D77FAB330FD77 (String_t* ___path0, const RuntimeMethod* method);
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m52D6BE0B09F5A4E7221B0884B7B513A9FF893419 (U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * __this, const RuntimeMethod* method);
// System.String UniGLTF.Zip.CommonHeader::get_FileName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CommonHeader_get_FileName_m684CEF085CB5FB9CC9A298115463124235BD82F7 (CommonHeader_tD7BA39A00E2E8C54E825E326F56F5939C97AECED * __this, const RuntimeMethod* method);
// System.Void UniGLTF.glTF/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m07FEF95C42B2189719A66AE768A5842250AD94CF (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, const RuntimeMethod* method);
// System.Boolean UniGLTF.glTFPrimitives::get_HasVertexColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool glTFPrimitives_get_HasVertexColor_m9024FF91F10D80B1FC3F68CE03B0D96979BC0113 (glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598 * __this, const RuntimeMethod* method);
// System.String UniGLTF.glTFAnimationTarget::GetPathName(UniGLTF.glTFAnimationTarget/AnimationPropertys)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFAnimationTarget_GetPathName_mD58077774B6D2346105D93E85CEB7B324CE1A3FA (int32_t ___property0, const RuntimeMethod* method);
// System.Void UniGLTF.glTFExtensions/ComponentVec::.ctor(UniGLTF.glComponentType,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentVec__ctor_mC03E31F6488799B07DFCD52447129D22F9676BE9 (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 * __this, int32_t ___componentType0, int32_t ___elementCount1, const RuntimeMethod* method);
// System.Void UniGLTF.glbImporter/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m73886182753EE7DE64BFF4AC8AA3B34C904006FE (U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * __this, const RuntimeMethod* method);
// System.Void UniGLTF.gltfExporter/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4A7648BFBFC6D7025720713B0C288C64F96A7762 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UniGLTF.UnityExtensions::ReverseZ(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Single System.Math::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Math_Min_mED21323DC72FBF9A825FD4210D4B9D693CE87FCF (float ___val10, float ___val21, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Single System.Math::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Math_Max_mEB87839DA28310AE4CB81A94D551874CFC2B1247 (float ___val10, float ___val21, const RuntimeMethod* method);
// UnityEngine.Vector2 UniGLTF.UnityExtensions::ReverseUV(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UnityExtensions_ReverseUV_m6358683D7B052D74F85E04C7D43D3DC2F9614002 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Single UnityEngine.BoneWeight::get_weight0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BoneWeight_get_weight0_m8B5490EC53D29525AF4AE32AF645FD3499113972 (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.BoneWeight::get_weight1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BoneWeight_get_weight1_mE4925B02C2A8D914DD768318B737FD108966A2DB (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.BoneWeight::get_weight2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BoneWeight_get_weight2_m1C8AD1CE89B909139F39675B41D5DF6532802D1F (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.BoneWeight::get_weight3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BoneWeight_get_weight3_mAC48658CD89909EF148D310CB59CC14002E13C62 (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2 (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BoneWeight_get_boneIndex0_m69A411B913A65F83640791F464303F363ADDBC41 (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BoneWeight_get_boneIndex1_m6CEEBBAAB3A769562A7C5C503863C81E14ECE4B7 (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BoneWeight_get_boneIndex2_m139F5D3569F3DC84E52F39820442BEBDDD0666C6 (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BoneWeight_get_boneIndex3_mA5CC40F94B4AD7F1FD57193D490D1FBDFF74C44D (BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.UShort4::.ctor(System.UInt16,System.UInt16,System.UInt16,System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UShort4__ctor_m5609420491AA680B4D20BB9D8558E00A54E359AB (UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB * __this, uint16_t ____x0, uint16_t ____y1, uint16_t ____z2, uint16_t ____w3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// UnityEngine.Material[] UniGLTF.UnityExtensions::GetSharedMaterials(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* UnityExtensions_GetSharedMaterials_mE943685A31249DD83C3D80933C1DF33CF42B997C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UniGLTF.TextureIO/TextureExportItem> UniGLTF.TextureIO::GetTextures(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TextureIO_GetTextures_m93E8EFD894DB481AFCAB61DEFDDA42AAEDE3FA4F (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m0, const RuntimeMethod* method);
// UnityEngine.Mesh UniGLTF.UnityExtensions::GetSharedMesh(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * UnityExtensions_GetSharedMesh_m266F27F8EB902DFCACE09833821A70EE3BDB71BD (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___t0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mB89D75983F403B440947CE6FB264503618F5B951 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SkinnedMeshRenderer>()
inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m1E9C5D6F74CD0F5F21102B83360E41013884A501 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* SkinnedMeshRenderer_get_bones_mFC9D33DF2CE433D0FBE5645CEA514A0DF95A1C2C (SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UniGLTF.UnityExtensions::ReverseZ(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  UnityExtensions_ReverseZ_m4D51025BECE0DBCF0F70102036822356877537B1 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::IndexOf(!0)
inline int32_t List_1_IndexOf_m066AEE31C72DF470B7FD54374C7ADED0C43D199B (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_IndexOf_mD3FF9D1890AD7EA2EA64E96EEFDF3035710B21AC_gshared)(__this, ___item0, method);
}
// System.Boolean UniGLTF.gltfExporter::UseSparse(System.Boolean,UnityEngine.Vector3,System.Boolean,UnityEngine.Vector3,System.Boolean,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool gltfExporter_UseSparse_m62F7C07FF517AF7EA6D2B2E955FFF66C68BF3233 (bool ___usePosition0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, bool ___useNormal2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normal3, bool ___useTangent4, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___tangent5, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.Transform> UniGLTF.gltfExporter::get_Nodes()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * gltfExporter_get_Nodes_m610390ED9BBEA441A7AB4A713B6FFCABF9EC1876_inline (gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7126A380EC86400442C76F6E88B55A2ABEF146FB (Func_2_t08105548739575F9FE3A646B6888D9042E732055 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t08105548739575F9FE3A646B6888D9042E732055 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m969D913F54135A789F9DD08639C3F42AA93ADD81_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UniGLTF.gltfExporter/MeshWithRenderer,UnityEngine.Mesh>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_m49DF9F51864902059B935A4953F3F4FCF61AA3BE (RuntimeObject* ___source0, Func_2_t08105548739575F9FE3A646B6888D9042E732055 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t08105548739575F9FE3A646B6888D9042E732055 *, const RuntimeMethod*))Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisRuntimeObject_m828ED764953A26E02F83A197A8930ECA62E6AAC8_gshared)(___source0, ___selector1, method);
}
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Mesh>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * Enumerable_ToList_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_mA9B87D5951385CA98350A1CE27AEB321CEFF0989 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m3AB0AB30DAC385C2DF8A16D5CB8D3D41F62C751F_gshared)(___source0, method);
}
// UniGLTF.glTFNode UniGLTF.gltfExporter::ExportNode(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>,System.Collections.Generic.List`1<UnityEngine.Mesh>,System.Collections.Generic.List`1<UnityEngine.SkinnedMeshRenderer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480 * gltfExporter_ExportNode_mBAA26D3FC6D331977F4EA5EEBD1E6AE49F419AEA (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___nodes1, List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * ___meshes2, List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 * ___skins3, const RuntimeMethod* method);
// System.Boolean UniGLTF.UnityExtensions::Has<UnityEngine.SkinnedMeshRenderer>(UnityEngine.Transform,T)
inline bool UnityExtensions_Has_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_mEA1212D8AD20345E735606E08B43969F5D89B606 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___t1, const RuntimeMethod* method)
{
	return ((  bool (*) (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 *, const RuntimeMethod*))UnityExtensions_Has_TisRuntimeObject_mCBBD06E70F74C699078DDEA6E8D87A1A42039B07_gshared)(___transform0, ___t1, method);
}
// System.Void UniGLTF.ImporterContext::ShowMeshes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImporterContext_ShowMeshes_mC54AE98601938ABC36D61BD3AFDA213EE3C9D706 (ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.GameObject>::Invoke(!0)
inline void Action_1_Invoke_mD4DFE26268AFEE6CA1AEA70EE0FEB61E599DC3E7 (Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Action_1_Invoke_mAAE01A16F138CEC8E1965D322EFB6A7045FE76F2_gshared)(__this, ___obj0, method);
}
// DepthFirstScheduler.ExecutionStatus DepthFirstScheduler.TaskChain::Next()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TaskChain_Next_mC25E0CEED34D236CDD4BDDBD5E4CC4F44BFD9878 (TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Matrix4x4_MultiplyPoint_mE92BEE4DED3B602983C2BBE06C44AD29564EDA83 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<UnityEngine.Material> UniGLTF.StaticMeshIntegrator/Integrator::get_Materials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * Integrator_get_Materials_m6D2DD3F295A80CF03D61169048106DC2BE88D31E (Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_materials; }
		List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * L_0 = __this->get_m_materials_4();
		return L_0;
	}
}
// System.Void UniGLTF.StaticMeshIntegrator/Integrator::Push(UnityEngine.Matrix4x4,UnityEngine.Mesh,UnityEngine.Material[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Integrator_Push_mE4F4AB343DCF67243CD11720BC6AA1294EA33CD6 (Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79 * __this, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___localToRoot0, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh1, MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___materials2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m8FA5E9A771A18C54032AE5470895574151D55083_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m13C775A28676EED93E673F62FCA6AB4610B99257_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__0_m94EDED9E92F81857DF1B8E1559724DFDF3623C9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__1_m3ECB93AF7BD11AC361B644E8B6A18E119EEF6DF6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__2_m99207BBFCAF1F67ED747E7FE3C70957DBFC9B08F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * V_6 = NULL;
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * G_B17_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B17_1 = NULL;
	List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * G_B17_2 = NULL;
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * G_B16_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B16_1 = NULL;
	List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * G_B16_2 = NULL;
	{
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_0 = (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_0__ctor_m69978FB15E83BA5D9C45321DEBF82798C3B0A8CA(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_1 = V_0;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_2 = ___localToRoot0;
		NullCheck(L_1);
		L_1->set_localToRoot_0(L_2);
		// var offset = m_positions.Count;
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_3 = V_0;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_4 = __this->get_m_positions_0();
		NullCheck(L_4);
		int32_t L_5;
		L_5 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_4, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		NullCheck(L_3);
		L_3->set_offset_1(L_5);
		// var hasNormal = m_normals.Count == m_positions.Count;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_6 = __this->get_m_normals_1();
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_6, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_8 = __this->get_m_positions_0();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_8, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_1 = (bool)((((int32_t)L_7) == ((int32_t)L_9))? 1 : 0);
		// var hasUv = m_uv.Count == m_positions.Count;
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_10 = __this->get_m_uv_2();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_10, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_12 = __this->get_m_positions_0();
		NullCheck(L_12);
		int32_t L_13;
		L_13 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_12, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_2 = (bool)((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
		// m_positions.AddRange(mesh.vertices.Select(x => localToRoot.MultiplyPoint(x)));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_14 = __this->get_m_positions_0();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_15 = ___mesh1;
		NullCheck(L_15);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_16;
		L_16 = Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595(L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_17 = V_0;
		Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * L_18 = (Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 *)il2cpp_codegen_object_new(Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441_il2cpp_TypeInfo_var);
		Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546(L_18, L_17, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__0_m94EDED9E92F81857DF1B8E1559724DFDF3623C9E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631((RuntimeObject*)(RuntimeObject*)L_16, L_18, /*hidden argument*/Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_RuntimeMethod_var);
		NullCheck(L_14);
		List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D(L_14, L_19, /*hidden argument*/List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_RuntimeMethod_var);
		// if(mesh.normals!=null && mesh.normals.Length == mesh.vertexCount)
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_20 = ___mesh1;
		NullCheck(L_20);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_21;
		L_21 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00df;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_22 = ___mesh1;
		NullCheck(L_22);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_23;
		L_23 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_24 = ___mesh1;
		NullCheck(L_24);
		int32_t L_25;
		L_25 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length)))) == ((uint32_t)L_25))))
		{
			goto IL_00df;
		}
	}
	{
		// if (!hasNormal) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		bool L_26 = V_1;
		if (L_26)
		{
			goto IL_00bd;
		}
	}
	{
		// if (!hasNormal) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_27 = __this->get_m_normals_1();
		NullCheck(L_27);
		int32_t L_28;
		L_28 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_27, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_3 = L_28;
		goto IL_00af;
	}

IL_009b:
	{
		// if (!hasNormal) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_29 = __this->get_m_normals_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E(L_29, L_30, /*hidden argument*/List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_RuntimeMethod_var);
		// if (!hasNormal) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		int32_t L_31 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00af:
	{
		// if (!hasNormal) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		int32_t L_32 = V_3;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_33 = __this->get_m_positions_0();
		NullCheck(L_33);
		int32_t L_34;
		L_34 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_33, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_32) < ((int32_t)L_34)))
		{
			goto IL_009b;
		}
	}

IL_00bd:
	{
		// m_normals.AddRange(mesh.normals.Select(x => localToRoot.MultiplyVector(x)));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_35 = __this->get_m_normals_1();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_36 = ___mesh1;
		NullCheck(L_36);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_37;
		L_37 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_36, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_38 = V_0;
		Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 * L_39 = (Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441 *)il2cpp_codegen_object_new(Func_2_tB224684875B4AE61A1AA0AA7CF56FECBA7C43441_il2cpp_TypeInfo_var);
		Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546(L_39, L_38, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__1_m3ECB93AF7BD11AC361B644E8B6A18E119EEF6DF6_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mE14D281BF6C77C116939EB2882208B9A6A82A546_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631((RuntimeObject*)(RuntimeObject*)L_37, L_39, /*hidden argument*/Enumerable_Select_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m30FB8048FA814865E02A42C50BA15A20B5850631_RuntimeMethod_var);
		NullCheck(L_35);
		List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D(L_35, L_40, /*hidden argument*/List_1_AddRange_m420501B726B498F21E1ADD0B81CAFEBBAF00157D_RuntimeMethod_var);
	}

IL_00df:
	{
		// if (mesh.uv != null && mesh.uv.Length == mesh.vertexCount)
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_41 = ___mesh1;
		NullCheck(L_41);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_42;
		L_42 = Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_013f;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_43 = ___mesh1;
		NullCheck(L_43);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_44;
		L_44 = Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_45 = ___mesh1;
		NullCheck(L_45);
		int32_t L_46;
		L_46 = Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F(L_45, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_44)->max_length)))) == ((uint32_t)L_46))))
		{
			goto IL_013f;
		}
	}
	{
		// if (!hasUv) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		bool L_47 = V_2;
		if (L_47)
		{
			goto IL_012e;
		}
	}
	{
		// if (!hasUv) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_48 = __this->get_m_uv_2();
		NullCheck(L_48);
		int32_t L_49;
		L_49 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_48, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		V_4 = L_49;
		goto IL_011f;
	}

IL_0109:
	{
		// if (!hasUv) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_50 = __this->get_m_uv_2();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_51;
		L_51 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		NullCheck(L_50);
		List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249(L_50, L_51, /*hidden argument*/List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_RuntimeMethod_var);
		// if (!hasUv) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		int32_t L_52 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_011f:
	{
		// if (!hasUv) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		int32_t L_53 = V_4;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_54 = __this->get_m_positions_0();
		NullCheck(L_54);
		int32_t L_55;
		L_55 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_54, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_53) < ((int32_t)L_55)))
		{
			goto IL_0109;
		}
	}

IL_012e:
	{
		// m_uv.AddRange(mesh.uv);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_56 = __this->get_m_uv_2();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_57 = ___mesh1;
		NullCheck(L_57);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_58;
		L_58 = Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143(L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413(L_56, (RuntimeObject*)(RuntimeObject*)L_58, /*hidden argument*/List_1_AddRange_mC7F870803E46DB8945DFBA59D2CFFBA26E772413_RuntimeMethod_var);
	}

IL_013f:
	{
		// for (int i = 0; i < mesh.subMeshCount; ++i)
		V_5 = 0;
		goto IL_0188;
	}

IL_0144:
	{
		// m_subMeshes.Add(mesh.GetIndices(i).Select(x => offset + x).ToArray());
		List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * L_59 = __this->get_m_subMeshes_3();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_60 = ___mesh1;
		int32_t L_61 = V_5;
		NullCheck(L_60);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_62;
		L_62 = Mesh_GetIndices_m8C8D25ABFA9D8A7AE23DAEB6FD7142E6BB46C49D(L_60, L_61, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_63 = V_0;
		NullCheck(L_63);
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_64 = L_63->get_U3CU3E9__2_2();
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_65 = L_64;
		G_B16_0 = L_65;
		G_B16_1 = L_62;
		G_B16_2 = L_59;
		if (L_65)
		{
			G_B17_0 = L_65;
			G_B17_1 = L_62;
			G_B17_2 = L_59;
			goto IL_0173;
		}
	}
	{
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_66 = V_0;
		U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * L_67 = V_0;
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_68 = (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)il2cpp_codegen_object_new(Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA_il2cpp_TypeInfo_var);
		Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1(L_68, L_67, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__2_m99207BBFCAF1F67ED747E7FE3C70957DBFC9B08F_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m5B4919E5FB1301A44BB5C2821A69D3ABA4072CA1_RuntimeMethod_var);
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_69 = L_68;
		V_6 = L_69;
		NullCheck(L_66);
		L_66->set_U3CU3E9__2_2(L_69);
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_70 = V_6;
		G_B17_0 = L_70;
		G_B17_1 = G_B16_1;
		G_B17_2 = G_B16_2;
	}

IL_0173:
	{
		RuntimeObject* L_71;
		L_71 = Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6((RuntimeObject*)(RuntimeObject*)G_B17_1, G_B17_0, /*hidden argument*/Enumerable_Select_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m9E0E03EB8310914233FD0A8FFC54EE0647EEC2D6_RuntimeMethod_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_72;
		L_72 = Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7(L_71, /*hidden argument*/Enumerable_ToArray_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m91793C14F138ACD285FF862C99DDFAEC232781C7_RuntimeMethod_var);
		NullCheck(G_B17_2);
		List_1_Add_m13C775A28676EED93E673F62FCA6AB4610B99257(G_B17_2, L_72, /*hidden argument*/List_1_Add_m13C775A28676EED93E673F62FCA6AB4610B99257_RuntimeMethod_var);
		// for (int i = 0; i < mesh.subMeshCount; ++i)
		int32_t L_73 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_73, (int32_t)1));
	}

IL_0188:
	{
		// for (int i = 0; i < mesh.subMeshCount; ++i)
		int32_t L_74 = V_5;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_75 = ___mesh1;
		NullCheck(L_75);
		int32_t L_76;
		L_76 = Mesh_get_subMeshCount_m60E2BCBFEEF21260C70D06EAEC3A2A51D80796FF(L_75, /*hidden argument*/NULL);
		if ((((int32_t)L_74) < ((int32_t)L_76)))
		{
			goto IL_0144;
		}
	}
	{
		// m_materials.AddRange(materials);
		List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * L_77 = __this->get_m_materials_4();
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_78 = ___materials2;
		NullCheck(L_77);
		List_1_AddRange_m8FA5E9A771A18C54032AE5470895574151D55083(L_77, (RuntimeObject*)(RuntimeObject*)L_78, /*hidden argument*/List_1_AddRange_m8FA5E9A771A18C54032AE5470895574151D55083_RuntimeMethod_var);
		// }
		return;
	}
}
// UnityEngine.Mesh UniGLTF.StaticMeshIntegrator/Integrator::ToMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * Integrator_ToMesh_m370696DE412B5D543EF5A778146D100DBA116709 (Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m9E6B743F729F390A508DDCD477B81957DF20DB4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5D65FF0775E5025619AE4F4CCD3E1536D94EF55);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// var mesh = new Mesh();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)il2cpp_codegen_object_new(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// mesh.name = "integrated";
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = V_0;
		NullCheck(L_1);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_1, _stringLiteralE5D65FF0775E5025619AE4F4CCD3E1536D94EF55, /*hidden argument*/NULL);
		// mesh.vertices = m_positions.ToArray();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = V_0;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_3 = __this->get_m_positions_0();
		NullCheck(L_3);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_4;
		L_4 = List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445(L_3, /*hidden argument*/List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_RuntimeMethod_var);
		NullCheck(L_2);
		Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD(L_2, L_4, /*hidden argument*/NULL);
		// if (m_normals.Count > 0)
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_5 = __this->get_m_normals_1();
		NullCheck(L_5);
		int32_t L_6;
		L_6 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_5, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		// if (m_normals.Count < m_positions.Count) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_7 = __this->get_m_normals_1();
		NullCheck(L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_7, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_9 = __this->get_m_positions_0();
		NullCheck(L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_9, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_8) >= ((int32_t)L_10)))
		{
			goto IL_0078;
		}
	}
	{
		// if (m_normals.Count < m_positions.Count) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_11 = __this->get_m_normals_1();
		NullCheck(L_11);
		int32_t L_12;
		L_12 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_11, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_1 = L_12;
		goto IL_006a;
	}

IL_0056:
	{
		// if (m_normals.Count < m_positions.Count) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_13 = __this->get_m_normals_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E(L_13, L_14, /*hidden argument*/List_1_Add_m76C6963F23F90A4707FF8C87E3E60F6341845E1E_RuntimeMethod_var);
		// if (m_normals.Count < m_positions.Count) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_006a:
	{
		// if (m_normals.Count < m_positions.Count) for (int i = m_normals.Count; i < m_positions.Count; ++i) m_normals.Add(Vector3.zero);
		int32_t L_16 = V_1;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_17 = __this->get_m_positions_0();
		NullCheck(L_17);
		int32_t L_18;
		L_18 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_17, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0056;
		}
	}

IL_0078:
	{
		// mesh.normals = m_normals.ToArray();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_19 = V_0;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_20 = __this->get_m_normals_1();
		NullCheck(L_20);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_21;
		L_21 = List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445(L_20, /*hidden argument*/List_1_ToArray_m9ACE6E61D22E8ED1ED98F19017BBA54112AA5445_RuntimeMethod_var);
		NullCheck(L_19);
		Mesh_set_normals_m3D06E214B63B49788710672B71C99F2365A83130(L_19, L_21, /*hidden argument*/NULL);
	}

IL_0089:
	{
		// if (m_uv.Count > 0)
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_22 = __this->get_m_uv_2();
		NullCheck(L_22);
		int32_t L_23;
		L_23 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_22, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_00f0;
		}
	}
	{
		// if (m_uv.Count < m_positions.Count) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_24 = __this->get_m_uv_2();
		NullCheck(L_24);
		int32_t L_25;
		L_25 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_24, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_26 = __this->get_m_positions_0();
		NullCheck(L_26);
		int32_t L_27;
		L_27 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_26, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_25) >= ((int32_t)L_27)))
		{
			goto IL_00df;
		}
	}
	{
		// if (m_uv.Count < m_positions.Count) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_28 = __this->get_m_uv_2();
		NullCheck(L_28);
		int32_t L_29;
		L_29 = List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_inline(L_28, /*hidden argument*/List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_RuntimeMethod_var);
		V_2 = L_29;
		goto IL_00d1;
	}

IL_00bd:
	{
		// if (m_uv.Count < m_positions.Count) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_30 = __this->get_m_uv_2();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_31;
		L_31 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		NullCheck(L_30);
		List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249(L_30, L_31, /*hidden argument*/List_1_Add_m812D073096B787A464235E3D337FDC66DAD10249_RuntimeMethod_var);
		// if (m_uv.Count < m_positions.Count) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		int32_t L_32 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00d1:
	{
		// if (m_uv.Count < m_positions.Count) for (int i = m_uv.Count; i < m_positions.Count; ++i) m_uv.Add(Vector2.zero);
		int32_t L_33 = V_2;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_34 = __this->get_m_positions_0();
		NullCheck(L_34);
		int32_t L_35;
		L_35 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_34, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_00bd;
		}
	}

IL_00df:
	{
		// mesh.uv = m_uv.ToArray();
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_36 = V_0;
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_37 = __this->get_m_uv_2();
		NullCheck(L_37);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_38;
		L_38 = List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691(L_37, /*hidden argument*/List_1_ToArray_m0FC3EF0717D6F25002A2CC33B30776642E743691_RuntimeMethod_var);
		NullCheck(L_36);
		Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC(L_36, L_38, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		// mesh.subMeshCount = m_subMeshes.Count;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_39 = V_0;
		List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * L_40 = __this->get_m_subMeshes_3();
		NullCheck(L_40);
		int32_t L_41;
		L_41 = List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_inline(L_40, /*hidden argument*/List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_RuntimeMethod_var);
		NullCheck(L_39);
		Mesh_set_subMeshCount_mF6F2199AE4FA096C1AE0CAD02E13B6FEA38C6283(L_39, L_41, /*hidden argument*/NULL);
		// for(int i=0; i<m_subMeshes.Count; ++i)
		V_3 = 0;
		goto IL_011d;
	}

IL_0105:
	{
		// mesh.SetIndices(m_subMeshes[i], MeshTopology.Triangles, i);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_42 = V_0;
		List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * L_43 = __this->get_m_subMeshes_3();
		int32_t L_44 = V_3;
		NullCheck(L_43);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_45;
		L_45 = List_1_get_Item_m9E6B743F729F390A508DDCD477B81957DF20DB4F_inline(L_43, L_44, /*hidden argument*/List_1_get_Item_m9E6B743F729F390A508DDCD477B81957DF20DB4F_RuntimeMethod_var);
		int32_t L_46 = V_3;
		NullCheck(L_42);
		Mesh_SetIndices_mCD0377083E978A3FF806CFCCD28410C042A77ECD(L_42, L_45, 0, L_46, /*hidden argument*/NULL);
		// for(int i=0; i<m_subMeshes.Count; ++i)
		int32_t L_47 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_011d:
	{
		// for(int i=0; i<m_subMeshes.Count; ++i)
		int32_t L_48 = V_3;
		List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * L_49 = __this->get_m_subMeshes_3();
		NullCheck(L_49);
		int32_t L_50;
		L_50 = List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_inline(L_49, /*hidden argument*/List_1_get_Count_m487FEEECB40714D1C8EC50AC64E73CD0463795AB_RuntimeMethod_var);
		if ((((int32_t)L_48) < ((int32_t)L_50)))
		{
			goto IL_0105;
		}
	}
	{
		// return mesh;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_51 = V_0;
		return L_51;
	}
}
// System.Void UniGLTF.StaticMeshIntegrator/Integrator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Integrator__ctor_m827534D6A5F2F178AE6826E54256D86EECA5F444 (Integrator_tBC7602B754D6BBD36DADB3D7C05F29F29B75BC79 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m0657384C4E785AFC6CCF0F8E525311EAC9E129A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m497DA7006D664307F20A0546E5B717AA4B5E8A86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// List<Vector3> m_positions = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_0, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_m_positions_0(L_0);
		// List<Vector3> m_normals = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_1 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_1, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_m_normals_1(L_1);
		// List<Vector2> m_uv = new List<Vector2>();
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_2 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)il2cpp_codegen_object_new(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_il2cpp_TypeInfo_var);
		List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53(L_2, /*hidden argument*/List_1__ctor_m0ED1CF21DC6B8863BF9FB9E9CE08331E11258F53_RuntimeMethod_var);
		__this->set_m_uv_2(L_2);
		// List<int[]> m_subMeshes = new List<int[]>();
		List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 * L_3 = (List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98 *)il2cpp_codegen_object_new(List_1_t0D351E0A1BFCAF2113576F196766F05AB1BD1A98_il2cpp_TypeInfo_var);
		List_1__ctor_m0657384C4E785AFC6CCF0F8E525311EAC9E129A6(L_3, /*hidden argument*/List_1__ctor_m0657384C4E785AFC6CCF0F8E525311EAC9E129A6_RuntimeMethod_var);
		__this->set_m_subMeshes_3(L_3);
		// List<Material> m_materials = new List<Material>();
		List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 * L_4 = (List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4 *)il2cpp_codegen_object_new(List_1_t434825DCF3B4911FA61F9B2A235567430BDDD9F4_il2cpp_TypeInfo_var);
		List_1__ctor_m497DA7006D664307F20A0546E5B717AA4B5E8A86(L_4, /*hidden argument*/List_1__ctor_m497DA7006D664307F20A0546E5B717AA4B5E8A86_RuntimeMethod_var);
		__this->set_m_materials_4(L_4);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.StaticMeshIntegrator/MeshWithMaterials
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_pinvoke(const MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8& unmarshaled, MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_pinvoke& marshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithMaterials': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_pinvoke_back(const MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_pinvoke& marshaled, MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8& unmarshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithMaterials': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.StaticMeshIntegrator/MeshWithMaterials
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_pinvoke_cleanup(MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UniGLTF.StaticMeshIntegrator/MeshWithMaterials
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_com(const MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8& unmarshaled, MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_com& marshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithMaterials': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_com_back(const MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_com& marshaled, MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8& unmarshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithMaterials': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.StaticMeshIntegrator/MeshWithMaterials
IL2CPP_EXTERN_C void MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshal_com_cleanup(MeshWithMaterials_tB4E17944F9D1427AED995048C7F8CB0246735ED8_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureConverter/<>c__DisplayClass1_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0__ctor_mD9C9A692F3A35CB353943EC6A687F20D584F4A31 (U3CU3Ec__DisplayClass1_0_t9116E9331CE9FA0B41D192076C87ED0195257A1D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color32 UniGLTF.TextureConverter/<>c__DisplayClass1_0::<Convert>b__0(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  U3CU3Ec__DisplayClass1_0_U3CConvertU3Eb__0_m5B6FCCABBFCFC49F636B79FDD42373E20C700796 (U3CU3Ec__DisplayClass1_0_t9116E9331CE9FA0B41D192076C87ED0195257A1D * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___x0, const RuntimeMethod* method)
{
	{
		// copyTexture.SetPixels32(copyTexture.GetPixels32().Select(x => colorConversion(x)).ToArray());
		ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * L_0 = __this->get_colorConversion_0();
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_1 = ___x0;
		NullCheck(L_0);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_2;
		L_2 = ColorConversion_Invoke_mE66566BF85440B31F587541C0571CDEE6B51413E(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  DelegatePInvokeWrapper_ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color0, const RuntimeMethod* method)
{
	typedef Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  (DEFAULT_CALL *PInvokeFunc)(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  returnValue = il2cppPInvokeFunc(___color0);

	return returnValue;
}
// System.Void UniGLTF.TextureConverter/ColorConversion::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorConversion__ctor_m088927E0961BC8E41EE00B3DC73BDBDF056EBDF3 (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.Color32 UniGLTF.TextureConverter/ColorConversion::Invoke(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ColorConversion_Invoke_mE66566BF85440B31F587541C0571CDEE6B51413E (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color0, const RuntimeMethod* method)
{
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  result;
	memset((&result), 0, sizeof(result));
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!currentDelegate->get_method_is_virtual_10())
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  (*FunctionPointerType) (Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___color0, targetMethod);
			}
			else
			{
				// closed
				typedef Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  (*FunctionPointerType) (void*, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___color0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis == NULL)
			{
				typedef Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)((RuntimeObject*)(reinterpret_cast<RuntimeObject*>(&___color0) - 1), targetMethod);
			}
			else
			{
				typedef Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  (*FunctionPointerType) (void*, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___color0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult UniGLTF.TextureConverter/ColorConversion::BeginInvoke(UnityEngine.Color32,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ColorConversion_BeginInvoke_m258850E188281DCBF4A492BC84EFDB3662B7246D (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D_il2cpp_TypeInfo_var, &___color0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// UnityEngine.Color32 UniGLTF.TextureConverter/ColorConversion::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ColorConversion_EndInvoke_mD2A341A94EF6BD326818EE61A525D162ED721793 (ColorConversion_tBB2157DA50AB451F9F2D62B3AF807164D5CACE99 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D *)UnBox ((RuntimeObject*)__result);;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureIO/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m309565F0B92B42DC9DC3A604A50AF69491B72626 (U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.TextureIO/<>c__DisplayClass2_0::<GetglTFTextureType>b__0(UniGLTF.glTFTextureInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass2_0_U3CGetglTFTextureTypeU3Eb__0_m70C245270D25743525BADAFC3E31187DDAC6D54B (U3CU3Ec__DisplayClass2_0_t240C0E0C963A3B50FA8B4D2A57F0D2D71682E4C5 * __this, glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F * ___x0, const RuntimeMethod* method)
{
	{
		// var textureInfo = material.GetTextures().FirstOrDefault(x => (x!=null) && x.index == textureIndex);
		glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F * L_0 = ___x0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		glTFTextureInfo_t3CFF22E18451956123290F583B53838BAAC43B7F * L_1 = ___x0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_index_0();
		int32_t L_3 = __this->get_textureIndex_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}

IL_0012:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureIO/<GetTextures>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesU3Ed__4__ctor_m7C53D342D4981F54636407A409EDC7CFBBDEAE9C (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.TextureIO/<GetTextures>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesU3Ed__4_System_IDisposable_Dispose_m7EB851F99A1004B37CF1C0E8902F27C8CFC83EE1 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureIO/<GetTextures>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetTexturesU3Ed__4_MoveNext_mE0807585488E8BEA2FE1008DE8536EA5A68A1906 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreShaderPropExporter_t79A6BBD54954D59BB1305B9C955B2D873D13AFC1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0065;
			}
			case 2:
			{
				goto IL_00e0;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// var props = ShaderPropExporter.PreShaderPropExporter.GetPropsForSupportedShader(m.shader.name);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = __this->get_m_3();
		NullCheck(L_2);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_3;
		L_3 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4;
		L_4 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PreShaderPropExporter_t79A6BBD54954D59BB1305B9C955B2D873D13AFC1_il2cpp_TypeInfo_var);
		ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * L_5;
		L_5 = PreShaderPropExporter_GetPropsForSupportedShader_m8202FDE799152768A0FE7AFE9F710FA44FED0051(L_4, /*hidden argument*/NULL);
		__this->set_U3CpropsU3E5__2_5(L_5);
		// if (props == null)
		ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * L_6 = __this->get_U3CpropsU3E5__2_5();
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		// yield return new TextureExportItem(m.mainTexture, glTFTextureTypes.BaseColor);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = __this->get_m_3();
		NullCheck(L_7);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_8;
		L_8 = Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC(L_7, /*hidden argument*/NULL);
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_9;
		memset((&L_9), 0, sizeof(L_9));
		TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD((&L_9), L_8, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_9);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0065:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_006c:
	{
		// foreach (var prop in props.Properties)
		ShaderProps_tC8FC59D82AD107E39030B0A8E4F1F59314D5CAB4 * L_10 = __this->get_U3CpropsU3E5__2_5();
		NullCheck(L_10);
		ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* L_11 = L_10->get_Properties_0();
		__this->set_U3CU3E7__wrap2_6(L_11);
		__this->set_U3CU3E7__wrap3_7(0);
		goto IL_00f5;
	}

IL_0086:
	{
		// foreach (var prop in props.Properties)
		ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* L_12 = __this->get_U3CU3E7__wrap2_6();
		int32_t L_13 = __this->get_U3CU3E7__wrap3_7();
		NullCheck(L_12);
		int32_t L_14 = L_13;
		ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_1 = L_15;
		// if (prop.ShaderPropertyType == ShaderPropExporter.ShaderPropertyType.TexEnv)
		ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  L_16 = V_1;
		int32_t L_17 = L_16.get_ShaderPropertyType_1();
		if (L_17)
		{
			goto IL_00e7;
		}
	}
	{
		// yield return new TextureExportItem(m.GetTexture(prop.Key), GetglTFTextureType(m.shader.name, prop.Key));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = __this->get_m_3();
		ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  L_19 = V_1;
		String_t* L_20 = L_19.get_Key_0();
		NullCheck(L_18);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_21;
		L_21 = Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB(L_18, L_20, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_22 = __this->get_m_3();
		NullCheck(L_22);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_23;
		L_23 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24;
		L_24 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_23, /*hidden argument*/NULL);
		ShaderProperty_t06AD328ABCEE5AC093B4B7ACF9707093E4CD5E30  L_25 = V_1;
		String_t* L_26 = L_25.get_Key_0();
		int32_t L_27;
		L_27 = TextureIO_GetglTFTextureType_mB87796412802D917D5BCF531BB79A724F6713FB4(L_24, L_26, /*hidden argument*/NULL);
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_28;
		memset((&L_28), 0, sizeof(L_28));
		TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD((&L_28), L_21, L_27, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_28);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00e0:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00e7:
	{
		int32_t L_29 = __this->get_U3CU3E7__wrap3_7();
		__this->set_U3CU3E7__wrap3_7(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1)));
	}

IL_00f5:
	{
		// foreach (var prop in props.Properties)
		int32_t L_30 = __this->get_U3CU3E7__wrap3_7();
		ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503* L_31 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length))))))
		{
			goto IL_0086;
		}
	}
	{
		__this->set_U3CU3E7__wrap2_6((ShaderPropertyU5BU5D_t1C5123AFCF0749EE5B44941AD70A39BB550C6503*)NULL);
		// }
		return (bool)0;
	}
}
// UniGLTF.TextureIO/TextureExportItem UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.Generic.IEnumerator<UniGLTF.TextureIO.TextureExportItem>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  U3CGetTexturesU3Ed__4_System_Collections_Generic_IEnumeratorU3CUniGLTF_TextureIO_TextureExportItemU3E_get_Current_m2C4BCFBC0A4634A0A26E6AB51AD7C78066A272FA (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	{
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesU3Ed__4_System_Collections_IEnumerator_Reset_m0BD99F097ACD0B9701333239E43EC436CE93BBF1 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetTexturesU3Ed__4_System_Collections_IEnumerator_Reset_m0BD99F097ACD0B9701333239E43EC436CE93BBF1_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetTexturesU3Ed__4_System_Collections_IEnumerator_get_Current_m4148E031733111F37029B662DF32DD6606948AD9 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_0 = __this->get_U3CU3E2__current_1();
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_1 = L_0;
		RuntimeObject * L_2 = Box(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.TextureIO/TextureExportItem> UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.Generic.IEnumerable<UniGLTF.TextureIO.TextureExportItem>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesU3Ed__4_System_Collections_Generic_IEnumerableU3CUniGLTF_TextureIO_TextureExportItemU3E_GetEnumerator_m0832FF1FBAEFBFA6C318A24A76678350890877D7 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * L_3 = (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 *)il2cpp_codegen_object_new(U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651_il2cpp_TypeInfo_var);
		U3CGetTexturesU3Ed__4__ctor_m7C53D342D4981F54636407A409EDC7CFBBDEAE9C(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * L_4 = V_0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = __this->get_U3CU3E3__m_4();
		NullCheck(L_4);
		L_4->set_m_3(L_5);
		U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.TextureIO/<GetTextures>d__4::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesU3Ed__4_System_Collections_IEnumerable_GetEnumerator_m0E516944566FF644CA0F3B012057CDA376D7C8B3 (U3CGetTexturesU3Ed__4_tFC5E61B84033DCB7117D973B53014233582F1651 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CGetTexturesU3Ed__4_System_Collections_Generic_IEnumerableU3CUniGLTF_TextureIO_TextureExportItemU3E_GetEnumerator_m0832FF1FBAEFBFA6C318A24A76678350890877D7(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.TextureIO/BytesWithMime
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_pinvoke(const BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC& unmarshaled, BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_pinvoke& marshaled)
{
	marshaled.___Bytes_0 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_Bytes_0());
	marshaled.___Mime_1 = il2cpp_codegen_marshal_string(unmarshaled.get_Mime_1());
}
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_pinvoke_back(const BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_pinvoke& marshaled, BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Bytes_0((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, marshaled.___Bytes_0));
	unmarshaled.set_Mime_1(il2cpp_codegen_marshal_string_result(marshaled.___Mime_1));
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureIO/BytesWithMime
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_pinvoke_cleanup(BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___Bytes_0);
	marshaled.___Bytes_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Mime_1);
	marshaled.___Mime_1 = NULL;
}
// Conversion methods for marshalling of: UniGLTF.TextureIO/BytesWithMime
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_com(const BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC& unmarshaled, BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_com& marshaled)
{
	marshaled.___Bytes_0 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_Bytes_0());
	marshaled.___Mime_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Mime_1());
}
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_com_back(const BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_com& marshaled, BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Bytes_0((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, marshaled.___Bytes_0));
	unmarshaled.set_Mime_1(il2cpp_codegen_marshal_bstring_result(marshaled.___Mime_1));
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureIO/BytesWithMime
IL2CPP_EXTERN_C void BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshal_com_cleanup(BytesWithMime_tE59C1AD56518C7A6F5FDC2B491CE6F7610A598FC_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___Bytes_0);
	marshaled.___Bytes_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___Mime_1);
	marshaled.___Mime_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.TextureIO/TextureExportItem
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_pinvoke(const TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9& unmarshaled, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_pinvoke& marshaled)
{
	Exception_t* ___Texture_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Texture' of type 'TextureExportItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Texture_0Exception, NULL);
}
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_pinvoke_back(const TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_pinvoke& marshaled, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9& unmarshaled)
{
	Exception_t* ___Texture_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Texture' of type 'TextureExportItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Texture_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureIO/TextureExportItem
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_pinvoke_cleanup(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UniGLTF.TextureIO/TextureExportItem
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_com(const TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9& unmarshaled, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_com& marshaled)
{
	Exception_t* ___Texture_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Texture' of type 'TextureExportItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Texture_0Exception, NULL);
}
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_com_back(const TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_com& marshaled, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9& unmarshaled)
{
	Exception_t* ___Texture_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Texture' of type 'TextureExportItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Texture_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureIO/TextureExportItem
IL2CPP_EXTERN_C void TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshal_com_cleanup(TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9_marshaled_com& marshaled)
{
}
// System.Void UniGLTF.TextureIO/TextureExportItem::.ctor(UnityEngine.Texture,UniGLTF.glTFTextureTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD (TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___texture0, int32_t ___textureType1, const RuntimeMethod* method)
{
	{
		// Texture = texture;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = ___texture0;
		__this->set_Texture_0(L_0);
		// TextureType = textureType;
		int32_t L_1 = ___textureType1;
		__this->set_TextureType_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD_AdjustorThunk (RuntimeObject * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___texture0, int32_t ___textureType1, const RuntimeMethod* method)
{
	TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9 *>(__this + _offset);
	TextureExportItem__ctor_m5EDFCE28F2B16B9808346580BB140BFEEFF977CD(_thisAdjusted, ___texture0, ___textureType1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureItem/<>c__DisplayClass6_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass6_0__ctor_m56FBAADE6D0201D8733BDC5370609E6D532D197F (U3CU3Ec__DisplayClass6_0_tDF0015826BF1D71934A4988CBA6A8B3956DF6C79 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.TextureItem/<>c__DisplayClass6_0::<ConvertTexture>b__0(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Texture2D>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass6_0_U3CConvertTextureU3Eb__0_m53CF4C05BA0ADEE7F7B1330FCD5636129619E893 (U3CU3Ec__DisplayClass6_0_tDF0015826BF1D71934A4988CBA6A8B3956DF6C79 * __this, KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Key_mEF264B1DD704042A07FA7767D09CD9B140608045_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var convertedTexture = Converts.FirstOrDefault(x => x.Key == prop);
		String_t* L_0;
		L_0 = KeyValuePair_2_get_Key_mEF264B1DD704042A07FA7767D09CD9B140608045_inline((KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 *)(&___x0), /*hidden argument*/KeyValuePair_2_get_Key_mEF264B1DD704042A07FA7767D09CD9B140608045_RuntimeMethod_var);
		String_t* L_1 = __this->get_prop_0();
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11__ctor_mFE512298EE902D29A2D71175B94100FD2501E843 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11_System_IDisposable_Dispose_m5558065F4017EF9AEA6CB5B2D8E230A59DC9B6D7 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{// begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{// begin finally (depth: 1)
		U3CGetTexturesForSaveAssetsU3Ed__11_U3CU3Em__Finally1_m5CFC8C292059B7CD3ECE6042A50CC0BFDE149E5F(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetTexturesForSaveAssetsU3Ed__11_MoveNext_m6F25E2307AB83DA066397F7663E087AC73481AE6 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_GetEnumerator_m1889B555EFCAE12D382994CEB941D4D74FAA2AAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Any_TisKeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986_mE8485C294FD1E6A3BFF22E28FA961C3F91860100_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m14D37307E545E87355A7180B90F7EC67DEA34B16_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m04294D9E012BC7B9B153DC5438BAAA0F2C7BA0A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Value_m453802FFD6BD090185B8F8E655E241D04E070D49_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * V_2 = NULL;
	KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_1 = __this->get_U3CU3E4__this_3();
			V_2 = L_1;
			int32_t L_2 = V_1;
			switch (L_2)
			{
				case 0:
				{
					goto IL_0027;
				}
				case 1:
				{
					goto IL_0050;
				}
				case 2:
				{
					goto IL_00a3;
				}
			}
		}

IL_0020:
		{
			V_0 = (bool)0;
			goto IL_00d5;
		}

IL_0027:
		{
			__this->set_U3CU3E1__state_0((-1));
			// if (!IsAsset)
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_3 = V_2;
			NullCheck(L_3);
			bool L_4;
			L_4 = TextureItem_get_IsAsset_mC52E33C68ECA2AC51D86432383C1CF1A5B64A995_inline(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0057;
			}
		}

IL_0036:
		{
			// yield return Texture;
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_5 = V_2;
			NullCheck(L_5);
			Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_6;
			L_6 = TextureItem_get_Texture_mF74878F920E1EDFB48735231F73E5AF3D4E742AF(L_5, /*hidden argument*/NULL);
			__this->set_U3CU3E2__current_1(L_6);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00d5;
		}

IL_0050:
		{
			__this->set_U3CU3E1__state_0((-1));
		}

IL_0057:
		{
			// if (m_converts.Any())
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_7 = V_2;
			NullCheck(L_7);
			Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * L_8 = L_7->get_m_converts_1();
			bool L_9;
			L_9 = Enumerable_Any_TisKeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986_mE8485C294FD1E6A3BFF22E28FA961C3F91860100(L_8, /*hidden argument*/Enumerable_Any_TisKeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986_mE8485C294FD1E6A3BFF22E28FA961C3F91860100_RuntimeMethod_var);
			if (!L_9)
			{
				goto IL_00ca;
			}
		}

IL_0064:
		{
			// foreach (var texture in m_converts)
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_10 = V_2;
			NullCheck(L_10);
			Dictionary_2_tEB18A32D0B180DF252D5178E45E11813020BE41C * L_11 = L_10->get_m_converts_1();
			NullCheck(L_11);
			Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9  L_12;
			L_12 = Dictionary_2_GetEnumerator_m1889B555EFCAE12D382994CEB941D4D74FAA2AAD(L_11, /*hidden argument*/Dictionary_2_GetEnumerator_m1889B555EFCAE12D382994CEB941D4D74FAA2AAD_RuntimeMethod_var);
			__this->set_U3CU3E7__wrap1_4(L_12);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_00ab;
		}

IL_007f:
		{
			// foreach (var texture in m_converts)
			Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * L_13 = __this->get_address_of_U3CU3E7__wrap1_4();
			KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986  L_14;
			L_14 = Enumerator_get_Current_m04294D9E012BC7B9B153DC5438BAAA0F2C7BA0A2_inline((Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *)L_13, /*hidden argument*/Enumerator_get_Current_m04294D9E012BC7B9B153DC5438BAAA0F2C7BA0A2_RuntimeMethod_var);
			V_3 = L_14;
			// yield return texture.Value;
			Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_15;
			L_15 = KeyValuePair_2_get_Value_m453802FFD6BD090185B8F8E655E241D04E070D49_inline((KeyValuePair_2_t1AE9F8628F1005DE60C3F1181B4CD7960B0FA986 *)(&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m453802FFD6BD090185B8F8E655E241D04E070D49_RuntimeMethod_var);
			__this->set_U3CU3E2__current_1(L_15);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_00d5;
		}

IL_00a3:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_00ab:
		{
			// foreach (var texture in m_converts)
			Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * L_16 = __this->get_address_of_U3CU3E7__wrap1_4();
			bool L_17;
			L_17 = Enumerator_MoveNext_m14D37307E545E87355A7180B90F7EC67DEA34B16((Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *)L_16, /*hidden argument*/Enumerator_MoveNext_m14D37307E545E87355A7180B90F7EC67DEA34B16_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_007f;
			}
		}

IL_00b8:
		{
			U3CGetTexturesForSaveAssetsU3Ed__11_U3CU3Em__Finally1_m5CFC8C292059B7CD3ECE6042A50CC0BFDE149E5F(__this, /*hidden argument*/NULL);
			Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * L_18 = __this->get_address_of_U3CU3E7__wrap1_4();
			il2cpp_codegen_initobj(L_18, sizeof(Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 ));
		}

IL_00ca:
		{
			// }
			V_0 = (bool)0;
			goto IL_00d5;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00ce;
	}

FAULT_00ce:
	{// begin fault (depth: 1)
		U3CGetTexturesForSaveAssetsU3Ed__11_System_IDisposable_Dispose_m5558065F4017EF9AEA6CB5B2D8E230A59DC9B6D7(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(206)
	}// end fault
	IL2CPP_CLEANUP(206)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00d5:
	{
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11_U3CU3Em__Finally1_m5CFC8C292059B7CD3ECE6042A50CC0BFDE149E5F (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m8464D1A0162AE786225345C59D904340E92B15A4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 * L_0 = __this->get_address_of_U3CU3E7__wrap1_4();
		Enumerator_Dispose_m8464D1A0162AE786225345C59D904340E92B15A4((Enumerator_tBD19D5598CA0979D6EC7207CAE151F887F4B9FC9 *)L_0, /*hidden argument*/Enumerator_Dispose_m8464D1A0162AE786225345C59D904340E92B15A4_RuntimeMethod_var);
		return;
	}
}
// UnityEngine.Texture2D UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.Generic.IEnumerator<UnityEngine.Texture2D>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_Generic_IEnumeratorU3CUnityEngine_Texture2DU3E_get_Current_m2B76F64E706CFF132DF6E50086EA1FFE4AEA5364 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_IEnumerator_Reset_m726C217B282E1AEF2532204BB07EC3824CFF3719 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_IEnumerator_Reset_m726C217B282E1AEF2532204BB07EC3824CFF3719_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_IEnumerator_get_Current_m96E27ACFCA79906E1A1FDE5A696E698F45EB4E44 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Texture2D> UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.Generic.IEnumerable<UnityEngine.Texture2D>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_Texture2DU3E_GetEnumerator_m68F26E45B906DDE0C5997DB88729E75B577F5D35 (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0035;
	}

IL_0022:
	{
		U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * L_3 = (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 *)il2cpp_codegen_object_new(U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594_il2cpp_TypeInfo_var);
		U3CGetTexturesForSaveAssetsU3Ed__11__ctor_mFE512298EE902D29A2D71175B94100FD2501E843(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * L_4 = V_0;
		TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_5 = __this->get_U3CU3E4__this_3();
		NullCheck(L_4);
		L_4->set_U3CU3E4__this_3(L_5);
	}

IL_0035:
	{
		U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.TextureItem/<GetTexturesForSaveAssets>d__11::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_IEnumerable_GetEnumerator_m10BD4A594E05BAC200E676425190F80E309F813E (U3CGetTexturesForSaveAssetsU3Ed__11_tCE18B4F0B1828E45A46813DB05FD2D0EF04C0594 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CGetTexturesForSaveAssetsU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_Texture2DU3E_GetEnumerator_m68F26E45B906DDE0C5997DB88729E75B577F5D35(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureItem/<ProcessCoroutine>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCoroutineU3Ed__15__ctor_m5191FF54D7347CBF6E13B208A8C82049E420C5BD (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UniGLTF.TextureItem/<ProcessCoroutine>d__15::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCoroutineU3Ed__15_System_IDisposable_Dispose_mCA2C4A654AB0E0A99D5F8FB1510DE441965D528C (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureItem/<ProcessCoroutine>d__15::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessCoroutineU3Ed__15_MoveNext_m9009487D9A7F4FBB86BCCD7FD5F9F8F3AA20BC60 (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_004b;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// ProcessOnAnyThread(gltf, storage);
		TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_4 = V_1;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_5 = __this->get_gltf_3();
		RuntimeObject* L_6 = __this->get_storage_4();
		NullCheck(L_4);
		TextureItem_ProcessOnAnyThread_mDF4F0FA7A98DCD6A3D9DDAC1C903BA6979F3B3FB(L_4, L_5, L_6, /*hidden argument*/NULL);
		// yield return ProcessOnMainThreadCoroutine(gltf);
		TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_7 = V_1;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = __this->get_gltf_3();
		NullCheck(L_7);
		RuntimeObject* L_9;
		L_9 = TextureItem_ProcessOnMainThreadCoroutine_m8D62A9EF877DF6A709F29F3E06FA6BFAADE133C5(L_7, L_8, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_9);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object UniGLTF.TextureItem/<ProcessCoroutine>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessCoroutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E0FF2AB8BA0267F3DDF39B5734AA0CD68015A2A (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureItem/<ProcessCoroutine>d__15::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_m192C28FBDF022A6BE8FD77C512C965FDC7E622D6 (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_m192C28FBDF022A6BE8FD77C512C965FDC7E622D6_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureItem/<ProcessCoroutine>d__15::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessCoroutineU3Ed__15_System_Collections_IEnumerator_get_Current_mB6AD278DA9868AC93D2B43021643BFD7EA370890 (U3CProcessCoroutineU3Ed__15_tAB93BDEE257FCB5947B42FBA2BE8F67B9760DA17 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17__ctor_m8704EEB5AE88C2405AF43E229ED3FBBC6E8671D6 (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17_System_IDisposable_Dispose_m928FDBCB96655E9F1563E79EF775D68E75441529 (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{// begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{// begin finally (depth: 1)
		U3CProcessOnMainThreadCoroutineU3Ed__17_U3CU3Em__Finally1_mD5FCAE594A726A3B440FE32B53282D40C0CAAF9E(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessOnMainThreadCoroutineU3Ed__17_MoveNext_m854EA723556241AD3D0F978BEE9535CE47B4FC58 (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITextureLoader_tECE164CB7ADED7358A0798D40319C3C90ED2BE03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * V_2 = NULL;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 3> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_1 = __this->get_U3CU3E4__this_2();
			V_2 = L_1;
			int32_t L_2 = V_1;
			if (!L_2)
			{
				goto IL_001c;
			}
		}

IL_0011:
		{
			int32_t L_3 = V_1;
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_006e;
			}
		}

IL_0015:
		{
			V_0 = (bool)0;
			goto IL_00aa;
		}

IL_001c:
		{
			__this->set_U3CU3E1__state_0((-1));
			// using (m_textureLoader)
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_4 = V_2;
			NullCheck(L_4);
			RuntimeObject* L_5 = L_4->get_m_textureLoader_3();
			__this->set_U3CU3E7__wrap1_4(L_5);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			// var textureType = TextureIO.GetglTFTextureType(gltf, m_textureIndex);
			glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = __this->get_gltf_3();
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_7 = V_2;
			NullCheck(L_7);
			int32_t L_8 = L_7->get_m_textureIndex_0();
			int32_t L_9;
			L_9 = TextureIO_GetglTFTextureType_m62835FF9380D4C9AABB1C880187D64F625E1C052(L_6, L_8, /*hidden argument*/NULL);
			// var colorSpace = TextureIO.GetColorSpace(textureType);
			int32_t L_10;
			L_10 = TextureIO_GetColorSpace_m91BFB360A44169A562FCCB0A7DA236144207049E(L_9, /*hidden argument*/NULL);
			// var isLinear = colorSpace == RenderTextureReadWrite.Linear;
			V_3 = (bool)((((int32_t)L_10) == ((int32_t)1))? 1 : 0);
			// yield return m_textureLoader.ProcessOnMainThread(isLinear);
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_11 = V_2;
			NullCheck(L_11);
			RuntimeObject* L_12 = L_11->get_m_textureLoader_3();
			bool L_13 = V_3;
			NullCheck(L_12);
			RuntimeObject* L_14;
			L_14 = InterfaceFuncInvoker1< RuntimeObject*, bool >::Invoke(2 /* System.Collections.IEnumerator UniGLTF.ITextureLoader::ProcessOnMainThread(System.Boolean) */, ITextureLoader_tECE164CB7ADED7358A0798D40319C3C90ED2BE03_il2cpp_TypeInfo_var, L_12, L_13);
			__this->set_U3CU3E2__current_1(L_14);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00aa;
		}

IL_006e:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			// TextureSamplerUtil.SetSampler(Texture, gltf.GetSamplerFromTextureIndex(m_textureIndex));
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_15 = V_2;
			NullCheck(L_15);
			Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_16;
			L_16 = TextureItem_get_Texture_mF74878F920E1EDFB48735231F73E5AF3D4E742AF(L_15, /*hidden argument*/NULL);
			glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_17 = __this->get_gltf_3();
			TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * L_18 = V_2;
			NullCheck(L_18);
			int32_t L_19 = L_18->get_m_textureIndex_0();
			NullCheck(L_17);
			glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_20;
			L_20 = glTF_GetSamplerFromTextureIndex_m86AA8B70E28301607E5EDE4F40902409BFA1FE7D(L_17, L_19, /*hidden argument*/NULL);
			TextureSamplerUtil_SetSampler_mE631879D75B949273A8758A1F36C60458B29BD25(L_16, L_20, /*hidden argument*/NULL);
			// }
			U3CProcessOnMainThreadCoroutineU3Ed__17_U3CU3Em__Finally1_mD5FCAE594A726A3B440FE32B53282D40C0CAAF9E(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_4((RuntimeObject*)NULL);
			// }
			V_0 = (bool)0;
			goto IL_00aa;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00a3;
	}

FAULT_00a3:
	{// begin fault (depth: 1)
		U3CProcessOnMainThreadCoroutineU3Ed__17_System_IDisposable_Dispose_m928FDBCB96655E9F1563E79EF775D68E75441529(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(163)
	}// end fault
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00aa:
	{
		bool L_21 = V_0;
		return L_21;
	}
}
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17_U3CU3Em__Finally1_mD5FCAE594A726A3B440FE32B53282D40C0CAAF9E (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_4();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap1_4();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Object UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98D4E10CAD7A2B7DF73AEC5B9E5DACE0E3709DA0 (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m9AA9A7D54AF56F2165145AA44550CDFF8D5796EE (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessOnMainThreadCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m9AA9A7D54AF56F2165145AA44550CDFF8D5796EE_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureItem/<ProcessOnMainThreadCoroutine>d__17::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m34E1F29DAEDB4060BC5F7088D7EC5865E0AA1AED (U3CProcessOnMainThreadCoroutineU3Ed__17_t46E9F74E2F182E1DEA9A1819A77A91A1FA5550D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.TextureItem/ColorSpaceScope
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_pinvoke(const ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668& unmarshaled, ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_pinvoke& marshaled)
{
	marshaled.___m_sRGBWrite_0 = static_cast<int32_t>(unmarshaled.get_m_sRGBWrite_0());
}
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_pinvoke_back(const ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_pinvoke& marshaled, ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668& unmarshaled)
{
	bool unmarshaled_m_sRGBWrite_temp_0 = false;
	unmarshaled_m_sRGBWrite_temp_0 = static_cast<bool>(marshaled.___m_sRGBWrite_0);
	unmarshaled.set_m_sRGBWrite_0(unmarshaled_m_sRGBWrite_temp_0);
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureItem/ColorSpaceScope
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_pinvoke_cleanup(ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UniGLTF.TextureItem/ColorSpaceScope
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_com(const ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668& unmarshaled, ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_com& marshaled)
{
	marshaled.___m_sRGBWrite_0 = static_cast<int32_t>(unmarshaled.get_m_sRGBWrite_0());
}
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_com_back(const ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_com& marshaled, ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668& unmarshaled)
{
	bool unmarshaled_m_sRGBWrite_temp_0 = false;
	unmarshaled_m_sRGBWrite_temp_0 = static_cast<bool>(marshaled.___m_sRGBWrite_0);
	unmarshaled.set_m_sRGBWrite_0(unmarshaled_m_sRGBWrite_temp_0);
}
// Conversion method for clean up from marshalling of: UniGLTF.TextureItem/ColorSpaceScope
IL2CPP_EXTERN_C void ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshal_com_cleanup(ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668_marshaled_com& marshaled)
{
}
// System.Void UniGLTF.TextureItem/ColorSpaceScope::.ctor(UnityEngine.RenderTextureReadWrite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope__ctor_m077D9D9306479598498C8550932C37B33D37670D (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, int32_t ___colorSpace0, const RuntimeMethod* method)
{
	{
		// m_sRGBWrite = GL.sRGBWrite;
		bool L_0;
		L_0 = GL_get_sRGBWrite_m34F6CD683D0C47990DB25093C4AE18CB7611BE86(/*hidden argument*/NULL);
		__this->set_m_sRGBWrite_0(L_0);
		int32_t L_1 = ___colorSpace0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___colorSpace0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		goto IL_001c;
	}

IL_0015:
	{
		// GL.sRGBWrite = false;
		GL_set_sRGBWrite_mA2EF1B4BE399C97E9BFB0FA582D09646F7D95629((bool)0, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_001c:
	{
		// GL.sRGBWrite = true;
		GL_set_sRGBWrite_mA2EF1B4BE399C97E9BFB0FA582D09646F7D95629((bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ColorSpaceScope__ctor_m077D9D9306479598498C8550932C37B33D37670D_AdjustorThunk (RuntimeObject * __this, int32_t ___colorSpace0, const RuntimeMethod* method)
{
	ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 *>(__this + _offset);
	ColorSpaceScope__ctor_m077D9D9306479598498C8550932C37B33D37670D(_thisAdjusted, ___colorSpace0, method);
}
// System.Void UniGLTF.TextureItem/ColorSpaceScope::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope__ctor_mFA842FB8D9838E2269AD999DD7BAD3C92A355D8A (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, bool ___sRGBWrite0, const RuntimeMethod* method)
{
	{
		// m_sRGBWrite = GL.sRGBWrite;
		bool L_0;
		L_0 = GL_get_sRGBWrite_m34F6CD683D0C47990DB25093C4AE18CB7611BE86(/*hidden argument*/NULL);
		__this->set_m_sRGBWrite_0(L_0);
		// GL.sRGBWrite = sRGBWrite;
		bool L_1 = ___sRGBWrite0;
		GL_set_sRGBWrite_mA2EF1B4BE399C97E9BFB0FA582D09646F7D95629(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ColorSpaceScope__ctor_mFA842FB8D9838E2269AD999DD7BAD3C92A355D8A_AdjustorThunk (RuntimeObject * __this, bool ___sRGBWrite0, const RuntimeMethod* method)
{
	ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 *>(__this + _offset);
	ColorSpaceScope__ctor_mFA842FB8D9838E2269AD999DD7BAD3C92A355D8A(_thisAdjusted, ___sRGBWrite0, method);
}
// System.Void UniGLTF.TextureItem/ColorSpaceScope::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorSpaceScope_Dispose_m8F11E9022EAE905929A0C4AE6E00655136E72931 (ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * __this, const RuntimeMethod* method)
{
	{
		// GL.sRGBWrite = m_sRGBWrite;
		bool L_0 = __this->get_m_sRGBWrite_0();
		GL_set_sRGBWrite_mA2EF1B4BE399C97E9BFB0FA582D09646F7D95629(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ColorSpaceScope_Dispose_m8F11E9022EAE905929A0C4AE6E00655136E72931_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ColorSpaceScope_tCA42D93E7088168A8BC9F66261759E6376CF2668 *>(__this + _offset);
	ColorSpaceScope_Dispose_m8F11E9022EAE905929A0C4AE6E00655136E72931(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__11__ctor_m5F89D0992A9CF7682909FB2B6F1D45D6B8679B6D (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__11_System_IDisposable_Dispose_mDFE363B2316099CDDDCC2CDE4FE784A459930FBD (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessOnMainThreadU3Ed__11_MoveNext_m88A527B13166F3E4E1F25C9748A138B098EB0CC2 (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Texture = new Texture2D(2, 2, TextureFormat.ARGB32, false, isLinear);
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_3 = V_1;
		bool L_4 = __this->get_isLinear_3();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3(L_5, 2, 2, 5, (bool)0, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		TextureLoader_set_Texture_mD1454DFCCC19367D3F475C05E4EAE7145E111938_inline(L_3, L_5, /*hidden argument*/NULL);
		// Texture.name = m_textureName;
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_6 = V_1;
		NullCheck(L_6);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_7;
		L_7 = TextureLoader_get_Texture_m20A25069C9C86C6ED9DD9E46EDDD15694AF8EBF8_inline(L_6, /*hidden argument*/NULL);
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_m_textureName_3();
		NullCheck(L_7);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_7, L_9, /*hidden argument*/NULL);
		// if (m_imageBytes != null)
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_10 = V_1;
		NullCheck(L_10);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = L_10->get_m_imageBytes_2();
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		// Texture.LoadImage(m_imageBytes);
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_12 = V_1;
		NullCheck(L_12);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_13;
		L_13 = TextureLoader_get_Texture_m20A25069C9C86C6ED9DD9E46EDDD15694AF8EBF8_inline(L_12, /*hidden argument*/NULL);
		TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * L_14 = V_1;
		NullCheck(L_14);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = L_14->get_m_imageBytes_2();
		bool L_16;
		L_16 = ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477(L_13, L_15, /*hidden argument*/NULL);
	}

IL_005a:
	{
		// yield break;
		return (bool)0;
	}
}
// System.Object UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m001E9A497B4044E8A4BAD660088CA46880726626 (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__11_System_Collections_IEnumerator_Reset_mB7FE99FC411C77A74C3F1ABBFC39D8A5B1122C00 (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessOnMainThreadU3Ed__11_System_Collections_IEnumerator_Reset_mB7FE99FC411C77A74C3F1ABBFC39D8A5B1122C00_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureLoader/<ProcessOnMainThread>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadU3Ed__11_System_Collections_IEnumerator_get_Current_mD51221A1960C7AD8FD0BBA7705B91955D6C8D468 (U3CProcessOnMainThreadU3Ed__11_t854EEC5988B47807E662817FB3693AA1513BA2AC * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetUnityWrapModeU3Ed__2__ctor_m43955C6CA3EC7DAEB78C49355EA8B52988516C83 (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetUnityWrapModeU3Ed__2_System_IDisposable_Dispose_mE9DF2B33B573367DE1EEF84EABBF247195563351 (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetUnityWrapModeU3Ed__2_MoveNext_m1F4CDED5BB8276B27000FF325324F0AC5D0F22D2 (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0043;
			}
			case 1:
			{
				goto IL_00b4;
			}
			case 2:
			{
				goto IL_00d6;
			}
			case 3:
			{
				goto IL_00f8;
			}
			case 4:
			{
				goto IL_011a;
			}
			case 5:
			{
				goto IL_0178;
			}
			case 6:
			{
				goto IL_0197;
			}
			case 7:
			{
				goto IL_01b6;
			}
			case 8:
			{
				goto IL_01d5;
			}
			case 9:
			{
				goto IL_0234;
			}
			case 10:
			{
				goto IL_0254;
			}
			case 11:
			{
				goto IL_0274;
			}
			case 12:
			{
				goto IL_0294;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0043:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (sampler.wrapS == sampler.wrapT)
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_2 = __this->get_sampler_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_wrapS_2();
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_4 = __this->get_sampler_3();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_wrapT_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_012c;
		}
	}
	{
		// switch (sampler.wrapS)
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_6 = __this->get_sampler_3();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_wrapS_2();
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) > ((int32_t)((int32_t)10497))))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_9 = V_1;
		if (!L_9)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)10497))))
		{
			goto IL_00e2;
		}
	}
	{
		goto IL_0126;
	}

IL_0089:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)33071))))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)33648))))
		{
			goto IL_0104;
		}
	}
	{
		goto IL_0126;
	}

IL_009e:
	{
		// yield return TypeWithMode(TextureWrapType.All, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_13;
		L_13 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(0, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_13);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00b4:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_00c0:
	{
		// yield return TypeWithMode(TextureWrapType.All, TextureWrapMode.Clamp);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_14;
		L_14 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(0, 1, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_14);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00d6:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_00e2:
	{
		// yield return TypeWithMode(TextureWrapType.All, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_15;
		L_15 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(0, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_15);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_00f8:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_0104:
	{
		// yield return TypeWithMode(TextureWrapType.All, TextureWrapMode.Mirror);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_16;
		L_16 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(0, 2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_16);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_011a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_0126:
	{
		// throw new NotImplementedException();
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_17 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_17, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetUnityWrapModeU3Ed__2_MoveNext_m1F4CDED5BB8276B27000FF325324F0AC5D0F22D2_RuntimeMethod_var)));
	}

IL_012c:
	{
		// switch (sampler.wrapS)
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_18 = __this->get_sampler_3();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_wrapS_2();
		V_1 = L_19;
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) > ((int32_t)((int32_t)10497))))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_21 = V_1;
		if (!L_21)
		{
			goto IL_0162;
		}
	}
	{
		int32_t L_22 = V_1;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)10497))))
		{
			goto IL_01a0;
		}
	}
	{
		goto IL_01de;
	}

IL_0150:
	{
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)33071))))
		{
			goto IL_0181;
		}
	}
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)33648))))
		{
			goto IL_01bf;
		}
	}
	{
		goto IL_01de;
	}

IL_0162:
	{
		// yield return TypeWithMode(TextureWrapType.U, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_25;
		L_25 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(1, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_25);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_0178:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_01e4;
	}

IL_0181:
	{
		// yield return TypeWithMode(TextureWrapType.U, TextureWrapMode.Clamp);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_26;
		L_26 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(1, 1, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_26);
		__this->set_U3CU3E1__state_0(6);
		return (bool)1;
	}

IL_0197:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_01e4;
	}

IL_01a0:
	{
		// yield return TypeWithMode(TextureWrapType.U, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_27;
		L_27 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(1, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_27);
		__this->set_U3CU3E1__state_0(7);
		return (bool)1;
	}

IL_01b6:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_01e4;
	}

IL_01bf:
	{
		// yield return TypeWithMode(TextureWrapType.U, TextureWrapMode.Mirror);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_28;
		L_28 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(1, 2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_28);
		__this->set_U3CU3E1__state_0(8);
		return (bool)1;
	}

IL_01d5:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_01e4;
	}

IL_01de:
	{
		// throw new NotImplementedException();
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_29 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetUnityWrapModeU3Ed__2_MoveNext_m1F4CDED5BB8276B27000FF325324F0AC5D0F22D2_RuntimeMethod_var)));
	}

IL_01e4:
	{
		// switch (sampler.wrapT)
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_30 = __this->get_sampler_3();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_wrapT_3();
		V_1 = L_31;
		int32_t L_32 = V_1;
		if ((((int32_t)L_32) > ((int32_t)((int32_t)10497))))
		{
			goto IL_0208;
		}
	}
	{
		int32_t L_33 = V_1;
		if (!L_33)
		{
			goto IL_021d;
		}
	}
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) == ((int32_t)((int32_t)10497))))
		{
			goto IL_025d;
		}
	}
	{
		goto IL_029d;
	}

IL_0208:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)33071))))
		{
			goto IL_023d;
		}
	}
	{
		int32_t L_36 = V_1;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)33648))))
		{
			goto IL_027d;
		}
	}
	{
		goto IL_029d;
	}

IL_021d:
	{
		// yield return TypeWithMode(TextureWrapType.V, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_37;
		L_37 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(2, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_37);
		__this->set_U3CU3E1__state_0(((int32_t)9));
		return (bool)1;
	}

IL_0234:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_023d:
	{
		// yield return TypeWithMode(TextureWrapType.V, TextureWrapMode.Clamp);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_38;
		L_38 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(2, 1, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_38);
		__this->set_U3CU3E1__state_0(((int32_t)10));
		return (bool)1;
	}

IL_0254:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_025d:
	{
		// yield return TypeWithMode(TextureWrapType.V, TextureWrapMode.Repeat);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_39;
		L_39 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(2, 0, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_39);
		__this->set_U3CU3E1__state_0(((int32_t)11));
		return (bool)1;
	}

IL_0274:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_027d:
	{
		// yield return TypeWithMode(TextureWrapType.V, TextureWrapMode.Mirror);
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_40;
		L_40 = TextureSamplerUtil_TypeWithMode_m5659C9F16990ABD3AD969C2AAA44ACC7117F42BD(2, 2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_40);
		__this->set_U3CU3E1__state_0(((int32_t)12));
		return (bool)1;
	}

IL_0294:
	{
		__this->set_U3CU3E1__state_0((-1));
		// break;
		goto IL_02a3;
	}

IL_029d:
	{
		// throw new NotImplementedException();
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_41 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_41, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_41, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetUnityWrapModeU3Ed__2_MoveNext_m1F4CDED5BB8276B27000FF325324F0AC5D0F22D2_RuntimeMethod_var)));
	}

IL_02a3:
	{
		// }
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode> UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<UniGLTF.TextureSamplerUtil.TextureWrapType,UnityEngine.TextureWrapMode>>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  U3CGetUnityWrapModeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CUniGLTF_TextureSamplerUtil_TextureWrapTypeU2CUnityEngine_TextureWrapModeU3EU3E_get_Current_m353A8235C57B17414AE6B7220BBE903C22CA14AB (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetUnityWrapModeU3Ed__2_System_Collections_IEnumerator_Reset_m8509F993ADB3C1420AB6C5FB5A4A0D4D9C9CE5B9 (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetUnityWrapModeU3Ed__2_System_Collections_IEnumerator_Reset_m8509F993ADB3C1420AB6C5FB5A4A0D4D9C9CE5B9_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetUnityWrapModeU3Ed__2_System_Collections_IEnumerator_get_Current_mE770BD3B48EA2D6DCBD0F1CF4D63BD7E79129FAD (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_0 = __this->get_U3CU3E2__current_1();
		KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A  L_1 = L_0;
		RuntimeObject * L_2 = Box(KeyValuePair_2_t68D56EDE9B0351CE5C0453FCB106FE358038A12A_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UniGLTF.TextureSamplerUtil/TextureWrapType,UnityEngine.TextureWrapMode>> UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<UniGLTF.TextureSamplerUtil.TextureWrapType,UnityEngine.TextureWrapMode>>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetUnityWrapModeU3Ed__2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CUniGLTF_TextureSamplerUtil_TextureWrapTypeU2CUnityEngine_TextureWrapModeU3EU3E_GetEnumerator_m3A1DBB56807857C178368865E2D0CCC86A86502B (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * L_3 = (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 *)il2cpp_codegen_object_new(U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339_il2cpp_TypeInfo_var);
		U3CGetUnityWrapModeU3Ed__2__ctor_m43955C6CA3EC7DAEB78C49355EA8B52988516C83(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * L_4 = V_0;
		glTFTextureSampler_t8E9387F3ABB0D85CE221B4BE927DE595424D1496 * L_5 = __this->get_U3CU3E3__sampler_4();
		NullCheck(L_4);
		L_4->set_sampler_3(L_5);
		U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.TextureSamplerUtil/<GetUnityWrapMode>d__2::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetUnityWrapModeU3Ed__2_System_Collections_IEnumerable_GetEnumerator_mD0F725C0EEA13259422EF11FA0BC3BC5C890C66B (U3CGetUnityWrapModeU3Ed__2_t45328ED339F7497906E62FFB74827A648DFFC339 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CGetUnityWrapModeU3Ed__2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CUniGLTF_TextureSamplerUtil_TextureWrapTypeU2CUnityEngine_TextureWrapModeU3EU3E_GetEnumerator_m3A1DBB56807857C178368865E2D0CCC86A86502B(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriangleUtil/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m8626952DE73DE07A08748E276FE725279D1FE2BA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * L_0 = (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 *)il2cpp_codegen_object_new(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m6DFF3D22470F722AE56EE9EEF7F7C01C3C2AF83D(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void TriangleUtil/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6DFF3D22470F722AE56EE9EEF7F7C01C3C2AF83D (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TriangleUtil/<>c::<FlipTriangle>b__0_0(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CFlipTriangleU3Eb__0_0_mEA4573DE39B2B9B27509ADDDF4BD2478252C4313 (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * __this, uint8_t ___x0, const RuntimeMethod* method)
{
	{
		// return FlipTriangle(src.Select(x => (Int32)x));
		uint8_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 TriangleUtil/<>c::<FlipTriangle>b__1_0(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CFlipTriangleU3Eb__1_0_mB7F5D99EF5A85FFA00C580D84A8A0C6AC5C05BB6 (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * __this, uint16_t ___x0, const RuntimeMethod* method)
{
	{
		// return FlipTriangle(src.Select(x => (Int32)x));
		uint16_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 TriangleUtil/<>c::<FlipTriangle>b__2_0(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CFlipTriangleU3Eb__2_0_m7539303FF28C44E6AEA0BEF518F3F6D03C0D62FE (U3CU3Ec_tEFF498677D25AE99CC72634385371C6F86C02721 * __this, uint32_t ___x0, const RuntimeMethod* method)
{
	{
		// return FlipTriangle(src.Select(x => (Int32)x));
		uint32_t L_0 = ___x0;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriangleUtil/<FlipTriangle>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipTriangleU3Ed__3__ctor_mA0A1E7FD9D802F18B71C5AF348E08BD918579F34 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void TriangleUtil/<FlipTriangle>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipTriangleU3Ed__3_System_IDisposable_Dispose_m3A357D8AC2ED7545EC143410B286A913A49FAAF3 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TriangleUtil/<FlipTriangle>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CFlipTriangleU3Ed__3_MoveNext_m62A58D9962FB4AA96BE5C18F134F117D98477C36 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_00a2;
			}
			case 2:
			{
				goto IL_00be;
			}
			case 3:
			{
				goto IL_00da;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// var it = src.GetEnumerator();
		RuntimeObject* L_2 = __this->get_src_3();
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370_il2cpp_TypeInfo_var, L_2);
		__this->set_U3CitU3E5__2_5(L_3);
	}

IL_0037:
	{
		// if (!it.MoveNext())
		RuntimeObject* L_4 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_4);
		bool L_5;
		L_5 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_4);
		if (L_5)
		{
			goto IL_0046;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_0046:
	{
		// var i0 = it.Current;
		RuntimeObject* L_6 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_6);
		int32_t L_7;
		L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB_il2cpp_TypeInfo_var, L_6);
		__this->set_U3Ci0U3E5__3_6(L_7);
		// if (!it.MoveNext())
		RuntimeObject* L_8 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_8);
		bool L_9;
		L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_8);
		if (L_9)
		{
			goto IL_0066;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_0066:
	{
		// var i1 = it.Current;
		RuntimeObject* L_10 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB_il2cpp_TypeInfo_var, L_10);
		__this->set_U3Ci1U3E5__4_7(L_11);
		// if (!it.MoveNext())
		RuntimeObject* L_12 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_12);
		bool L_13;
		L_13 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_12);
		if (L_13)
		{
			goto IL_0086;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_0086:
	{
		// var i2 = it.Current;
		RuntimeObject* L_14 = __this->get_U3CitU3E5__2_5();
		NullCheck(L_14);
		int32_t L_15;
		L_15 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB_il2cpp_TypeInfo_var, L_14);
		V_1 = L_15;
		// yield return i2;
		int32_t L_16 = V_1;
		__this->set_U3CU3E2__current_1(L_16);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00a2:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return i1;
		int32_t L_17 = __this->get_U3Ci1U3E5__4_7();
		__this->set_U3CU3E2__current_1(L_17);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00be:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return i0;
		int32_t L_18 = __this->get_U3Ci0U3E5__3_6();
		__this->set_U3CU3E2__current_1(L_18);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_00da:
	{
		__this->set_U3CU3E1__state_0((-1));
		// while (true)
		goto IL_0037;
	}
}
// System.Int32 TriangleUtil/<FlipTriangle>d__3::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CFlipTriangleU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m05E671A61562969448DD8A511BB81013ABB31CF0 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TriangleUtil/<FlipTriangle>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFlipTriangleU3Ed__3_System_Collections_IEnumerator_Reset_mB05A7E73BBBD6E4B337283236D579F70AF7ABAE2 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CFlipTriangleU3Ed__3_System_Collections_IEnumerator_Reset_mB05A7E73BBBD6E4B337283236D579F70AF7ABAE2_RuntimeMethod_var)));
	}
}
// System.Object TriangleUtil/<FlipTriangle>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFlipTriangleU3Ed__3_System_Collections_IEnumerator_get_Current_m615CFAF9825FDCE5F7E315F8D0C909586A63C016 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_U3CU3E2__current_1();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Int32> TriangleUtil/<FlipTriangle>d__3::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CFlipTriangleU3Ed__3_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m1CE18EEBC190F67FE9C390B162831BFFFC07C18B (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * L_3 = (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 *)il2cpp_codegen_object_new(U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640_il2cpp_TypeInfo_var);
		U3CFlipTriangleU3Ed__3__ctor_mA0A1E7FD9D802F18B71C5AF348E08BD918579F34(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * L_4 = V_0;
		RuntimeObject* L_5 = __this->get_U3CU3E3__src_4();
		NullCheck(L_4);
		L_4->set_src_3(L_5);
		U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator TriangleUtil/<FlipTriangle>d__3::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CFlipTriangleU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m349AE1268EA825BA7F9BD2F50D7C73C4D8049BB3 (U3CFlipTriangleU3Ed__3_t5A0D02CAC446518AC18E4BCBAA39584767852640 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CFlipTriangleU3Ed__3_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m1CE18EEBC190F67FE9C390B162831BFFFC07C18B(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityExtensions/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m7B5AB0CFB9E0135636BEBA5FB6EA9CE52C57CA9F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * L_0 = (U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C *)il2cpp_codegen_object_new(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m626309C50812FC25705BCB786A53C4C35026CA35(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UniGLTF.UnityExtensions/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m626309C50812FC25705BCB786A53C4C35026CA35 (U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UniGLTF.UnityExtensions/<>c::<ReverseZ>b__23_0(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CU3Ec_U3CReverseZU3Eb__23_0_m771A630CC73915C1DAB2E483C08AED67B8ACF7AE (U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	{
		// var globalMap = root.Traverse().ToDictionary(x => x, x => PosRot.FromGlobalTransform(x));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		return L_0;
	}
}
// UniGLTF.PosRot UniGLTF.UnityExtensions/<>c::<ReverseZ>b__23_1(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4  U3CU3Ec_U3CReverseZU3Eb__23_1_mDC5C2E997D892FCBA04A84C14A2EE64C35EBC3E0 (U3CU3Ec_t12973514CC6E47560A4A8A59A89DDC028A1C387C * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	{
		// var globalMap = root.Traverse().ToDictionary(x => x, x => PosRot.FromGlobalTransform(x));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		PosRot_t1F303B953A440B1248B05C1C4DAA4ECDC0D843B4  L_1;
		L_1 = PosRot_FromGlobalTransform_m78E34B88E24EFF2EC01A5B93C78CFD824FD30156(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityExtensions/<>c__DisplayClass17_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass17_0__ctor_m01B439A97A24762E8C6C731F949FEA35641AC782 (U3CU3Ec__DisplayClass17_0_t641D155E04F0EFA0F3C94152F3D4A62E35FBDC4C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.UnityExtensions/<>c__DisplayClass17_0::<FindDescenedant>b__0(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass17_0_U3CFindDescenedantU3Eb__0_m7D7A86F43DFF11C16C06FE0435E8B89C41ABA6F2 (U3CU3Ec__DisplayClass17_0_t641D155E04F0EFA0F3C94152F3D4A62E35FBDC4C * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	{
		// return t.Traverse().First(x => x.name == name);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_name_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18__ctor_m025FE1617B5206D83BFDCA46C51FA31986A8017C (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18_System_IDisposable_Dispose_m92FA03456F1597B41BB6872242E6C92673D6C135 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{// begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{// begin finally (depth: 1)
		U3CAncestorsU3Ed__18_U3CU3Em__Finally1_m0C4FA0C1CD5BD57AF660BC46A9E3CABE10238022(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityExtensions/<Ancestors>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CAncestorsU3Ed__18_MoveNext_m69E6692D307A770D37063FF7BD5F75EFDA2270EC (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			switch (L_1)
			{
				case 0:
				{
					goto IL_0020;
				}
				case 1:
				{
					goto IL_0041;
				}
				case 2:
				{
					goto IL_009e;
				}
			}
		}

IL_0019:
		{
			V_0 = (bool)0;
			goto IL_00cb;
		}

IL_0020:
		{
			__this->set_U3CU3E1__state_0((-1));
			// yield return t;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_t_3();
			__this->set_U3CU3E2__current_1(L_2);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00cb;
		}

IL_0041:
		{
			__this->set_U3CU3E1__state_0((-1));
			// if (t.parent != null)
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_t_3();
			NullCheck(L_3);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
			L_4 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_5;
			L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_00c0;
			}
		}

IL_005b:
		{
			// foreach (Transform x in t.parent.Ancestors())
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_t_3();
			NullCheck(L_6);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
			L_7 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_6, /*hidden argument*/NULL);
			RuntimeObject* L_8;
			L_8 = UnityExtensions_Ancestors_m378E737E6047E14CB95F88AD5CC3B5CCD47CB80C(L_7, /*hidden argument*/NULL);
			NullCheck(L_8);
			RuntimeObject* L_9;
			L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>::GetEnumerator() */, IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CU3E7__wrap1_5(L_9);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_00a6;
		}

IL_0080:
		{
			// foreach (Transform x in t.parent.Ancestors())
			RuntimeObject* L_10 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_10);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
			L_11 = InterfaceFuncInvoker0< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>::get_Current() */, IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var, L_10);
			V_2 = L_11;
			// yield return x;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = V_2;
			__this->set_U3CU3E2__current_1(L_12);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_00cb;
		}

IL_009e:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_00a6:
		{
			// foreach (Transform x in t.parent.Ancestors())
			RuntimeObject* L_13 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_13);
			bool L_14;
			L_14 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_0080;
			}
		}

IL_00b3:
		{
			U3CAncestorsU3Ed__18_U3CU3Em__Finally1_m0C4FA0C1CD5BD57AF660BC46A9E3CABE10238022(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
		}

IL_00c0:
		{
			// }
			V_0 = (bool)0;
			goto IL_00cb;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00c4;
	}

FAULT_00c4:
	{// begin fault (depth: 1)
		U3CAncestorsU3Ed__18_System_IDisposable_Dispose_m92FA03456F1597B41BB6872242E6C92673D6C135(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(196)
	}// end fault
	IL2CPP_CLEANUP(196)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00cb:
	{
		bool L_15 = V_0;
		return L_15;
	}
}
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18_U3CU3Em__Finally1_m0C4FA0C1CD5BD57AF660BC46A9E3CABE10238022 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap1_5();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// UnityEngine.Transform UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CAncestorsU3Ed__18_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_mEF312FDB687D6FA927F438825356D7EBC5C4DCAE (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAncestorsU3Ed__18_System_Collections_IEnumerator_Reset_m3B2E397EF34A70954CA020CC657A61EFF7D077EE (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CAncestorsU3Ed__18_System_Collections_IEnumerator_Reset_m3B2E397EF34A70954CA020CC657A61EFF7D077EE_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CAncestorsU3Ed__18_System_Collections_IEnumerator_get_Current_m88CBE86FE36CEFF667409A81744362AADEAD2781 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CAncestorsU3Ed__18_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m3D43D4A8C5EE2D723C43A0E029FBE44F3853CE36 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * L_3 = (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 *)il2cpp_codegen_object_new(U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232_il2cpp_TypeInfo_var);
		U3CAncestorsU3Ed__18__ctor_m025FE1617B5206D83BFDCA46C51FA31986A8017C(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_U3CU3E3__t_4();
		NullCheck(L_4);
		L_4->set_t_3(L_5);
		U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityExtensions/<Ancestors>d__18::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CAncestorsU3Ed__18_System_Collections_IEnumerable_GetEnumerator_m204A01D18C60D86735597E9367A247A70926E764 (U3CAncestorsU3Ed__18_t4C231BE6800FFEDD46DC8A5F90AF949B2FF01232 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CAncestorsU3Ed__18_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m3D43D4A8C5EE2D723C43A0E029FBE44F3853CE36(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15__ctor_m4D33DE3ED8C86FEFC24593760FF19CC933031F63 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15_System_IDisposable_Dispose_m9179915A2581C26DCC68E28B33966E27EF495245 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{// begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{// begin finally (depth: 1)
		U3CGetChildrenU3Ed__15_U3CU3Em__Finally1_mD287C3FAD9B8FC7589C3879518B1931EC758AF27(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityExtensions/<GetChildren>d__15::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetChildrenU3Ed__15_MoveNext_m0CBDBC68CBAAC30D6FE64AE3C8E1C91120E5C081 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 3> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			if (!L_1)
			{
				goto IL_0012;
			}
		}

IL_000a:
		{
			int32_t L_2 = V_1;
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0057;
			}
		}

IL_000e:
		{
			V_0 = (bool)0;
			goto IL_0084;
		}

IL_0012:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach (Transform child in self)
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_self_3();
			NullCheck(L_3);
			RuntimeObject* L_4;
			L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_005f;
		}

IL_0034:
		{
			// foreach (Transform child in self)
			RuntimeObject* L_5 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_5);
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			V_2 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// yield return child;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = V_2;
			__this->set_U3CU3E2__current_1(L_7);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0084;
		}

IL_0057:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_005f:
		{
			// foreach (Transform child in self)
			RuntimeObject* L_8 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_8);
			bool L_9;
			L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_006c:
		{
			U3CGetChildrenU3Ed__15_U3CU3Em__Finally1_mD287C3FAD9B8FC7589C3879518B1931EC758AF27(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			// }
			V_0 = (bool)0;
			goto IL_0084;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_007d;
	}

FAULT_007d:
	{// begin fault (depth: 1)
		U3CGetChildrenU3Ed__15_System_IDisposable_Dispose_m9179915A2581C26DCC68E28B33966E27EF495245(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	}// end fault
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0084:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15_U3CU3Em__Finally1_mD287C3FAD9B8FC7589C3879518B1931EC758AF27 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		return;
	}
}
// UnityEngine.Transform UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CGetChildrenU3Ed__15_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m46D6063123E0B5876981137D3EF0BD73137C0AFC (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__15_System_Collections_IEnumerator_Reset_mA1AD165672DC79E503BA338C9E4A3F92573B7C6A (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetChildrenU3Ed__15_System_Collections_IEnumerator_Reset_mA1AD165672DC79E503BA338C9E4A3F92573B7C6A_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetChildrenU3Ed__15_System_Collections_IEnumerator_get_Current_m36229AC61C3160CFE5DA38F4BA72261B450FC859 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__15_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mBC7B2CE354CE73EA3776D981E7521580F5AD5403 (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * L_3 = (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D *)il2cpp_codegen_object_new(U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D_il2cpp_TypeInfo_var);
		U3CGetChildrenU3Ed__15__ctor_m4D33DE3ED8C86FEFC24593760FF19CC933031F63(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_U3CU3E3__self_4();
		NullCheck(L_4);
		L_4->set_self_3(L_5);
		U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityExtensions/<GetChildren>d__15::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__15_System_Collections_IEnumerable_GetEnumerator_mD5E340F9A7782F18E5064D64F1C40B5D4EBF180D (U3CGetChildrenU3Ed__15_tAC592EEBC19475682060ACF40E4A043B01F9ED4D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CGetChildrenU3Ed__15_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mBC7B2CE354CE73EA3776D981E7521580F5AD5403(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16__ctor_mA430402B6A443F4987D0BD109D3857C86F165CE7 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_System_IDisposable_Dispose_m63AC1B9B2306930A993CDE46593963FABFC0C4D2 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)-4)))) > ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}

IL_0012:
	{
	}

IL_0013:
	try
	{// begin try (depth: 1)
		{
			int32_t L_3 = V_0;
			if ((((int32_t)L_3) == ((int32_t)((int32_t)-4))))
			{
				goto IL_001e;
			}
		}

IL_0018:
		{
			int32_t L_4 = V_0;
			if ((((int32_t)L_4) == ((int32_t)2)))
			{
				goto IL_001e;
			}
		}

IL_001c:
		{
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_001e:
		{
		}

IL_001f:
		try
		{// begin try (depth: 2)
			IL2CPP_LEAVE(0x2F, FINALLY_0021);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0021;
		}

FINALLY_0021:
		{// begin finally (depth: 2)
			U3CTraverseU3Ed__16_U3CU3Em__Finally2_mBE81E429EACDA457E04B1BA2AA26572D9D9458DE(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(33)
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(33)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x2F, FINALLY_0028);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{// begin finally (depth: 1)
		U3CTraverseU3Ed__16_U3CU3Em__Finally1_m3E4E7206D320303533A27F78181CF7EF2D403B86(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityExtensions/<Traverse>d__16::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTraverseU3Ed__16_MoveNext_mAC83009491D1C416FADFDCCFCE21B4612C0E3318 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			switch (L_1)
			{
				case 0:
				{
					goto IL_0020;
				}
				case 1:
				{
					goto IL_0041;
				}
				case 2:
				{
					goto IL_00ad;
				}
			}
		}

IL_0019:
		{
			V_0 = (bool)0;
			goto IL_00f4;
		}

IL_0020:
		{
			__this->set_U3CU3E1__state_0((-1));
			// yield return t;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_t_3();
			__this->set_U3CU3E2__current_1(L_2);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00f4;
		}

IL_0041:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach (Transform x in t)
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_t_3();
			NullCheck(L_3);
			RuntimeObject* L_4;
			L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_00cf;
		}

IL_0063:
		{
			// foreach (Transform x in t)
			RuntimeObject* L_5 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_5);
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			V_2 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// foreach (Transform y in x.Traverse())
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = V_2;
			RuntimeObject* L_8;
			L_8 = UnityExtensions_Traverse_m8EE8F917FF6348D2FE8C29671A5DD40797CC9FC7(L_7, /*hidden argument*/NULL);
			NullCheck(L_8);
			RuntimeObject* L_9;
			L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>::GetEnumerator() */, IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CU3E7__wrap2_6(L_9);
			__this->set_U3CU3E1__state_0(((int32_t)-4));
			goto IL_00b5;
		}

IL_008f:
		{
			// foreach (Transform y in x.Traverse())
			RuntimeObject* L_10 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_10);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
			L_11 = InterfaceFuncInvoker0< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>::get_Current() */, IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var, L_10);
			V_3 = L_11;
			// yield return y;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = V_3;
			__this->set_U3CU3E2__current_1(L_12);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_00f4;
		}

IL_00ad:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-4));
		}

IL_00b5:
		{
			// foreach (Transform y in x.Traverse())
			RuntimeObject* L_13 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_13);
			bool L_14;
			L_14 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_008f;
			}
		}

IL_00c2:
		{
			U3CTraverseU3Ed__16_U3CU3Em__Finally2_mBE81E429EACDA457E04B1BA2AA26572D9D9458DE(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap2_6((RuntimeObject*)NULL);
		}

IL_00cf:
		{
			// foreach (Transform x in t)
			RuntimeObject* L_15 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_15);
			bool L_16;
			L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0063;
			}
		}

IL_00dc:
		{
			U3CTraverseU3Ed__16_U3CU3Em__Finally1_m3E4E7206D320303533A27F78181CF7EF2D403B86(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			// }
			V_0 = (bool)0;
			goto IL_00f4;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00ed;
	}

FAULT_00ed:
	{// begin fault (depth: 1)
		U3CTraverseU3Ed__16_System_IDisposable_Dispose_m63AC1B9B2306930A993CDE46593963FABFC0C4D2(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(237)
	}// end fault
	IL2CPP_CLEANUP(237)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00f4:
	{
		bool L_17 = V_0;
		return L_17;
	}
}
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_U3CU3Em__Finally1_m3E4E7206D320303533A27F78181CF7EF2D403B86 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_U3CU3Em__Finally2_mBE81E429EACDA457E04B1BA2AA26572D9D9458DE (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0(((int32_t)-3));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap2_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// UnityEngine.Transform UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CTraverseU3Ed__16_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m4536046E82C29FCB81552AE50F0BB71901F89661 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__16_System_Collections_IEnumerator_Reset_mA5653B0EF412BEE75E76905BC9D71CFBC9A9242F (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTraverseU3Ed__16_System_Collections_IEnumerator_Reset_mA5653B0EF412BEE75E76905BC9D71CFBC9A9242F_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTraverseU3Ed__16_System_Collections_IEnumerator_get_Current_m47B17599E446D130A17D333B13F300244B8734E9 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__16_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mFE22F7E9DDF8BB14CF22F5922CFBE2047EB3C689 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * L_3 = (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C *)il2cpp_codegen_object_new(U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C_il2cpp_TypeInfo_var);
		U3CTraverseU3Ed__16__ctor_mA430402B6A443F4987D0BD109D3857C86F165CE7(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_U3CU3E3__t_4();
		NullCheck(L_4);
		L_4->set_t_3(L_5);
		U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityExtensions/<Traverse>d__16::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__16_System_Collections_IEnumerable_GetEnumerator_mE28B76E6A8ACF4B77DEF7B4A5140FE12103D7677 (U3CTraverseU3Ed__16_tFDBD85F87969E881B66557122AE918C5043E827C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CTraverseU3Ed__16_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mFE22F7E9DDF8BB14CF22F5922CFBE2047EB3C689(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1__ctor_m8C97A9196FC9472495BF20093F78AC045D966FA5 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1_System_IDisposable_Dispose_m19124E00BC17283674FBBB346E952D8FC0E72AF9 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{// begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{// begin finally (depth: 1)
		U3CGetChildrenU3Ed__1_U3CU3Em__Finally1_m101F8977A5C05E58E8BB61FCF22381162324D398(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean UniHumanoid.UnityExtensions/<GetChildren>d__1::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetChildrenU3Ed__1_MoveNext_m8B0FD3DBBD58DB3E7A458A869384CB21234EBCF6 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 3> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			if (!L_1)
			{
				goto IL_0012;
			}
		}

IL_000a:
		{
			int32_t L_2 = V_1;
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0057;
			}
		}

IL_000e:
		{
			V_0 = (bool)0;
			goto IL_0084;
		}

IL_0012:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach (Transform child in parent)
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_parent_3();
			NullCheck(L_3);
			RuntimeObject* L_4;
			L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_005f;
		}

IL_0034:
		{
			// foreach (Transform child in parent)
			RuntimeObject* L_5 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_5);
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			V_2 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// yield return child;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = V_2;
			__this->set_U3CU3E2__current_1(L_7);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0084;
		}

IL_0057:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_005f:
		{
			// foreach (Transform child in parent)
			RuntimeObject* L_8 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_8);
			bool L_9;
			L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_006c:
		{
			U3CGetChildrenU3Ed__1_U3CU3Em__Finally1_m101F8977A5C05E58E8BB61FCF22381162324D398(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			// }
			V_0 = (bool)0;
			goto IL_0084;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_007d;
	}

FAULT_007d:
	{// begin fault (depth: 1)
		U3CGetChildrenU3Ed__1_System_IDisposable_Dispose_m19124E00BC17283674FBBB346E952D8FC0E72AF9(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	}// end fault
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0084:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1_U3CU3Em__Finally1_m101F8977A5C05E58E8BB61FCF22381162324D398 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		return;
	}
}
// UnityEngine.Transform UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CGetChildrenU3Ed__1_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_mC7E99171F9CB4480B3C0362B0E3E4B88F04F9B9D (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetChildrenU3Ed__1_System_Collections_IEnumerator_Reset_mB868A68C8D7C10108CD73B8B0860B50316DB4580 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetChildrenU3Ed__1_System_Collections_IEnumerator_Reset_mB868A68C8D7C10108CD73B8B0860B50316DB4580_RuntimeMethod_var)));
	}
}
// System.Object UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetChildrenU3Ed__1_System_Collections_IEnumerator_get_Current_mAC9AE97CC0046F5F9391ADA50A7C146C8B7C52B9 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__1_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m014CB9794B4D01FB5452561C6FE8FCC04E44A06A (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * L_3 = (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 *)il2cpp_codegen_object_new(U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21_il2cpp_TypeInfo_var);
		U3CGetChildrenU3Ed__1__ctor_m8C97A9196FC9472495BF20093F78AC045D966FA5(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_U3CU3E3__parent_4();
		NullCheck(L_4);
		L_4->set_parent_3(L_5);
		U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniHumanoid.UnityExtensions/<GetChildren>d__1::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetChildrenU3Ed__1_System_Collections_IEnumerable_GetEnumerator_m56D10DA223A8CF5127482590ED66F62ECA5E0D51 (U3CGetChildrenU3Ed__1_tAD3AC411A3CC2EE6425207C843096C8C5B66CF21 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CGetChildrenU3Ed__1_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_m014CB9794B4D01FB5452561C6FE8FCC04E44A06A(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2__ctor_mCCC31783688C9F2ADE0A999E8AC201054F2880BE (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_System_IDisposable_Dispose_m0D56D8F1A83F3BB32FD3D4771D5BB1F8A9593F70 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)-4)))) > ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}

IL_0012:
	{
	}

IL_0013:
	try
	{// begin try (depth: 1)
		{
			int32_t L_3 = V_0;
			if ((((int32_t)L_3) == ((int32_t)((int32_t)-4))))
			{
				goto IL_001e;
			}
		}

IL_0018:
		{
			int32_t L_4 = V_0;
			if ((((int32_t)L_4) == ((int32_t)2)))
			{
				goto IL_001e;
			}
		}

IL_001c:
		{
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_001e:
		{
		}

IL_001f:
		try
		{// begin try (depth: 2)
			IL2CPP_LEAVE(0x2F, FINALLY_0021);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0021;
		}

FINALLY_0021:
		{// begin finally (depth: 2)
			U3CTraverseU3Ed__2_U3CU3Em__Finally2_mEC2B9792DA117D865CE44A3545BE04E6F86BE07B(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(33)
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(33)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x2F, FINALLY_0028);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{// begin finally (depth: 1)
		U3CTraverseU3Ed__2_U3CU3Em__Finally1_m89BFF8CC58BB7E79B14B123CB8E598F30E0BC9D5(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean UniHumanoid.UnityExtensions/<Traverse>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTraverseU3Ed__2_MoveNext_mB0CE46269FB343AE39593955CCEFA5F365A4702D (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			switch (L_1)
			{
				case 0:
				{
					goto IL_0020;
				}
				case 1:
				{
					goto IL_0041;
				}
				case 2:
				{
					goto IL_00ad;
				}
			}
		}

IL_0019:
		{
			V_0 = (bool)0;
			goto IL_00f4;
		}

IL_0020:
		{
			__this->set_U3CU3E1__state_0((-1));
			// yield return parent;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_parent_3();
			__this->set_U3CU3E2__current_1(L_2);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00f4;
		}

IL_0041:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach (Transform child in parent)
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_parent_3();
			NullCheck(L_3);
			RuntimeObject* L_4;
			L_4 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_3, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5(L_4);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_00cf;
		}

IL_0063:
		{
			// foreach (Transform child in parent)
			RuntimeObject* L_5 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_5);
			RuntimeObject * L_6;
			L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_5);
			V_2 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_6, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// foreach (Transform descendant in Traverse(child))
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = V_2;
			RuntimeObject* L_8;
			L_8 = UnityExtensions_Traverse_m47E2CE7236D50DBEDB2D8F2C183F141B0D1BECBD(L_7, /*hidden argument*/NULL);
			NullCheck(L_8);
			RuntimeObject* L_9;
			L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>::GetEnumerator() */, IEnumerable_1_t64A142138D8BB01DB1E632636D7FAF3A561F0792_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CU3E7__wrap2_6(L_9);
			__this->set_U3CU3E1__state_0(((int32_t)-4));
			goto IL_00b5;
		}

IL_008f:
		{
			// foreach (Transform descendant in Traverse(child))
			RuntimeObject* L_10 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_10);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
			L_11 = InterfaceFuncInvoker0< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>::get_Current() */, IEnumerator_1_t410369E77AF5FBCF0814A07B279351813272D150_il2cpp_TypeInfo_var, L_10);
			V_3 = L_11;
			// yield return descendant;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = V_3;
			__this->set_U3CU3E2__current_1(L_12);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_00f4;
		}

IL_00ad:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-4));
		}

IL_00b5:
		{
			// foreach (Transform descendant in Traverse(child))
			RuntimeObject* L_13 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_13);
			bool L_14;
			L_14 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_008f;
			}
		}

IL_00c2:
		{
			U3CTraverseU3Ed__2_U3CU3Em__Finally2_mEC2B9792DA117D865CE44A3545BE04E6F86BE07B(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap2_6((RuntimeObject*)NULL);
		}

IL_00cf:
		{
			// foreach (Transform child in parent)
			RuntimeObject* L_15 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_15);
			bool L_16;
			L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0063;
			}
		}

IL_00dc:
		{
			U3CTraverseU3Ed__2_U3CU3Em__Finally1_m89BFF8CC58BB7E79B14B123CB8E598F30E0BC9D5(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
			// }
			V_0 = (bool)0;
			goto IL_00f4;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00ed;
	}

FAULT_00ed:
	{// begin fault (depth: 1)
		U3CTraverseU3Ed__2_System_IDisposable_Dispose_m0D56D8F1A83F3BB32FD3D4771D5BB1F8A9593F70(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(237)
	}// end fault
	IL2CPP_CLEANUP(237)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00f4:
	{
		bool L_17 = V_0;
		return L_17;
	}
}
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_U3CU3Em__Finally1_m89BFF8CC58BB7E79B14B123CB8E598F30E0BC9D5 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_U3CU3Em__Finally2_mEC2B9792DA117D865CE44A3545BE04E6F86BE07B (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0(((int32_t)-3));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap2_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// UnityEngine.Transform UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * U3CTraverseU3Ed__2_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m32C66870A9E701FA696333C440025F156D46AED4 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTraverseU3Ed__2_System_Collections_IEnumerator_Reset_m76C3ED0186EF3D29822CC189DE8CC6D1E8CDC044 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTraverseU3Ed__2_System_Collections_IEnumerator_Reset_m76C3ED0186EF3D29822CC189DE8CC6D1E8CDC044_RuntimeMethod_var)));
	}
}
// System.Object UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTraverseU3Ed__2_System_Collections_IEnumerator_get_Current_m0D444F51BF5BA8A960D2388214A6DFC751E52A9C (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.Generic.IEnumerable<UnityEngine.Transform>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__2_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mD771DF30A8558B87DAB20E2A5C7297A9DCA7C214 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * L_3 = (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE *)il2cpp_codegen_object_new(U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE_il2cpp_TypeInfo_var);
		U3CTraverseU3Ed__2__ctor_mCCC31783688C9F2ADE0A999E8AC201054F2880BE(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * L_4 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_U3CU3E3__parent_4();
		NullCheck(L_4);
		L_4->set_parent_3(L_5);
		U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniHumanoid.UnityExtensions/<Traverse>d__2::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTraverseU3Ed__2_System_Collections_IEnumerable_GetEnumerator_mC7E867C5986353F41E16673F4E8545A0A24EFF97 (U3CTraverseU3Ed__2_tD38DDC301B33FB87477C5695924A9E6FD921E7BE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CTraverseU3Ed__2_System_Collections_Generic_IEnumerableU3CUnityEngine_TransformU3E_GetEnumerator_mD771DF30A8558B87DAB20E2A5C7297A9DCA7C214(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38__ctor_m0F8CB11CDEEB9A2122DB966A8494DF2DA361ACE8 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_m7E8148B3070FA5E174A5F6FB907DE23A0B0A79D4 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)-4)))) > ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}

IL_0012:
	{
	}

IL_0013:
	try
	{// begin try (depth: 1)
		{
			int32_t L_3 = V_0;
			if ((((int32_t)L_3) == ((int32_t)((int32_t)-4))))
			{
				goto IL_001e;
			}
		}

IL_0018:
		{
			int32_t L_4 = V_0;
			if ((((int32_t)L_4) == ((int32_t)2)))
			{
				goto IL_001e;
			}
		}

IL_001c:
		{
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_001e:
		{
		}

IL_001f:
		try
		{// begin try (depth: 2)
			IL2CPP_LEAVE(0x2F, FINALLY_0021);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0021;
		}

FINALLY_0021:
		{// begin finally (depth: 2)
			U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m2AB2E5BE5DB44EE6BA61E91B7C4CA9BC09B69D89(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(33)
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(33)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x2F, FINALLY_0028);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{// begin finally (depth: 1)
		U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_mAA7C19E69E95CD26FF1FA925D21A0BBB78984177(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<TravserseDir>d__38::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTravserseDirU3Ed__38_MoveNext_m4E7F76E6477C71AC17153D9F7386795CF688CDC6 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_tC110BB72C4D6ACD305C792639F4DB4A5660C9C1E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t532DE44511E4D7A7C2E9CD3ECBBA02853A6D6C1F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  V_2;
	memset((&V_2), 0, sizeof(V_2));
	UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			switch (L_1)
			{
				case 0:
				{
					goto IL_0020;
				}
				case 1:
				{
					goto IL_0051;
				}
				case 2:
				{
					goto IL_00be;
				}
			}
		}

IL_0019:
		{
			V_0 = (bool)0;
			goto IL_0105;
		}

IL_0020:
		{
			__this->set_U3CU3E1__state_0((-1));
			// if (IsDirectoryExists)
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * L_2 = __this->get_address_of_U3CU3E4__this_3();
			bool L_3;
			L_3 = UnityPath_get_IsDirectoryExists_mA8D3ADDD40998DC76BEB9BB25AA9DA09B023430F((UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 *)L_2, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_00fa;
			}
		}

IL_0037:
		{
			// yield return this;
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_4 = __this->get_U3CU3E4__this_3();
			__this->set_U3CU3E2__current_1(L_4);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0105;
		}

IL_0051:
		{
			__this->set_U3CU3E1__state_0((-1));
			// foreach(var child in ChildDirs)
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * L_5 = __this->get_address_of_U3CU3E4__this_3();
			RuntimeObject* L_6;
			L_6 = UnityPath_get_ChildDirs_mF40B2AA1B6D32227A6358AE26060BDE21B87DE66((UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 *)L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			RuntimeObject* L_7;
			L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>::GetEnumerator() */, IEnumerable_1_tC110BB72C4D6ACD305C792639F4DB4A5660C9C1E_il2cpp_TypeInfo_var, L_6);
			__this->set_U3CU3E7__wrap1_5(L_7);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_00e0;
		}

IL_0078:
		{
			// foreach(var child in ChildDirs)
			RuntimeObject* L_8 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_8);
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_9;
			L_9 = InterfaceFuncInvoker0< UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>::get_Current() */, IEnumerator_1_t532DE44511E4D7A7C2E9CD3ECBBA02853A6D6C1F_il2cpp_TypeInfo_var, L_8);
			V_2 = L_9;
			// foreach(var x in child.TravserseDir())
			RuntimeObject* L_10;
			L_10 = UnityPath_TravserseDir_m293B9AC260627603FCB50777CBD01224439FC242((UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 *)(&V_2), /*hidden argument*/NULL);
			NullCheck(L_10);
			RuntimeObject* L_11;
			L_11 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>::GetEnumerator() */, IEnumerable_1_tC110BB72C4D6ACD305C792639F4DB4A5660C9C1E_il2cpp_TypeInfo_var, L_10);
			__this->set_U3CU3E7__wrap2_6(L_11);
			__this->set_U3CU3E1__state_0(((int32_t)-4));
			goto IL_00c6;
		}

IL_00a0:
		{
			// foreach(var x in child.TravserseDir())
			RuntimeObject* L_12 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_12);
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_13;
			L_13 = InterfaceFuncInvoker0< UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>::get_Current() */, IEnumerator_1_t532DE44511E4D7A7C2E9CD3ECBBA02853A6D6C1F_il2cpp_TypeInfo_var, L_12);
			V_3 = L_13;
			// yield return x;
			UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_14 = V_3;
			__this->set_U3CU3E2__current_1(L_14);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_0105;
		}

IL_00be:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-4));
		}

IL_00c6:
		{
			// foreach(var x in child.TravserseDir())
			RuntimeObject* L_15 = __this->get_U3CU3E7__wrap2_6();
			NullCheck(L_15);
			bool L_16;
			L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_00a0;
			}
		}

IL_00d3:
		{
			U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m2AB2E5BE5DB44EE6BA61E91B7C4CA9BC09B69D89(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap2_6((RuntimeObject*)NULL);
		}

IL_00e0:
		{
			// foreach(var child in ChildDirs)
			RuntimeObject* L_17 = __this->get_U3CU3E7__wrap1_5();
			NullCheck(L_17);
			bool L_18;
			L_18 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0078;
			}
		}

IL_00ed:
		{
			U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_mAA7C19E69E95CD26FF1FA925D21A0BBB78984177(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_5((RuntimeObject*)NULL);
		}

IL_00fa:
		{
			// }
			V_0 = (bool)0;
			goto IL_0105;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_00fe;
	}

FAULT_00fe:
	{// begin fault (depth: 1)
		U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_m7E8148B3070FA5E174A5F6FB907DE23A0B0A79D4(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(254)
	}// end fault
	IL2CPP_CLEANUP(254)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0105:
	{
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_mAA7C19E69E95CD26FF1FA925D21A0BBB78984177 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_5();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap1_5();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m2AB2E5BE5DB44EE6BA61E91B7C4CA9BC09B69D89 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0(((int32_t)-3));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap2_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_m236B35B6BCE536E88CC4BC9FB897FE1D9ABB2540 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_m35BD4F36C2376A3A570E4CF234060A207AC38BA3 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_m35BD4F36C2376A3A570E4CF234060A207AC38BA3_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_get_Current_mE4E5BE97901F9A62BB24DD29E9B82932BF33FEB9 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_1 = L_0;
		RuntimeObject * L_2 = Box(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mB6850E495BD9BA243C03DB4080D674777FDD5385 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * L_3 = (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 *)il2cpp_codegen_object_new(U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1_il2cpp_TypeInfo_var);
		U3CTravserseDirU3Ed__38__ctor_m0F8CB11CDEEB9A2122DB966A8494DF2DA361ACE8(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * L_4 = V_0;
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_5 = __this->get_U3CU3E3__U3CU3E4__this_4();
		NullCheck(L_4);
		L_4->set_U3CU3E4__this_3(L_5);
		U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_IEnumerable_GetEnumerator_mCA5969BF5F41E7FA1BF58CFA78729CC61FCED121 (U3CTravserseDirU3Ed__38_t6020F80B5A4FDD3286E20AB7F910796C7ABCD2C1 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mB6850E495BD9BA243C03DB4080D674777FDD5385(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40__ctor_m00CEF083D5ECEE8135AB2F44FB34156AC402DC30 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40_System_IDisposable_Dispose_mE0273315C8ACE11093AE77D07BD06B297785EBE6 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<get_ChildDirs>d__40::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3Cget_ChildDirsU3Ed__40_MoveNext_mBAFD13291093317E969F8B74F114E27CDCE7B4EF (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// foreach(var x in Directory.GetDirectories(FullPath))
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * L_3 = __this->get_address_of_U3CU3E4__this_3();
		String_t* L_4;
		L_4 = UnityPath_get_FullPath_m775FE6912534B097964B340558FB2598C3631D63((UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 *)L_3, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5;
		L_5 = Directory_GetDirectories_mACD36932126E03B5983AE0AE438B516B1CDDD7B7(L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E7__wrap1_5(L_5);
		__this->set_U3CU3E7__wrap2_6(0);
		goto IL_006e;
	}

IL_0036:
	{
		// foreach(var x in Directory.GetDirectories(FullPath))
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = __this->get_U3CU3E7__wrap1_5();
		int32_t L_7 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		// yield return UnityPath.FromFullpath(x);
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_11;
		L_11 = UnityPath_FromFullpath_mE2D127BEF63CCA22B3EE92DB7A6C5805BE897097(L_10, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0059:
	{
		__this->set_U3CU3E1__state_0((-1));
		int32_t L_12 = __this->get_U3CU3E7__wrap2_6();
		__this->set_U3CU3E7__wrap2_6(((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)));
	}

IL_006e:
	{
		// foreach(var x in Directory.GetDirectories(FullPath))
		int32_t L_13 = __this->get_U3CU3E7__wrap2_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = __this->get_U3CU3E7__wrap1_5();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0036;
		}
	}
	{
		__this->set_U3CU3E7__wrap1_5((StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)NULL);
		// }
		return (bool)0;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_mEDBCC15EBB07E3F3FAA7AC114673DA5C10FD0F4F (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mC236479C9A834B44352D9158BC0B52ACFFEEF302 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mC236479C9A834B44352D9158BC0B52ACFFEEF302_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_get_Current_m684B6002BAECBA044076AD4E00E2FC0C2E1C9A00 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_1 = L_0;
		RuntimeObject * L_2 = Box(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m8BD83073BDFEC08A32FAE4234DD77EAF8E813274 (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * L_3 = (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D *)il2cpp_codegen_object_new(U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D_il2cpp_TypeInfo_var);
		U3Cget_ChildDirsU3Ed__40__ctor_m00CEF083D5ECEE8135AB2F44FB34156AC402DC30(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * L_4 = V_0;
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_5 = __this->get_U3CU3E3__U3CU3E4__this_4();
		NullCheck(L_4);
		L_4->set_U3CU3E4__this_3(L_5);
		U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m6745AE8B7E242EE02691C0DC0FD884AA1677BC6F (U3Cget_ChildDirsU3Ed__40_tD6DA1CECA090EA1CC35A99A19A6E7BC749C7EA2D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m8BD83073BDFEC08A32FAE4234DD77EAF8E813274(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42__ctor_m09184C98C0D1E5C6B64B15A4BAC2392D388384B5 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42_System_IDisposable_Dispose_m6930C8ACD49D18E0D582756A596DB4264DD208A3 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<get_ChildFiles>d__42::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3Cget_ChildFilesU3Ed__42_MoveNext_m588D0CDD6B4C0255630FFBCA1D3D7ECA4CBB81FE (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// foreach (var x in Directory.GetFiles(FullPath))
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 * L_3 = __this->get_address_of_U3CU3E4__this_3();
		String_t* L_4;
		L_4 = UnityPath_get_FullPath_m775FE6912534B097964B340558FB2598C3631D63((UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0 *)L_3, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5;
		L_5 = Directory_GetFiles_m832F37C2ABC12EEAB3B1736C5196C50C0B65C1DC(L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E7__wrap1_5(L_5);
		__this->set_U3CU3E7__wrap2_6(0);
		goto IL_006e;
	}

IL_0036:
	{
		// foreach (var x in Directory.GetFiles(FullPath))
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = __this->get_U3CU3E7__wrap1_5();
		int32_t L_7 = __this->get_U3CU3E7__wrap2_6();
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		// yield return UnityPath.FromFullpath(x);
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_11;
		L_11 = UnityPath_FromFullpath_mE2D127BEF63CCA22B3EE92DB7A6C5805BE897097(L_10, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0059:
	{
		__this->set_U3CU3E1__state_0((-1));
		int32_t L_12 = __this->get_U3CU3E7__wrap2_6();
		__this->set_U3CU3E7__wrap2_6(((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)));
	}

IL_006e:
	{
		// foreach (var x in Directory.GetFiles(FullPath))
		int32_t L_13 = __this->get_U3CU3E7__wrap2_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = __this->get_U3CU3E7__wrap1_5();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0036;
		}
	}
	{
		__this->set_U3CU3E7__wrap1_5((StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)NULL);
		// }
		return (bool)0;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_mB49E2CEB64EF541FB8BA038F7C2713741EA3581C (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m0C8D5063B2894A80CC9E2385D69B640EAFA87852 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m0C8D5063B2894A80CC9E2385D69B640EAFA87852_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_get_Current_m58EC775E4CDD505905C89B0295C12168EFDA0329 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_0 = __this->get_U3CU3E2__current_1();
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_1 = L_0;
		RuntimeObject * L_2 = Box(UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mC9570B670C3C240E62A695D22695F3BF2F5707D6 (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * L_3 = (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 *)il2cpp_codegen_object_new(U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872_il2cpp_TypeInfo_var);
		U3Cget_ChildFilesU3Ed__42__ctor_m09184C98C0D1E5C6B64B15A4BAC2392D388384B5(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * L_4 = V_0;
		UnityPath_tAD703FBB29C74FB53BA173109CDD13A2D9103EB0  L_5 = __this->get_U3CU3E3__U3CU3E4__this_4();
		NullCheck(L_4);
		L_4->set_U3CU3E4__this_3(L_5);
		U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mAB0903405F6A79060AABC8CA859A3C704068AD4F (U3Cget_ChildFilesU3Ed__42_tA06F7397FBA6C33AFB45B627A20517E528166872 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mC9570B670C3C240E62A695D22695F3BF2F5707D6(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12__ctor_mA6BC0F3C4B655D6E7766E1119248C4CC6B67D520 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_System_IDisposable_Dispose_m6E8904532AB281C3748F2FCCC7D29CC0692ED043 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)-4)))) > ((uint32_t)1))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1))) <= ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}

IL_0014:
	{
	}

IL_0015:
	try
	{// begin try (depth: 1)
		{
			int32_t L_3 = V_0;
			if ((((int32_t)L_3) == ((int32_t)((int32_t)-4))))
			{
				goto IL_0022;
			}
		}

IL_001a:
		{
			int32_t L_4 = V_0;
			if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1))) > ((uint32_t)1))))
			{
				goto IL_0022;
			}
		}

IL_0020:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}

IL_0022:
		{
		}

IL_0023:
		try
		{// begin try (depth: 2)
			IL2CPP_LEAVE(0x33, FINALLY_0025);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{// begin finally (depth: 2)
			U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally2_mC47713BD6B4401CEF1B89EB169202144EECFA2D6(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(37)
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x33, FINALLY_002c);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{// begin finally (depth: 1)
		U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally1_mB54C15DA0BAFC35C7C63051BB36E24D810F807A1(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x33, IL_0033)
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessOnMainThreadU3Ed__12_MoveNext_m037E2EB91071F23E0EE6CE6DADD59511CE1CE06C (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral849D9073DD7D266CCFFD877500171B0CB572D56C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876BA9D37F5B3B86B1953A81D0C931AE6AFB2BED);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * V_2 = NULL;
	String_t* V_3 = NULL;
	FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * V_4 = NULL;
	String_t* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 6> __leave_targets;

IL_0000:
	try
	{// begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_1 = __this->get_U3CU3E4__this_2();
			V_2 = L_1;
			int32_t L_2 = V_1;
			switch (L_2)
			{
				case 0:
				{
					goto IL_0027;
				}
				case 1:
				{
					goto IL_00e7;
				}
				case 2:
				{
					goto IL_0106;
				}
			}
		}

IL_0020:
		{
			V_0 = (bool)0;
			goto IL_019c;
		}

IL_0027:
		{
			__this->set_U3CU3E1__state_0((-1));
			// var tmp = Path.GetTempFileName();
			IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
			String_t* L_3;
			L_3 = Path_GetTempFileName_mD4D7B9D2329A6D5FF86A05EFDD67E08E3150B34C(/*hidden argument*/NULL);
			V_3 = L_3;
			// using (var f = new FileStream(tmp, FileMode.Create))
			String_t* L_4 = V_3;
			FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * L_5 = (FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 *)il2cpp_codegen_object_new(FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26_il2cpp_TypeInfo_var);
			FileStream__ctor_mDC286819520925AB5873921EFFEADA1EC1648CFB(L_5, L_4, 2, /*hidden argument*/NULL);
			V_4 = L_5;
		}

IL_003d:
		try
		{// begin try (depth: 2)
			// f.Write(m_segments.Array, m_segments.Offset, m_segments.Count);
			FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * L_6 = V_4;
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_7 = V_2;
			NullCheck(L_7);
			ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * L_8 = L_7->get_address_of_m_segments_3();
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9;
			L_9 = ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)L_8, /*hidden argument*/ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_RuntimeMethod_var);
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_10 = V_2;
			NullCheck(L_10);
			ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * L_11 = L_10->get_address_of_m_segments_3();
			int32_t L_12;
			L_12 = ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)L_11, /*hidden argument*/ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_13 = V_2;
			NullCheck(L_13);
			ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * L_14 = L_13->get_address_of_m_segments_3();
			int32_t L_15;
			L_15 = ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)L_14, /*hidden argument*/ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
			NullCheck(L_6);
			VirtualActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(29 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_6, L_9, L_12, L_15);
			// }
			IL2CPP_LEAVE(0x73, FINALLY_0067);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0067;
		}

FINALLY_0067:
		{// begin finally (depth: 2)
			{
				FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * L_16 = V_4;
				if (!L_16)
				{
					goto IL_0072;
				}
			}

IL_006b:
			{
				FileStream_t6342275F1C1E26F5EEB5AD510933C95B78A5DA26 * L_17 = V_4;
				NullCheck(L_17);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_17);
			}

IL_0072:
			{
				IL2CPP_END_FINALLY(103)
			}
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(103)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_JUMP_TBL(0x73, IL_0073)
		}

IL_0073:
		{
			// using (var d = new Deleter(tmp))
			String_t* L_18 = V_3;
			Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * L_19 = (Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D *)il2cpp_codegen_object_new(Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D_il2cpp_TypeInfo_var);
			Deleter__ctor_m4B8FC64020C404285F8D737E244827AA0F8753A3(L_19, L_18, /*hidden argument*/NULL);
			__this->set_U3CdU3E5__2_3(L_19);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			// var url = "file:///" + tmp.Replace("\\", "/");
			String_t* L_20 = V_3;
			NullCheck(L_20);
			String_t* L_21;
			L_21 = String_Replace_m98184150DC4E2FBDF13E723BF5B7353D9602AC4D(L_20, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, /*hidden argument*/NULL);
			String_t* L_22;
			L_22 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral876BA9D37F5B3B86B1953A81D0C931AE6AFB2BED, L_21, /*hidden argument*/NULL);
			V_5 = L_22;
			// Debug.LogFormat("UnityWebRequest: {0}", url);
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_23 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_24 = L_23;
			String_t* L_25 = V_5;
			NullCheck(L_24);
			ArrayElementTypeCheck (L_24, L_25);
			(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_25);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
			Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6(_stringLiteral849D9073DD7D266CCFFD877500171B0CB572D56C, L_24, /*hidden argument*/NULL);
			// using (var m_uwr = new WWW(url))
			String_t* L_26 = V_5;
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_27 = (WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 *)il2cpp_codegen_object_new(WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2_il2cpp_TypeInfo_var);
			WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5(L_27, L_26, /*hidden argument*/NULL);
			__this->set_U3Cm_uwrU3E5__3_4(L_27);
			__this->set_U3CU3E1__state_0(((int32_t)-4));
			// yield return m_uwr;
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_28 = __this->get_U3Cm_uwrU3E5__3_4();
			__this->set_U3CU3E2__current_1(L_28);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_019c;
		}

IL_00e7:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-4));
			goto IL_010e;
		}

IL_00f1:
		{
			// yield return null;
			__this->set_U3CU3E2__current_1(NULL);
			__this->set_U3CU3E1__state_0(2);
			V_0 = (bool)1;
			goto IL_019c;
		}

IL_0106:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-4));
		}

IL_010e:
		{
			// while (!m_uwr.isDone)
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_29 = __this->get_U3Cm_uwrU3E5__3_4();
			NullCheck(L_29);
			bool L_30;
			L_30 = WWW_get_isDone_m916B54D53395990DB59C64413798FBCAFB08E0E3(L_29, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_00f1;
			}
		}

IL_011b:
		{
			// if (!string.IsNullOrEmpty(m_uwr.error))
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_31 = __this->get_U3Cm_uwrU3E5__3_4();
			NullCheck(L_31);
			String_t* L_32;
			L_32 = WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC(L_31, /*hidden argument*/NULL);
			bool L_33;
			L_33 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_32, /*hidden argument*/NULL);
			if (L_33)
			{
				goto IL_0141;
			}
		}

IL_012d:
		{
			// Debug.Log(m_uwr.error);
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_34 = __this->get_U3Cm_uwrU3E5__3_4();
			NullCheck(L_34);
			String_t* L_35;
			L_35 = WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC(L_34, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
			Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_35, /*hidden argument*/NULL);
			// yield break;
			V_0 = (bool)0;
			goto IL_016b;
		}

IL_0141:
		{
			// Texture = m_uwr.textureNonReadable;
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_36 = V_2;
			WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_37 = __this->get_U3Cm_uwrU3E5__3_4();
			NullCheck(L_37);
			Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_38;
			L_38 = WWW_get_textureNonReadable_m5656C1450CDA1CF1A55B8BC1EBBE21CCCE9FDD0C(L_37, /*hidden argument*/NULL);
			NullCheck(L_36);
			UnityWebRequestTextureLoader_set_Texture_m38A430E5B42A5AFDD7B8DBDF5AA1329C6DD2A5A8_inline(L_36, L_38, /*hidden argument*/NULL);
			// Texture.name = m_textureName;
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_39 = V_2;
			NullCheck(L_39);
			Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_40;
			L_40 = UnityWebRequestTextureLoader_get_Texture_m7B2713B4793DD204D1BCFB369982E16E5D5ADBA3_inline(L_39, /*hidden argument*/NULL);
			UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * L_41 = V_2;
			NullCheck(L_41);
			String_t* L_42 = L_41->get_m_textureName_4();
			NullCheck(L_40);
			Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_40, L_42, /*hidden argument*/NULL);
			// }
			U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally2_mC47713BD6B4401CEF1B89EB169202144EECFA2D6(__this, /*hidden argument*/NULL);
			goto IL_0173;
		}

IL_016b:
		{
			U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally2_mC47713BD6B4401CEF1B89EB169202144EECFA2D6(__this, /*hidden argument*/NULL);
			goto IL_0182;
		}

IL_0173:
		{
			__this->set_U3Cm_uwrU3E5__3_4((WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 *)NULL);
			// }
			U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally1_mB54C15DA0BAFC35C7C63051BB36E24D810F807A1(__this, /*hidden argument*/NULL);
			goto IL_018a;
		}

IL_0182:
		{
			U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally1_mB54C15DA0BAFC35C7C63051BB36E24D810F807A1(__this, /*hidden argument*/NULL);
			goto IL_019c;
		}

IL_018a:
		{
			__this->set_U3CdU3E5__2_3((Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D *)NULL);
			// }
			V_0 = (bool)0;
			goto IL_019c;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_0195;
	}

FAULT_0195:
	{// begin fault (depth: 1)
		U3CProcessOnMainThreadU3Ed__12_System_IDisposable_Dispose_m6E8904532AB281C3748F2FCCC7D29CC0692ED043(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(405)
	}// end fault
	IL2CPP_CLEANUP(405)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_019c:
	{
		bool L_43 = V_0;
		return L_43;
	}
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally1_mB54C15DA0BAFC35C7C63051BB36E24D810F807A1 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * L_0 = __this->get_U3CdU3E5__2_3();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * L_1 = __this->get_U3CdU3E5__2_3();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_U3CU3Em__Finally2_mC47713BD6B4401CEF1B89EB169202144EECFA2D6 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0(((int32_t)-3));
		WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_0 = __this->get_U3Cm_uwrU3E5__3_4();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2 * L_1 = __this->get_U3Cm_uwrU3E5__3_4();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// System.Object UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m500F65CE058DB7A7DB6F7F48D6C3C200F3FA603E (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessOnMainThreadU3Ed__12_System_Collections_IEnumerator_Reset_m26884EB48C1754801435A50E4F68913F2DD6B56A (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessOnMainThreadU3Ed__12_System_Collections_IEnumerator_Reset_m26884EB48C1754801435A50E4F68913F2DD6B56A_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityWebRequestTextureLoader/<ProcessOnMainThread>d__12::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessOnMainThreadU3Ed__12_System_Collections_IEnumerator_get_Current_m388366F5058CF259497BF6373B68EDD4F2828815 (U3CProcessOnMainThreadU3Ed__12_t2FC08AA32D790D155D72E436890437179BBC4EED * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityWebRequestTextureLoader/Deleter::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deleter__ctor_m4B8FC64020C404285F8D737E244827AA0F8753A3 (Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * __this, String_t* ___path0, const RuntimeMethod* method)
{
	{
		// public Deleter(string path)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// m_path = path;
		String_t* L_0 = ___path0;
		__this->set_m_path_0(L_0);
		// }
		return;
	}
}
// System.Void UniGLTF.UnityWebRequestTextureLoader/Deleter::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deleter_Dispose_m2C996CCFCD5A4F443D2669A377566D8C1D5B67E1 (Deleter_tDA0DC8BEFF665E09105C822711B6D8D5CE51F62D * __this, const RuntimeMethod* method)
{
	{
		// if (File.Exists(m_path))
		String_t* L_0 = __this->get_m_path_0();
		bool L_1;
		L_1 = File_Exists_mDAEBF2732BC830270FD98346F069B04E97BB1D5B(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// File.Delete(m_path);
		String_t* L_2 = __this->get_m_path_0();
		File_Delete_m82FE53535A3911380F7E4C8AD44D77FAB330FD77(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mC1775FAD6D59D1E4952A1516A887A49BBD982999 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * L_0 = (U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 *)il2cpp_codegen_object_new(U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m52D6BE0B09F5A4E7221B0884B7B513A9FF893419(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m52D6BE0B09F5A4E7221B0884B7B513A9FF893419 (U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UniGLTF.Zip.ZipArchiveStorage/<>c::<ToString>b__0_0(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CToStringU3Eb__0_0_mE00B579ECB061BCA9FD0C4CF1791B97E4B397B02 (U3CU3Ec_t7EA39EE55FCC54D2BE2AE289233F201EFC14B215 * __this, CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("<ZIPArchive\n{0}>", String.Join("", Entries.Select(x => x.ToString() + "\n").ToArray()));
		CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7 * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_1, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass5_0__ctor_mCC0E692FDB57BF4378FD8D699E0BAFAC885873E3 (U3CU3Ec__DisplayClass5_0_t47560D8B7EC8B115B6DE13A1B94A6659521D6499 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::<Get>b__0(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_m55F0D12D9FD7B53C57BE999B276E150E0949304C (U3CU3Ec__DisplayClass5_0_t47560D8B7EC8B115B6DE13A1B94A6659521D6499 * __this, CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7 * ___x0, const RuntimeMethod* method)
{
	{
		// var found = Entries.FirstOrDefault(x => x.FileName == url);
		CentralDirectoryFileHeader_t8A2E98649CE6950CEA045E1AA1B0EE55F1B4B8A7 * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = CommonHeader_get_FileName_m684CEF085CB5FB9CC9A298115463124235BD82F7(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_url_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.glTF/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m452912C1AF8FD575ABA590083F3218F664A10038 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * L_0 = (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 *)il2cpp_codegen_object_new(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m07FEF95C42B2189719A66AE768A5842250AD94CF(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UniGLTF.glTF/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m07FEF95C42B2189719A66AE768A5842250AD94CF (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__9_0(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__9_0_mB4871E93E8D17623BC5DDAF7EBB24E7009A935EC (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint8_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<Byte>(accessor, view).Select(x => (int)(x));
		uint8_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__9_1(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__9_1_mE5445FFDA26D64A9D85CA9CF381FD8B02DC54410 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint16_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<UInt16>(accessor, view).Select(x => (int)(x));
		uint16_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__9_2(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__9_2_m05FD2389E3E20869F91BDB57EBF40E6D86D33BC9 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint32_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<UInt32>(accessor, view).Select(x => (int)(x));
		uint32_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__10_0(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__10_0_m9D2389E3ABAB9015A700DF5F3068EE7CEFF790A7 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint8_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<Byte>(count, byteOffset, view).Select(x => (int)(x));
		uint8_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__10_1(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__10_1_m5DFC7A0344B0C72E83047E74D6C9AFB98254E33F (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint16_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<UInt16>(count, byteOffset, view).Select(x => (int)(x));
		uint16_t L_0 = ___x0;
		return L_0;
	}
}
// System.Int32 UniGLTF.glTF/<>c::<_GetIndices>b__10_2(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3C_GetIndicesU3Eb__10_2_m17106FA79F6F2FBC9E36E53002E2A52D37AF5765 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, uint32_t ___x0, const RuntimeMethod* method)
{
	{
		// return GetAttrib<UInt32>(count, byteOffset, view).Select(x => (int)(x));
		uint32_t L_0 = ___x0;
		return L_0;
	}
}
// System.Boolean UniGLTF.glTF/<>c::<GetUniqueMaterialName>b__23_0(UniGLTF.glTFMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CGetUniqueMaterialNameU3Eb__23_0_m5D96016203F05CECF0DFCEA5D939272A23924710 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * ___x0, const RuntimeMethod* method)
{
	{
		// if (materials.Any(x => string.IsNullOrEmpty(x.name))
		glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_name_0();
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String UniGLTF.glTF/<>c::<GetUniqueMaterialName>b__23_1(UniGLTF.glTFMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CGetUniqueMaterialNameU3Eb__23_1_mAECDF4D3487C68354DC877359C6272884F317245 (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * ___x0, const RuntimeMethod* method)
{
	{
		// || materials.Select(x => x.name).Distinct().Count() != materials.Count)
		glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_name_0();
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<UniGLTF.glTFPrimitives> UniGLTF.glTF/<>c::<MaterialHasVertexColor>b__26_0(UniGLTF.glTFMesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec_U3CMaterialHasVertexColorU3Eb__26_0_m7AA23F9DE742DDE0250C4CA14DE1E22FB6C16D2F (U3CU3Ec_t1D8CED5B92D75AC8BF8A9F0AA7288AE1B1BF34A7 * __this, glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755 * ___x0, const RuntimeMethod* method)
{
	{
		// var hasVertexColor = meshes.SelectMany(x => x.primitives).Any(x => x.material == materialIndex && x.HasVertexColor);
		glTFMesh_t78F56E0C82B6DCA4DB71FA1689CD371BD92A0755 * L_0 = ___x0;
		NullCheck(L_0);
		List_1_t7FE5C3220B7491713400A31684BD0F0E85C78EAB * L_1 = L_0->get_primitives_1();
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.glTF/<>c__DisplayClass26_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass26_0__ctor_mBDFE912E458563AC194014C6EF2CD89AC1BADFE5 (U3CU3Ec__DisplayClass26_0_t36893A0919E05A0D1E8591BF6B047DAF5EB6A18B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.glTF/<>c__DisplayClass26_0::<MaterialHasVertexColor>b__1(UniGLTF.glTFPrimitives)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass26_0_U3CMaterialHasVertexColorU3Eb__1_m44BA5439D1BAC274B22D78C605DDF049060E963D (U3CU3Ec__DisplayClass26_0_t36893A0919E05A0D1E8591BF6B047DAF5EB6A18B * __this, glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598 * ___x0, const RuntimeMethod* method)
{
	{
		// var hasVertexColor = meshes.SelectMany(x => x.primitives).Any(x => x.material == materialIndex && x.HasVertexColor);
		glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_material_3();
		int32_t L_2 = __this->get_materialIndex_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0015;
		}
	}
	{
		glTFPrimitives_tA9F5D688054490EA84F48DFC1AA8857ECEAC0598 * L_3 = ___x0;
		NullCheck(L_3);
		bool L_4;
		L_4 = glTFPrimitives_get_HasVertexColor_m9024FF91F10D80B1FC3F68CE03B0D96979BC0113(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.glTFAnimation/<>c__DisplayClass6_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass6_0__ctor_mC5EC976C13F927C48996A888E2FB01AAB39983E1 (U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.glTFAnimation/<>c__DisplayClass6_0::<AddChannelAndGetSampler>b__0(UniGLTF.glTFAnimationChannel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass6_0_U3CAddChannelAndGetSamplerU3Eb__0_m2A3B93BD0ABC5C7F63733B3003D2BD8A4E8DCF97 (U3CU3Ec__DisplayClass6_0_t4261B156B47057565C0753AF40FF2BCC2040D5F9 * __this, glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B * ___x0, const RuntimeMethod* method)
{
	{
		// var channel = channels.FirstOrDefault(x => x.target.node == nodeIndex && x.target.path == glTFAnimationTarget.GetPathName(property));
		glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B * L_0 = ___x0;
		NullCheck(L_0);
		glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A * L_1 = L_0->get_target_1();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_node_0();
		int32_t L_3 = __this->get_nodeIndex_0();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_002f;
		}
	}
	{
		glTFAnimationChannel_tCD563F95A19DF46EA3A1AC45DFA5CCC1C558B12B * L_4 = ___x0;
		NullCheck(L_4);
		glTFAnimationTarget_tFD1334225FEC62CA99C6BB3E7CF820AD1703377A * L_5 = L_4->get_target_1();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_path_1();
		int32_t L_7 = __this->get_property_1();
		String_t* L_8;
		L_8 = glTFAnimationTarget_GetPathName_mD58077774B6D2346105D93E85CEB7B324CE1A3FA(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_002f:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.glTFExtensions/ComponentVec::.ctor(UniGLTF.glComponentType,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentVec__ctor_mC03E31F6488799B07DFCD52447129D22F9676BE9 (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 * __this, int32_t ___componentType0, int32_t ___elementCount1, const RuntimeMethod* method)
{
	{
		// ComponentType = componentType;
		int32_t L_0 = ___componentType0;
		__this->set_ComponentType_0(L_0);
		// ElementCount = elementCount;
		int32_t L_1 = ___elementCount1;
		__this->set_ElementCount_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ComponentVec__ctor_mC03E31F6488799B07DFCD52447129D22F9676BE9_AdjustorThunk (RuntimeObject * __this, int32_t ___componentType0, int32_t ___elementCount1, const RuntimeMethod* method)
{
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *>(__this + _offset);
	ComponentVec__ctor_mC03E31F6488799B07DFCD52447129D22F9676BE9(_thisAdjusted, ___componentType0, ___elementCount1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.glbImporter/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m778DB907FB2F4BFDDBD84F6E4798A9AC8857D7DA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * L_0 = (U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD *)il2cpp_codegen_object_new(U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m73886182753EE7DE64BFF4AC8AA3B34C904006FE(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UniGLTF.glbImporter/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m73886182753EE7DE64BFF4AC8AA3B34C904006FE (U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.glbImporter/<>c::<ParseGlbChanks>b__3_0(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CParseGlbChanksU3Eb__3_0_mFAD6CC8C3FFFDA66DD3C55A6CC3BBDE9D35456D7 (U3CU3Ec_tB734BEE250BBB8551A56BC9F7389DB1F06FFBBDD * __this, uint8_t ___x0, const RuntimeMethod* method)
{
	{
		// var chunkTypeBytes = bytes.Skip(pos).Take(4).Where(x => x != 0).ToArray();
		uint8_t L_0 = ___x0;
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfExporter/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m21108C2FFD72E8A2B45929787E2F05378CF2498D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * L_0 = (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 *)il2cpp_codegen_object_new(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m4A7648BFBFC6D7025720713B0C288C64F96A7762(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UniGLTF.gltfExporter/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4A7648BFBFC6D7025720713B0C288C64F96A7762 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_0(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportPrimitivesU3Eb__31_0_m6DF1F4EE243376BA334D2BAC9118C5FF789EA3D2 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___y0, const RuntimeMethod* method)
{
	{
		// var positions = mesh.vertices.Select(y => y.ReverseZ()).ToArray();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___y0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_1(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportPrimitivesU3Eb__31_1_m67E8BAB5DFA7779B4EEA6720DE97F641558FDA71 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gltf.accessors[positionAccessorIndex].min = positions.Aggregate(positions[0], (a, b) => new Vector3(Mathf.Min(a.x, b.x), Math.Min(a.y, b.y), Mathf.Min(a.z, b.z))).ToArray();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		float L_4;
		L_4 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_1, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___a0;
		float L_6 = L_5.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = ___b1;
		float L_8 = L_7.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_9;
		L_9 = Math_Min_mED21323DC72FBF9A825FD4210D4B9D693CE87FCF(L_6, L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___a0;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___b1;
		float L_13 = L_12.get_z_4();
		float L_14;
		L_14 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_2(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportPrimitivesU3Eb__31_2_m506650F5206DEDC9CCA607F9C77E46782BDAF3D2 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gltf.accessors[positionAccessorIndex].max = positions.Aggregate(positions[0], (a, b) => new Vector3(Mathf.Max(a.x, b.x), Math.Max(a.y, b.y), Mathf.Max(a.z, b.z))).ToArray();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		float L_4;
		L_4 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_1, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___a0;
		float L_6 = L_5.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = ___b1;
		float L_8 = L_7.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_9;
		L_9 = Math_Max_mEB87839DA28310AE4CB81A94D551874CFC2B1247(L_6, L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___a0;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___b1;
		float L_13 = L_12.get_z_4();
		float L_14;
		L_14 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_3(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportPrimitivesU3Eb__31_3_m771B0E2A6B7C2C44958E3D97FF3DAFB96D5BDD8C (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___y0, const RuntimeMethod* method)
{
	{
		// var normalAccessorIndex = gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, mesh.normals.Select(y => y.ReverseZ()).ToArray(), glBufferTarget.ARRAY_BUFFER);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___y0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector2 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_4(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  U3CU3Ec_U3CExportPrimitivesU3Eb__31_4_m059330FD93E50916DAA127B6310AE48E956508B0 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___y0, const RuntimeMethod* method)
{
	{
		// var uvAccessorIndex = gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, mesh.uv.Select(y => y.ReverseUV()).ToArray(), glBufferTarget.ARRAY_BUFFER);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___y0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = UnityExtensions_ReverseUV_m6358683D7B052D74F85E04C7D43D3DC2F9614002(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector4 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_5(UnityEngine.BoneWeight)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  U3CU3Ec_U3CExportPrimitivesU3Eb__31_5_m7BFCE2A6B451C08FF3D75937687668C235C2E156 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8  ___y0, const RuntimeMethod* method)
{
	{
		// var weightAccessorIndex = gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, boneweights.Select(y => new Vector4(y.weight0, y.weight1, y.weight2, y.weight3)).ToArray(), glBufferTarget.ARRAY_BUFFER);
		float L_0;
		L_0 = BoneWeight_get_weight0_m8B5490EC53D29525AF4AE32AF645FD3499113972((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		float L_1;
		L_1 = BoneWeight_get_weight1_mE4925B02C2A8D914DD768318B737FD108966A2DB((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		float L_2;
		L_2 = BoneWeight_get_weight2_m1C8AD1CE89B909139F39675B41D5DF6532802D1F((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		float L_3;
		L_3 = BoneWeight_get_weight3_mAC48658CD89909EF148D310CB59CC14002E13C62((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniGLTF.UShort4 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_6(UnityEngine.BoneWeight)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  U3CU3Ec_U3CExportPrimitivesU3Eb__31_6_m26C2CB8FE82F7C593C8AA5B762B854EFEDBE58D8 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8  ___y0, const RuntimeMethod* method)
{
	{
		// var jointsAccessorIndex = gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, boneweights.Select(y => new UShort4((ushort)y.boneIndex0, (ushort)y.boneIndex1, (ushort)y.boneIndex2, (ushort)y.boneIndex3)).ToArray(), glBufferTarget.ARRAY_BUFFER);
		int32_t L_0;
		L_0 = BoneWeight_get_boneIndex0_m69A411B913A65F83640791F464303F363ADDBC41((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = BoneWeight_get_boneIndex1_m6CEEBBAAB3A769562A7C5C503863C81E14ECE4B7((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = BoneWeight_get_boneIndex2_m139F5D3569F3DC84E52F39820442BEBDDD0666C6((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = BoneWeight_get_boneIndex3_mA5CC40F94B4AD7F1FD57193D490D1FBDFF74C44D((BoneWeight_t48ACF6336A02E6D89797F11624FBC9108CF076D8 *)(&___y0), /*hidden argument*/NULL);
		UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  L_4;
		memset((&L_4), 0, sizeof(L_4));
		UShort4__ctor_m5609420491AA680B4D20BB9D8558E00A54E359AB((&L_4), (uint16_t)((int32_t)((uint16_t)L_0)), (uint16_t)((int32_t)((uint16_t)L_1)), (uint16_t)((int32_t)((uint16_t)L_2)), (uint16_t)((int32_t)((uint16_t)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt32 UniGLTF.gltfExporter/<>c::<ExportPrimitives>b__31_7(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t U3CU3Ec_U3CExportPrimitivesU3Eb__31_7_mAE1C48551CBA0AD80795978072C0D8F5C3FD69F5 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, int32_t ___y0, const RuntimeMethod* method)
{
	{
		// var indices = TriangleUtil.FlipTriangle(mesh.GetIndices(j)).Select(y => (uint)y).ToArray();
		int32_t L_0 = ___y0;
		return L_0;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportMorphTarget>b__33_0(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportMorphTargetU3Eb__33_0_m7AF270F2E24CAF0D5533D4F16B273A06B4BC8C3B (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___y0, const RuntimeMethod* method)
{
	{
		// var blendShapeTangents = mesh.tangents.Select(y => (Vector3)y).ToArray();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = ___y0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportMorphTarget>b__33_1(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportMorphTargetU3Eb__33_1_m533F29D581352E546A346473234373EB6C0FF232 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gltf.accessors[blendShapePositionAccessorIndex].min = blendShapeVertices.Aggregate(blendShapeVertices[0], (a, b) => new Vector3(Mathf.Min(a.x, b.x), Math.Min(a.y, b.y), Mathf.Min(a.z, b.z))).ToArray();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		float L_4;
		L_4 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_1, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___a0;
		float L_6 = L_5.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = ___b1;
		float L_8 = L_7.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_9;
		L_9 = Math_Min_mED21323DC72FBF9A825FD4210D4B9D693CE87FCF(L_6, L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___a0;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___b1;
		float L_13 = L_12.get_z_4();
		float L_14;
		L_14 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c::<ExportMorphTarget>b__33_2(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec_U3CExportMorphTargetU3Eb__33_2_m73157F161913E45B46EC653E367B4E06DB4FED6F (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gltf.accessors[blendShapePositionAccessorIndex].max = blendShapeVertices.Aggregate(blendShapeVertices[0], (a, b) => new Vector3(Mathf.Max(a.x, b.x), Math.Max(a.y, b.y), Mathf.Max(a.z, b.z))).ToArray();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		float L_4;
		L_4 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_1, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___a0;
		float L_6 = L_5.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = ___b1;
		float L_8 = L_7.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_9;
		L_9 = Math_Max_mEB87839DA28310AE4CB81A94D551874CFC2B1247(L_6, L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___a0;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___b1;
		float L_13 = L_12.get_z_4();
		float L_14;
		L_14 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityEngine.Material> UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_0(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec_U3CFromGameObjectU3Eb__35_0_m2385AAB6BF1C7A3DA996D57B779467DF5668A42C (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	{
		// Materials = Nodes.SelectMany(x => x.GetSharedMaterials()).Where(x => x != null).Distinct().ToList();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_1;
		L_1 = UnityExtensions_GetSharedMaterials_mE943685A31249DD83C3D80933C1DF33CF42B997C(L_0, /*hidden argument*/NULL);
		return (RuntimeObject*)L_1;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_1(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CFromGameObjectU3Eb__35_1_m7208E6BA98A5F39B2144E967710DB97D3C326568 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Materials = Nodes.SelectMany(x => x.GetSharedMaterials()).Where(x => x != null).Distinct().ToList();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<UniGLTF.TextureIO/TextureExportItem> UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_2(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec_U3CFromGameObjectU3Eb__35_2_m6CEB0A1C7A07F3990D1539AC3E492931D7DA9EAC (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___x0, const RuntimeMethod* method)
{
	{
		// var unityTextures = Materials.SelectMany(x => TextureIO.GetTextures(x)).Where(x => x.Texture != null).Distinct().ToList();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___x0;
		RuntimeObject* L_1;
		L_1 = TextureIO_GetTextures_m93E8EFD894DB481AFCAB61DEFDDA42AAEDE3FA4F(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_3(UniGLTF.TextureIO/TextureExportItem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CFromGameObjectU3Eb__35_3_m9A89FC7D1ECA90FA8AF0F8B080B8BC701F06B95A (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var unityTextures = Materials.SelectMany(x => TextureIO.GetTextures(x)).Where(x => x.Texture != null).Distinct().ToList();
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_0 = ___x0;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_1 = L_0.get_Texture_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_4(UniGLTF.TextureIO/TextureExportItem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * U3CU3Ec_U3CFromGameObjectU3Eb__35_4_m59474DDC7DA65BE35521D062E8CED120AF3B5ECE (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  ___x0, const RuntimeMethod* method)
{
	{
		// TextureManager = new TextureExportManager(unityTextures.Select(x => x.Texture));
		TextureExportItem_t51D2A02ED2C112CA5E77C60B8E85EE30B52150D9  L_0 = ___x0;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_1 = L_0.get_Texture_0();
		return L_1;
	}
}
// UniGLTF.gltfExporter/MeshWithRenderer UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_6(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  U3CU3Ec_U3CFromGameObjectU3Eb__35_6_mFE95D4DAD8060D322C98A09B34B3E94AB9AAF850 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mB89D75983F403B440947CE6FB264503618F5B951_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// .Select(x => new MeshWithRenderer
		// {
		//     Mesh = x.GetSharedMesh(),
		//     Rendererer = x.GetComponent<Renderer>(),
		// })
		il2cpp_codegen_initobj((&V_0), sizeof(MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709 ));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1;
		L_1 = UnityExtensions_GetSharedMesh_m266F27F8EB902DFCACE09833821A70EE3BDB71BD(L_0, /*hidden argument*/NULL);
		(&V_0)->set_Mesh_0(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = ___x0;
		NullCheck(L_2);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_3;
		L_3 = Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mB89D75983F403B440947CE6FB264503618F5B951(L_2, /*hidden argument*/Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mB89D75983F403B440947CE6FB264503618F5B951_RuntimeMethod_var);
		(&V_0)->set_Rendererer_1(L_3);
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_7(UniGLTF.gltfExporter/MeshWithRenderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CFromGameObjectU3Eb__35_7_mDDC491D2E789F40262BFAA911BE9CD44F4B6E783 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (x.Mesh == null)
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_0 = ___x0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = L_0.get_Mesh_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0010:
	{
		// if (x.Rendererer.sharedMaterials == null
		// || x.Rendererer.sharedMaterials.Length == 0)
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_3 = ___x0;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_4 = L_3.get_Rendererer_1();
		NullCheck(L_4);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_5;
		L_5 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_6 = ___x0;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_7 = L_6.get_Rendererer_1();
		NullCheck(L_7);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_8;
		L_8 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		if ((((RuntimeArray*)L_8)->max_length))
		{
			goto IL_002d;
		}
	}

IL_002b:
	{
		// return false;
		return (bool)0;
	}

IL_002d:
	{
		// return true;
		return (bool)1;
	}
}
// UnityEngine.Mesh UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_8(UniGLTF.gltfExporter/MeshWithRenderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * U3CU3Ec_U3CFromGameObjectU3Eb__35_8_mC75D042592AD44D673A39E43BD8545D5B831107B (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  ___x0, const RuntimeMethod* method)
{
	{
		// Meshes = unityMeshes.Select(x => x.Mesh).ToList();
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_0 = ___x0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = L_0.get_Mesh_0();
		return L_1;
	}
}
// UnityEngine.SkinnedMeshRenderer UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_9(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * U3CU3Ec_U3CFromGameObjectU3Eb__35_9_mF7D182DF636E819E4B11988351B117FD26007C75 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m1E9C5D6F74CD0F5F21102B83360E41013884A501_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// .Select(x => x.GetComponent<SkinnedMeshRenderer>()).Where(x =>
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		NullCheck(L_0);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_1;
		L_1 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m1E9C5D6F74CD0F5F21102B83360E41013884A501(L_0, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m1E9C5D6F74CD0F5F21102B83360E41013884A501_RuntimeMethod_var);
		return L_1;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_10(UnityEngine.SkinnedMeshRenderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CFromGameObjectU3Eb__35_10_mAC2AE783E5F88029CF42C87923BF79F9802BD7E9 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// x != null
		// && x.bones != null
		// && x.bones.Length > 0)
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_2 = ___x0;
		NullCheck(L_2);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_3;
		L_3 = SkinnedMeshRenderer_get_bones_mFC9D33DF2CE433D0FBE5645CEA514A0DF95A1C2C(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_4 = ___x0;
		NullCheck(L_4);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_5;
		L_5 = SkinnedMeshRenderer_get_bones_mFC9D33DF2CE433D0FBE5645CEA514A0DF95A1C2C(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		return (bool)((!(((uint32_t)(((RuntimeArray*)L_5)->max_length)) <= ((uint32_t)0)))? 1 : 0);
	}

IL_001c:
	{
		return (bool)0;
	}
}
// UnityEngine.Mesh UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_12(UniGLTF.gltfExporter/MeshWithRenderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * U3CU3Ec_U3CFromGameObjectU3Eb__35_12_mFB03ABF6DE33058C0F475D4F0ECBA395FD08BCDA (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  ___y0, const RuntimeMethod* method)
{
	{
		// gltf.nodes = Nodes.Select(x => ExportNode(x, Nodes, unityMeshes.Select(y => y.Mesh).ToList(), unitySkins)).ToList();
		MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709  L_0 = ___y0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_1 = L_0.get_Mesh_0();
		return L_1;
	}
}
// UnityEngine.Matrix4x4 UniGLTF.gltfExporter/<>c::<FromGameObject>b__35_14(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  U3CU3Ec_U3CFromGameObjectU3Eb__35_14_m84E2296DF23829792A1D06420AFCFBA876E45A48 (U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * __this, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___y0, const RuntimeMethod* method)
{
	{
		// var matrices = x.sharedMesh.bindposes.Select(y => y.ReverseZ()).ToArray();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = ___y0;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_1;
		L_1 = UnityExtensions_ReverseZ_m4D51025BECE0DBCF0F70102036822356877537B1(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfExporter/<>c__DisplayClass30_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0__ctor_mFAA33E1DB762D18013B950DFB333168FEAF20DED (U3CU3Ec__DisplayClass30_0_tD105192BEFDF5A92F21A1211C6994A11ECF88B07 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UniGLTF.gltfExporter/<>c__DisplayClass30_0::<ExportNode>b__0(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec__DisplayClass30_0_U3CExportNodeU3Eb__0_m4790AE25511B9EB7423030E7E5396818CFDF36AF (U3CU3Ec__DisplayClass30_0_tD105192BEFDF5A92F21A1211C6994A11ECF88B07 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___y0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_IndexOf_m066AEE31C72DF470B7FD54374C7ADED0C43D199B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// children = x.transform.GetChildren().Select(y => nodes.IndexOf(y)).ToArray(),
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get_nodes_0();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = ___y0;
		NullCheck(L_0);
		int32_t L_2;
		L_2 = List_1_IndexOf_m066AEE31C72DF470B7FD54374C7ADED0C43D199B(L_0, L_1, /*hidden argument*/List_1_IndexOf_m066AEE31C72DF470B7FD54374C7ADED0C43D199B_RuntimeMethod_var);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfExporter/<>c__DisplayClass33_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass33_0__ctor_m3248CE84E27F6DCBF5BE615AB29D078691DBA98A (U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c__DisplayClass33_0::<ExportMorphTarget>b__3(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass33_0_U3CExportMorphTargetU3Eb__3_m0C709405E6C2D1F4BE49D090CD84C6D17068CB7B (U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3 * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		// .Where(x => UseSparse(
		//     usePosition, blendShapeVertices[x],
		//     useNormal, blendShapeNormals[x],
		//     useTangent, blendShapeTangents[x]))
		bool L_0 = __this->get_usePosition_0();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_1 = __this->get_blendShapeVertices_1();
		int32_t L_2 = ___x0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		bool L_5 = __this->get_useNormal_2();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_6 = __this->get_blendShapeNormals_3();
		int32_t L_7 = ___x0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		bool L_10 = __this->get_useTangent_4();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_11 = __this->get_blendShapeTangents_5();
		int32_t L_12 = ___x0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		bool L_15;
		L_15 = gltfExporter_UseSparse_m62F7C07FF517AF7EA6D2B2E955FFF66C68BF3233(L_0, L_4, L_5, L_9, L_10, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c__DisplayClass33_0::<ExportMorphTarget>b__4(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass33_0_U3CExportMorphTargetU3Eb__4_mA6EE3A6ACE0DF1DD9B01B7ED4DE7BD8095C7DC20 (U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3 * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		// blendShapeVertices = sparseIndices.Select(x => blendShapeVertices[x].ReverseZ()).ToArray();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_0 = __this->get_blendShapeVertices_1();
		int32_t L_1 = ___x0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c__DisplayClass33_0::<ExportMorphTarget>b__5(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass33_0_U3CExportMorphTargetU3Eb__5_m5E7273A7BA9C637121C523460AB2B2CA01620124 (U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3 * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		// blendShapeNormals = sparseIndices.Select(x => blendShapeNormals[x].ReverseZ()).ToArray();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_0 = __this->get_blendShapeNormals_3();
		int32_t L_1 = ___x0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UniGLTF.gltfExporter/<>c__DisplayClass33_0::<ExportMorphTarget>b__6(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass33_0_U3CExportMorphTargetU3Eb__6_m3D35B88B3BA5AD9A49F0FBF8D4DB12D568F5D7CA (U3CU3Ec__DisplayClass33_0_t2E80DF599CACA34939568679FF85089C758946B3 * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		// blendShapeTangents = sparseIndices.Select(x => blendShapeTangents[x].ReverseZ()).ToArray();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_0 = __this->get_blendShapeTangents_5();
		int32_t L_1 = ___x0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = UnityExtensions_ReverseZ_m7CB26F5E398187F5ED34CA6EEADFC326FD2F7B82(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfExporter/<>c__DisplayClass35_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass35_0__ctor_mA21C6144988B84F178CE35466C3E0A2C1C694D23 (U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniGLTF.glTFMaterial UniGLTF.gltfExporter/<>c__DisplayClass35_0::<FromGameObject>b__5(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * U3CU3Ec__DisplayClass35_0_U3CFromGameObjectU3Eb__5_mEA04C397EF45311B7296A767B1B80EAA42B352FA (U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMaterialExporter_tFA443B0F21B6D006C58F8A9ECEE40C81A4FEB9D3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gltf.materials = Materials.Select(x => materialExporter.ExportMaterial(x, TextureManager)).ToList();
		RuntimeObject* L_0 = __this->get_materialExporter_0();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___x0;
		gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * L_2 = __this->get_U3CU3E4__this_3();
		NullCheck(L_2);
		TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A * L_3 = L_2->get_TextureManager_7();
		NullCheck(L_0);
		glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC * L_4;
		L_4 = InterfaceFuncInvoker2< glTFMaterial_tCB6C1C75E648092919DC52174930C228B124E9EC *, Material_t8927C00353A72755313F046D0CE85178AE8218EE *, TextureExportManager_t000BD80CB5970B6BD65B0230D08C706B367E2D9A * >::Invoke(0 /* UniGLTF.glTFMaterial UniGLTF.IMaterialExporter::ExportMaterial(UnityEngine.Material,UniGLTF.TextureExportManager) */, IMaterialExporter_tFA443B0F21B6D006C58F8A9ECEE40C81A4FEB9D3_il2cpp_TypeInfo_var, L_0, L_1, L_3);
		return L_4;
	}
}
// UniGLTF.glTFNode UniGLTF.gltfExporter/<>c__DisplayClass35_0::<FromGameObject>b__11(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480 * U3CU3Ec__DisplayClass35_0_U3CFromGameObjectU3Eb__11_m6E95073ABAF5BF331CFE49A95A5EA9DC0BF96A0B (U3CU3Ec__DisplayClass35_0_t528DAE96BBCEDBD9F67D95066CBC1DED1F94FD83 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_m49DF9F51864902059B935A4953F3F4FCF61AA3BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToList_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_mA9B87D5951385CA98350A1CE27AEB321CEFF0989_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m7126A380EC86400442C76F6E88B55A2ABEF146FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t08105548739575F9FE3A646B6888D9042E732055_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CFromGameObjectU3Eb__35_12_mFB03ABF6DE33058C0F475D4F0ECBA395FD08BCDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t08105548739575F9FE3A646B6888D9042E732055 * G_B2_0 = NULL;
	List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * G_B2_1 = NULL;
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * G_B2_2 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B2_3 = NULL;
	Func_2_t08105548739575F9FE3A646B6888D9042E732055 * G_B1_0 = NULL;
	List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * G_B1_1 = NULL;
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * G_B1_2 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B1_3 = NULL;
	{
		// gltf.nodes = Nodes.Select(x => ExportNode(x, Nodes, unityMeshes.Select(y => y.Mesh).ToList(), unitySkins)).ToList();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___x0;
		gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * L_1 = __this->get_U3CU3E4__this_3();
		NullCheck(L_1);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_2;
		L_2 = gltfExporter_get_Nodes_m610390ED9BBEA441A7AB4A713B6FFCABF9EC1876_inline(L_1, /*hidden argument*/NULL);
		List_1_t1BAF10702DA2D2FD0FE7F6AB3F798C925560F063 * L_3 = __this->get_unityMeshes_1();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var);
		Func_2_t08105548739575F9FE3A646B6888D9042E732055 * L_4 = ((U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var))->get_U3CU3E9__35_12_22();
		Func_2_t08105548739575F9FE3A646B6888D9042E732055 * L_5 = L_4;
		G_B1_0 = L_5;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_0;
		if (L_5)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_0;
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var);
		U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5 * L_6 = ((U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t08105548739575F9FE3A646B6888D9042E732055 * L_7 = (Func_2_t08105548739575F9FE3A646B6888D9042E732055 *)il2cpp_codegen_object_new(Func_2_t08105548739575F9FE3A646B6888D9042E732055_il2cpp_TypeInfo_var);
		Func_2__ctor_m7126A380EC86400442C76F6E88B55A2ABEF146FB(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec_U3CFromGameObjectU3Eb__35_12_mFB03ABF6DE33058C0F475D4F0ECBA395FD08BCDA_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7126A380EC86400442C76F6E88B55A2ABEF146FB_RuntimeMethod_var);
		Func_2_t08105548739575F9FE3A646B6888D9042E732055 * L_8 = L_7;
		((U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC5449F5D284B9498123289F6C31B181AB2E46A5_il2cpp_TypeInfo_var))->set_U3CU3E9__35_12_22(L_8);
		G_B2_0 = L_8;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0031:
	{
		RuntimeObject* L_9;
		L_9 = Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_m49DF9F51864902059B935A4953F3F4FCF61AA3BE(G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisMeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_m49DF9F51864902059B935A4953F3F4FCF61AA3BE_RuntimeMethod_var);
		List_1_tC4F348E4AB2D2B0C505D1B56D6CBE2CFDB59784B * L_10;
		L_10 = Enumerable_ToList_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_mA9B87D5951385CA98350A1CE27AEB321CEFF0989(L_9, /*hidden argument*/Enumerable_ToList_TisMesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_mA9B87D5951385CA98350A1CE27AEB321CEFF0989_RuntimeMethod_var);
		List_1_t5BD7A84C0FA82AD14BF1D2AADD77DCD6BCB8DB40 * L_11 = __this->get_unitySkins_2();
		glTFNode_t9A977E27716CB6CE71E934DCF1B899120711A480 * L_12;
		L_12 = gltfExporter_ExportNode_mBAA26D3FC6D331977F4EA5EEBD1E6AE49F419AEA(G_B2_3, G_B2_2, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfExporter/<>c__DisplayClass35_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass35_1__ctor_m151072966451EAB0B859E9286EF8B5AED80C4CDF (U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniGLTF.gltfExporter/<>c__DisplayClass35_1::<FromGameObject>b__16(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass35_1_U3CFromGameObjectU3Eb__16_m48BB150DFECBEF5A0DF26344A3F04039656DB639 (U3CU3Ec__DisplayClass35_1_t3B3EDFD9E28017F34510104533803332E9E4F5CC * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___y0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityExtensions_Has_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_mEA1212D8AD20345E735606E08B43969F5D89B606_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// foreach (var z in Nodes.Where(y => y.Has(x)))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___y0;
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_1 = __this->get_x_0();
		bool L_2;
		L_2 = UnityExtensions_Has_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_mEA1212D8AD20345E735606E08B43969F5D89B606(L_0, L_1, /*hidden argument*/UnityExtensions_Has_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_mEA1212D8AD20345E735606E08B43969F5D89B606_RuntimeMethod_var);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.gltfExporter/MeshWithRenderer
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_pinvoke(const MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709& unmarshaled, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_pinvoke& marshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithRenderer': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_pinvoke_back(const MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_pinvoke& marshaled, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709& unmarshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithRenderer': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.gltfExporter/MeshWithRenderer
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_pinvoke_cleanup(MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UniGLTF.gltfExporter/MeshWithRenderer
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_com(const MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709& unmarshaled, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_com& marshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithRenderer': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_com_back(const MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_com& marshaled, MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709& unmarshaled)
{
	Exception_t* ___Mesh_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Mesh' of type 'MeshWithRenderer': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Mesh_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UniGLTF.gltfExporter/MeshWithRenderer
IL2CPP_EXTERN_C void MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshal_com_cleanup(MeshWithRenderer_t2C6D92BCBBFC074CED6087A93E90B336467DF709_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.gltfImporter/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_m0DEF8FE4C58A2E4CDBC341B59677C90AF444A282 (U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniGLTF.gltfImporter/<>c__DisplayClass3_0::<LoadVrmAsync>b__0(DepthFirstScheduler.Unit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0_U3CLoadVrmAsyncU3Eb__0_m2A0A44539D94A5704709EC874F6D9D27444EF784 (U3CU3Ec__DisplayClass3_0_tEE13FF60DD4C8B8D35226B73B5BEED256C4D7F16 * __this, Unit_t0B0EB3030F80545B338317B8757267C1CBF13F9D  ____0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mD4DFE26268AFEE6CA1AEA70EE0FEB61E599DC3E7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (show)
		bool L_0 = __this->get_show_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// context.ShowMeshes();
		ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		ImporterContext_ShowMeshes_mC54AE98601938ABC36D61BD3AFDA213EE3C9D706(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// onLoaded(context.Root);
		Action_1_tA00EE0A45DD8953ADBDE415255E9E21CFECEC13D * L_2 = __this->get_onLoaded_2();
		ImporterContext_t0A64B7DB2E206D3265BD3E8E6D36F3045B9057A9 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = L_3->get_Root_6();
		NullCheck(L_2);
		Action_1_Invoke_mD4DFE26268AFEE6CA1AEA70EE0FEB61E599DC3E7(L_2, L_4, /*hidden argument*/Action_1_Invoke_mD4DFE26268AFEE6CA1AEA70EE0FEB61E599DC3E7_RuntimeMethod_var);
		// },
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DepthFirstScheduler.Scheduler/ThreadPoolScheduler/<>c__DisplayClass0_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0__ctor_mB874B616AF0FA6B4C4011E4B210C3EE45D747B53 (U3CU3Ec__DisplayClass0_0_tE82D0128C1DFC11ACA808215BDEB39E3AB8B3FDD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DepthFirstScheduler.Scheduler/ThreadPoolScheduler/<>c__DisplayClass0_0::<Enqueue>b__0(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0_U3CEnqueueU3Eb__0_m1F0DD04DD12EA2448B9B82CB8C4FC485CA89380C (U3CU3Ec__DisplayClass0_0_tE82D0128C1DFC11ACA808215BDEB39E3AB8B3FDD * __this, RuntimeObject * ____0, const RuntimeMethod* method)
{
	{
		// if (item == null)
		TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * L_0 = __this->get_item_0();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var status = item.Next();
		TaskChain_t42DF3CA0C3B15C301BA21897AA59C59385289809 * L_1 = __this->get_item_0();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = TaskChain_Next_mC25E0CEED34D236CDD4BDDBD5E4CC4F44BFD9878(L_1, /*hidden argument*/NULL);
		// if (status != ExecutionStatus.Continue)
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0009;
		}
	}
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_m69978FB15E83BA5D9C45321DEBF82798C3B0A8CA (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::<Push>b__0(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__0_m94EDED9E92F81857DF1B8E1559724DFDF3623C9E (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x0, const RuntimeMethod* method)
{
	{
		// m_positions.AddRange(mesh.vertices.Select(x => localToRoot.MultiplyPoint(x)));
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_0 = __this->get_address_of_localToRoot_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___x0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Matrix4x4_MultiplyPoint_mE92BEE4DED3B602983C2BBE06C44AD29564EDA83((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::<Push>b__1(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__1_m3ECB93AF7BD11AC361B644E8B6A18E119EEF6DF6 (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___x0, const RuntimeMethod* method)
{
	{
		// m_normals.AddRange(mesh.normals.Select(x => localToRoot.MultiplyVector(x)));
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_0 = __this->get_address_of_localToRoot_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___x0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UniGLTF.StaticMeshIntegrator/Integrator/<>c__DisplayClass7_0::<Push>b__2(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec__DisplayClass7_0_U3CPushU3Eb__2_m99207BBFCAF1F67ED747E7FE3C70957DBFC9B08F (U3CU3Ec__DisplayClass7_0_tF8DC6131D06957773EBA589EDA9026140C9A73BA * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		// m_subMeshes.Add(mesh.GetIndices(i).Select(x => offset + x).ToArray());
		int32_t L_0 = __this->get_offset_1();
		int32_t L_1 = ___x0;
		return ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TextureItem_get_IsAsset_mC52E33C68ECA2AC51D86432383C1CF1A5B64A995_inline (TextureItem_t83F65B9CA38D2A0120C1C7740BA17EACC9B280FA * __this, const RuntimeMethod* method)
{
	{
		// get;
		bool L_0 = __this->get_U3CIsAssetU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TextureLoader_set_Texture_mD1454DFCCC19367D3F475C05E4EAE7145E111938_inline (TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___value0, const RuntimeMethod* method)
{
	{
		// private set;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ___value0;
		__this->set_U3CTextureU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * TextureLoader_get_Texture_m20A25069C9C86C6ED9DD9E46EDDD15694AF8EBF8_inline (TextureLoader_tEC2D3CF5439F193ECDE8EC2E7B2B6D148B1D9ADA * __this, const RuntimeMethod* method)
{
	{
		// get;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = __this->get_U3CTextureU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityWebRequestTextureLoader_set_Texture_m38A430E5B42A5AFDD7B8DBDF5AA1329C6DD2A5A8_inline (UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___value0, const RuntimeMethod* method)
{
	{
		// private set;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ___value0;
		__this->set_U3CTextureU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * UnityWebRequestTextureLoader_get_Texture_m7B2713B4793DD204D1BCFB369982E16E5D5ADBA3_inline (UnityWebRequestTextureLoader_tE6A349BA6CAB2B1EDFEF0AEAF807828764B55347 * __this, const RuntimeMethod* method)
{
	{
		// get;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = __this->get_U3CTextureU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * gltfExporter_get_Nodes_m610390ED9BBEA441A7AB4A713B6FFCABF9EC1876_inline (gltfExporter_t4A632154339013531C98BF039634C1F2A5E2F174 * __this, const RuntimeMethod* method)
{
	{
		// get;
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get_U3CNodesU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD6469330B5381EC92DDF375773100F6372E6114C_gshared_inline (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_mEFB776105F87A4EAB1CAC3F0C96C4D0B79F3F03D_gshared_inline (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  Enumerator_get_Current_m39BB9CD07FEC0DBEDFE938630364A23C9A87FC3F_gshared_inline (Enumerator_tE4E91EE5578038530CF0C46227953BA787E7A0A0 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_0 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )__this->get_current_3();
		return (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8425596BB4249956819960EC76E618357F573E76_gshared_inline (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m79996A0C101669893B6ACC314BEA31597A00F9CB_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)__this->get__array_0();
		return (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__offset_1();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
